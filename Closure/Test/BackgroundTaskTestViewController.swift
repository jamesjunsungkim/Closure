//
//  ViewController.swift
//  Practice
//
//  Created by James Kim on 7/1/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout

class TestViewController: UIViewController {
    // UI
    fileprivate var resultLabel:UILabel!
    fileprivate var startButton:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        startButton.addTarget(self, action: #selector(didTapButton(_:)), for: .touchUpInside)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Actions
    @objc func reinstateBackgroundTask() {
        if updateTimer != nil && (backgroundTask == UIBackgroundTaskInvalid) {
            registerBackgroundTask()
        }
    }
    
    @objc fileprivate func didTapButton(_ sender:UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            resetCalculation()
            updateTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self,
                                               selector: #selector(calculateNextNumber), userInfo: nil, repeats: true)
            registerBackgroundTask()
        } else {
            updateTimer?.invalidate()
            updateTimer = nil
            if backgroundTask != UIBackgroundTaskInvalid {
                endBackgroundTask()
            }
        }
    }
    
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            [weak self] in
            self?.endBackgroundTask()
        })
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }
    
    @objc func calculateNextNumber() {
        let result = current.adding(previous)
        
        let bigNumber = NSDecimalNumber(mantissa: 1, exponent: 40, isNegative: false)
        if result.compare(bigNumber) == .orderedAscending {
            previous = current
            current = result
            position += 1
        } else {
            // This is just too much.... Start over.
            resetCalculation()
        }
        
        let resultsMessage = "Position \(position) = \(current)"
        
        switch UIApplication.shared.applicationState {
        case .active:
            resultLabel.text = resultsMessage
        case .background:
            print("App is backgrounded. Next number = \(resultsMessage)")
            print("Background time remaining = \(UIApplication.shared.backgroundTimeRemaining) seconds")
        case .inactive:
            break
        }
    }
    
    func resetCalculation() {
        previous = NSDecimalNumber.one
        current = NSDecimalNumber.one
        position = 1
    }
    
    // MARK: Fileprivate
    
    fileprivate var previous = NSDecimalNumber.one
    fileprivate var current = NSDecimalNumber.one
    var position:UInt = 1
    var updateTimer: Timer?
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    
    
}

extension TestViewController {
    fileprivate func setupUI() {
        resultLabel = UILabel()
        
        startButton = UIButton()
        startButton.backgroundColor = .red
        
        let group: [UIView] = [resultLabel, startButton]
        group.forEach(view.addSubview(_:))
        
        constraint(resultLabel, startButton) { (_resultLabel, _startButton) in
            _resultLabel.top.equalToSuperview().offset(100)
            _resultLabel.centerX.equalToSuperview()
            
            _startButton.top.equalTo(resultLabel.snp.bottom).offset(30)
            _startButton.centerX.equalToSuperview()
            _startButton.sizeEqualTo(width: 200, height: 30)
        }
    }
}






import Foundation

struct TestNetworkSession:NetworkSession {
    func fetchList(fromPath path: String, completion: @escaping (NetworkResult<[NetworkSession.JSON], APIError>) -> Void) {
        
    }
    
    func fetch(fromPath path: String, completion: @escaping (NetworkResult<Any, APIError>) -> Void) {
        
    }
    
    func post(value: Any, toPath path: String, completion: @escaping (NetworkResult<Any, APIError>) -> Void) {
        
    }
    
    func postJSON(value: NetworkSession.JSON, toPath path: String, completion: @escaping (NetworkResult<NetworkSession.JSON, APIError>) -> Void) {
        
    }
    
    
    public var fetchResult: NetworkSession.JSON!
    public var fetchListResult: [NetworkSession.JSON]!
    public var urlForUploadedFile: String!
    public var uidForSignedUpUser: String!
    
    func fetchJSON(fromPath path: String, completion: @escaping (NetworkResult<NetworkSession.JSON, APIError>) -> Void) {
        completion(.success(fetchResult))
    }
    
    func fetchList(fromPath path: String, queryOrdedByChild child: String, queryEqualTo value: Any?, completion: @escaping (NetworkResult<[NetworkSession.JSON], APIError>) -> Void) {
        completion(.success(fetchListResult))
    }
    
    func upload(data: Data, toPath path: String, completion: @escaping (NetworkResult<String, APIError>) -> Void) {
        completion(.success(urlForUploadedFile))
    }
    
    func post(value: NetworkSession.JSON, toPath path: String, completion: @escaping (NetworkResult<NetworkSession.JSON, APIError>) -> Void) {
        completion(.success(value))
    }
    
    func patch(value: Any, toPath path: String, completion: @escaping (NetworkResult<Any, APIError>) -> Void) {
        completion(.success(value))
    }
    
    func delete(atPath path: String, completion: @escaping (NetworkResult<Void, APIError>) -> Void) {
        completion(.success(()))
    }
    
    func signup(withEmail email: String, password: String, completion: @escaping (NetworkResult<String, APIError>) -> Void) {
        completion(.success(uidForSignedUpUser))
    }
    
    func login(withEmail email: String, password: String, completion: @escaping (NetworkResult<String, APIError>) -> Void) {
        completion(.success(uidForSignedUpUser))
    }
    
    func signout(completion: @escaping (NetworkResult<Void, APIError>) -> Void) {
        completion(.success(()))
    }
}

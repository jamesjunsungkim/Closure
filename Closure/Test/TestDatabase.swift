//
//  TestDatabase.swift
//  Closure
//
//  Created by montapinunt Pimonta on 7/27/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

struct TestDatabase {
    struct TestReview{
        static let dict = [User.Keys.uid : "TestUID", User.Keys.name : "TestName",User.Keys.emailAddress: "" ]
        static let user = User(from: TestDatabase.TestReview.dict)
        
        static let reviewDict:[String:Any] = [Review.Keys.good:rGood, Review.Keys.bad:rBad,
                                              Review.Keys.goal:rGoal,Review.Keys.rateValue:rRating, Review.Keys.thoughts:rThought, Review.Keys.imageURL:firstURL,Review.Keys.date:rDate]
        static let editReviewDict:[String:Any] = [Review.Keys.good:editGood, Review.Keys.bad:editBad,
                                                    Review.Keys.goal:editGoal,Review.Keys.rateValue:editRating, Review.Keys.thoughts:editThought, Review.Keys.imageURL:firstURL,Review.Keys.date:editDate]
        static let firstURL = "FirstURL"
        static let editedURL = "EditedURL"
        
        static let rGood = "RGOOD"
        static let editGood = "Editgood"
        
        static let rBad = "RBad"
        static let editBad = "editbad"
        
        static let rGoal = "Rgoal"
        static let editGoal = "editgoal"
        
        static let rRating:Int16 = 1
        static let editRating:Int16 = 3
        
        static let rThought = "RThought"
        static let editThought = "edit thought"
        
        static let rDate = "2018-07-26"
        static let anotherDate = "2018-07-23"
        static let editDate = "2018-07-22"
        
        static let path = "path"
        
        static let imageData = UIImage.create(forKey: .noImage).jpegData(forKey: .defaultResolution)
        static let editImageData = UIImage(named:"page1")!.jpegData(forKey: .defaultResolution)
    }
}





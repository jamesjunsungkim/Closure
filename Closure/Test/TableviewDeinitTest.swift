//
//  TableviewDeinitTest.swift
//  Closure
//
//  Created by montapinunt Pimonta on 9/3/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

final class TableviewDeinitTest: DefaultViewController {
    
    fileprivate var tableView: UITableView!
    fileprivate var button: UIButton!
    
    init() {
        super.init(nibName: nil, bundle: nil)
        setupUI()
        
        button.addTarget(self, action: #selector(buttnClicked), for: .touchUpInside)
    }
    
    @objc fileprivate func buttnClicked() {
        isDeleted.toggle()
        tableView.reloadData()
    }
    
    fileprivate var isDeleted = false
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension TableviewDeinitTest:UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isDeleted ? 0 : 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "A", for: indexPath) as! TestCell
        cell.textLabel?.text = "\(indexPath.row)"
        return cell
    }
}

extension TableviewDeinitTest {
    fileprivate func setupUI() {
        tableView = UITableView.init(frame: .zero, style: .plain)
        button = UIButton.create(title: "A", titleColor: .white, fontSize: 15, backgroundColor: .black)
        
        tableView.dataSource = self
        tableView.register(TestCell.self, forCellReuseIdentifier: "A")
        
        view.addSubview(tableView)
        view.addSubview(button)
        tableView.setEdgesToSuperView()
        
        button.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(10)
            make.sizeEqualTo(width: 120, height: 20)
        }
    }
}

class TestCell:UITableViewCell {
    static var count = 0
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        print("INIT")
        TestCell.count += 1
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func prepareForReuse() {
        print("PREPARE")
    }
    
    deinit {
        print("DEINIT")
        TestCell.count -= 1
    }
    
}








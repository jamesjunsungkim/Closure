//
//  test+global.swift
//  Closure
//
//  Created by James Kim on 7/25/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CoreData

public func addRandomDummyReview(into moc: NSManagedObjectContext,wordCountManager:WordCountManager, completion:(()->Void)? = nil) {
    /**
      let's add random datas depending on date when it takes place.
     like if this method is excecuted 2018-09-10, we add it on that date, previous day, the day before, so on.
     */
    
    let thaifoodImageData = UIImage(named: "patthai")!.jpegData
    let thaifood2ImageData = UIImage(named: "thaifood2")!.jpegData
    let resortImageData = UIImage(named: "resortpic")!.jpegData
    
    var currentDate = Date()
    
    let showerWeather = NonCDWeather(temperature: 23, condition: 500, city: "Huai khwang", weatherIconName: "shower3", weatherDescription: "light rain", countryCodeInEnglish: "th_TH", countryName:"Thailand")
    let sunnyWeather = NonCDWeather(temperature: 30, condition: 800, city: "Huai khwang", weatherIconName: "sunny", weatherDescription: "sunny", countryCodeInEnglish: "th_TH", countryName:"Thailand")
    let cloudWeather = NonCDWeather(temperature: 25, condition: 804, city: "Huai khwang", weatherIconName: "cloudy2", weatherDescription: "cloud", countryCodeInEnglish: "th_TH", countryName:"Thailand")
    
    _ = Review.findByDateOrCreate(into: moc, wordCountManager: wordCountManager, good: "I ran 5km today", bad: "I didn't sleep enough today. Not again!", goals: "sleep more than 6 hours", ratingValue: 0, date: currentDate, imageData: nil, thoughts: "I'd like to", isFavorite: true, nonCDweather: sunnyWeather)
    
    currentDate = currentDate.addDay(value: -1)
    _ = Review.findByDateOrCreate(into: moc, wordCountManager: wordCountManager, good: "Fianlly, I went to eat the pat thai!", bad: "I coudln't resist the urge to devour the food", goals: "Need to eat less tomorrow", ratingValue: 1, date: currentDate, imageData: thaifoodImageData, thoughts: nil, isFavorite: true, nonCDweather: sunnyWeather)
    
    currentDate = currentDate.addDay(value: -1)
    _ = Review.findByDateOrCreate(into: moc, wordCountManager: wordCountManager, good: "I managed to apologize to tom for being moody", bad: "I got into an argument with Tom.", goals: "Think about what caused this situation.", ratingValue: 3, date: currentDate, imageData: nil, thoughts: "Seomtime, I don't think straight in frustrating situations", isFavorite: false, nonCDweather: showerWeather)
    
    currentDate = currentDate.addDay(value: -1)
    _ = Review.findByDateOrCreate(into: moc, wordCountManager: wordCountManager, good: "Family dinner at my favorite thai restaurant!", bad: "nothing for today!", goals: "appreciate my familiy for unconditionally loving me", ratingValue: 1, date: currentDate, imageData: thaifood2ImageData, thoughts: nil, isFavorite: false, nonCDweather: showerWeather)
    
    currentDate = currentDate.addDay(value: -1)
    _ = Review.findByDateOrCreate(into: moc, wordCountManager: wordCountManager, good: "Went outside to see rainbow", bad: "not today", goals: "Sometime it's valuable to slow down a bit and see big picture", ratingValue: 3, date: currentDate, imageData: nil, thoughts: nil, isFavorite: false, nonCDweather: cloudWeather)
    
    currentDate = currentDate.addDay(value: -1)
    _ = Review.findByDateOrCreate(into: moc, wordCountManager: wordCountManager, good: "I ran into a good picture of resort I'd love to try", bad: "not today", goals: "I will bring my family to this resport. ", ratingValue: 1, date: currentDate, imageData: resortImageData, thoughts: nil, isFavorite: true, nonCDweather: sunnyWeather)
    
    currentDate = currentDate.addDay(value: -1)
    _ = Review.findByDateOrCreate(into: moc, wordCountManager: wordCountManager, good: "I installed this app to organize my days better!", bad: "I forgot to clean my room:(", goals: "I will clean it tomorrow.. for real..", ratingValue: 2, date: currentDate, imageData: nil, thoughts: nil, isFavorite: true, nonCDweather: cloudWeather)
    completion?()
}









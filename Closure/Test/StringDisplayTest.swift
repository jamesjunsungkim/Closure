//
//  StringDisplayTest.swift
//  Closure
//
//  Created by montapinunt Pimonta on 9/1/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

final class StringDisplayTest: DefaultViewController {
    fileprivate var display: StringDisplayView!
    fileprivate var button:UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        
        button = UIButton.create(title: "HI", titleColor: .black, fontSize: 15, backgroundColor: .white)
        view.addSubview(button)
        button.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-30)
            make.sizeEqualTo(width: 120, height: 30)
        }
        button.addTarget(self, action: #selector(buttonckd), for: .touchUpInside)
    }
    
    @objc func buttonckd() {
        display.createDisplayString(title: "HI", values: ["A", "Askdfnkalsdfn", "Aqlmrlqmrkqwnrk", "A","sdfknadkfanlf"], targetWidth: 350, fontSize: 15, displayBackgroundColor: .white, borderColor: .mainBlue)
    }
}
extension StringDisplayTest{
    fileprivate func setupUI() {
        
        view.backgroundColor = .white
        
        display = StringDisplayView()
        
        display.backgroundColor = .red
        view.addSubview(display)
        
        display.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.centerX.equalToSuperview()
            make.width.equalTo(350)
        }
    }
}

//
//  AppDelegate.swift
//  Closure
//
//  Created by James Kim on 7/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import Firebase
import CoreData
import FBSDKCoreKit
import DropDown
import RxSwift
import Siren

import FirebaseMessaging
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    fileprivate var mainWindow: UIWindow?
    fileprivate var managedObjectContext: NSManagedObjectContext!

    fileprivate var appStatus:AppStatus!
    
    fileprivate weak var blurryBackgroundVC: BlurryBackgroundViewController!
    fileprivate weak var createOrCheckPassCodeViewController: CreateOrCheckPassCodeViewController!
    
    /**
     shouldDismiss boolean is used to track if it needs to show pass code vc. it depends on how much time is assigned by user until it requires passcode to unlock. (setting)
     
     isAppActive is used to indicate if the app needs to lock the app or not because I'd like to show the blurry vc when users do the double tap but not asking for passcode unless it goes to the background.
     
     hasRequiredTouchID is used to indicate if the app has required the touch ID. because asking touch make the app goes to the background mode and when it comes back to the app, the application did become active get triggered again which falls into the infinite loop. so we use this bool to prevent it from reoccurring.
     
     */
    
    fileprivate var bag: DisposeBag!
    fileprivate var shouldDismissBlurryVCWithoutCheckingPassCode = true
    fileprivate var isAppActive = true
    fileprivate var hasRequiredTouchID = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
//        let groupForRemovalFromUserdefault: [UserDefaults.Key] = [.countMainDictionary, .countLimitationDictionay, .countAddItFirstDictionary, .lastDayToSendEmail,]
//        groupForRemovalFromUserdefault.forEach({UserDefaults.removeValue(forKey: $0)})
        AppStatus.showAllLog = false
        
        /**
         if the passcode toggle is on, we need to show passcode vc.
         when app is launched, appdidbecomeactive is triggered so we don't need to excecute presentApplicationScreenAfterCheckingWithPasscodeIfneeded.
         */
        
        setupFirebase()
        setupCoreDataStackAndSetupScreenAndRootVC(isMemoryContext: false, application: application)
        setupThirdPartyLogin(application:application, launchOptions: launchOptions)
        enableDependenciesRelatedWithKeyboard()
        checkIfNewVersionIsAvailableAndIfsoPromptUserToUpdate()
        
//        showTestVC(TableviewDeinitTest())
        
        return true
    }
    
    
    // MARK: Public
    public func saveUserToUserDefaultAndSwitchToMainTabVCAndShowBlurryVC(WithUser user:User) {
        UserDefaults.store(object: user.toDictionary, forKey: .currentUser)
        appStatus.registerUser(user)
        
        let option = UIWindow.TransitionOptions.init(direction: .toRight, style: .linear)
        let target = MainTabBarController(appStatus: appStatus)
        
        /**
         I feel like I should keep shouldAllowLockScreen boolean for later even tho app doesn't seem to need this for anything for now.
         */
        AppStatus.shouldAllowLockScreen = true
        showBlurryVCIfPossible(needsToLock: true)
        
        if mainWindow != nil {
            mainWindow?.setRootViewController(target, options: option)
        } else {
            setMainWindowKeyAndVisible()
            mainWindow?.rootViewController = target
        }
    }
    
    public func switchToSignUpNav() {
        AppStatus.shouldAllowLockScreen = false
        
        let targetVC = UINavigationController.createDefaultNavigationController(rootViewController: WalkThroughViewController(appStatus: appStatus))
        
        var opt = UIWindow.TransitionOptions(direction: .toLeft, style: .easeInOut)
        opt.duration = 0.25
        
        if mainWindow != nil {
            mainWindow?.setRootViewController(targetVC, options: opt)
        } else {
            setMainWindowKeyAndVisible()
            mainWindow?.rootViewController = targetVC
        }
    }
    
    // MARK: Fileprivate
    private func setupFirebase() {
        let conf = Bundle.main.object(forInfoDictionaryKey: "Configuration") as! String
        var filePath: String!
        switch conf {
        case "Debug":
            filePath = Bundle.main.path(forResource: "GoogleService-Info-dev", ofType: "plist")
        case "Release":
            filePath = Bundle.main.path(forResource: "GoogleService-Info-pro", ofType: "plist")
        default: assertionFailure()
        }
        guard let fileopts = FirebaseOptions(contentsOfFile: filePath!) else {assertionFailure(); return}
        FirebaseApp.configure(options: fileopts)
        FirebaseConfiguration.shared.setLoggerLevel(.min)
    }
    
    private func setupCoreDataStackAndSetupScreenAndRootVC(isMemoryContext:Bool = false, application:UIApplication) {
        /**
         ------------------------- LOGIC RUN DOWN ------------------------------
         it takes a while to load the managed object context. about 1~2 seconds.
         but during that period of time, it's just pure dark since no window.
         we will display the launch screen until managedobject is set up ready then proceed normally.
         */
        
        guard !isMemoryContext else {
            managedObjectContext = NSManagedObjectContext.memoryContext()
             self.createAppSatusThenSetupScreenAndRootVC(application: application)
            return
        }
        
        mainWindow = UIWindow(frame: UIScreen.main.bounds)
        mainWindow?.makeKeyAndVisible()
        mainWindow?.rootViewController = LaunchScreenTillSetupReady()
        
        createClosureContainer {[unowned self] (container) in
            self.managedObjectContext = container.viewContext
            self.createAppSatusThenSetupScreenAndRootVC(application: application)
        }
    }
    
    private func createAppSatusThenSetupScreenAndRootVC(application:UIApplication) {
        guard managedObjectContext != nil else {assertionFailure("must create coredata stack before this method is excecuted");return}
        appStatus = AppStatus(context: managedObjectContext, application: application)
        
        if UserDefaults.checkIfValueExist(forKey: .currentUser) {
            let dict = UserDefaults.retrieveValueOrFatalError(forKey: .currentUser) as! [String:Any]
            saveUserToUserDefaultAndSwitchToMainTabVCAndShowBlurryVC(WithUser: User(from: dict))
        } else {
            switchToSignUpNav()
        }
    }
    
    fileprivate func setMainWindowKeyAndVisible() {
        mainWindow = UIWindow(frame: UIScreen.main.bounds)
        mainWindow?.makeKeyAndVisible()
    }
    
    private func showTestVC(_ VC:UIViewController) {
        mainWindow = UIWindow.init(frame: UIScreen.main.bounds)
        mainWindow?.makeKeyAndVisible()
        mainWindow?.windowLevel = UIWindowLevelAlert
        mainWindow?.rootViewController = VC
    }

    private func setupThirdPartyLogin(application: UIApplication,launchOptions:[UIApplicationLaunchOptionsKey: Any]?) {
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    private func enableDependenciesRelatedWithKeyboard() {
        DropDown.startListeningToKeyboard()
    }
    
    private func checkIfNewVersionIsAvailableAndIfsoPromptUserToUpdate() {
        let siren = Siren.shared
        siren.showAlertAfterCurrentVersionHasBeenReleasedForDays = 2
//        siren.alertMessaging = SirenAlertMessaging(updateTitle: <#T##String#>, updateMessage: <#T##String#>, updateButtonMessage: <#T##String#>, nextTimeButtonMessage: <#T##String#>, skipVersionButtonMessage: <#T##String#>)
        siren.checkVersion(checkType: .daily)
    }
    
     func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        return handled
    }
  
    
    public func showBlurryVCIfPossible(needsToLock:Bool = false) {
        guard AppStatus.shouldAllowLockScreen else {return}
        guard appStatus != nil, appStatus.passCodeManager.isPassCodeOnTracker.value  else {return}
        // if it's on, we put the blurry image on top of it so it doesn't show anything on background
        guard blurryBackgroundVC == nil else {return}
        
        let blurryVC = BlurryBackgroundViewController()
        blurryBackgroundVC = blurryVC
        
        performOnMain {
            UIView.getMostTopViewController()?
                .present(blurryVC, animated: false, completion: nil)
        }
        
        guard !needsToLock else {
            // needs to show passCodeVC
            shouldDismissBlurryVCWithoutCheckingPassCode = false
            return
        }
        
        appStatus.passCodeManager.fireTimerAndThen {[unowned self] in
            self.shouldDismissBlurryVCWithoutCheckingPassCode = false
        }
    }
    
    fileprivate func resetForPassCodeProcess() {
        bag = nil
        blurryBackgroundVC = nil
        createOrCheckPassCodeViewController = nil
        shouldDismissBlurryVCWithoutCheckingPassCode = true
        hasRequiredTouchID = false
    }
    
   
    fileprivate func presentApplicationScreenAfterCheckingWithPasscodeIfneeded() {
        guard appStatus != nil, blurryBackgroundVC != nil, createOrCheckPassCodeViewController == nil else {return}
        
        let mostTopVC = UIView.getMostTopViewController()
        
        let dismissBlurryVCAndReset = {[unowned self] in
            self.blurryBackgroundVC.dismiss(animated: false, completion: {
                self.resetForPassCodeProcess()
            })
        }
        
        guard isAppActive else {
            dismissBlurryVCAndReset()
            return
        }
        
        let openPassCodeVC = {[unowned self] in
            guard self.createOrCheckPassCodeViewController == nil else {return}
            
            let passcodeVC = CreateOrCheckPassCodeViewController(passCodeManager: self.appStatus.passCodeManager, shouldHideCancelButton: true)
            self.createOrCheckPassCodeViewController = passcodeVC
            
            self.bag = DisposeBag()
            
            passcodeVC.successObservable.subscribe(onNext: {
                waitFor(milliseconds: 300, completion: {
                    self.blurryBackgroundVC.dismiss(animated: false, completion: {
                        self.resetForPassCodeProcess()
                    })
                })}).disposed(by: self.bag)
            
            performOnMain {
                mostTopVC?.presentDefaultVC(targetVC: passcodeVC, userInfo: nil, shouldPushOnNavigationController: false)
            }
        }
        
        guard !shouldDismissBlurryVCWithoutCheckingPassCode else {
            dismissBlurryVCAndReset()
            return
        }
        
        let isTouchIDOn = appStatus.passCodeManager.isTouchIDOnTracker.value
        
        guard !hasRequiredTouchID else {return}
        
        switch isTouchIDOn {
        case true:
            //prevent it from taking place again because appbecomeactive is called several times.
            hasRequiredTouchID = true
            
            appStatus.passCodeManager.startAuthenticationProcess {(result) in
                switch result {
                case .notAvailable: assertionFailure();
                case .failure(_):
                    // if it fails, open the checkpasscode vc
                    waitFor(milliseconds: 50, completion: {
                        openPassCodeVC()
                    })
                case .success:
                    dismissBlurryVCAndReset()
                }
            }
            
        case false:
            //touch id is not available.
            openPassCodeVC()
        }
    }
    
    func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        // when receiving the memory warning, refresh all objects to ensure enough space
        managedObjectContext.refreshAllObjects()
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // this one gets called when system asking for permissions.
        /**
         This one gets called
         */
        showBlurryVCIfPossible()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        isAppActive = false
        managedObjectContext.refreshAllObjects()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        isAppActive = true
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        /**
         0. Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
         
         1. We need to check if any setting has changed when it comes back active like Photo permission or notification.
         */
        
        isAppActive = true
        presentApplicationScreenAfterCheckingWithPasscodeIfneeded()
        appStatus.notificationManager.checkIfNotificationSettingChangedAndIfSoToggleOff()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
   

    

}


//
//  Typealiases+cl.swift
//  Closure
//
//  Created by James Kim on 7/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import SwipeCellKit
import CoreData

// Handler
typealias block = () -> Void
typealias failureWithError = (Error) -> Void

// ViewController
typealias DefaultViewController = UIViewController & NameDescribable & DefaultSegue
typealias PopOverViewController = UIViewController & NameDescribable & PopOverable
typealias FormSheetViewController = UIViewController & NameDescribable & FormSheetable

// TableView & CollectionView
typealias ReusableTableViewCell = Reusable & UITableViewCell
typealias ReusableCollectionViewCell = Reusable & UICollectionViewCell
typealias CoreDataReusableTableViewCell = CoredataReusable & UITableViewCell
typealias CoreDataReusableCollectionViewCell = CoredataReusable & UICollectionViewCell

typealias AdditionalReusableTableViewCell = AdditionalReusable & UITableViewCell & NameDescribable

typealias SwipableDefaultTableViewCell = Reusable & SwipeTableViewCell
typealias SwipableDefaultCollectionViewCell = Reusable & SwipeCollectionViewCell
typealias SwipableCoredataTableviewCell = CoredataReusable & SwipeTableViewCell
typealias SwipableCoreDataCollectionViewCell = CoredataReusable & SwipeCollectionViewCell

// Model

typealias CDBaseModel = NSManagedObject & Managed & UidFetchable & NameDescribable & Unwrappable & DictionaryCreatable
typealias BaseModel = NameDescribable & Unwrappable & DictionaryCreatable


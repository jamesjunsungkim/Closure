//
//  GlobalProperty.swift
//  Closure
//
//  Created by James Kim on 7/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation

private(set) var countDictionary = [String:Int]()

public func assertCount(limit:Int, shouldCrash:Bool = false, fileName:String = #file, functionName:String = #function) {
    #if TARGET_IPHONE_SIMULATOR
    guard limit > 0 else {assertionFailure();return}
    
    let controllerName = String(fileName.split(separator: "/").last!.split(separator: ".")[0])
    let key = controllerName + ".\(functionName)"
    
    guard let count = countDictionary[key] else {
        countDictionary[key] = 1
        return
    }
    
    let currentCount = count+1
    
    guard currentCount <= limit else {
        shouldCrash ? assertionFailure() :
            print("function goes over the limit, file:\(fileName), func: \(functionName), currentCount:\(currentCount), limit:\(limit)")
        countDictionary[key] = currentCount
        return
    }
    
    countDictionary[key] = currentCount
    print("file:\(fileName), func: \(functionName), currentCount:\(currentCount), limit:\(limit)")
    #endif
}

public func resetAssertCountDictionary(fileName: String = #file, functionName:String) {
    #if TARGET_IPHONE_SIMULATOR
    let controllerName = String(fileName.split(separator: "/").last!.split(separator: ".")[0])
    let key = controllerName+".\(functionName)()"
    countDictionary.removeValue(forKey: key)
    #endif
}


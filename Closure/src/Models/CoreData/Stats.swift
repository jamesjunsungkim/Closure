////
////  Stats.swift
////  Closure
////
////  Created by montapinunt Pimonta on 8/4/18.
////  Copyright © 2018 James Kim. All rights reserved.
////
//
//import UIKit
//import CoreData
//
//final class Stats: CDBaseModel {
//
//    @NSManaged fileprivate(set) var numberOfFavorites: UInt64
//    @NSManaged fileprivate(set) var numberOfReviews: UInt64
//    @NSManaged fileprivate(set) var streakDayCount: UInt64
//
//    @NSManaged fileprivate(set) var reviews: Set<Review>
//
//
//}

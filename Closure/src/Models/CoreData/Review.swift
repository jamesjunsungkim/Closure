//
//  Review.swift
//  Closure
//
//  Created by James Kim on 7/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import PKHUD
import CoreData

final class Review: CDBaseModel {
    
    @NSManaged fileprivate(set) var date: Date
    @NSManaged fileprivate(set) var creationDate: Date
    @NSManaged fileprivate(set) var dateString: String
    @NSManaged fileprivate(set) var good: String
    @NSManaged fileprivate(set) var bad: String
    @NSManaged fileprivate(set) var goal: String
    @NSManaged fileprivate(set) var rateValue: Int16
    @NSManaged fileprivate(set) var imageData: Data?
    @NSManaged fileprivate(set) var thoughts: String?
    @NSManaged fileprivate(set) var isFavorite: Bool
    
    @NSManaged fileprivate(set) var imageURLstring: String?
    @NSManaged fileprivate(set) var isUploaded: Bool
    
    @NSManaged fileprivate(set) var weather: Weather?
    
    struct Keys {
        static let date = "date"
        static let dateString = "dateString"
        static let creationDate = "creationDate"
        static let good = "good"
        static let bad = "bad"
        static let goal = "goal"
        static let rateValue = "rateValue"
        static let isUploaded = "isUploaded"
        static let imageURL = "imageURL"
        static let imageData = "imageData"
        static let thoughts = "thoughts"
        static let isFavorite = "isFavorite"
        static let weather = "weather"
    }
    
    override func awakeFromNib() {
        
    }
    
    // MARK: Managed
    var managedObjectContextFromProtocol: NSManagedObjectContext? {
        return managedObjectContext
    }
    
    static var defaultSortDescriptors: [NSSortDescriptor] {
        return [NSSortDescriptor(key: "date", ascending: false)]
    }

    
    // MARK: Public
    public var shouldOverwrite = false
    
    public var imageURL: URL? {
        return imageURLstring != nil ? imageURLstring!.convertToURL() : nil
    }
    
    public var image:UIImage? {
        return imageData != nil ? UIImage(data: imageData!) : nil
    }
    
    public func setURL(url:String) {
        imageURLstring = url
    }
    
    public var rate:Rate {
        return Rate.init(rawValue: rateValue)!
    }
    
    public var isCreatedOnSameDayAsRegistration: Bool {
        return date.toYearToDateString(type: .current) == creationDate.toYearToDateString(type: .current)
    }
    
    public var toDictionary: [String:Any] {
        return [
            Keys.date: dateString,
            Keys.creationDate : creationDate.toYearToMillisecondString(type: .UTC),
            Keys.good:good.checkIfNotEmptyOrNull,
            Keys.bad:bad.checkIfNotEmptyOrNull,
            Keys.goal:goal.checkIfNotEmptyOrNull,
            Keys.isFavorite: isFavorite,
            Keys.rateValue:rateValue,
            Keys.thoughts: thoughts.checkIfEmpty ? NSNull() : thoughts!,
            Keys.imageURL: imageURLstring.unwrapOrNull()
        ].addValueIfNotEmpty(forKey: Keys.weather, value: weather, configuration: {$0.toDictionary})
    }
    
    public func edit(appStatus: AppStatus, withGood good: String, bad: String, goals:String, ratingValue: Int16, date: Date, imageData: Data?, thoughts:String?, isFavorite: Bool, weather: Weather?) {
        /**
         if it's edited.. remove words in the review first and then add a new entry.
         
         we need to remove the previous weather before we move on to new one.
         
         in case there is weather associated with the review and no new weather passed through arguments,
         we just stick with  the previous one.
         */
        
        removePharagraphsFromWordManager(appStatus.wordCountManager)

        if let previousWeather = self.weather {
            appStatus.mainContext.delete(previousWeather)
        }
        
        self.good = good
        self.bad = bad
        self.goal = goals
        self.rateValue = ratingValue
        self.date = date
        self.dateString = date.toYearToDateString(type:.current)
        self.imageData = imageData
        self.thoughts = thoughts
        self.isFavorite = isFavorite
        self.isUploaded = false
        
        if weather == nil, self.weather != nil {
            //do nothing. we stick with the previous one.
        } else {
            self.weather = weather
        }
        
        addPharagraphsToWordManager(appStatus.wordCountManager)
        self.saveEditedData()
    }
    
    public func removeReviewAndPharagraphFromWordDictionary(wordManager:WordCountManager) {
        removePharagraphsFromWordManager(wordManager)
        self.removeFromContext()
    }
    
    public func didFinishUpload() {
        // this triggeres update notification which is okay I guess? should i change the logic.
        isUploaded = true
        self.saveEditedData()
    }

    public func addPharagraphsToWordManager(_ manager: WordCountManager) {
        manager.addParagraph([good, bad, goal, thoughts])
    }
    
    public func removePharagraphsFromWordManager(_ manager: WordCountManager) {
        manager.removeParagraph([good, bad, goal, thoughts])
    }
    
    public func uploadReviewToServer(appStatus:AppStatus, path:String, completion: ((CompletionResult<Review,Error?>)->Void)? = nil) {
        
        /**
         0. if it's testing mode, we don't send it to backend.
         1. if syncOnToggle is off, we don't send it to backend.
         2. if the cellur sync is off, we need to check if it's ceullar.
         */
        
        guard !UserDefaults.retrieveValue(forKey: .isTestingMode, defaultValue: false) else {
            completion?(.success(self))
            return
        }
        
        guard appStatus.syncOnTracker.value else {
            completion?(.success(self))
            return
        }
        
        if appStatus.currentConnection == .cellular {
            guard appStatus.syncOnCelluarTracker.value else {
                completion?(.success(self))
                return
            }
        }
        
        
        uploadImageAndThenSaveReviewToServer(appStatus: appStatus, path: path, completion: completion)
    }
    
    
    // MARK: Static
    
    public static func findByDateOrCreate(into moc: NSManagedObjectContext, wordCountManager: WordCountManager, good:String, bad:String, goals:String, ratingValue: Int16, date:Date = Date(), imageData: Data?, thoughts:String?, isFavorite:Bool, nonCDweather:NonCDWeather?)-> Review {
        
        let result = Review.findOrFetchByDate(from: moc, dateString: date.toYearToDateString(type:.current))
        
        guard result == nil else {
            // if there is an existing one, we need to remove weather. only if weather is not nil.
            result!.shouldOverwrite = true
            return result!
        }
        
        let weather = nonCDweather?.toWeather(into: moc)
        
        let review: Review = moc.insertObject()
        review.date = date
        review.dateString = date.toYearToDateString(type:.current)
        review.creationDate = Date()
        review.rateValue = ratingValue
        review.good = good.trimmingCharacters(in: .whitespaces)
        review.bad = bad.trimmingCharacters(in: .whitespaces)
        review.goal = goals.trimmingCharacters(in: .whitespaces)
        review.isUploaded = false
        review.imageData = imageData
        review.imageURLstring = nil
        review.thoughts = thoughts.checkIfEmpty ? nil : thoughts
        review.isFavorite = isFavorite
        review.weather = weather
        
        logJson(review.toDictionary, type: .coreData)
        
        //I moved the logic to add words to wordcountmanager but I brough it back because it's hard to do track for update cases.
        review.addPharagraphsToWordManager(wordCountManager)
        
        _ = moc.saveOrRollback()
        return review
    }
    
    public static func findOrFetchByDate(from moc: NSManagedObjectContext, dateString:String)-> Review? {
        let predicate = NSPredicate(format: "dateString == %@", dateString)
        return Review.findOrFetch(in: moc, matching: predicate)
    }
    
    public static func getPredicateForSelectedMonth(_ date: Date) -> NSPredicate {
        return getPredicateFromStartToEnd(startDate: date.startDateOfGivenMonth, endDate: date.endDateOfGivenMonth)
    }
    
    public static func convertFromJsonAndInsert(withJSON dict:[String:Any], into moc:NSManagedObjectContext, wordCountManager: WordCountManager) {
        // FIXME: let's upload pictures to the account.
        
        let json = dict.toJSON
        
        let _good = json[Keys.good].string.unwrapOrBlank()
        let _bad = json[Keys.bad].string.unwrapOrBlank()
        let _goals = json[Keys.goal].string.unwrapOrBlank()
        let _ratingValue = json[Keys.rateValue].int16Value
        
        let _imageData = dict[Keys.imageData] as? Data
        let _thoughts = json[Keys.thoughts].string
        let _isFavorite = json[Keys.isFavorite].boolValue
        let _date = json[Keys.date].stringValue
        
        var _nonCDWeather: NonCDWeather!
        
        if let _weather = dict[Keys.weather] as? [String:Any] {
            _nonCDWeather = NonCDWeather.init(json: _weather.toJSON)
        } else {
            _nonCDWeather = nil
        }
        
        _ = Review.findByDateOrCreate(into: moc, wordCountManager: wordCountManager, good: _good, bad: _bad, goals: _goals, ratingValue: _ratingValue, date: _date.toDate(type: .current), imageData: _imageData, thoughts: _thoughts, isFavorite: _isFavorite, nonCDweather: _nonCDWeather)
    }
    
    public static func getPredicateFromStartToEnd(startDate: Date, endDate:Date) -> NSPredicate {
        /**
         Between doesn't include the same day equation so I subtract and add one day to dates so that target dates get included in the result.
         */
        return NSPredicate(format: "%K BETWEEN {%@, %@}",#keyPath(Review.date),
                           startDate as NSDate,
                           endDate as NSDate)
    }
    
    public static func checkIfReviewExistsAndCreatedOnSameDayAsRegistration(from moc: NSManagedObjectContext, forDate date: Date) -> Bool {
        let datePredicate = NSPredicate(format: "%K == %@", #keyPath(Review.dateString), date.toYearToDateString(type:.current))
        
        guard let r = Review.findOrFetch(in: moc, matching: datePredicate) else {
            return false
        }
        return r.isCreatedOnSameDayAsRegistration
    }
    
    public static func getCount(from moc: NSManagedObjectContext, startDate:Date, endDate:Date, rate:Rate)->Int {
        
        let request = Review.sortedFetchRequest
        request.returnsObjectsAsFaults = true
        
        let groupForPredicates = [getPredicateFromStartToEnd(startDate: startDate, endDate: endDate), NSPredicate(format: "rateValue == %ld", rate.rawValue)]
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: groupForPredicates)
        
        guard let count = try? moc.count(for: request) else {
            assertionFailure()
            return 0
        }
        
        return count
    }
    
    public static func getMonthlyCount(from moc: NSManagedObjectContext, date:Date?, rate: Rate?)-> Int {
        
        let request = Review.sortedFetchRequest
        request.returnsObjectsAsFaults = true
        
        var groupForPredicate = [NSPredicate]()
        
        if rate != nil {
            groupForPredicate.append(NSPredicate(format: "rateValue == %ld", rate!.rawValue))
        }
        
        if date != nil {
            groupForPredicate.append(getPredicateForSelectedMonth(date!))
        }
        
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: groupForPredicate)
        
        guard let count = try? moc.count(for: request) else {
            assertionFailure()
            return 0
        }
        
        return count
    }
    
    static func FetchUnsentReviews(from moc:NSManagedObjectContext) -> [Review] {
        let predicate = NSPredicate(format: "%K == %@", #keyPath(Review.isUploaded), NSNumber(booleanLiteral: false))
        return fetch(in: moc, configurationBlock: {(request) in
            request.predicate = predicate
            request.returnsObjectsAsFaults = true
        })
    }
    
    // MARK: Fileprivate
    
    fileprivate func uploadImageAndThenSaveReviewToServer(appStatus: AppStatus, path:String, completion:((CompletionResult<Review,Error?>)->Void)? = nil) {
        
        guard appStatus.currentConnection != .none else {
            completion?(.failure(nil))
            return
        }
        HUD.show(.labeledProgress(title: nil, subtitle: nil))
        
        performOnMain {[unowned self] in
            self.checkImageAndUpload(appStatus: appStatus, path: path, completion: { (result) in
                HUD.hide()
                switch result {
                case .success(let optionalURL):
                    if let url = optionalURL {
                        self.setURL(url: url)
                    }
                    /**
                     Reason why we don't pass completion to the pst review method is
                     at this point, we don't care about the method since it runs on background
                     so from what users see, it's compelted at that point.
                     */
                    self.postReviewToServer(appStatus: appStatus, path: path)
                    completion?(.success(self))
                case .failure(let e):
                    if case let APIError.firebaseError(error) = e {
                        completion?(.failure(error))
                    } else {
                        assertionFailure()
                    }
                }
            })
        }
    }
    
    fileprivate func postReviewToServer(appStatus: AppStatus, path:String, completion:(()->Void)? = nil) {
        performOnBackground {[unowned self] in
            appStatus.networkManager.postJSON(value: self.toDictionary, toPath: path, completion: { (result) in
                performOnMain {
                    switch result {
                    case .success(_):
                        self.didFinishUpload()
                        completion?()
                    case .failure(_):completion?()
                    }
                }
            })
            
        }
    }
    
    fileprivate func checkImageAndUpload(appStatus:AppStatus, path: String, completion:@escaping (CompletionResult<String?,APIError>)->Void) {
        guard let originalData = imageData else {completion(.success(nil));return}
        
        /**
         // FIXME: free account should be limited to 100KB.. Too stingy but I'm afraid it will be too expensive. will be revisited later.
         but for now, I will set it to 1MB.
         */
//        let targetData = originalData.resizeImageData(toMB: appStatus.subscriptionVersion.allowedImageSize)
        
        appStatus.networkManager.upload(data: originalData, toPath: path) { (result) in
            switch result {
            case .success(let url): completion(.success(url))
            case .failure(let e): completion(.failure(e))
            }
        }
    }
}

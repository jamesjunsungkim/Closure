//
//  Weather.swift
//  Closure
//
//  Created by James Kim on 9/13/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CoreData

final class Weather: CDBaseModel {
    
    @NSManaged fileprivate(set) var temperature:Int16
    @NSManaged fileprivate(set) var condition:Int16
    @NSManaged fileprivate(set) var city:String
    @NSManaged fileprivate(set) var weatherIconName:String
    @NSManaged fileprivate(set) var weatherDescription:String
    @NSManaged fileprivate(set) var countryCodeInEnglish:String
    
    
    struct Keys {
        static let temperature = "temperature"
        static let condition = "condition"
        static let city = "city"
        static let weatherIconName = "weatherIconName"
        static let weatherDescription = "weatherDescription"
        static let countryCodeInEnglish = "countryCodeInEnglish"
    }
    
    override func awakeFromInsert() {
        logJson(toDictionary, type: .coreData)
    }
    
    deinit {
        logInfo("Weather Deinitialized", type: .coreData)
        logJson(toDictionary, type: .coreData)
    }
    
    var managedObjectContextFromProtocol: NSManagedObjectContext? {
        return managedObjectContext
    }
    
    // MARK: Public
    public var countryCodeInCurrentLanguage:String {
        return NonCDWeather.getCountryName(countryCode: countryCodeInEnglish)
    }
    
    public var toNonCDWeather:NonCDWeather {
        return NonCDWeather(temperature: Int(temperature), condition: Int(condition), city: city, weatherIconName: weatherIconName, weatherDescription: weatherDescription, countryCodeInEnglish: countryCodeInEnglish, countryName: countryCodeInCurrentLanguage)
    }
    
    public static func create(into moc:NSManagedObjectContext, temperature:Int, condition:Int, city:String, weatherIconName:String, weatherDescription:String, countryCodeInEnglish:String) -> Weather {
        /**
         let's save it when saving the review instead of saving it twice.
         and it doesn't make sense saving weather separately since weather exists for reviews.
         */
        let weather: Weather = moc.insertObject()
        weather.temperature = Int16(temperature)
        weather.condition = Int16(condition)
        weather.city = city
        weather.weatherIconName = weatherIconName
        weather.weatherDescription = weatherDescription
        weather.countryCodeInEnglish = countryCodeInEnglish
        
        logJson(weather.toDictionary, type: .coreData)
//        _ = moc.saveOrRollback()
        
        return weather
    }
    
    public var toDictionary: [String:Any] {
        return [
            Keys.temperature: temperature,
            Keys.condition: condition,
            Keys.city: city,
            Keys.weatherIconName:weatherIconName,
            Keys.weatherDescription: weatherDescription,
            Keys.countryCodeInEnglish: countryCodeInEnglish
        ]
    }
    
    // MARK: Fileprivate
    fileprivate func getWeatherIcon(condition: Int) -> String {
        switch (condition) {
        case 0...300 :
            return "tstorm1"
            
        case 301...500 :
            return "light_rain"
            
        case 501...600 :
            return "shower3"
            
        case 601...700 :
            return "snow4"
            
        case 701...771 :
            return "fog"
            
        case 772...799 :
            return "tstorm3"
            
        case 800 :
            return "sunny"
            
        case 801...804 :
            return "cloudy2"
            
        case 900...903, 905...1000  :
            return "tstorm3"
            
        case 903 :
            return "snow5"
            
        case 904 :
            return "sunny"
            
        default :
            return "question_mark"
        }
        
    }
}















////
////  SearchConditionRawValue.swift
////  Closure
////
////  Created by James Kim on 9/6/18.
////  Copyright © 2018 James Kim. All rights reserved.
////
//
//import Foundation
//import CoreData
//
//final class SearchConditionRawValue: CDBaseModel {
//    
//    @NSManaged fileprivate(set) var searchText:String
//    @NSManaged fileprivate(set) var creationDate:Date
//    @NSManaged fileprivate(set) var conditionDate:Date?
//    @NSManaged fileprivate(set) var conditionBool:Bool
//    @NSManaged fileprivate(set) var index: UInt16
//    
//    // MARK: Public
//    public var toSearchCondition:FeedSearchCondition {
//        switch index {
//        case 0: return .textBad
//        case 1: return .textGood
//        case 2: return .textGoal
//        case 3: return .textThought
//        case 4: return .isFavorite
//        case 5: return .imageIncluded
//        case 6: return .rateAwesome
//        case 7: return .rateGood
//        case 8: return .rateOkay
//        case 9: return .rateUnsatisfactory
//        case 10: return .rateTerrible
//        case 11:
//        guard let _date = conditionDate else {assertionFailure(); return.textGood}
//        return .startDate(_date.toYearToDateString(type:.current))
//        case 12:
//            guard let _date = conditionDate else {assertionFailure(); return.textGood}
//            return .endDate(_date.toYearToDateString(type:.current))
//        case 13:
//            return .sortByAscendingRate(conditionBool)
//        case 14:
//            return .sortByAscendingDate(conditionBool)
//        default: assertionFailure(); return.textGood
//        }
//    }
//    
////    public func convertAndSaveToDisk(into moc: NSManagedObjectContext, from conditions:[FeedSearchCondition]) {
////        let _ = conditions.map { (condition) -> SearchConditionRawValue in
////            let newObj: SearchConditionRawValue = moc.insertObject()
////            let dateRelatedCodition = condition.index == 11 || condition.index == 12
////            let sortRelatedCondition = condition.index == 13 || condition.index == 14
////            
////            
////            newObj.index = UInt16(condition.index)
////            newObj.creationDate = Date()
//////            newObj.conditionDate = dateRelatedCodition ?
////            if case let .startDate(_date) = condition {
////                
////            }
////            
////            return newObj
////        }
////        
////    }
//    
//}
//
//
//
//
//
//
//
//
//
//
//
//

//
//  Rate.swift
//  Closure
//
//  Created by James Kim on 8/1/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

public enum Rate:Int16{
    case awesome = 0
    case good 
    case ok
    case unsatisfactory
    case terrible
    
    // MARK: Public
    public var description: String {
        switch self {
        case .awesome: return "It was awesome".localized
        case .good: return "Good".localized
        case .ok : return "Nah, okay".localized
        case .unsatisfactory: return "Unsatisfactory".localized
        case .terrible: return "Terrible".localized
        }
    }
    
    public var title:String {
        switch self {
        case .awesome: return "Awesome".localized
        case .good: return "Good".localized
        case .ok : return "Okay".localized
        case .unsatisfactory: return "Unsatisfactory".localized
        case .terrible: return "Terrible".localized
        }
    }
    
    public var notLocalizedTitle:String {
        switch self {
        case .awesome: return "Awesome"
        case .good: return "Good"
        case .ok : return "Okay"
        case .unsatisfactory: return "Unsatisfactory"
        case .terrible: return "Terrible"
        }
    }
    
    public var image: UIImage {
        switch self {
        case .awesome: return UIImage(named:"rate_awesome")!
        case .good: return UIImage(named:"rate_good")!
        case .ok : return UIImage(named:"rate_ok")!
        case .unsatisfactory: return UIImage(named:"rate_unsatisfactory")!
        case .terrible: return UIImage(named:"rate_terrible")!
        }
    }
    
    public var currentColor: UIColor {
        switch self {
        case .awesome: return UserDefaults.readColor(forKey: .awesomeColor, defaultColor: defaultColor)
        case .good: return UserDefaults.readColor(forKey: .goodColor, defaultColor: defaultColor)
        case .ok: return UserDefaults.readColor(forKey: .okColor, defaultColor: defaultColor)
        case .unsatisfactory: return UserDefaults.readColor(forKey: .unsatisfactoryColor, defaultColor: defaultColor)
        case .terrible: return UserDefaults.readColor(forKey: .terribleColor, defaultColor: defaultColor)
        }
    }
    
    public var defaultColor:UIColor {
        switch self {
        case .awesome: return Rate.awesomeDefaultColor
        case .good: return Rate.goodDefaultColor
        case .ok: return Rate.okDefaultColor
        case .unsatisfactory: return Rate.unsatisfactoryDefaultColor
        case .terrible: return Rate.terribleDefaultColor
        }
    }
    
    // MARK: Static
    static let awesomeDefaultColor = UIColor.mainBlue
    static let goodDefaultColor = UIColor.create(R: 128, G: 212, B: 255, alpha: 0.5)
    static let okDefaultColor = UIColor.create(R: 255, G: 255, B: 102)
    static let unsatisfactoryDefaultColor = UIColor.create(R: 255, G: 179, B: 102, alpha: 0.4)
    static let terribleDefaultColor = UIColor.create(R: 255, G: 102, B: 102)
    
    
    
    
    
}

//
//  Weather.swift
//  Closure
//
//  Created by James Kim on 9/13/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreData

struct NonCDWeather {
    let temperature:Int
    let condition:Int
    let city:String
    let weatherIconName:String
    
    let weatherDescription:String
    let countryCodeInEnglish:String
    let countryName:String
    
    public static func getCountryName(countryCode:String) -> String {
        /**
         we don't need to translate it. the return value is in the language of the local.
         */
//        let local = currentLanguage.locale
        let englishLocal = Language.english.locale
        guard let result = englishLocal.localizedString(forRegionCode: countryCode) else {assertionFailure();return "Unavailable"}
        return result
    }
}

extension NonCDWeather {
    
    init?(json:JSON) {
        
        func getWeatherIcon(condition: Int) -> String {
            switch (condition) {
            case 0...300 :
                return "tstorm1"
                
            case 301...500 :
                return "light_rain"
                
            case 501...600 :
                return "shower3"
                
            case 601...700 :
                return "snow4"
                
            case 701...771 :
                return "fog"
                
            case 772...799 :
                return "tstorm3"
                
            case 800 :
                return "sunny"
                
            case 801...804 :
                return "cloudy2"
                
            case 900...903, 905...1000  :
                return "tstorm3"
                
            case 903 :
                return "snow5"
                
            case 904 :
                return "sunny"
                
            default :
                return "question_mark"
            }
            
        }

        guard let tempResult = json["main"]["temp"].double else {
            return nil
        }
        
        self.temperature = Int(tempResult - 273.15)
        
        self.city = json["name"].string!
        
        self.condition = json["weather"][0]["id"].intValue
        self.weatherDescription = json["weather"][0]["description"].stringValue
        
        self.weatherIconName = getWeatherIcon(condition: json["weather"][0]["id"].intValue)
        
        self.countryCodeInEnglish = json["sys"]["country"].stringValue
        self.countryName = NonCDWeather.getCountryName(countryCode: json["sys"]["country"].stringValue)
    }
    
    public func toWeather(into moc:NSManagedObjectContext) -> Weather {
        return Weather.create(into: moc, temperature: temperature, condition: condition, city: city, weatherIconName: weatherIconName, weatherDescription: weatherDescription, countryCodeInEnglish: countryCodeInEnglish)
    }
    
}

//
//  NetworkSession.swift
//  Closure
//
//  Created by James Kim on 7/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

protocol NetworkSession {
    typealias JSON = [String:Any]
    // Data
    func fetch(fromPath path: String, completion: @escaping (NetworkResult<Any, APIError>) -> Void)
    func fetchJSON(fromPath path: String, completion: @escaping (NetworkResult<JSON,APIError>)->Void)
    func fetchList(fromPath path: String, completion:  @escaping (NetworkResult<[JSON],APIError>)->Void)
    func fetchList(fromPath path: String, queryOrdedByChild child: String, queryEqualTo value: Any?, completion:  @escaping (NetworkResult<[JSON],APIError>)->Void)
    
    func upload(data: Data, toPath path: String, completion:@escaping (NetworkResult<String,APIError>)->Void)
    
    func post(value: Any, toPath path: String, completion: @escaping (NetworkResult<Any, APIError>) -> Void)
    func postJSON(value: JSON, toPath path: String, completion:  @escaping (NetworkResult<JSON,APIError>)->Void)
    func patch(value: Any, toPath path: String, completion: @escaping (NetworkResult<Any,APIError>)->Void)
    func delete(atPath path: String, completion: @escaping (NetworkResult<Void,APIError>)->Void)
//    func downloadImage(from url:String) -> UIImage
    
    // User
    func signup(withEmail email: String, password: String, completion: @escaping (NetworkResult<String,APIError>)->Void)
    func login(withEmail email: String, password: String, completion: @escaping (NetworkResult<String,APIError>)->Void)
    func signout(completion: @escaping (NetworkResult<Void,APIError>)->Void)
}

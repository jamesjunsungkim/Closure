//
//  FirebaseNetwork.swift
//  Connect
//
//  Created by James Kim on 7/6/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation
import Firebase

struct FirebaseSession:NetworkSession {
    
    func signout(completion: @escaping (NetworkResult<Void, APIError>) -> Void) {
        do {
            try Auth.auth().signOut()
            completion(.success(()))
        } catch let error {
            completion(.failure(.firebaseError(error)))
        }
    }
    
    func signup(withEmail email: String, password: String, completion: @escaping (NetworkResult<String, APIError>) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            guard error == nil else {
                completion(.failure(.firebaseError(error!)))
                return
            }
            let uid = result!.user.uid
            completion(.success(uid))
        }
    }
    
    func login(withEmail email: String, password: String, completion: @escaping (NetworkResult<String, APIError>) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            guard error == nil else {
                completion(.failure(.firebaseError(error!)))
                return
            }
            let uid = result!.user.uid
            completion(.success(uid))
        }
    }
    func fetch(fromPath path: String, completion: @escaping (NetworkResult<Any, APIError>) -> Void) {
        Database.database().reference().child(path)
            .observeSingleEvent(of: .value, with: { (snapshot) in
                guard snapshot.exists(), let value = snapshot.value else {
                    completion(.failure(.invalidData))
                    return
                }
                
                completion(.success(value))
            }, withCancel: {(error) in
                completion(.failure(.firebaseError(error)))
            })
    }
    
    func fetchJSON(fromPath path: String, completion: @escaping (NetworkResult<NetworkSession.JSON, APIError>) -> Void) {
        Database.database().reference().child(path)
            .observeSingleEvent(of: .value, with: { (snapshot) in
                guard let result = snapshot.value as? [String:Any] else {
                    completion(.failure(.invalidData))
                    return
                }
                completion(.success(result))
            }, withCancel: {(error) in
                completion(.failure(.firebaseError(error)))
            })
    }
    
    func fetchList(fromPath path: String, completion: @escaping (NetworkResult<[NetworkSession.JSON], APIError>) -> Void) {
        Database.database().reference().child(path)
            .observeSingleEvent(of: .value, with: { (snapshot) in
                
                guard snapshot.exists() else {
                    completion(.success([[String:Any]]()))
                    return
                }
                
                guard let _r = snapshot.value as? [String:[String:Any]] else {
                    completion(.failure(.invalidData))
                    return
                }
                
                let result = _r.map({$0.value})
                completion(.success(result))
            }, withCancel: {(error) in
                completion(.failure(.firebaseError(error)))
            })
    }
    
    func fetchList(fromPath path: String, queryOrdedByChild child: String, queryEqualTo value: Any?, completion: @escaping (NetworkResult<[NetworkSession.JSON], APIError>) -> Void) {
        Database.database().reference().child(path)
            .observeSingleEvent(of: .value, with: { (snapshot) in
                guard let result = snapshot.value as? [[String:Any]] else {
                    completion(.failure(.invalidData))
                    return
                }
                completion(.success(result))
            }, withCancel: {(error) in
                completion(.failure(.firebaseError(error)))
            })
    }
    
    func post(value: Any, toPath path: String, completion: @escaping (NetworkResult<Any, APIError>) -> Void) {
        Database.database().reference().child(path)
            .setValue(value) { (error,_) in
                guard error == nil else {
                    completion(.failure(.firebaseError(error!)))
                    return
                }
                completion(.success(value))
        }
    }
    
    func postJSON(value: NetworkSession.JSON, toPath path: String, completion: @escaping (NetworkResult<NetworkSession.JSON, APIError>) -> Void) {
        Database.database().reference().child(path)
            .setValue(value) { (error,_) in
                guard error == nil else {
                    completion(.failure(.firebaseError(error!)))
                    return
                }
                completion(.success(value))
        }
    }
    
    func upload(data: Data, toPath path: String, completion: @escaping (NetworkResult<String, APIError>) -> Void) {
        performOnMain {
            let ref = Storage.storage().reference().child(path)
            ref.putData(data, metadata: nil) { (metadata, error) in
                guard error == nil else {
                    completion(.failure(.firebaseError(error!)))
                    return
                }
                ref.downloadURL(completion: {(url, error) in
                    guard let url = url, error == nil else {
                        completion(.failure(.firebaseError(error!)))
                        return
                    }
                    completion(.success(url.absoluteString))
                })
            }
        }
    }
    
    func patch(value: Any, toPath path: String, completion: @escaping (NetworkResult<Any, APIError>) -> Void) {
        Database.database().reference().child(path)
            .setValue(value) { (error,_) in
                guard error == nil else {
                    completion(.failure(.firebaseError(error!)))
                    return
                }
                completion(.success(value))
        }
    }
    
    func delete(atPath path: String, completion: @escaping (NetworkResult<Void, APIError>) -> Void) {
        Database.database().reference().child(path)
            .removeValue { (error, _) in
                guard error == nil else {
                    completion(.failure(.firebaseError(error!)))
                    return
                }
                completion(.success(()))
        }
    }
}


//
//  NetworkResult+APIError.swift
//  Closure
//
//  Created by James Kim on 7/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//
import Foundation

public enum CompletionResult<T,U> {
    case success(T)
    case failure(U)
}

public enum CompletionWithoutData<U> {
    case success
    case failure(U)
}

public enum NetworkResult<T,U:Error> {
    case success(T)
    case failure(U)
}

enum APIError:Error, Equatable {
    case invalidData
    case firebaseError(Error)
    
    var localizedDescription: String {
        switch self {
        case .invalidData: return "invalid data"
        case .firebaseError(let error): return error.localizedDescription
        }
    }
    
    static func == (lhs:APIError, rhs:APIError) ->Bool {
        return lhs.localizedDescription == rhs.localizedDescription
    }
}












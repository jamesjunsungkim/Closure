//
//  NetworkManager.swift
//  Connect
//
//  Created by James Kim on 7/6/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import SDWebImage
    
final class NetworkManager {
    private var session: NetworkSession
    
    typealias JSON = NetworkSession.JSON
    
    init(session:NetworkSession = FirebaseSession()) {
        self.session = session
        assertCount(limit: 1)
    }
    
    // MARK: Public
    public func changeSession(_ session: NetworkSession) {
        self.session = session
    }
    
    public func signup(withEmail email: String, password: String, completion: @escaping (NetworkResult<String, APIError>) -> Void) {
        session.signup(withEmail: email, password: password, completion: completion)
    }
    
    public func login(withEmail email: String, password: String, completion: @escaping (NetworkResult<String, APIError>) -> Void) {
        session.login(withEmail: email, password: password, completion: completion)
    }
    
    public func signout(completion: @escaping (NetworkResult<Void, APIError>) -> Void) {
        session.signout(completion: completion)
    }
    
    public func fetch(fromPath path: String, completion: @escaping (NetworkResult<Any, APIError>) -> Void) {
        session.fetch(fromPath: path, completion: completion)
    }
    
    public func fetchJSON(fromPath path: String, completion: @escaping (NetworkResult<NetworkSession.JSON, APIError>) -> Void) {
        session.fetchJSON(fromPath: path, completion: completion)
    }
    
    public func fetchJSONList(fromPath path: String, queryOrdedByChild child: String, queryEqualTo value: Any?, completion: @escaping (NetworkResult<[NetworkSession.JSON], APIError>) -> Void) {
        logInfo("is about to fetch list from \(path), queryOrdedBy \(child), queryEqualto \(value.unwrapOr(defaultValue: "-"))",type: .network)
        
        session.fetchList(fromPath: path, queryOrdedByChild: child, queryEqualTo: value, completion: completion)
    }
    
    public func fetchJSONList(fromPath path: String, completion: @escaping (NetworkResult<[NetworkSession.JSON], APIError>) -> Void) {
        session.fetchList(fromPath: path, completion: completion)
    }
    
    public func post(value: Any, toPath path: String, completion: @escaping (NetworkResult<Any, APIError>) -> Void) {
        // since it's the same method as setvalue.. so I will just use patch instead of creating a new one with the same logic.
        if let dict = value as? [String:Any] {logJson(dict, type: .network)}
        session.post(value: value, toPath: path, completion: completion)
    }
    
    public func postJSON(value: NetworkSession.JSON, toPath path: String, completion: @escaping (NetworkResult<JSON, APIError>) -> Void) {
        logJson(value, type: .network)
        session.postJSON(value: value, toPath: path, completion: completion)
    }
    
    
    public func upload(data: Data, toPath path: String, completion: @escaping (NetworkResult<String, APIError>) -> Void) {
        session.upload(data: data, toPath: path, completion: completion)
    }
    
    public func patch(value: Any, toPath path: String, completion: @escaping (NetworkResult<Any, APIError>) -> Void) {
        if let dict = value as? [String:Any] {logJson(dict, type: .network)}
        session.patch(value: value, toPath: path, completion: completion)
    }
    
    public func delete(atPath path: String, completion: @escaping (NetworkResult<Void, APIError>) -> Void) {
        session.delete(atPath: path, completion: completion)
    }
    
//    public func downloadImage(fromURL url:String) {
//        let a = SDWebImagePrefetcher.shared()
////        a.prefetchURLs([URL]?, progress: {(currentNum, totalNum) in
////
////        }, completed: <#T##SDWebImagePrefetcherCompletionBlock?##SDWebImagePrefetcherCompletionBlock?##(UInt, UInt) -> Void#>)
//        let b = SDWebImageManager.shared()
//        guard let c = b.imageCache else {return}
//
//    }
    
    // MARK: Fileprivate
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

//
//  Unwrapable.swift
//  Connect
//
//  Created by James Kim on 5/15/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation

public protocol Unwrappable {}

extension Unwrappable {
//    public var className:String {
//        return String(describing: self)
//    }
    
    static var className: String {
        return String(describing: Self.self)
    }
    
    static func unwrapInstanceFrom(userInfo: [String:Any]?) -> Self {
        return (userInfo![className] as! Self)
    }
    
    static func unwrapListFrom(userInfo: [String:Any]?) -> [Self] {
        return (userInfo![className] as! [Self])
    }
    
    static func unwrapOptionalInstanceFrom(userInfo: [String:Any]?) -> Self? {
        return userInfo?[className] as? Self
    }
    
    public var toUserInfo: [String:Any] {
        return [Self.className:self]
    }
}

extension Array where Element: Unwrappable {
    public var toUserInfo:[String:Any] {
        /**
         there are some scenarios where we send empty array
         */
//        guard self.count != 0 else {assertionFailure(); return [:]}
        return [Element.className: self]
    }
}





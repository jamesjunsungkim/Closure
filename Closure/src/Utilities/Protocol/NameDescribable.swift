//
//  NameDescribable.swift
//  Connect
//
//  Created by James Kim on 5/31/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation

protocol NameDescribable {}

extension NameDescribable {
    static var className: String {
        return String(describing: self)
    }
    
    public var className: String {
        return String(describing: Self.self)
    }
    
    var observerDisposedDescription:()->Void {
        return { logInfo("observer is disposed from: \(String(describing: Self.self))",type: .observation) }
    }
}

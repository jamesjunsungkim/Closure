//
//  Reusable.swift
//  Connect
//
//  Created by James Kim on 5/23/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CoreData

protocol Reusable:AnyObject, NameDescribable {
    associatedtype Object
    func setup(withObject object: Object, parentViewController: UIViewController, currentIndexPath: IndexPath, userInfo:[String:Any]? )
    func update(withObject object: Object, atIndexPath indexPath: IndexPath)
    func didGetSelected()
    func didGetDeselected()
}

extension Reusable {
    static var reuseIdentifier: String {
        return Self.className
    }
    
    static var keyForPrepareForRuese: String {
        return "prepareForReuse"
    }
    func setup(withObject object: Object, parentViewController: UIViewController, currentIndexPath: IndexPath, userInfo:[String:Any]? ){}
    func update(withObject object: Object, atIndexPath indexPath: IndexPath){}
    func didGetSelected(){}
    func didGetDeselected(){}
}

protocol AdditionalReusable: AnyObject {
    func setup(withObject object: Any, parentViewController: UIViewController, currentIndexPath: IndexPath, userInfo:[String:Any]? )
    func update(withObject object: Any, atIndexPath indexPath: IndexPath)
    func didGetSelected()
    func didGetDeselected()
}
extension AdditionalReusable {
    func setup(withObject object: Any, parentViewController: UIViewController, currentIndexPath: IndexPath, userInfo:[String:Any]? ) {}
    func update(withObject object: Any, atIndexPath indexPath: IndexPath) {}
    func didGetSelected() {}
    func didGetDeselected() {}
}

protocol CoredataReusable:AnyObject, NameDescribable {
    associatedtype Object:NSFetchRequestResult
    func setup(withObject object: Object, parentViewController: UIViewController, currentIndexPath: IndexPath, userInfo:[String:Any]?)
    func update(withObject object: Object, atIndexPath indexPath: IndexPath)
    func didGetSelected()
    func didGetDeselected()

}

extension CoredataReusable {
    func setup(withObject object: Object, parentViewController: UIViewController, currentIndexPath: IndexPath, userInfo:[String:Any]? ){}
    func update(withObject object: Object, atIndexPath indexPath: IndexPath){}
    func didGetSelected(){}
    func didGetDeselected(){}
    
    static var reuseIdentifier: String {
        return Self.className
    }
    
    var keyForPrepareForRuese:String {
        return "prepareForReuse"
    }
    
    static var keyForPrepareForRuese: String {
        return "prepareForReuse"
    }
    
}

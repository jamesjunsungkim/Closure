//
//  FromBottomUpTransition.swift
//  Closure
//
//  Created by James Kim on 7/17/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation
import MZFormSheetPresentationController

final class FromBottomUpTransition: NSObject, MZFormSheetPresentationViewControllerTransitionProtocol {
    
    func entryFormSheetControllerTransition(_ formSheetController: UIViewController, completionHandler: @escaping MZTransitionCompletionHandler) {
        let screenSize = UIScreen.main.bounds
        
        let viewSize = formSheetController.view.frame
        formSheetController.view.frame = CGRect(x: screenSize.width == viewSize.width ? 0 : (screenSize.width-viewSize.width)/2, y: screenSize.height, width: viewSize.width, height: viewSize.height)
        let newSize = formSheetController.view.frame
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            formSheetController.view.frame = CGRect(x: newSize.minX, y: newSize.minY-viewSize.height, width: newSize.width, height: newSize.height)
        }) { (_) in
            completionHandler()
        }
    }
    
    func exitFormSheetControllerTransition(_ formSheetController: UIViewController, completionHandler: @escaping MZTransitionCompletionHandler) {
        let formSheetRect = formSheetController.view.frame
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
            formSheetController.view.frame = CGRect(x: formSheetRect.origin.x, y: UIScreen.main.bounds.height + 10, width: formSheetRect.width, height: formSheetRect.height)
        }) { (_) in
            completionHandler()
        }
    }
}

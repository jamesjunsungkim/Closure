////
////  CustomReminderManager.swift
////  Closure
////
////  Created by James Kim on 8/17/18.
////  Copyright © 2018 James Kim. All rights reserved.
////
//
//import Foundation
//import RxSwift
//
//final class CustomReminderManager {
//
//    fileprivate(set) var reminderArray: [Reminder] = []
//
//
//    init() {
//
//        initialSetup()
//    }
//    
//    // MARK: Public
//    public var reminderObservable: Observable<[Reminder]> {
//        return reminderSubject.asObservable()
//    }
//
//
//    public func add(reminder:Reminder) {
//        reminderArray.append(reminder)
//        reminderSubject.onNext(reminderArray)
//        saveToUserDefault()
//    }
//
//    public func remove(reminder:Reminder) {
//        reminderArray.removeItem(condition: {$0.uuid == reminder.uuid})
//        reminderSubject.onNext(reminderArray)
//        saveToUserDefault()
//    }
//
//    public func edit(reminder:Reminder) {
//        reminderArray.replace(element: reminder)
//        reminderSubject.onNext(reminderArray)
//        saveToUserDefault()
//    }
//
//
//    // MARK: Fileprivate
//    fileprivate let reminderSubject = PublishSubject<[Reminder]>()
//
//    fileprivate func initialSetup() {
//        //Load reminders
//        let group = UserDefaults.retrieveValue(forKey: .customReminder, defaultValue: [[String:Any]]())
//        reminderArray = group.compactMap({Reminder(json: $0.toJSON)})
//    }
//
//    fileprivate func saveToUserDefault() {
//        let target = reminderArray.map({$0.toDictionary})
//        UserDefaults.store(object: target, forKey: .customReminder)
//    }
//
//}

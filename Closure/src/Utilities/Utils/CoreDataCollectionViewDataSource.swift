//
//  CoreDataTableViewDataSource.swift
//  Connect
//
//  Created by James Kim on 5/27/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CoreData

fileprivate enum Update<T> {
    case insert(IndexPath)
    case update(IndexPath, T)
    case move(IndexPath, IndexPath)
    case delete(IndexPath)
}

final class CoreDataCollectionViewDataSource<A:CoreDataReusableCollectionViewCell>:NSObject, UICollectionViewDataSource, NSFetchedResultsControllerDelegate {
    
    typealias Object = A.Object
    typealias Cell = A
    
    required init(collectionView:UICollectionView, fetchedResultsController:NSFetchedResultsController<Object>, parentViewController: UIViewController, userInfo:[String:Any]? = nil, observeFetchResult:(([Object])->Void)? = nil, additionalSetupCell: ((A)->())? = nil, prepareForReuse: ((A)->())? = nil) {
        self.collectionView = collectionView
        self.fetchedResultsController = fetchedResultsController
        self.parentViewController = parentViewController
        self.userInfo = userInfo
        self.observeFetchResult = observeFetchResult
        self.additionalSetupCell = additionalSetupCell
        self.prepareForReuse = prepareForReuse
        super.init()
        fetchedResultsController.delegate = self
        try! fetchedResultsController.performFetch()
        self.observeFetchResult?(fetchedObjects.unwrapOr(defaultValue: [Object]()))
        collectionView.dataSource = self
        collectionView.register(A.self, forCellWithReuseIdentifier: A.reuseIdentifier)
        collectionView.reloadData()
    }
    
    
    
    public var selectedObject: Object? {
        guard let indexPathes = collectionView.indexPathsForSelectedItems else {return nil}
        return objectAtIndexPath(indexPathes.first!)
    }
    
    public func objectAtIndexPath(_ indexPath: IndexPath) -> Object {
        return fetchedResultsController.object(at: indexPath)
    }
    
    public var fetchedObjects: [Object]? {
        return fetchedResultsController.fetchedObjects
    }
    
    public func indexPath(forObject ob:Object) -> IndexPath {
        guard let ip = fetchedResultsController.indexPath(forObject: ob) else {assertionFailure(); return IndexPath(item: 0, section: 0)}
        return ip
    }
    
    public func cleanFetchResultControllerCache() {
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: fetchedResultsController.cacheName)
    }
    
    public func reconfigureFetchRequest(_ configure: (NSFetchRequest<Object>) -> ()) {
        cleanFetchResultControllerCache()
        configure(fetchedResultsController.fetchRequest)
        do { try fetchedResultsController.performFetch() }
        catch { fatalError("fetch request failed") }
        self.observeFetchResult?(fetchedObjects.unwrapOr(defaultValue: [Object]()))
        collectionView.reloadData()
    }
    
    public func cellForRow(at indexPath: IndexPath) -> Cell? {
        // if it's not visible, return nil
        return collectionView.cellForItem(at: indexPath) as? Cell
    }
    
    // MARK: - Filepriavte
    fileprivate weak var collectionView: UICollectionView!
    fileprivate weak var parentViewController: UIViewController!
    
    fileprivate let fetchedResultsController: NSFetchedResultsController<Object>
    
    fileprivate let observeFetchResult: (([Object])->Void)?
    fileprivate let userInfo: [String:Any]?
    
    fileprivate let additionalSetupCell: ((A)->Void)?
    fileprivate let prepareForReuse: ((A)->Void)?
    
    fileprivate var updates:[Update<Object>] = []
    
    fileprivate func processUpdates(_ updates: [Update<Object>]?) {
        guard let updates = updates else {return collectionView.reloadData()}
        collectionView.performBatchUpdates({[unowned self] in
            for update in updates {
                switch update {
                case .insert(let indexPath):
                    self.collectionView.insertItems(at: [indexPath])
                    observeFetchResult?(fetchedObjects.unwrapOr(defaultValue: [Object]()))
                case .update(let indexPath, let object):
                    // if the cell is invisible, it might return nil.
                    guard let cell = self.collectionView.cellForItem(at: indexPath) as? Cell else { continue }
                    cell.update(withObject: object, atIndexPath: indexPath)
                case .move(let indexPath, let newIndexPath):
                    self.collectionView.deleteItems(at: [indexPath])
                    self.collectionView.insertItems(at: [newIndexPath])
                    observeFetchResult?(fetchedObjects.unwrapOr(defaultValue: [Object]()))
                case .delete(let indexPath):
                    self.collectionView.deleteItems(at: [indexPath])
                    observeFetchResult?(fetchedObjects.unwrapOr(defaultValue: [Object]()))
                }
            }
        }, completion: nil)
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        guard let sections = fetchedResultsController.sections else {return 0}
        return sections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let section = fetchedResultsController.sections?[section] else {return 0}
        return section.numberOfObjects
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cell.reuseIdentifier, for: indexPath) as? Cell else {fatalError()}
        let userInfoWithPrepareForReuse = userInfo?.addValueIfNotEmpty(forKey: Cell.keyForPrepareForRuese, value: prepareForReuse)
        cell.setup(withObject: objectAtIndexPath(indexPath), parentViewController: parentViewController, currentIndexPath: indexPath, userInfo: userInfoWithPrepareForReuse)
        additionalSetupCell?(cell)
        return cell
    }
    
    // MARK: - NSFetchedResultsControllerDelegate
   
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        updates = []
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            guard let indexPath = newIndexPath else { fatalError("Index path should be not nil") }
            updates.append(.insert(indexPath))
        case .update:
            guard let indexPath = indexPath else { fatalError("Index path should be not nil") }
            let object = objectAtIndexPath(indexPath)
            updates.append(.update(indexPath, object))
        case .move:
            guard let indexPath = indexPath else { fatalError("Index path should be not nil") }
            guard let newIndexPath = newIndexPath else { fatalError("New index path should be not nil") }
            updates.append(.move(indexPath, newIndexPath))
        case .delete:
            guard let indexPath = indexPath else { fatalError("Index path should be not nil") }
            updates.append(.delete(indexPath))
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        processUpdates(updates)
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        let targetIndex = IndexSet(integer:sectionIndex)
        switch type {
        case .insert:
            collectionView.insertSections(targetIndex)
            observeFetchResult?(fetchedObjects.unwrapOr(defaultValue: [Object]()))
        case .delete:
            collectionView.deleteSections(targetIndex)
            observeFetchResult?(fetchedObjects.unwrapOr(defaultValue: [Object]()))
        default: break;
        }
    }
}

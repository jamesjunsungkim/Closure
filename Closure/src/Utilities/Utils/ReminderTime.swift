//
//  ReminderTime.swift
//  Closure
//
//  Created by montapinunt Pimonta on 8/13/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation

public enum ReminderTime:String {
    case Morning
    case Afternoon
    case Evening
    case Night
    case Random
    
    public var localized: String {
        return self.rawValue.localized
    }
    
    public var displayTime: String {
        switch self {
        case .Morning: return "8-12AM"
        case .Afternoon: return "12-4PM"
        case .Evening: return "4-8PM"
        case .Night: return "8-12PM"
        case .Random: return "8AM-12PM"
        }
    }
    
    public var index:Int {
        switch self {
        case .Morning: return 0
        case .Afternoon: return 1
        case .Evening: return 2
        case .Night: return 3
        case .Random: return 4
        }
    }
    
    public var toSelectedTime:Date {
        let minimumHour = 8*3600 // 8AM. 28800
        let fourhourInSeconds = 4*3600 // 4 hrs 14400
        
        
        func addFourHourGap(howMany:Int) ->Int {
            return minimumHour+(fourhourInSeconds*howMany)
        }
        
        var randomNumInSeconds:Int
        switch self {
        case .Morning:
            // 8 - 12 AM.
            randomNumInSeconds = Int.random(start: minimumHour, to: addFourHourGap(howMany: 1))
        case .Afternoon:
            randomNumInSeconds = Int.random(start: addFourHourGap(howMany: 1), to: addFourHourGap(howMany: 2))
        case .Evening:
            randomNumInSeconds = Int.random(start: addFourHourGap(howMany: 2), to: addFourHourGap(howMany: 3))
        case .Night:
            randomNumInSeconds = Int.random(start: addFourHourGap(howMany: 3), to: addFourHourGap(howMany: 4))
        case .Random:
            randomNumInSeconds = Int.random(start: minimumHour, to: addFourHourGap(howMany: 4))
        }
        
        let targetTime = randomNumInSeconds.convertSecondsToHourMinSecond()
        
        return Date(year: 2018, month: 09, day: 10, hour: targetTime.hour, minute: targetTime.min, second: targetTime.second)
        
    }
    
    static public var pool: [ReminderTime] = [.Morning, .Afternoon, .Evening, .Night, .Random]
}

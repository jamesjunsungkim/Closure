//
//  DiskStatusManager.swift
//  Closure
//
//  Created by James Kim on 8/27/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

final class DiskStatusManager {
    
    // MARK: Public
    public var totalDiskSpace: String {
        return ByteCountFormatter.string(fromByteCount: totalDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.binary)
    }
    
    public var freeDiskSpace:String {
        return ByteCountFormatter.string(fromByteCount: freeDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.binary)
    }
    
    public var usedDiskSpace:String {
        return ByteCountFormatter.string(fromByteCount: usedDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.binary)
    }
    
    public var isFreeSpaceMoreThan1GB : Bool {
        let gb = freeDiskSpaceInBytes/1073741824
        return gb > 1
    }
    
    public var isFreeSpaceMoreThan500MB : Bool {
        let gb = freeDiskSpaceInBytes/1073741824
        return gb > 1/2
    }
    
    // MARK: Fileprivate
    fileprivate var totalDiskSpaceInBytes:Int64 {
        do {
            let systemAttributes = try FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String)
            let space = (systemAttributes[FileAttributeKey.systemSize] as? NSNumber)?.int64Value
            return space!
        } catch {
            return 0
        }
    }
    
    fileprivate var freeDiskSpaceInBytes: Int64 {
        do {
            let systemAttributes = try FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String)
            let freeSpace = (systemAttributes[FileAttributeKey.systemFreeSize] as? NSNumber)?.int64Value
            return freeSpace!
        } catch {
            return 0
        }
    }
    
    fileprivate var usedDiskSpaceInBytes:Int64 {
        let usedSpace = totalDiskSpaceInBytes - freeDiskSpaceInBytes
        return usedSpace
    }
}

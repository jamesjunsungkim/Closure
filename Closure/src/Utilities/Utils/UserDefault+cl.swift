//
//  UserDefault.swift
//  Connect
//
//  Created by montapinunt Pimonta on 5/7/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

extension UserDefaults {
    
    enum Key : String {
        case defaultResolution
        
        case uidForSignedInUser
        case currentUser
        
        case forImprovements
        case forAchievemnets
        case forGoals
        
        case terribleColor
        case unsatisfactoryColor
        case okColor
        case goodColor
        case awesomeColor
        
        case countMainDictionary
        case countLimitationDictionay
        case countAddItFirstDictionary
        
        //Setting
        
        //Setting - notification and reminder
        case wordDeletionWarningAllowed
        case internetDisconnectionWarningAllowed
        case defaultReminderAllowed
        case customReminderAllowed
        
        case defaultReminderTime
        case customReminder
        
        case isPassCodeOn
        case passCodeRequiredTime
        case isTouchIDOn
        
        case currentLanguage
        
        case isSyncOn
        case isSyncOverCelluarOn
        
        case imageSizeOptimization
        
        case appOpenCount
        
        case lastDayToSendEmail
        case currentTemperatureUnitPreference
        
        case currentSecurityQuestion
        
        case isTestingMode
    }
    
    
    // MARK: - Static
    static func userRequestToSignOut() {
        let keys:[UserDefaults.Key] = [.uidForSignedInUser]
        keys.forEach({UserDefaults.removeValue(forKey: $0)})
    }
    
    static func retrieveValue<T>(forKey key: Key, defaultValue:T) -> T {
        return checkIfValueExist(forKey: key) ? UserDefaults.standard.object(forKey: key.rawValue) as! T : defaultValue
    }
    
    static func retrieveValueOrFatalError(forKey key: Key)->Any {
        return UserDefaults.standard.object(forKey: key.rawValue)!
    }
    
    static func store(object: Any, forKey key: Key) {
        UserDefaults.standard.setValue(object, forKey: key.rawValue)
        
        if key == .currentUser {
            guard let json = object as? [String:Any] else {assertionFailure();return}
            UserDefaults.store(object: json[User.Keys.uid] as Any, forKey: .uidForSignedInUser)
        }
    }
    
    static func storeColor(_ color: UIColor, forKey key: Key) {
        UserDefaults.store(object: color.components, forKey: key)
    }
    
    static func readColor(forKey key: Key, defaultColor: UIColor)->UIColor {
        let result = UserDefaults.retrieveValue(forKey: key, defaultValue: [CGFloat]())
        guard result.count == 4 else {return defaultColor}
        let color = UIColor(red: result[0], green: result[1], blue: result[2], alpha: result[3])
        return color
    }
    
    static func removeValue(forKey key: Key) {
        UserDefaults.standard.removeObject(forKey: key.rawValue)
    }
    
    static func checkIfValueExist(forKey key: Key)->Bool {
        return UserDefaults.standard.object(forKey: key.rawValue) != nil
    }
    
}









//
//  LanguageManager.swift
//  Closure
//
//  Created by montapinunt Pimonta on 8/25/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation
import RxSwift

// This way, I could use it on the string extention..
fileprivate(set) var currentLanguageDictionary: [String:String]!
fileprivate(set) var currentLanguage: Language!
//public var wordsFortTrnalsation = [String]()

final class LanguageManager:Unwrappable {
    
    init() {
        // load the dictionary..
        guard let currentLanguage = Language(rawValue: languageTracker.value) else {assertionFailure();return}
        loadDictionary(with: currentLanguage)
    }
    
    // MARK: Public
    
    public var changeLanguageObservable:Observable<Language> {
        return changeLanguageSubject.asObservable()
    }
    
    public func retrieveTranslation(for ph:String) -> String {
        // if it's english, just return input.
        guard languageTracker.value != "english" else {return ph}
        //TODO: should crash if the target word is not available.
        guard let word = currentLanguageDictionary[ph] else {return ""}
        return word
    }
    
    fileprivate(set) lazy var languageTracker = Tracker.init(
        forUserDefaultKey: .currentLanguage, defaultValue: userPreferredLanguage,
        additionalDidSet: {[unowned self] (stringValue) in
            guard let _currentLanguage = Language(rawValue: stringValue) else {assertionFailure();return}
            guard currentLanguage != _currentLanguage else {return}
            self.loadDictionary(with: _currentLanguage)
            self.changeLanguageSubject.onNext(_currentLanguage)
    })
    
    //MARK: Fileprivate
    fileprivate let changeLanguageSubject = PublishSubject<Language>()
    
    fileprivate var userPreferredLanguage: String {
        /**
         we provide three languages. korean, english, thai.
         */
        guard let lan = Locale.preferredLanguages.first else {return "english"}
        switch lan {
        case "en": return "english"
        case "thai": return "thai"
        case "ko": return "korean"
        default: return "english"
        }
    }
    
    fileprivate func loadDictionary(with lan: Language) {
        currentLanguage = lan
        
        guard lan != .english else {return}
        guard let path = Bundle.main.path(forResource: lan.rawValue, ofType: "plist"),
            let dict = NSDictionary(contentsOfFile: path) as? [String:String] else {assertionFailure();return}
        currentLanguageDictionary = dict
    }
    
   
}

// not the best approach.. I guess..
extension String {
    public var localized:String {
        guard currentLanguage != .english else {
//            if !wordsFortTrnalsation.contains(self){wordsFortTrnalsation.append(self)}
            return self
        }
//        if !wordsFortTrnalsation.contains(self){
//            wordsFortTrnalsation.append("<key>\(self)</key>\n<string></string>")
//        }
        guard let translatedWord = currentLanguageDictionary[self] else {return self + "_"}
        return translatedWord
    }
}





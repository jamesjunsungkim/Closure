//
//  FeedSearchCondition.swift
//  Closure
//
//  Created by James Kim on 8/31/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation

public enum PredicateType {
    case and
    case or
}

public enum ConditionType {
    case predicate
    case sortDescriptor
}

public enum FeedSearchCondition: Equatable, Unwrappable {
    
    case textBad
    case textGood
    case textGoal
    case textThought
    
    case isFavorite
    case imageIncluded
    
    case rateAwesome
    case rateGood
    case rateOkay
    case rateUnsatisfactory
    case rateTerrible
    
    case startDate(String)
    case endDate(String)
    
    // Sort Descriptor
    
    case sortByAscendingRate(Bool)
    case sortByAscendingDate(Bool)
    
    public var toPredicate:NSPredicate {
        switch self {
            /**
             in case of text-related predicates, it's hard to create it separately from view controller since
             it requires text.. it has to be done when user actually types.
             */
//        case .textBad: return 0
//        case .textGood : return 1
//        case .textGoal : return 2
//        case .textThought: return 3
        case .isFavorite :
            return NSPredicate(format: "%K == %@", #keyPath(Review.isFavorite), NSNumber(booleanLiteral: true))
        case .imageIncluded : return NSPredicate(format: "%K != nil", #keyPath(Review.imageData))
        case .rateAwesome: return NSPredicate(format: "%K == %ld", #keyPath(Review.rateValue), Rate.awesome.rawValue)
        case .rateGood:return NSPredicate(format: "%K == %ld", #keyPath(Review.rateValue), Rate.good.rawValue)
        case .rateOkay: return NSPredicate(format: "%K == %ld", #keyPath(Review.rateValue), Rate.ok.rawValue)
        case .rateUnsatisfactory :
            return NSPredicate(format: "%K == %ld", #keyPath(Review.rateValue), Rate.unsatisfactory.rawValue)
        case .rateTerrible :return NSPredicate(format: "%K == %ld", #keyPath(Review.rateValue), Rate.terrible.rawValue)
        case .startDate(let dateStringValue):
            return NSPredicate(format: "%K >= %@", #keyPath(Review.date), dateStringValue.toDate(type:.current) as NSDate)
        case .endDate(let dateStringValue) :
            /**
             when choosing a date from calendar, it gives a time without any hour and minute. like 2018-09-30:00:00:00
             but enddate has to be one second before it turns to be the next date in order to fetch data.
             */
            let initialDate = dateStringValue.toDate(type:.current)
            let targetDate = initialDate.addDay(value: 1).addSecond(value: -1)
            return NSPredicate(format: "%K <= %@", #keyPath(Review.date), targetDate as NSDate)
        case .sortByAscendingDate(_), .sortByAscendingRate(_):
            assertionFailure(); return NSPredicate(value: true)
        default: return NSPredicate(value: true)
        }
    }
    
    public var predicateType: PredicateType {
        switch self {
        case .textBad, .textGood, .textGoal, .textThought, .rateAwesome,
             .rateGood, .rateOkay, .rateUnsatisfactory, .rateTerrible: return .or
        case .isFavorite, .imageIncluded, .endDate(_), .startDate(_): return .and
        default: assertionFailure(); return .or
        }
    }
    
    public var textBasedCondition: Bool {
        switch self {
        case .textBad, .textGood, .textGoal,.textThought : return true
        default: return false
        }
    }
    
    public var conditionType: ConditionType {
        switch self {
        case .sortByAscendingRate(_), .sortByAscendingDate(_): return .sortDescriptor
        default: return .predicate
        }
    }
    
    public var toSortDescriptor:NSSortDescriptor {
        switch self {
        case .sortByAscendingDate(let booleanValue):
            return NSSortDescriptor(key: "date", ascending: booleanValue)
        case .sortByAscendingRate(let booleanValue):
            return NSSortDescriptor(key: "rateValue", ascending: !booleanValue)
        default: assertionFailure(); return NSSortDescriptor(key: nil, ascending: true)
        }
    }
    
    public var notLocalizedDescription:String {
        switch self {
        case .textBad: return "Rooms for improvement"
        case .textGood : return "Achievements"
        case .textGoal : return "Goal for the following day"
        case .textThought: return "Thought of the day"
        case .isFavorite : return "Favorite"
        case .imageIncluded :return "Picture of the day"
        case .rateAwesome: return Rate.awesome.notLocalizedTitle
        case .rateGood:return Rate.good.notLocalizedTitle
        case .rateOkay: return Rate.ok.notLocalizedTitle
        case .rateUnsatisfactory : return Rate.unsatisfactory.notLocalizedTitle
        case .rateTerrible : return Rate.terrible.notLocalizedTitle
        case .startDate(let date): return "Start Date: \(date)"
        case .endDate(let date) :return "End Date: \(date)"
        case .sortByAscendingRate(let bool): return bool ? "Lowest rate first" : "Highest rate first"
        case .sortByAscendingDate(let bool) : return bool ? "Earliest date first": "Latest date first"
        }
    }
    
    public var index:Int {
        switch self {
        case .textBad: return 0
        case .textGood : return 1
        case .textGoal : return 2
        case .textThought: return 3
        case .isFavorite : return 4
        case .imageIncluded :return 5
        case .rateAwesome: return 6
        case .rateGood:return 7
        case .rateOkay: return 8
        case .rateUnsatisfactory : return 9
        case .rateTerrible :return 10
        case .sortByAscendingRate(_): return 11
        case .sortByAscendingDate(_) : return 12
        case .startDate(_): return 13
        case .endDate(_) :return 14
        }
    }
}









//
//  SettingDescriptionFooter.swift
//  Closure
//
//  Created by James Kim on 8/15/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

final class SettingDescriptionView:UIView {
    
    //UI
    fileprivate var label: UILabel!
    
    init(description:String, textAlignment: NSTextAlignment = .center) {
        self.descriptionForFotter = description
        self.textAlignment = textAlignment
        super.init(frame: .zero)
        setupUI()
    }
    
    // MARK: Fileprivate
    fileprivate let descriptionForFotter: String
    fileprivate let textAlignment: NSTextAlignment
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}
extension SettingDescriptionView {
    fileprivate func setupUI() {
        label = UILabel.create(text: descriptionForFotter, textAlignment: textAlignment, fontSize: 15, numberOfLine:0)
        
        addSubview(label)
        
        if textAlignment == .center {
            label.setEdgesToSuperView()
        } else {
            label.snp.makeConstraints { (make) in
                make.left.equalToSuperview().offset(10)
                make.bottom.equalToSuperview().offset(-3)
            }
        }
    }
}

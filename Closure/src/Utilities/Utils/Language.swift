//
//  Language.swift
//  Closure
//
//  Created by James Kim on 8/13/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation

public enum Language:String {
    case english
    case thai
    case korean
    
    public var description: String {
        switch self {
        case .english : return "EN"
        case .thai: return "TH"
        case .korean: return "KR"
        }
    }
    
    public var index: Int {
        switch self {
        case .english : return 0
        case .korean: return 1
        case .thai: return 2
        }
    }
    
    public var locale:Locale {
        switch self {
        case .english : return Locale.init(identifier: "en_US")
        case .thai: return Locale.init(identifier: "th_TH")
        case .korean: return Locale.init(identifier: "ko_KR")
        }
    }
    
    // TODO: Change the thai date foramt properly.
    public var toYearToDayFormat:String {
        switch self {
        case .english : return "yyyy MMMM"
        case .thai: return "yyyy'년' MM'월'"
        case .korean: return "yyyy'년' MM'월'"
        }
    }
    
    
    public var toLongMonthToDayFormat:String {
        switch self {
        case .english : return "MMMM dd"
        case .thai: return "MM'월' dd'일'"
        case .korean: return "MM'월' dd'일'"
        }
    }
    
    public var toShortMonthToDayFormat:String {
        switch self {
        case .english : return "MMM dd"
        case .thai: return "MM'월' dd'일'"
        case .korean: return "MM'월' dd'일'"
        }
    }
    
    public var toDayDateFormat:String {
        switch self {
        case .english : return "MMMM dd"
        case .thai: return "M'월'dd'일'"
        case .korean: return "M'월'dd'일'"
        }
    }
    
    public var toMonthDateFormat:String {
        switch self {
        case .english : return "MMMM"
        case .thai: return "MM'월'"
        case .korean: return "MM'월'"
        }
    }
}

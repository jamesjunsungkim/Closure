//
//  FilterDisposeBag.swift
//  Closure
//
//  Created by James Kim on 7/28/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation
import RxSwift

final class FilterDisposeBag {
    static var defaultBag:DisposeBag {
        return DisposeBag()
    }
    
    fileprivate(set) var dict = [ObjectIdentifier:Disposable]()
    
    public func checkIfValueExist(forKey key: ObjectIdentifier) -> Bool {
        return dict[key] != nil
    }
    
    public func insert(withIdentifier i:ObjectIdentifier, newObserver: Disposable) {
        guard dict[i] == nil else {return}
        dict[i] = newObserver
    }
    
    public func remove(withIdentifier i:ObjectIdentifier) {
        guard let observer = dict[i] else {assertionFailure();return}
        observer.dispose()
        dict.removeValue(forKey: i)
    }
    
    deinit {
        dict.values.forEach({$0.dispose()})
    }
}

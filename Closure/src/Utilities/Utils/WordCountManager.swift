//
//  WordCountManager.swift
//  Closure
//
//  Created by James Kim on 8/11/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation
import RxSwift
import CoreData

public enum WordAdditionFailure {
    case counterDictionaryContainWord
    case duplicateInDictionary
}

fileprivate struct WordEntry {
    let tag: NSLinguisticTag
    let word: String
}

public final class WordCountManager {
    init(context:NSManagedObjectContext) {
        
        self.mainContext = context
        /**
         ------------------------- LOGIC RUN DOWN ------------------------------
         0. we load dictionaries from userdefaults
         1. set up observations for contextDidSaveNotification to update word dictionaries in order to keep the code neat.
         */
        
        readSavedDictionaryFromUserDefault()
        setupObservers()
    }

    deinit {
        NotificationCenter.default.removeObserver(contextDidSaveNotificationToken)
    }
    
    // MARK: Public
    
    public var limitationWordsObservable:Observable<[NSLinguisticTag:Set<String>]> {
        return limitationDictionarySubject.asObservable()
    }
    
    public var addItFirstWordsObservable:Observable<[NSLinguisticTag:Set<String>]> {
        return addItFirstDictionarySubject.asObservable()
    }
    
    public var signalToReloadTopWordsObservable:Observable<Void> {
        return signalToReloadTopWordsSubject.asObservable()
    }
    
    public func addParagraph(_ phs:[String?]) {
        let words = phs.reduce([WordEntry](), {$0 + splitParahAndFilterThem(pa: $1)})
        words.forEach({
            addWord($0, toDictionary: &mainDictionary)
//            addWord($0, toDictionary: &deleteDictionary)
        })
        saveMainDictToUserDefault()
    }
    
    public func removeParagraph(_ phs:[String?]) {
        let words = phs.reduce([WordEntry](), {$0 + splitParahAndFilterThem(pa: $1)})
        words.forEach({
            removeWord($0, fromDictionary: &mainDictionary)
//            removeWord($0, fromDictionary: &deleteDictionary)
        })
        saveMainDictToUserDefault()
    }
    
    public func addLimitationWord(forTag tag: NSLinguisticTag, word:String, failure: ((WordAdditionFailure,NSLinguisticTag)->Void)? = nil) {
        let targetWord = word.lowercased()
        // let's make sure that additfirst dictionay doesn't contain the word
        
        if let counterTag = checkIfWordExist(inTargetDict: addItFirstIfDetectedDictionary, input: targetWord) {
            failure?(WordAdditionFailure.counterDictionaryContainWord, counterTag)
            return
        }
        
        if let counterTag = checkIfWordExist(inTargetDict: limitationDictionary, input: targetWord) {
            failure?(WordAdditionFailure.duplicateInDictionary, counterTag)
            return
        }
        
        limitationDictionary.addValue(
            forKey: tag, initialValue: Set<String>.init([targetWord]),
            configurationForExistingValue: {
                _ = $0.insert(targetWord)
                return $0
        })
        
        // you can put this in the closure because it has access to the dict and tries to save dict at the same time.
        saveLimitationDictToUserDefaultAndSendOutNewData()
    }
    
    public func removeLimitationWord(forTag tag: NSLinguisticTag, word:String) {
        let targetWord = word.lowercased()
        guard var targetSet = limitationDictionary[tag], targetSet.contains(targetWord) else {assertionFailure();return}
        _ = targetSet.remove(targetWord)
        limitationDictionary[tag] = targetSet
        saveLimitationDictToUserDefaultAndSendOutNewData()
    }
    
    public func addWordToAddFirstDictionary(forTag tag: NSLinguisticTag, word:String, failure: ((WordAdditionFailure,NSLinguisticTag)->Void)? = nil) {
        let targetWord = word.lowercased()
        
        // let's make sure that additfirst dictionay doesn't contain the word
        
        if let counterTag = checkIfWordExist(inTargetDict: limitationDictionary, input: targetWord) {
            failure?(WordAdditionFailure.counterDictionaryContainWord, counterTag)
            return
        }
        
        if let counterTag = checkIfWordExist(inTargetDict: addItFirstIfDetectedDictionary, input: targetWord) {
            failure?(WordAdditionFailure.duplicateInDictionary, counterTag)
            return
        }

        addItFirstIfDetectedDictionary.addValue(
            forKey: tag, initialValue: Set<String>.init([targetWord]),
            configurationForExistingValue: {
                _ = $0.insert(targetWord)
                return $0
        })
        
        
        // if it's saved to the dict already, remove it and put it into the new tag.
        if let oldTag = self.checkIfWordExistInMainDictionary(input: targetWord) {
            var dict = self.mainDictionary[oldTag]!
            let targetCount = dict[targetWord]!
            dict.removeValue(forKey: targetWord)
            self.mainDictionary[oldTag] = dict
            
            var targetDict = self.mainDictionary[tag].unwrapOr(defaultValue: [String:Int]())
            targetDict[targetWord] = targetCount
            self.mainDictionary[tag] = targetDict
            self.saveMainDictToUserDefault()
        }
        
        // you can put this in the closure because it has access to the dict and tries to save dict at the same time.
        saveAddFirstDictToUserDefaultAndSendOutNewData()
      
    }
    
    public func removeWordFromAddFirstDictionary(forTag tag: NSLinguisticTag, word:String) {
        let targetWord = word.lowercased()
        
        guard var targetSet = addItFirstIfDetectedDictionary[tag], targetSet.contains(targetWord) else {assertionFailure();return}
        _ = targetSet.remove(targetWord)
        addItFirstIfDetectedDictionary[tag] = targetSet
        
        guard let newTag = filterAndCreateWordEntry(pharagraph: targetWord).first?.tag else {assertionFailure();return}
        
        guard let oldTag = checkIfWordExistInMainDictionary(input: targetWord) else {assertionFailure();return}
        var dict = mainDictionary[oldTag]!
        let targetCount = dict[targetWord]!
        dict.removeValue(forKey: targetWord)
        mainDictionary[oldTag] = dict
        
        var targetDict = mainDictionary[newTag].unwrapOr(defaultValue: [String:Int]())
        targetDict[targetWord] = targetCount
        mainDictionary[newTag] = targetDict
        
        saveMainDictToUserDefault()
        saveAddFirstDictToUserDefaultAndSendOutNewData()
    }
    
    public func retrieveTopFiveWords(forTag tag: NSLinguisticTag) -> [(String,Int)] {
        // when trying to get a list, if it's included in the limitation array, it won't get included in the result.
        guard let target = mainDictionary[tag] else {return []}
        
        let sortedDict = target.filter({
            guard let targetLimitation = limitationDictionary[tag] else {return true}
            return !targetLimitation.contains($0.key)
        }).sorted(by: {
            guard $0.value != $1.value else {
                // count is the same
                return $0.key < $1.key
            }
            return $0.value > $1.value
        })
        
        guard sortedDict.count >= 5 else {
            return sortedDict
        }
        let result = Array(sortedDict[0...4])
        return result
    }
    
    public func retrieveLimitationWords(forTag tag: NSLinguisticTag)->[String] {
        return limitationDictionary[tag].unwrapOr(defaultValue: Set<String>()).toArray
    }
    
    public func retrieveAddFirstDictionaryWords(forTag tag: NSLinguisticTag) -> [String] {
        return addItFirstIfDetectedDictionary[tag].unwrapOr(defaultValue: Set<String>()).toArray
    }
    
    public func removeDictionary() {
        mainDictionary.removeAll()
    }
    
    public func reset() {
        let group : [UserDefaults.Key] = [.countMainDictionary, .countLimitationDictionay, .countAddItFirstDictionary]
        group.forEach({UserDefaults.removeValue(forKey: $0)})
        
        mainDictionary.removeAll()
        limitationDictionary.removeAll()
        addItFirstIfDetectedDictionary.removeAll()
    }
    
    // MARK: Actions
    fileprivate lazy var obvserContextDidSaveNotification:(ContextDidSaveNotification)->Void = {[unowned self] (note) in
        /**
         ------------------------- LOGIC RUN DOWN ------------------------------
         0. we listen to changes on objects from notification and update dictionaries.
         
         // FIXME: How to trace update cases? so I just moved all back to where it was like when creating it or editing it.
         I located all the word count logic to review's method when it creates or deletes, edits.
         
         */
//        if !note.insertedObjects.isEmpty {
//            for r in note.insertedObjects {
//                guard let review = r as? Review else {continue}
//                review.addPharagraphsToWordManager(self)
//            }
//        }
//
//        if !note.deletedObjects.isEmpty {
//            for r in note.deletedObjects {
//                guard let review = r as? Review else {continue}
//                review.removePharagraphsFromWordManager(self)
//            }
//        }
//
//        if !note.updatedObjects.isEmpty {
//
//        }
    }
    
    // MARK: Fileprivate
    fileprivate weak var mainContext:NSManagedObjectContext!
    fileprivate var contextDidSaveNotificationToken: NSObjectProtocol!
    
    fileprivate var mainDictionary: [NSLinguisticTag: [String:Int]] = [:]
    //    fileprivate var deleteDictionary: [NSLinguisticTag: [String:Int]] = [:]
    fileprivate var limitationDictionary: [NSLinguisticTag:Set<String>] = [:]
    fileprivate var addItFirstIfDetectedDictionary: [NSLinguisticTag:Set<String>] = [:]
    
    fileprivate let signalToReloadTopWordsSubject = PublishSubject<Void>()
    fileprivate let limitationDictionarySubject = PublishSubject<[NSLinguisticTag:Set<String>]>()
    fileprivate let addItFirstDictionarySubject = PublishSubject<[NSLinguisticTag:Set<String>]>()
    
    fileprivate func splitParahAndFilterThem(pa:String?) ->[WordEntry] {
        return filterAndCreateWordEntry(pharagraph: pa?.lowercased())
    }
    
    fileprivate func setupObservers() {
         contextDidSaveNotificationToken = mainContext.addContextDidSaveNotificationObserver(obvserContextDidSaveNotification)
    }
    
    fileprivate func filterAndCreateWordEntry(pharagraph:String?) -> [WordEntry] {
        guard let targetString = pharagraph else {return []}
        let options: NSLinguisticTagger.Options = [.omitWhitespace, .omitPunctuation, .joinNames]
        let schemes = NSLinguisticTagger.availableTagSchemes(forLanguage: "en")
        let tagger = NSLinguisticTagger(tagSchemes: schemes, options: Int(options.rawValue))
        tagger.string = targetString
        
        var result = [WordEntry]()
        tagger.enumerateTags(in: NSMakeRange(0, (targetString as NSString).length), scheme: .nameTypeOrLexicalClass, options: options) { (t, tokenRange, _, stopPoint) in
            guard let tag = t else {assertionFailure(); return}
            let targetWord = (targetString as NSString).substring(with: tokenRange)
            
            
            guard let userPreferredTag = checkIfWordExist(inTargetDict: addItFirstIfDetectedDictionary, input: targetWord) else {
                result.append(WordEntry(tag: tag, word: targetWord))
                return
            }
            result.append((WordEntry(tag: userPreferredTag, word: targetWord)))
        }
        
        return result
    }
    
    
    fileprivate func addWord(_ entry:WordEntry, toDictionary dict: inout [NSLinguisticTag: [String:Int]] ) {
        dict.addValue(forKey: entry.tag, initialValue: [entry.word:1]) { (existingValue) -> [String : Int] in
            var result = existingValue
            if let targetValue = existingValue[entry.word] {
                result[entry.word] = targetValue+1
            } else {
                result[entry.word] = 1
            }
            return result
        }
    }
    
    fileprivate func removeWord(_ entry:WordEntry, fromDictionary dict: inout [NSLinguisticTag: [String:Int]] ) {
        guard var targetDict = dict[entry.tag], let existingCount = targetDict[entry.word] else {assertionFailure();return}
        
        if existingCount > 1 {
            targetDict[entry.word] = existingCount-1
            dict[entry.tag] = targetDict
        } else if existingCount == 1 {
            targetDict.removeValue(forKey: entry.word)
            dict[entry.tag] = targetDict
        } else {
            assertionFailure()
        }
    }

    
    fileprivate func saveMainDictToUserDefault() {
        UserDefaults.store(object: mainDictionary, forKey: .countMainDictionary)
    }
    
    fileprivate func saveLimitationDictToUserDefaultAndSendOutNewData() {
        UserDefaults.store(object: limitationDictionary.transformValue(block: {Array($0)}), forKey: .countLimitationDictionay)
        limitationDictionarySubject.onNext(limitationDictionary)
        signalToReloadTopWordsSubject.onNext(())
    }
    
    fileprivate func saveAddFirstDictToUserDefaultAndSendOutNewData() {
        UserDefaults.store(object: addItFirstIfDetectedDictionary.transformValue(block: {Array($0)}), forKey: .countAddItFirstDictionary)
        addItFirstDictionarySubject.onNext(addItFirstIfDetectedDictionary)
        signalToReloadTopWordsSubject.onNext(())
    }
    
    fileprivate func readSavedDictionaryFromUserDefault() {
        mainDictionary = UserDefaults.retrieveValue(forKey: .countMainDictionary, defaultValue: [NSLinguisticTag: [String:Int]]())
        limitationDictionary = UserDefaults.retrieveValue(forKey: .countLimitationDictionay, defaultValue: [NSLinguisticTag: [String]]()).transformValue(block: {Set.init($0)})
        addItFirstIfDetectedDictionary =  UserDefaults.retrieveValue(forKey: .countAddItFirstDictionary, defaultValue: [NSLinguisticTag: [String]]()).transformValue(block: {Set.init($0)})
    }
    
    
    fileprivate func checkIfWordExist(inTargetDict dict:[NSLinguisticTag:Set<String>], input:String) -> NSLinguisticTag? {
        for entry in dict {
            guard entry.value.contains(input) else {
                continue
            }
            return entry.key
        }
        return nil
    }
    
    fileprivate func checkIfWordExistInMainDictionary(input:String) -> NSLinguisticTag? {
        for entry in mainDictionary {
            guard let _ = entry.value[input] else {
                continue
            }
            return entry.key
        }
        return nil
    }
    
    
}

//
//  ImageSizeOptimization.swift
//  Closure
//
//  Created by James Kim on 8/25/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation

public enum ImageSizeOptimization:String {
    case optimizedSize
    case originalSize
    
    public var index:Int {
        switch self {
        case .optimizedSize: return 0
        case .originalSize: return 1
        }
    }
    
    public var description:String {
        switch self {
        case .optimizedSize: return "Optimize Device Storage".localized
        case .originalSize: return "Download and Keep Originals".localized
        }
    }
}

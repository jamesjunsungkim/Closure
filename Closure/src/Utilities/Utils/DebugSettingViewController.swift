//
//  DebugSettingViewController.swift
//  Closure
//
//  Created by James Kim on 9/3/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout

final class DebugSettingViewController: DefaultViewController {
    
    //UI
    fileprivate var allSwitch:UISwitch!
    fileprivate var viewControllerLogSwitch:UISwitch!
    fileprivate var observationLogSwitch:UISwitch!
    fileprivate var actionLogSwitch:UISwitch!
    fileprivate var viewLogSwitch:UISwitch!
    fileprivate var objectInstanceLogSwitch:UISwitch!
    fileprivate var networkJSONLogSwitch:UISwitch!
    fileprivate var coredataJSONLogSwitch:UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enterViewControllerMemoryLog(self)
        
        setupUI()
        addTargets()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    // MARK: Actions
    
    @objc fileprivate func observeSwitchToggle(sender:UISwitch) {
//         [viewControllerLogSwitch,observationLogSwitch, actionLogSwitch, viewLogSwitch, objectInstanceLogSwitch, networkJSONLogSwitch, coredataJSONLogSwitch]
        switch sender.tag {
        case 0:
            AppStatus.showViewControllerLog.toggle()
        case 1:
            AppStatus.showObservationLog.toggle()
        case 2:
            AppStatus.showActionLog.toggle()
        case 3:
            AppStatus.showViewInstanceLog.toggle()
        case 4:
            AppStatus.showObjectInstancesLog.toggle()
        case 5:
            AppStatus.showNetworkJSONLog.toggle()
        case 6:
            AppStatus.showCoredataJSONLog.toggle()
        default:assertionFailure()
        }
    }
    
    @objc fileprivate func toggleAllSwitch() {
        group.forEach({
            $0.isOn = allSwitch.isOn
        })
        
        /**
         when all toggle is changed, didSet method changes status of all debug-related toggles. so we don't need to change it here.
         */
        
        AppStatus.showAllLog.toggle()
       
    }
    
    // MARK: Fileprivate
    fileprivate lazy var group:[UISwitch] = [viewControllerLogSwitch,observationLogSwitch, actionLogSwitch, viewLogSwitch, objectInstanceLogSwitch, networkJSONLogSwitch, coredataJSONLogSwitch]
    
    fileprivate func addTargets() {
        
        for (index, sw) in group.enumerated() {
            sw.addTarget(self, action: #selector(observeSwitchToggle(sender:)), for: .valueChanged)
            sw.tag = index
        }
        
        allSwitch.addTarget(self, action: #selector(toggleAllSwitch), for: .valueChanged)
    }
}

extension DebugSettingViewController {
    fileprivate func setupUI() {
        view.backgroundColor = .white
        
        let allSwitchLabel = UILabel.create(text: "Toggle All", textAlignment: .left, fontSize: 15)
        
        allSwitch = UISwitch.create(tintColor: .mainBlue, onTintColor: .mainBlue, isOn: AppStatus.showAllLog)
        
        let controllerLogLabel = UILabel.create(text: "Show controller log", textAlignment: .left, fontSize: 15)
        viewControllerLogSwitch = UISwitch.create(tintColor: .mainBlue, onTintColor: .mainBlue, isOn: AppStatus.showViewControllerLog)
        
        let observationLogLabel = UILabel.create(text: "Show observation log", textAlignment: .left, fontSize: 15)
        observationLogSwitch = UISwitch.create(tintColor: .mainBlue, onTintColor: .mainBlue, isOn: AppStatus.showActionLog)
        
        let actionLogLabel = UILabel.create(text: "Show action log", textAlignment: .left, fontSize: 15)
        actionLogSwitch = UISwitch.create(tintColor: .mainBlue, onTintColor: .mainBlue, isOn: AppStatus.showActionLog)
        
        let viewLogLabel = UILabel.create(text: "Show view log", textAlignment: .left, fontSize: 15)
        viewLogSwitch = UISwitch.create(tintColor: .mainBlue, onTintColor: .mainBlue, isOn: AppStatus.showViewInstanceLog)
        
        let objectLogLabel = UILabel.create(text: "Show object instance log", textAlignment: .left, fontSize: 15)
        objectInstanceLogSwitch = UISwitch.create(tintColor: .mainBlue, onTintColor: .mainBlue, isOn: AppStatus.showObjectInstancesLog)
        
        let networkLogLabel = UILabel.create(text: "Show network JSON log", textAlignment: .left, fontSize: 15)
        networkJSONLogSwitch = UISwitch.create(tintColor: .mainBlue, onTintColor: .mainBlue, isOn: AppStatus.showNetworkJSONLog)
        
        let coredataLogLabel = UILabel.create(text: "Show coredata JSON log", textAlignment: .left, fontSize: 15)
        coredataJSONLogSwitch = UISwitch.create(tintColor: .mainBlue, onTintColor: .mainBlue, isOn: AppStatus.showCoredataJSONLog)
        
        
        let group:[UIView] = [allSwitchLabel, allSwitch, controllerLogLabel, viewControllerLogSwitch, observationLogLabel, observationLogSwitch, actionLogLabel, actionLogSwitch,  viewLogLabel, viewLogSwitch, objectLogLabel, objectInstanceLogSwitch, networkLogLabel, networkJSONLogSwitch, coredataLogLabel, coredataJSONLogSwitch]
        group.forEach(view.addSubview(_:))
        
        let spaceBetweenLines:CGFloat = 30
        let switchPadding:CGFloat = 20
        
        constraint(allSwitchLabel, allSwitch, controllerLogLabel, viewControllerLogSwitch, observationLogLabel, observationLogSwitch, actionLogLabel, actionLogSwitch) { ( _allSwitchLabel, _allSwitch, _controllerLogLabel, _viewControllerLogSwitch, _observationLogLabel, _observationLogSwitch, _actionLogLabel, _actionLogSwitch) in
            
            _allSwitchLabel.left.top.equalToSuperview().offset(35)
            
            _allSwitch.centerY.equalTo(allSwitchLabel)
            _allSwitch.right.equalToSuperview().offset(-switchPadding)
            
            _controllerLogLabel.top.equalTo(allSwitchLabel.snp.bottom).offset(spaceBetweenLines)
            _controllerLogLabel.left.equalTo(allSwitchLabel)
            
            _viewControllerLogSwitch.centerY.equalTo(controllerLogLabel)
            _viewControllerLogSwitch.right.equalToSuperview().offset(-switchPadding)
            
            _observationLogLabel.top.equalTo(controllerLogLabel.snp.bottom).offset(spaceBetweenLines)
            _observationLogLabel.left.equalTo(controllerLogLabel)
            
            _observationLogSwitch.centerY.equalTo(observationLogLabel)
            _observationLogSwitch.right.equalToSuperview().offset(-switchPadding)
            
            _actionLogLabel.top.equalTo(observationLogLabel.snp.bottom).offset(spaceBetweenLines)
            _actionLogLabel.left.equalTo(observationLogLabel)
            
            _actionLogSwitch.centerY.equalTo(actionLogLabel)
            _actionLogSwitch.right.equalToSuperview().offset(-switchPadding)
        }
        
        constraint(viewLogLabel, viewLogSwitch, objectLogLabel, objectInstanceLogSwitch, networkLogLabel, networkJSONLogSwitch, coredataLogLabel, coredataJSONLogSwitch) { ( _viewLogLabel, _viewLogSwitch, _objectLogLabel, _objectInstanceLogSwitch, _networkLogLabel, _networkJSONLogSwitch, _coredataLogLabel, _coredataJSONLogSwitch) in
            
            _viewLogLabel.top.equalTo(actionLogLabel.snp.bottom).offset(spaceBetweenLines)
            _viewLogLabel.left.equalTo(actionLogLabel)
            
            _viewLogSwitch.centerY.equalTo(viewLogLabel)
            _viewLogSwitch.right.equalToSuperview().offset(-switchPadding)
            
            _objectLogLabel.top.equalTo(viewLogLabel.snp.bottom).offset(spaceBetweenLines)
            _objectLogLabel.left.equalTo(viewLogLabel)
            
            _objectInstanceLogSwitch.centerY.equalTo(objectLogLabel)
            _objectInstanceLogSwitch.right.equalToSuperview().offset(-switchPadding)
            
            _networkLogLabel.top.equalTo(objectLogLabel.snp.bottom).offset(spaceBetweenLines)
            _networkLogLabel.left.equalTo(objectLogLabel)
            
            _networkJSONLogSwitch.centerY.equalTo(networkLogLabel)
            _networkJSONLogSwitch.right.equalToSuperview().offset(-switchPadding)
            
            _coredataLogLabel.top.equalTo(networkLogLabel.snp.bottom).offset(spaceBetweenLines)
            _coredataLogLabel.left.equalTo(controllerLogLabel)
            
            _coredataJSONLogSwitch.centerY.equalTo(coredataLogLabel)
            _coredataJSONLogSwitch.right.equalToSuperview().offset(-switchPadding)
            
        }
        
    }
}


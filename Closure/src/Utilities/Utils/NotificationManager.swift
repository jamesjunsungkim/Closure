//
//  NotificationManager.swift
//  Closure
//
//  Created by James Kim on 9/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import FirebaseMessaging
import UserNotifications
import RxSwift

final class NotificationManager:NSObject, MessagingDelegate, UNUserNotificationCenterDelegate {
    
    init(application:UIApplication, appStatus:AppStatus) {
        self.appStatus = appStatus
        self.application = application
        super.init()
        
        initialSetup()
        checkAndUpdateAuthroizedStatus()
    }
    
    // MARK: Public
    fileprivate(set) var reminderArray: [Reminder] = []
    
    public var isAuthorized:Bool {
        /**
         we check previous value and after value so that we can tell when to tun off all notification-related features.
         */
        return authorizationStatus == .authorized
    }
    
    public var authorizationResultObservable:Observable<Bool> {
        return authorizationResultPublishSubject.asObservable()
    }
    
    fileprivate(set) lazy var defaultReminderAllowed = Tracker.init(forUserDefaultKey: .defaultReminderAllowed, defaultValue: isAuthorized, additionalDidSet: {[unowned self] (value) in
        self.checkIfAuthorizedAndIfNotUnCheckIt(value)
        self.observeDefaultReminderPermission(value)
    })
    
    fileprivate(set) var defaultReminderTimeTracker = Tracker.init(forUserDefaultKey: .defaultReminderTime, defaultValue: "Morning")
    
    fileprivate(set) lazy var customReminderAllowed =
        Tracker.init(forUserDefaultKey: .customReminderAllowed, defaultValue: isAuthorized, additionalDidSet: {[unowned self] (value) in
        self.checkIfAuthorizedAndIfNotUnCheckIt(value)
        self.observeCustomReminderPermission(value)
    })
    
    fileprivate(set) lazy var wordDeletionWarningTracker =
        Tracker.init(forUserDefaultKey: .wordDeletionWarningAllowed,
                     defaultValue: isAuthorized,
                     additionalDidSet: {[unowned self] (value) in
                        self.checkIfAuthorizedAndIfNotUnCheckIt(value)
        })
    
    fileprivate(set) lazy var internetDisconnectionWarningTracker = Tracker.init(forUserDefaultKey: .internetDisconnectionWarningAllowed, defaultValue: isAuthorized, additionalDidSet: {[unowned self] (value) in
        self.checkIfAuthorizedAndIfNotUnCheckIt(value)
    })
    
    public var reminderObservable: Observable<[Reminder]> {
        return customerReminderSubject.asObservable()
    }
    
    
    
    public func add(reminder:Reminder) {
        reminderArray.append(reminder)
        customerReminderSubject.onNext(reminderArray)
        saveToUserDefault()
        guard customReminderAllowed.value else {return}
        addNotification(from: reminder)
    }
    
    public func remove(reminder:Reminder) {
        reminderArray.removeItem(condition: {$0.uuid == reminder.uuid})
        customerReminderSubject.onNext(reminderArray)
        saveToUserDefault()
        removeNotification(from: reminder)
    }
    
    public func edit(reminder:Reminder) {
        guard let index = reminderArray.index(of: reminder) else {assertionFailure();return}
        let previousReminder = reminderArray[index]
        removeNotification(from: previousReminder)
        reminderArray.remove(at: index)
        
        reminderArray.insert(reminder, at: index)
        
        customerReminderSubject.onNext(reminderArray)
        saveToUserDefault()
        
        guard customReminderAllowed.value else {return}
        addNotification(from: reminder)
    }
    
    
    // MARK: Actions
    fileprivate lazy var checkIfAuthorizedAndIfNotUnCheckIt:(Bool)->Void = {[unowned self] (isOn) in
        // our interest is only when user doesn't allow the notification.
        guard isOn else {return}
        guard !self.isAuthorized, let status = self.authorizationStatus else {return}
        
        switch status {
        case .authorized: assertionFailure()
            // it shouldn't fall into this category after the condition above.
        case .denied:
            performOnMain {
                let targetVC = UIView.getMostTopViewController()
                self.adjustUIWhenNotAuthorized()
                
                targetVC?.presentDefaultAlert(withTitle: "Error".localized, message: "Permission denied.\nPlease change the setting to enable Reminder.".localized, okAction: {
                    targetVC?.openAppSettings()
                })
                
            }
        case .notDetermined:
            self.requestNotificationAuthroization()
        default:assertionFailure();break
        }
    }
    
    fileprivate lazy var observeDefaultReminderPermission:(Bool)->Void = {[unowned self] isAllowed in
        /**
         ------------------------- LOGIC RUN DOWN ------------------------------
         Whenever user determines permssion on reminder, we take actions differently.
         
         if user doesn't allow it
         0. we find the list of the notifications and get rid of them.
         
         if user allows it,
         0. we fetch the chosen time from the default time tracker and create an notification.
         1. it's going to be a daily reminder. but only WHEN user hasn't made a review yet.
         2. which means whenever user creates review, we need to remove notification.
         */
        
        switch isAllowed {
        case true:
            // we need to create date components and then set the notification at the date.
            guard let reminderTime = ReminderTime(rawValue:  self.defaultReminderTimeTracker.value) else {assertionFailure();return}

            let targetComponents = Calendar.current.dateComponents([.hour, .minute, .second], from: reminderTime.toSelectedTime)

            let trigger = UNCalendarNotificationTrigger(dateMatching: targetComponents, repeats: true)

            let content = self.createNotification(title: "Reminder".localized, body: "It's time to write a clousre for your day!".localized)

            let request = UNNotificationRequest(identifier: self.defaultReminder, content: content, trigger: trigger)

            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            
        case false:
            self.removeNotification(with: self.defaultReminder)
        }
    }
    
    fileprivate lazy var observeCustomReminderPermission:(Bool)->Void = {[unowned self] isAllowed in
        switch isAllowed {
        case true:
            /**
             ------------------------- LOGIC RUN DOWN ------------------------------
             0. when it's allowed, get a list of custom reminder
             1. conver them to request array
             2. add them to notification center.
             3. use reminder's uuid as identifier so that when user delets a specific reminder, we delete the thing as well.
             */
            guard !self.reminderArray.isEmpty else {return}
            for reminder in self.reminderArray {
                self.addNotification(from: reminder)
            }
            
        case false:
            guard !self.reminderArray.isEmpty else {return}
            self.reminderArray.forEach({self.removeNotification(from: $0)})
        }
    }

    
    public func requestNotificationAuthroization() {
        guard !isAuthorized else {return}
        let options:UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: options) { [unowned self] (granted, err) in
            // TODO: when does it return error?? maybe only simulator??
            guard err == nil else {
                assertionFailure()
                self.authorizationResultPublishSubject.onNext(false)
                return
            }
            self.authorizationResultPublishSubject.onNext(granted)
        }
    }
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //If you don't want to show notification when app is open, do something here else and make a return here.
        //Even you you don't implement this delegate method, you will not see the notification on the specified controller. So, you have to implement this delegate and make sure the below line execute. i.e. completionHandler.
        
        completionHandler([.alert, .badge, .sound]) 
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
        let targetVC = AddReviewViewController(appStatus: appStatus, targetDate: Date(), review: nil)
        UIView.getMostTopViewController()?.presentDeckTransition(targetVC: targetVC, userInfo:nil)
    }
    
    public func checkIfNotificationSettingChangedAndIfSoToggleOff() {
        /**
         since this computed property adjust ui when we get it. so we don't need to worry about it.
         
         since this is a callback, it takes some time... how should we do it?
         */
        checkAndUpdateAuthroizedStatus()
        waitFor(milliseconds: 100, completion: {[unowned self] in
            let currentStatus = self.authorizationStatus == .authorized
            if !currentStatus {
                self.adjustUIWhenNotAuthorized()
            }
        })
    }

    
    // MARK: Fileprivate
    fileprivate weak var appStatus:AppStatus!
    fileprivate weak var application: UIApplication!

    fileprivate var authorizationStatus:UNAuthorizationStatus!
    
    fileprivate let authorizationResultPublishSubject = PublishSubject<Bool>()
    fileprivate let customerReminderSubject = PublishSubject<[Reminder]>()
    
    fileprivate let defaultReminder = "defaultReminder"
    // for custom reminder, we should use UUID for identifer in order to remove them.
//    fileprivate let customIdentifier = "customerReminder"
    
    
    fileprivate func initialSetup() {
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
        
        let group = UserDefaults.retrieveValue(forKey: .customReminder, defaultValue: [[String:Any]]())
        guard group.count != 0 else {return}
        reminderArray = group.compactMap({Reminder(json: $0.toJSON)})
    }
    
    fileprivate func checkAndUpdateAuthroizedStatus() {
        UNUserNotificationCenter.current().getNotificationSettings {[unowned self] (settings) in
            self.authorizationStatus = settings.authorizationStatus
        }
    }
    
    fileprivate func removeNotification(with identifier:String) {
        UNUserNotificationCenter.current()
            .removePendingNotificationRequests(withIdentifiers: [identifier])
    }
    
    fileprivate func adjustUIWhenNotAuthorized() {
        /**
         -------------------------- UI ------------------------------
         */
        waitFor(milliseconds: 150, completion: {
            [self.defaultReminderAllowed, self.customReminderAllowed, self.wordDeletionWarningTracker, self.internetDisconnectionWarningTracker].forEach({$0.value = false})
        })
    }
    
    fileprivate func createNotification(title:String, body:String) -> UNMutableNotificationContent{
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default()
        return content
    }
    
    fileprivate func addNotification(from reminder:Reminder) {
        for date in reminder.toDate {
            let targetComponents = Calendar.current.dateComponents([.weekday, .hour, .minute], from: date)
            let trigger = UNCalendarNotificationTrigger(dateMatching: targetComponents, repeats: true)
            let message = reminder.messageTracker.value
            let content = createNotification(title: "Reminder".localized, body: message.isEmpty ? "It's time to close your day with closure.".localized : message )
            let request = UNNotificationRequest(identifier: reminder.uuid + "\(date.weekday)", content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        }
    }
    
    fileprivate func removeNotification(from reminder:Reminder) {
        UNUserNotificationCenter.current()
            .removePendingNotificationRequests(withIdentifiers:reminder.toDate.map({
                reminder.uuid+"\($0.weekday)"}))

    }
    
    fileprivate func saveToUserDefault() {
        let target = reminderArray.map({$0.toDictionary})
        UserDefaults.store(object: target, forKey: .customReminder)
    }
}











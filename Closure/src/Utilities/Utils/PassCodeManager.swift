//
//  PassCodeManager.swift
//  Closure
//
//  Created by James Kim on 8/18/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation
import KeychainAccess
import LocalAuthentication

enum AuthenticationResult {
    case notAvailable
    case success
    case failure(Error)
}

final class PassCodeManager {
    
    init(appStatus: AppStatus) {
        self.appStatus = appStatus
        /**
         since we save passcode to dictionary with key of user uid, we need to read it WHEN user data is available.
         when it's login or sign up stage, there is no uid so we can't just excecute it in initializer.
         
         I will put the logic in didSetMethod of user in appstatus
         */
//        try? keyChain.remove(Keys.passCode)
        
//        currentPassCode = retrievePassCodeIfExist()
        setupObserver()
        loadSecurityQuestion()
    }
    
    public struct Keys {
        static let passCode = "passCode"
        static let securityQuestionAswer = "securityQuestionAswer"
    }
    
    
    // MARK: Public
    fileprivate(set) var currentPassCode: String?
    fileprivate(set) var currentSecurityQuestionAnswer: String?
    
    fileprivate(set) var currentSecurityQuestion:String?
    
    public var passCodeReuiqredTime: PassCodeRequiredTime {
        return PassCodeRequiredTime(rawValue: passCodeRequiredTimeTracker.value)!
    }
    
    fileprivate var backgroundTask:UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    fileprivate(set) var isPassCodeOnTracker = Tracker.init(forUserDefaultKey: .isPassCodeOn, defaultValue: false)
    
    fileprivate(set) lazy var passCodeRequiredTimeTracker = Tracker.init(forUserDefaultKey: .passCodeRequiredTime, defaultValue: "immediately")
    
    fileprivate(set) lazy var isTouchIDOnTracker = Tracker.init(forUserDefaultKey: .isTouchIDOn, defaultValue: isBioAuthenticationAvailable, additionalDidSet: {[unowned self] (value) in
        guard value else {return}
        guard self.isBioAuthenticationAvailable else {
            // it's not available.
            performOnMain {
                UIView.getMostTopViewController()?.presentDefaultError(message: "TouchID is not available on your device.", okAction: {
                    self.isTouchIDOnTracker.value = false
                })
            }
            return
        }
    })
    
    public func reset() {
        isTouchIDOnTracker.value = false
        isPassCodeOnTracker.value = false
        removeCurrentPassCodeAndSecurityQuestion()
    }
    
    public func fireTimerAndThen(_ block: @escaping ()->Void) {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countTime(timer:)), userInfo: ["block":block], repeats: true)
        registerBackgroundTask()
    }
    
    public func initialLoadPassCodeIfExist() {
        currentPassCode = retrieveIfExist(for: Keys.passCode)
    }
    
    public func retrieveIfExist(for key: String) -> String? {
        do {
            let value = try keyChain.get(key)
            return value
        } catch {
            return nil
        }
    }
    
    public func saveAndRemoveOldPassCodeAndSecurityQuestion(newPassCode: String) {
        removeCurrentPassCodeAndSecurityQuestion()
        keyChain[Keys.passCode] = newPassCode
        currentPassCode = newPassCode
    }
    
    
    public func removeCurrentPassCodeAndSecurityQuestion() {
        // when removing passcode, we need to remove security question and answer
        try? keyChain.remove(Keys.passCode)
        currentPassCode = nil
        
        try? keyChain.remove(Keys.securityQuestionAswer)
        currentSecurityQuestion = nil
        
        UserDefaults.removeValue(forKey: .currentSecurityQuestion)
    }
    
    public func saveSequrityQuestionAndAnswer(question:String, answer:String) {
        UserDefaults.store(object: question, forKey: .currentSecurityQuestion)
        keyChain[Keys.securityQuestionAswer] = answer
        
        currentSecurityQuestion = question
        currentSecurityQuestionAnswer = answer
    }
    
    public func startAuthenticationProcess(result: @escaping (AuthenticationResult)->Void) {
        let context = LAContext()
        var error: NSError?
        
        guard context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            // no biometery
            result(.notAvailable)
            return
        }
        
        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics,
                               localizedReason: "We'd like to allow you to unlock the app".localized,
                               reply: {(success, authenticationError) in
                                performOnMain {
                                    guard authenticationError == nil else {
                                        result(.failure(authenticationError!))
                                        return
                                    }
                                    guard success else {
                                        result(.notAvailable)
                                        return
                                    }
                                    result(.success)
                                }
        })
    }
    
    public var isBioAuthenticationAvailable:Bool {
        var error:NSError?
        return LAContext().canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error)
    }
    
    // MARK: Fileprivate
    fileprivate weak var appStatus: AppStatus!
    fileprivate lazy var keyChain = Keychain(service: appStatus.currentUser.uid)
    
    fileprivate var timer:Timer!
    fileprivate var accumulatedTimeInSecondsAfterAppBecomingInActive = 0
    
    @objc fileprivate func countTime(timer:Timer) {
        let desireTime = passCodeReuiqredTime.seconds
        
        accumulatedTimeInSecondsAfterAppBecomingInActive += 1
        guard accumulatedTimeInSecondsAfterAppBecomingInActive < desireTime else {
            guard let userInfo = timer.userInfo as? [String:()->Void], let targetClosure = userInfo["block"] else {assertionFailure();return}
            print("timer reaches the goal")
            accumulatedTimeInSecondsAfterAppBecomingInActive = 0
            timer.invalidate()
            endBackgroundTask()
            performOnMain {
                targetClosure()
            }
            
            return
        }
        print("time: ",accumulatedTimeInSecondsAfterAppBecomingInActive)
    }
    
    fileprivate func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {[weak self] in
            self?.endBackgroundTask()
        })
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    fileprivate func endBackgroundTask() {
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }
    
    fileprivate func setupObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(observeAppDidBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }
   
    @objc fileprivate func observeAppDidBecomeActive() {
        accumulatedTimeInSecondsAfterAppBecomingInActive = 0
        endBackgroundTask()
        guard timer != nil else {return}
        timer.invalidate()
        timer = nil
    }
    
    fileprivate func loadSecurityQuestion() {
        currentSecurityQuestionAnswer = retrieveIfExist(for: Keys.securityQuestionAswer)
        
        guard currentSecurityQuestionAnswer != nil else {
//            // remove the question from user default since there is no answer
            UserDefaults.removeValue(forKey: .currentSecurityQuestion)
            return
        }
        
        currentSecurityQuestion = UserDefaults.retrieveValue(forKey: .currentSecurityQuestion, defaultValue: "Question unavilable.".localized)
    }
}






//
//  DefaultTableViewDataSource.swift
//  Connect
//
//  Created by montapinunt Pimonta on 5/26/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

class DefaultTableViewDataSource<A:ReusableTableViewCell>: NSObject, UITableViewDataSource {
    
    typealias Object = A.Object
    typealias Cell = A
    typealias Section = Int
    
    init(tableView: UITableView, parentViewController: UIViewController, initialData: [Section:[Object]]? = nil, userInfo: [String:Any]? = nil, observableCell: ((A)->())? = nil) {
        self.tableView = tableView
        self.parentViewController = parentViewController
        self.mainUserInfo = userInfo
        self.observeMainCell = observableCell
        super.init()
        tableView.dataSource = self
        tableView.register(Cell.self, forCellReuseIdentifier: Cell.reuseIdentifier)
        objectDictionary = initialData.unwrapOr(defaultValue: [Int:[Object]]())
        tableView.reloadData()
    }
    
    public var currentData: [Section:[Object]] {
        return objectDictionary
    }
    
    public func update(data: [Section:[Object]]) {
        objectDictionary = data
        
        tableView.reloadData()
    }
    
    public func update(data:[Section:[Object]], atSection section: Section) {
        validate(section: section)
        objectDictionary[section]! = data[section]!
        let indexSet = IndexSet.init(integer: section)
        tableView.reloadSections(indexSet, with: .automatic)
    }
    
    public func update(additionalData:[Any], atSection section:Section) {
        validate(section: section)
        additionalCellDataDictionary[section]! = additionalData
        tableView.reloadSections(section.toIndexSet, with: .automatic)
    }
    
    public func append(data:[Object], atSection section: Section) {
        validate(section: section)
        objectDictionary[section]!.append(contentsOf: data)
        let indexSet = IndexSet.init(integer: section)
        tableView.reloadSections(indexSet, with: .automatic)
    }
    
    public func object(atIndexPath indexPath: IndexPath) -> Object {
        validate(indexPath: indexPath)
        return objectDictionary[indexPath.section]![indexPath.row]
    }
    
    public func additionalData(atIndexPath indexPath: IndexPath) -> Any {
        validate(indexPath: indexPath)
        return additionalCellDataDictionary[indexPath.section]![indexPath.row]
    }
    
    public func cellForRow(at indexPath: IndexPath) -> Cell {
        guard let result = tableView.cellForRow(at: indexPath) as? Cell else {assertionFailure();return Cell()}
        return result
    }
    
    public func addNewCellType<T:AdditionalReusableTableViewCell>(type:T.Type, atSection section:Section, cellData:[Any] = [], userInfo: [String:Any]? = nil, observeOtherCell: ((AdditionalReusableTableViewCell)->Void)? = nil) {
        // sections have to exist in chronological order.
        additionalCellDataDictionary[section] = cellData
        objectDictionary.removeValue(forKey: section)
        
        additionalUserInfos[section] = userInfo
        additionalCellObservers[section] = observeOtherCell
        tableView.register(T.self, forCellReuseIdentifier: T.className)
        if cellData.count != 0 {
            tableView.reloadData()
        }
    }
    
    
    // DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return objectDictionary.keys.count + +additionalCellDataDictionary.keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Section) -> Int {
        validate(section: section)
        return (objectDictionary[section]?.count ?? 0) + (additionalCellDataDictionary[section]?.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let checkIfAdditionalDataExist = additionalCellDataDictionary[indexPath.section]
        
        guard checkIfAdditionalDataExist == nil else {
            let cell = tableView.dequeueReusableCell(withIdentifier: CustomReminderCell.className, for: indexPath) as! AdditionalReusableTableViewCell
            cell.setup(withObject: fetchAdditionalData(atIndexPath: indexPath), parentViewController: parentViewController, currentIndexPath: indexPath, userInfo: fetchAdditioanlUserInfo(forIndexPath: indexPath))
            fetchAdditionalCellObserver(for: indexPath)?(cell)
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.reuseIdentifier, for: indexPath) as! Cell
        cell.setup(withObject: object(atIndexPath: indexPath), parentViewController: parentViewController, currentIndexPath: indexPath, userInfo: mainUserInfo)
        observeMainCell?(cell)

        return cell
    }

    

    
    
    // MARK: - Fileprivate
    
    fileprivate weak var parentViewController: UIViewController!
    fileprivate let tableView: UITableView
    fileprivate var objectDictionary = [Section:[Object]]()
    fileprivate let observeMainCell: ((A)->())?
    fileprivate let mainUserInfo: [String:Any]?
    
    
    fileprivate var additionalCellDataDictionary: [Section:[Any]] = [:]
    fileprivate var additionalCellObservers: [Section: ((AdditionalReusableTableViewCell)->Void)?] = [:]
    fileprivate var additionalUserInfos: [Section:[String:Any]?] = [:]

    
    fileprivate func validate(indexPath: IndexPath) {
        validate(section: indexPath.section, row: indexPath.row)
    }
    
    fileprivate func validate(section: Int, row: Int? = nil) {
        guard objectDictionary.keys.contains(section) || additionalCellDataDictionary.keys.contains(section) else {assertionFailure();return}
    }
    
    fileprivate func fetchAdditionalData(atIndexPath indexPath:IndexPath)-> Any {
        guard let targetArray = additionalCellDataDictionary[indexPath.section] else {assertionFailure();return "WHAT"}
        return targetArray[indexPath.row]
    }
    
    fileprivate func fetchAdditioanlUserInfo(forIndexPath indexPath:IndexPath)-> [String:Any]? {
        guard let target = additionalUserInfos[indexPath.section] else {return nil}
        return target
    }
    
    fileprivate func fetchAdditionalCellObserver(for indexPath: IndexPath) -> ((AdditionalReusableTableViewCell)->Void)? {
        guard let target = additionalCellObservers[indexPath.section] else {return nil}
        return target
    }
}






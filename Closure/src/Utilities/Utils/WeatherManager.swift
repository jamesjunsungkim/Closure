//
//  WeatherManager.swift
//  Closure
//
//  Created by James Kim on 9/13/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CoreLocation
import RxSwift
import Alamofire
import PKHUD

public enum LocationServiceStatus {
    case phoneLocationServiceDisabled
    case authorizedWhenInUse
    case denied
    case notDetermined
    case irrelevant
}

final class WeatherManager: NSObject, CLLocationManagerDelegate {
    
    override init() {
        /**
         ------------------------- LOGIC RUN DOWN ------------------------------
         0. call checkIfWeatherServiceAvailable and depending on status, take a proper action.
         1. if denied or not determined, ask for a permission. If It's already authorzied, we need to manually start updating location.
         2. when user determins their action, it calls didChangeAuthorization
         3. if they allow the authorization, we start updating location
         4. And then it calls did Update location, we get the weather information from url.
         5. we send it out through observable.
         */
        super.init()
        initialSetup()
    }
    
    // MARK: Public
    
    public var weatherFetchObservable: Observable<CompletionResult<NonCDWeather?,Error>> {
        return weatherFetchResultSubject.asObservable()
    }
    
    public func checkIfWeatherServiceAvailable() -> LocationServiceStatus {
        guard CLLocationManager.locationServicesEnabled() else {
            currentStatus = .phoneLocationServiceDisabled
            return .phoneLocationServiceDisabled
        }
        
        switch CLLocationManager.authorizationStatus() {
            
        case .authorizedWhenInUse:
            currentStatus = .authorizedWhenInUse
            return .authorizedWhenInUse
            
        case .denied:
            currentStatus = .denied
            return .denied
            
        case .notDetermined:
            currentStatus = .notDetermined
            return .notDetermined
            
        default:
            currentStatus = .irrelevant
            return .irrelevant
        }
    }
    
    public func askForLocationPermission() {
        AppStatus.shouldAllowLockScreen = false
        locationManager.requestWhenInUseAuthorization()
    }
    
    public func startUpdatingLocation() {
        locationManager.startUpdatingLocation()
        if !needsToGetWeather {
            needsToGetWeather = true
        }
    }

    // callback when asking permission
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        /**
         0. why i need shouldAllowLockScreen boolean
         when users are prompted with permssion questions, the app becomes inactive. if passcode toggle is on, it opens up the blurry vc. let's prevent that with this boolean.
         
         1. this method gets called very first time when checking user authorization status. we need to ignore it very first time. otherwise, it allows the passcode vc to pop up during signup process.
         */

        
        guard !needToIgnoreVeryFirstTime else {needToIgnoreVeryFirstTime = false;return}
        AppStatus.shouldAllowLockScreen = true
        
        switch status {
        case .authorizedWhenInUse:
            currentStatus = .authorizedWhenInUse
            startUpdatingLocation()
        case .denied:
            currentStatus = .denied
        default: break
        }
    }
    
    // callback when starting updating locations
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // based on description, horizontal accuracy must be above 0 otherwise the data is invalid
        guard let location = locations.last, location.horizontalAccuracy > 0, needsToGetWeather, !isCurrentlyFetchingWeather else {return}
        /**
         we have to toggle this bollean otherwise it will get triggered twice.
         */
        
        locationManager.stopUpdatingLocation()
        needsToGetWeather = false
        isCurrentlyFetchingWeather = true
        
        let latitude = String(location.coordinate.latitude)
        let longitude = String(location.coordinate.longitude)
        
        let params : [String:String] = ["lat" : latitude, "lon" : longitude, "appid" :appID]
        performOnBackground {[unowned self] in
            Alamofire.request(self.weatherURL, method: .get, parameters: params)
                .validate()
                .responseJSON { (response) in
                    logInfo("Hello, I fetched weather data.",type: .network)
                    self.isCurrentlyFetchingWeather = false
                    switch response.result { 
                    case .success(let v):
                        guard let json = v as? [String:Any] else {return}
                        let weather = NonCDWeather(json: json.toJSON)
                        self.weatherFetchResultSubject.onNext(.success(weather))
                    case .failure(let e):
                        self.weatherFetchResultSubject.onNext(.failure(e))
                    }
            }
        }
        
    }
    
    
    // MARK: Fileprivate
//    fileprivate weak var appStatus:AppStatus!
    
    fileprivate let locationManager = CLLocationManager()
    
    fileprivate weak var viewControllerToPresentProgressIndicator: UIViewController!
    
    fileprivate(set) var currentStatus: LocationServiceStatus!
    
    fileprivate let weatherFetchResultSubject = PublishSubject<CompletionResult<NonCDWeather?,Error>>()
    
    fileprivate var needsToGetWeather = true
    fileprivate(set) var isCurrentlyFetchingWeather = false
    
    fileprivate let weatherURL = "http://api.openweathermap.org/data/2.5/weather"
    fileprivate let appID = "a7a315b7ed73658c224c17960c338e27"
    
    fileprivate var needToIgnoreVeryFirstTime = true
    
    fileprivate func initialSetup() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
    }
}

//
//  SectionInfo.swift
//  Closure
//
//  Created by James Kim on 8/13/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

struct SectionInfo {
    let section: Int
    let description: String
    
    let header: UIView?
    let footer: UIView?
    
}

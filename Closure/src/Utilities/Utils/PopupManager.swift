////
////  PopupManager.swift
////  Closure
////
////  Created by montapinunt Pimonta on 8/12/18.
////  Copyright © 2018 James Kim. All rights reserved.
////
//
//import UIKit
//
//final class CheckBoxPopupManager {
//
//    init(appStatus:AppStatus) {
//        self.appStatus = appStatus
//        loadFlagsFromUserdefaultsAndUpdateProperties()
//    }
//
//    //MARK: Public
//    public func showWordDeletionWarningIfNeeded(onVC vc: UIViewController, actionBlock:(()->Void)? = nil) {
//        guard wordDeletionTracker.value else {
//            actionBlock?()
//            return
//        }
//        let checkPopupViewController = CheckPopupViewController(body: "It will add this word to the filter list.\nThis word will be filtered out.")
//        vc.presentPopup(targetVC: checkPopupViewController, cancelButtonTitle: "Cancel", cancelAction: nil, okButtonTitle: "Got it", okAction: {[unowned self] in
//            if checkPopupViewController.isChecked  {
//                self.changeFlagForWordDeletionAndSaveIt(needsToShow: false)
//            }
//            actionBlock?()
//
//        }, completion: nil, panGestureDismissal: false)
//    }
//
//    public func showInternetDisconnectionIfNeeded(onVC vc: UIViewController, actionBlock: (()->Void)? = nil) {
//        guard internectDisconnectionTracker.value else {
//            actionBlock?()
//            return
//        }
//        let checkPopupViewController = CheckPopupViewController(body: "Internet disconnected.\nYou won't be able to save it to server but to local storage.")
//
//        vc.presentPopup(targetVC: checkPopupViewController, cancelButtonTitle: "Cancel", cancelAction: nil, okButtonTitle: "Got it", okAction: {[unowned self] in
//            if checkPopupViewController.isChecked {
//                self.changeFlagForInternetDisconnectionAndSaveIt(needsToShow: false)
//            }
//            actionBlock?()
//        }, completion: nil, panGestureDismissal: false)
//    }
//
//    //MARK: Fileprivate
//    fileprivate weak var appStatus: AppStatus!
//
//    fileprivate weak var wordDeletionTracker: Tracker<Bool>!
//    fileprivate weak var internectDisconnectionTracker: Tracker<Bool>!
//
//    fileprivate func loadFlagsFromUserdefaultsAndUpdateProperties() {
//        wordDeletionTracker = appStatus.wordDeletionWarningTracker
//        internectDisconnectionTracker = appStatus.internetDisconnectionWarningTracker
//       wordDeletionTracker.value =  UserDefaults.retrieveValue(forKey: .flagForWordDeletionWarning, defaultValue: true)
//        internectDisconnectionTracker.value =  UserDefaults.retrieveValue(forKey: .flagForInternetDisconnection, defaultValue: true)
//    }
//
//    fileprivate func changeFlagForWordDeletionAndSaveIt(needsToShow: Bool) {
//        wordDeletionTracker.value = needsToShow
//        UserDefaults.store(object: needsToShow, forKey: .flagForWordDeletionWarning )
//    }
//    
//    fileprivate func changeFlagForInternetDisconnectionAndSaveIt(needsToShow: Bool) {
//        internectDisconnectionTracker.value = needsToShow
//        UserDefaults.store(object: needsToShow, forKey: .flagForInternetDisconnection )
//    }
//
//}

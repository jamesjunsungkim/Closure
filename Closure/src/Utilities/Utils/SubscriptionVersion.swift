//
//  SubscriptionVersion.swift
//  Closure
//
//  Created by James Kim on 8/13/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation

public enum SubscriptionVersion {
    case free
    case paid
    
    public var description: String {
        switch self {
        case .free: return "Free".localized
        case .paid: return "Premium".localized
        }
    }
    
    public var allowedImageSize: Double {
        switch self {
        case .free: return 0.1
        case .paid : return 0.3
        }
    }
}

//
//  Log.swift
//  Connect
//
//  Created by James Kim on 5/7/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import DateToolsSwift

// sometimes view controller log makes it look messed up.

private(set) var viewControllerMemoryDictionary: [String:String] = [:]
private(set) var referenceMemeoryDictionary : [String:[String:String?]] = [:]

enum LogEvent: String {
    case error = "ERROR:"
    case info = "INFO:"
    case debug = "DEBUG:"
    case verbose = "VERBOSE:"
    case warning = "WARNING:"
    case severe = "SEVERE:"
}

public enum LogType {
    case viewController
    case observation
    case action
    case view
    case objectInstance
    case network
    case coreData
    
    public var shouldShow:Bool {
        switch self {
        case .viewController: return AppStatus.showViewControllerLog
        case .observation : return AppStatus.showObservationLog
        case .action: return AppStatus.showActionLog
        case .view: return AppStatus.showViewInstanceLog
        case .objectInstance : return AppStatus.showObjectInstancesLog
        case .network : return AppStatus.showNetworkJSONLog
        case .coreData: return AppStatus.showCoredataJSONLog
        }
    }
}

public func enterViewControllerMemoryLog(_ VC: UIViewController) {
    #if targetEnvironment(simulator)
    let newEntry = getNameAndMemoryAddress(VC)
    viewControllerMemoryDictionary[newEntry.name] = newEntry.address
    if AppStatus.showViewControllerLog {
        print("New entry in Memery: \n\(newEntry)")
    }
    #endif
}

public func leaveViewControllerMomeryLog(_ VC: UIViewController) {
    #if targetEnvironment(simulator)
    let deletedEntry = getNameAndMemoryAddress(VC)
    viewControllerMemoryDictionary.removeValue(forKey: deletedEntry.name)
    if AppStatus.showViewControllerLog {
        print("Instance removed from memory: \n\(deletedEntry)")
    }
    #endif
}

public func checkIfViewControllerDeallocated(_ VC: UIViewController, shouldCrash:Bool = true) {
    #if targetEnvironment(simulator)
    let targetEntry = getNameAndMemoryAddress(VC)
    /**
     it should wait because this function has reference to the target vc. of course, it's not deallocated right away..
     */
    waitFor(milliseconds: 200, completion: {
        guard !viewControllerMemoryDictionary.keys.contains(targetEntry.name) else {
            switch shouldCrash {
            case true:
                assertionFailure()
            case false:
                print("\n\n\n\n\nname: \(targetEntry.name), address: \(targetEntry.address) is not deallocated!!")
            }
            return
        }
    })
    #endif
}

public func enterReferenceDictionary(forType type: AnyClass, withUID uid: String?) {
    let newEntry = nameFor(type: type)
    let identifier = ObjectIdentifier.init(type).debugDescription
    var dict: [String:String?]!
    
    if referenceMemeoryDictionary.checkIfValueExists(forKey: newEntry) {
        dict = referenceMemeoryDictionary[newEntry]!
        dict[identifier] = uid
    } else {
        dict = [identifier:uid]
    }
    referenceMemeoryDictionary[newEntry] = dict
}

public func leaveReferenceDictionary(forType type: AnyClass) {
    let deletedEntry = nameFor(type: type)
    let identifier = ObjectIdentifier.init(type).debugDescription
    if referenceMemeoryDictionary[deletedEntry] != nil {
        referenceMemeoryDictionary[deletedEntry]!.removeValue(forKey: identifier)
        if referenceMemeoryDictionary[deletedEntry]!.count == 0 {
            referenceMemeoryDictionary.removeValue(forKey: deletedEntry)
        }
    }
}

private func nameFor(type: AnyClass)-> String {
    return NSStringFromClass(type).components(separatedBy: ".")[1]
}

fileprivate func getNameAndMemoryAddress(_ VC: UIViewController) -> (name:String,address:String) {
    let r = String(describing: VC)
        .trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
        .split(separator: ".")[1]
        .split(separator: ":")
        .map({String($0)})
    
    return (r[0], r[1])
}

private func reduce(array: [String])->String {
    let sum = array.reduce("") {$0+"\n\($1)"}
    return sum
}

func logDebug(_ message: String?, fileName: String = #file, line: Int = #line, column: Int = #column, funcName: String = #function) {
    capturingLog(message, event: LogEvent.debug, fileName: fileName, line: line, column: column, funcName: funcName)
}

public func _logInfo_NEEDTOCHANGE(_ message: String? = nil, fileName: String = #file, line: Int = #line, column: Int = #column, funcName: String = #function) {
    capturingLog(message, event: LogEvent.info, fileName: fileName, line: line, column: column, funcName: funcName)
}

public func logInfo(_ message: String? = nil, type:LogType, fileName: String = #file, line: Int = #line, column: Int = #column, funcName: String = #function) {
    guard type.shouldShow else {return}
    capturingLog(message, event: LogEvent.info, fileName: fileName, line: line, column: column, funcName: funcName)
}

public func logJson(_ dict: [String:Any], type:LogType , fileName: String = #file, line: Int = #line, column: Int = #column, funcName: String = #function) {
    guard type.shouldShow else {return}
    capturingLog(dict.toPrettyJson, event: LogEvent.info, fileName: fileName, line: line, column: column, funcName: funcName)
}

func logError(_ message: String? = nil, fileName: String = #file, line: Int = #line, column: Int = #column, funcName: String = #function) {
    capturingLog(message, event: LogEvent.error, fileName: fileName, line: line, column: column, funcName: funcName)
}

func logVerbose(_ message: String?, fileName: String = #file, line: Int = #line, column: Int = #column, funcName: String = #function) {
    capturingLog(message, event: LogEvent.verbose, fileName: fileName, line: line, column: column, funcName: funcName)
}
func logWarning(_ message: String? = nil, fileName: String = #file, line: Int = #line, column: Int = #column, funcName: String = #function) {
    capturingLog(message, event: LogEvent.warning, fileName: fileName, line: line, column: column, funcName: funcName)
}
func logSevere(_ message: String?, fileName: String = #file, line: Int = #line, column: Int = #column, funcName: String = #function) {
    capturingLog(message, event: LogEvent.severe, fileName: fileName, line: line, column: column, funcName: funcName)
}

fileprivate func capturingLog(_ message: String?, event: LogEvent, fileName: String, line: Int, column: Int, funcName: String) {
//    let time = Date().format(with: "hh:mm:ss")
    let fileName = sourceFileName(fileName)
    
    if message != nil {
//        print("\(time) \(event.rawValue) \(fileName).\(funcName): \(line) - \(message!)")
        print("\(fileName).\(funcName): \(line) - \(message!)")
    } else {
//        print("\(time) \(event.rawValue) \(fileName).\(funcName): \(line)")
        print("\(fileName).\(funcName): \(line)")
    }
    //    let localMsg = "\(date)\n\(mainMessage)"
    //    #if TARGET_OS_SIMULATOR
    //    #else
    //    #endif
}

fileprivate func sourceFileName(_ filePath: String) -> String {
    let components = filePath.components(separatedBy: "/")
    let lastComponent = components.last.unwrapOrBlank()
    return lastComponent.components(separatedBy: ".").first.unwrapOrBlank()
}


//
//  Tracker.swift
//  Closure
//
//  Created by James Kim on 7/28/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation
import RxSwift


final class Tracker<A>: Unwrappable {
    
    init(initialValue:A, didSetBlock:((A)->Void)? = nil) {
        self.value = initialValue
        self.didSetBlock = didSetBlock
        isLoaded = true
    }
    
    init(forUserDefaultKey key: UserDefaults.Key, defaultValue:A, additionalDidSet:((A)->Void)? = nil, blockForInitalLoading:((A)->Void)? = nil) {
        self.value = UserDefaults.retrieveValue(forKey: key, defaultValue: defaultValue)
        self.didSetBlock = { (_value) in
            UserDefaults.store(object: _value, forKey: key)
            additionalDidSet?(_value)
        }
        blockForInitalLoading?(value)
        isLoaded = true
    }

    deinit {
        //TODO: Need to create an way to monitor all instances.
    }
    
    // MARK: Public
    public var value:A {
        didSet {
            guard isLoaded else {return}
            valueSubject.onNext(value)
            didSetBlock?(value)
        }
    }
    
    public var valueObservable: Observable<A> {
        return valueSubject.asObservable()
    }
    
    
    // MARK: Fileprivate
    fileprivate var isLoaded = false
    fileprivate let didSetBlock: ((A)->Void)?
    fileprivate let valueSubject = PublishSubject<A>()
}











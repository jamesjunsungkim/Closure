//
//  EmailManager.swift
//  Closure
//
//  Created by montapinunt Pimonta on 8/24/18.
//  Copyright © 2018 James Kim. All rights reserved.


import Alamofire

public enum EmailError {
    case overLimit
    case alamofireError(Error)
}

final class EmailManager {
    
    public func sendEmail(isBugReport: Bool,fromEmail:String, subject: String, body:String, result:@escaping (CompletionWithoutData<EmailError>)->Void) {
        
        guard checkLastDayToSendEmailAndCompare() else {
            result(.failure(.overLimit))
            return
        }

        let url = "https://api.sendgrid.com/v3/mail/send"
        let developerEmail = "jamesjunsungkim@gmail.com"
        
        // personalization has to be array of dictionariess
        let parameter : [String:Any] = [
            "personalizations": [
                ["to": [["email": developerEmail ]],
                 "subject": subject]
            ],
            "from":["email":fromEmail],
            "content": [
                ["type":"text/plain", "value":body]
            ]
        ]
        
        Alamofire.request(url, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: header)
            .validate()
            .response { (response) in
                if let e = response.error {
                    result(.failure(.alamofireError(e)))
                    return
                }
                result(.success)
        }
    }
    
    // MARK: Fileprivate
    
    fileprivate let header = [
        "Authorization": "Bearer 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",
        "Content-Type": "application/json"]
    
    
    fileprivate func checkLastDayToSendEmailAndCompare()->Bool {
        let lastTimeSentEmail = UserDefaults.retrieveValue(forKey: .lastDayToSendEmail, defaultValue: "2018-08-10")
        let currentDate = Date().toYearToDateString(type:.current)
        guard currentDate != lastTimeSentEmail else {return false}
        UserDefaults.store(object: currentDate, forKey: .lastDayToSendEmail)
        return true
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

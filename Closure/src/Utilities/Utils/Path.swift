//
//  FireDatabaseReference.swift
//  Connect
//
//  Created by James Kim on 5/7/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation
import Firebase
import FirebaseStorage



public enum Path {
    case toUser(withUID:String)
    case toReviews(withUID:String)
    case toReview(withUID: String, date: String)
    case toFacebookUser(withEmail:String)
    case toReleaseNote(withVersion:String)
    
    var stringValue: String {
        switch self {
        case .toUser(let uid): return "User/Email/\(uid)"
        case .toReviews(withUID: let uid): return "Review/\(uid)"
        case .toReview(let uid, let dateStringValue): return "Review/\(uid)/\(dateStringValue)"
        case .toFacebookUser(withEmail: let email): return "User/FacebookUser/\(email)"
        case .toReleaseNote(withVersion: let version):
            return "ReleaseNote/\(version)"
//        default: fatalError()
        }
    }


}

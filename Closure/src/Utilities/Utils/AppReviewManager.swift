//
//  ReviewManager.swift
//  Closure
//
//  Created by James Kim on 8/28/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation
import StoreKit

final class AppReviewManager {
    
    init() {
        loadOpenCountAndIncrementIt()
        assertCount(limit: 1)
    }
    
    // MARK: Public
    public func checkAndAskReview() {
        switch currentCount {
        case 10:
            requestReview()
        case _ where currentCount % 30 == 0 :
            requestReview()
        default:break
        }
    }
    
    // MARK: Fileprivate
    fileprivate var currentCount: Int!
    
    fileprivate func loadOpenCountAndIncrementIt() {
        let previousCount = UserDefaults.retrieveValue(forKey: .appOpenCount, defaultValue: 1)
        currentCount = previousCount + 1
        UserDefaults.store(object: currentCount, forKey: .appOpenCount)
    }
    
    fileprivate func requestReview() {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        }
    }
}

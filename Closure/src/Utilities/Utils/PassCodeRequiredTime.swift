//
//  PassCodeRequiredTime.swift
//  Closure
//
//  Created by James Kim on 8/18/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation

public enum PassCodeRequiredTime:String {
    case immediately
    case after1Min
    case after3Min
    case after5Min
    case after10Min
    
    public var description:String {
        switch self {
        case .immediately : return "Immediately".localized
        case .after1Min: return "After 1 minute".localized
        case .after3Min: return "After 3 minutes".localized
        case .after5Min: return "After 5 minutes".localized
        case .after10Min: return "After 10 minutes".localized
        }
    }
    
    public var index:Int {
        switch self {
        case .immediately : return 0
        case .after1Min: return 1
        case .after3Min: return 2
        case .after5Min: return 3
        case .after10Min: return 4
        }
    }
    
    public var seconds: Int {
        switch self {
        case .immediately : return 0
        case .after1Min: return 60
        case .after3Min: return 180
        case .after5Min: return 300
        case .after10Min: return 600
        }
    }
}

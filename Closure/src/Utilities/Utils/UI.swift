//
//  UI.swift
//  Connect
//
//  Created by James Kim on 6/9/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

struct UI {
    
    static let viewBackgroundColor = UIColor.white
    
    
    struct Component {
        static let headerSize = 20.toCGFloat
        static let titleColor = UIColor.black
        static let titleSize = 18.toCGFloat
        static let smallTitleSize = 15.toCGFloat
        static let contentColor = UIColor.black
        static let contentSize = 15.toCGFloat
        static let backgroundColor = UIColor.white
        static let borderColor = UIColor.mainBlue
    }
    
    struct Button {
        static let backgroundColor = UIColor.mainBlue
        static let fontSize = 20.toCGFloat
        static let titleColor = UIColor.white
    }
    
    struct ScrollView {
        static let padding: CGFloat = 16
    }
    
    // MARK: Public
    public static func configure(withViewController VC: UIViewController) {
        VC.view.backgroundColor = UI.viewBackgroundColor
    }
    
    public static func configure(withView v: UIView) {
        v.backgroundColor = UI.Component.backgroundColor
        v.layer.borderColor = UI.Component.borderColor.cgColor
    }
    
}



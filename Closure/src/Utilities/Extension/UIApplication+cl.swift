//
//  UIApplication+cl.swift
//  Closure
//
//  Created by James Kim on 8/25/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

extension UIApplication {
    
    class var appVersion: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    
    class var appBuild: String {
        return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    }
    
    class func versionBuild() -> String {
        let version = appVersion, build = appBuild
        
        return version == build ? "v\(version)" : "v\(version)(\(build))"
    }
}

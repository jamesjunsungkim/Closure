//
//  UITableView+cn.swift
//  Connect
//
//  Created by montapinunt Pimonta on 5/25/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

extension UITableView {
    
    static func create(style: UITableViewStyle = .plain, backgroundColor: UIColor = .white) -> UITableView {
        let t = UITableView(frame: .zero, style: style)
        t.backgroundColor = backgroundColor
        t.alwaysBounceVertical = true
        return t
    }
    
    // MARK: - Fileprivate
}

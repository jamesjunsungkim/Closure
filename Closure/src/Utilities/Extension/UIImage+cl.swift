//
//  UIImage.swift
//  Connect
//
//  Created by montapinunt Pimonta on 5/7/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

extension UIImage {
    
    public enum Name: String {
        case personPlaceHolder = "placeholder_person"
        case noImage = "no_image_found"
        case cancel = "cancel_button"
        case check = "check"
        case checkedBox = "checked_box"
        case uncheckedBox = "unchecked_box"
        case plusBlueCircle = "circle_plus_blue"
        case minusButton = "minus_button"
        case deleteButton = "delete_button"
        case resetButton = "reset_button"
        case emptyBox = "empty_box"
        case questionMark = "question_mark"
    }
    
    public enum ResolutionKey:String {
        case fullResolution
        case profileResolution
        case defaultResolution
        
        var resolution: CGFloat {
            switch self {
            case .fullResolution: return 1.0
            case .profileResolution: return 0.1
            case .defaultResolution:
                return UserDefaults.retrieveValue(forKey: .defaultResolution, defaultValue: 0.2)
            }
        }
    }
    
    public var jpegData: Data {
        guard let result = UIImageJPEGRepresentation(self, 1) else {fatalError("Cannot convert it into data")}
        return result
    }
    
    public func jpegData(forKey key: ResolutionKey) -> Data {
        guard let result = UIImageJPEGRepresentation(self, key.resolution) else {fatalError("Cannot convert it into data")}
        return result
    }
    
    
    public var pngData: Data? {
        return UIImagePNGRepresentation(self)
    }
    

    public func resizeImage(width: CGFloat, height: CGFloat) -> UIImage {
        let size = CGSize(width: width, height: height)
        let rect = CGRect(origin: CGPoint.zero, size: size)

        UIGraphicsBeginImageContext(size)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }

    public func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    public func resize(toMB size: Double) -> UIImage? {
        guard let imageData = UIImagePNGRepresentation(self) else { return nil }
        
        var resizingImage = self
        var imageSizeKB = Double(imageData.count) / 1000.0
        
        while imageSizeKB > size*1024 {
            guard let resizedImage = resizingImage.resized(withPercentage: 0.9),
                let imageData = UIImagePNGRepresentation(resizedImage)
                else { return nil }
            
            resizingImage = resizedImage
            imageSizeKB = Double(imageData.count) / 1000.0
        }
        
        return resizingImage
    }
    
    func withAlpha(_ alpha: CGFloat) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: .zero, blendMode: .normal, alpha: alpha)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    
    // MARK: - Static
    
    static func create(forKey key: Name) -> UIImage {
        return UIImage(named: key.rawValue)!
    }
    
    static func create(for name:String) -> UIImage {
        guard let target = UIImage(named: name) else {assertionFailure(); return UIImage()}
        return target
    }
    
    static func image(withColor color: UIColor) -> UIImage {
        
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        
        let context = UIGraphicsGetCurrentContext()
        color.setFill()
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    
    
}

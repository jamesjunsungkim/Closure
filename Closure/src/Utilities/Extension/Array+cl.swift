//
//  Array.swift
//  Connect
//
//  Created by James Kim on 5/6/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation

extension Array {
    
    // MARK: - Public
    public mutating func replace(element: Iterator.Element, atIndex index: Int) {
        assert(index < self.count)
        remove(at: index)
        insert(element, at: index)
    }
    
    public mutating func removeItem(condition:(Iterator.Element)->Bool) {
        var result = [Element]()
        for item in self where !condition(item) {
            result.append(item)
        }
        self = result
    }
    
    public var decomposed:(Iterator.Element, [Iterator.Element])? {
        guard let x = first else {return nil}
        return (x, Array(self[1..<count]))
    }
    
    public func sliced(size: Int) -> [[Iterator.Element]] {
        var result = [[Iterator.Element]]()
        for idx in stride(from: startIndex, to: endIndex, by: size) {
            let end = Swift.min(idx+size, endIndex)
            result.append(Array(self[idx..<end]))
        }
        return result
    }
    
    public func removeElement(condition: (Iterator.Element)->Bool) -> [Element]{
        var result = [Element]()
        for x in self where !condition(x) {
            result.append(x)
        }
        return result
    }
    
    public func split(size:Int)-> (left:[Element], right:[Element]) {
        return (self.prefix(upTo: size).toArray, self.suffix(from: size).toArray)
    }
    
    public func convertToDictionaryWithIndex(configureValue: (Element)->Any) -> [Int:Any] {
        var result = [Int:Any]()
        var currentIndex = 0
        
        for el in self {
            result[currentIndex] = configureValue(el)
            currentIndex += 1
        }
        
        return result
    }
    
    public func convertToDictionary(configureKey:(Element)->AnyHashable) -> [AnyHashable:Element] {
        var result = [AnyHashable:Element]()
        self.forEach({result[configureKey($0)] = $0})
        
        return result
    }
    
    public func splitAndTakeDifferentAction(condition:(Element)->Bool, trueAction:(Element)->Void, falseAction:(Element)->Void) {
        var trueArray = [Element]()
        var falseArray = [Element]()
        
        for x in self {
            condition(x) ? trueArray.append(x) : falseArray.append(x)
        }
        
        trueArray.forEach(trueAction)
        falseArray.forEach(falseAction)
    }
    
    public func categorizeIntoTwoArrays(condition:(Element)->Bool) ->(satisfied:[Element],unsatisfied:[Element]) {
        var satisfiedArray = [Element]()
        var unsatisfiedArray = [Element]()
        
        for x in self {
            condition(x) ? satisfiedArray.append(x) : unsatisfiedArray.append(x)
        }
        
        return (satisfiedArray,unsatisfiedArray)
    }
    
    public func checkIfNotEmptyOrPresent(defaultArray:[Element]) ->[Element] {
        guard !self.isEmpty else {return defaultArray}
        return self
    }
    
    public func addElementIfNotEmpty(_ el:Element?) -> [Element] {
        guard let element = el else {return self}
        var result = self
        result.append(element)
        return result
    }
}

extension Array where Iterator.Element: Equatable {
    public mutating func replace(element: Element) {
        guard let targetIndex = self.index(of: element) else {assertionFailure(); return}
        remove(at: targetIndex)
        insert(element, at: targetIndex)
    }
    
    public func contains(list:[Element]) ->Bool {
        for el in list where !self.contains(el) {
            return false
        }
        return true
    }
}

extension ArraySlice {
    var toArray: Array<Element> {
        return Array(self)
    }
}

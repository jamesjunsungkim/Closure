//
//  Boolean+cl.swift
//  Closure
//
//  Created by montapinunt Pimonta on 9/3/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

extension Bool {
    public mutating func toggle(shouldBeTrue:Bool = false) {
        guard !shouldBeTrue else {
            self = true
            return
        }
        self = !self
    }
}

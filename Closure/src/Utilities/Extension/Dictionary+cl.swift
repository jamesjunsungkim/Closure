//
//  Dictionary.swift
//  Connect
//
//  Created by James Kim on 5/8/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import SwiftyJSON

extension Dictionary {
    func cl_keyvalues() -> [[String:Any]] {
        return self.keys.map({["key": $0, "value": self[$0]!]})
    }
    func cl_sortedKeyValues() -> [[String:Any]] {
        let sortByKey = NSSortDescriptor(key: "key", ascending: true)
        return (self.cl_keyvalues() as NSArray).sortedArray(using: [sortByKey]) as! [[String:Any]]
    }
    
    public var valueArray: [Value] {
        var result = [Value]()
        self.values.forEach({result.append($0)})
        return result
    }
    
    public var toJSON: JSON {
        return JSON(self)
    }
    
    public mutating func addValue(forKey key: Key, initialValue: Value, configurationForExistingValue: (inout Value)->Value) {
        if var existingValue = self[key] {
            self[key] = configurationForExistingValue(&existingValue)
        } else {
            self[key] = initialValue
        }
    }
    
    public func transformValue<T>(block:(Value)->T) -> [Key:T] {
        var result = [Key:T]()
        for (key,value) in self {
            result[key] = block(value)
        }
        return result
    }
}

extension Dictionary where Key == Int {
    public func updateObject(atSection section: Int, withData data:Value)->[Int:Value] {
        var dict = self
        dict[section] = data
        return dict
    }
}

extension Dictionary where Key == String {
    public func checkIfValueExists(forKey key: String) -> Bool {
        return self[key] != nil
    }
}

extension Dictionary where Key == String, Value == Any {
    public func addValueIfNotEmpty<A>(forKey key: String, value: A?, configuration: ((A)->Any)? = nil)-> Dictionary {
        var dict = self
        if value != nil {
            dict[key] = configuration != nil ? configuration!(value!): value!
        }
        return dict
    }
    
    public var toPrettyJson: String {
        let data = try! JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
        let result = String(data: data, encoding: .utf8)
        return result.unwrapOrBlank()
    }
    
    public func isEqual(toJSON:[String:Any]) -> Bool {
        for x in self.keys {
            if let stringValue = self[x] as? String {
                guard let target = toJSON[x] as? String else {return false}
                if stringValue != target {return false}
            } else if let intValue = self[x] as? Int {
                guard let target = toJSON[x] as? Int else {return false}
                if intValue != target {return false}
            }else if let intValue = self[x] as? Double {
                guard let target = toJSON[x] as? Double else {return false}
                if intValue != target {return false}
            }else if let intValue = self[x] as? CGFloat {
                guard let target = toJSON[x] as? CGFloat else {return false}
                if intValue != target {return false}
            }else if let dateValue = self[x] as? Date {
                guard let target = toJSON[x] as? Date else {return false}
                if dateValue != target {return false}
            } else if let imageValue = self[x] as? UIImage {
                guard let target = toJSON[x] as? UIImage else {return false}
                if imageValue != target {return false}
            }
        }
        return true
    }
    
    public func containImage() -> Bool {
        for key in self.keys {
            if let _ = self[key] as? UIImage {
                return true
            }
        }
        return false
    }
}

extension Dictionary where Value == Any {
    func value<T>(forKey key: Key, defaultValue: @autoclosure ()->T)->T {
        if let value = self[key] as? T {
            return value
        } else {
            return defaultValue()
        }
    }
}


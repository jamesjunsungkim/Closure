//
//  Date.swift
//  Connect
//
//  Created by montapinunt Pimonta on 5/7/18.
//  Copyright © 2018 James Kim. All rights reserved.
//
import Foundation
import DateToolsSwift

public enum DateConversionType {
    case UTC
    case current
    
    public var toLocal: Locale {
        switch self {
        case .UTC: return Locale(identifier: "en_US_POSIX")
        case .current: return Locale.current
        }
    }
    
    public var toTimeZone: TimeZone {
        switch self {
        case .UTC: return TimeZone(abbreviation: "UTC")!
        case .current: return TimeZone.current
        }
    }
    
    public var toCalendar: Calendar {
        switch self {
        case .UTC: return Calendar(identifier: .gregorian)
        case .current: return Calendar.current
        }
    }
}

extension Date {
    public var dayUTC: Int {
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        return calendar.component(.day, from: self)
    }
    
    public var monthUTC: Int {
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        return calendar.component(.month, from: self)
    }
    
    public var yearUTC: Int {
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        return calendar.component(.year, from: self)
    }
    
    public var hourUTC: Int {
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        return calendar.component(.hour, from: self)
    }
    
    public var minuteUTC: Int {
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        return calendar.component(.minute, from: self)
    }
    
    public var secondUTC: Int {
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        return calendar.component(.second, from: self)
    }
    
    public func getYearMonthDay()->Date {
        return Date(year: self.year, month: self.month, day: self.day)
    }
    
    public var isInCurrentWeek: Bool {
        return cal.isDate(self, equalTo: Date(), toGranularity: .weekOfYear)
    }
    
    public var isInCurrentMonth: Bool {
        return cal.isDate(self, equalTo: Date(), toGranularity: .month)
    }
    
    public var isInCurrentYear: Bool {
        return cal.isDate(self, equalTo: Date(), toGranularity: .year)
    }
    
    public var toGlobalTime: Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    public var toLocalTime: Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    public var toDateWithoutHourAndMinutes: Date {
        /**
         When printing date on console, it shows UTC time.
         */
        return Date(year: year, month: month, day: day, hour: 0, minute: 0, second: 0)
    }

    // MARK: - Static
    public static func initWith( hour: Int, minute: Int) -> Date {
        let c = Date()
        return Date(year: c.year, month: c.month, day: c.day, hour: hour, minute: minute, second: c.second)
    }
    
    static func ConvertToDate(_ target: String, type:DateConversionType) -> Date {
        let formatter = DateFormatter()
        
        if target.contains("T") {
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        } else {
            formatter.dateFormat = "yyyy-MM-dd"
        }
        
        formatter.locale = type.toLocal
        formatter.timeZone = type.toTimeZone
        return formatter.date(from: target)!
    }
    
    static func getUSLocale() -> Locale {
        return Locale(identifier: "en_US_POSIX")
    }
    
    static func getUTCTimeZone() -> TimeZone {
        return TimeZone(abbreviation: "UTC")!
    }
    
    public static func getWeek(forDate date:Date) -> (start:Date, end:Date) {
        //get this week monday first
        let thisWeekMonday = date.get(.Previous, .monday, considerToday: true)
        let thisWeekSunday = thisWeekMonday.get(.Next, .sunday)
        
        return (thisWeekMonday, thisWeekSunday)
    }
    
    // MARK:- Format Date to UTC
    public func toYearToDateString(type:DateConversionType) -> String {
        return format(with: "yyyy-MM-dd", timeZone: type.toTimeZone, locale: type.toLocal)
    }
    
    public func toYearToMillisecondString(type:DateConversionType)->String {
        return format(with: "yyyy-MM-dd'T'HH:mm:ss", timeZone: type.toTimeZone, locale: type.toLocal)
    }
    
    public func toHourToMinString(type:DateConversionType) -> String {
        return format(with: "HH:mm",  timeZone: type.toTimeZone, locale: type.toLocal)
    }
    
    public func toHourToAmPmString(type:DateConversionType) -> String {
       let formatter = DateFormatter()
        formatter.locale = type.toLocal
        formatter.timeZone = type.toTimeZone
        formatter.dateFormat = "hh:mm a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        return formatter.string(from: self)
    }
    
    public func toLongMonthToDayString(type:DateConversionType) -> String {
        return format(with: "MMMM dd", timeZone: type.toTimeZone, locale: type.toLocal)
    }
    
    public func toShortMonthToDayString(type:DateConversionType) -> String {
        return format(with: "MMM dd", timeZone: type.toTimeZone, locale: type.toLocal)
    }
    
    public func toLongMonthToYearString() ->String {
        return format(with: "MMMM yyyy", timeZone: Date.getUTCTimeZone(), locale: Date.getUSLocale())
    }
    
    public func printInLocalTime() {
        print(self.toLocalTime)
    }
    
    public func addYear(value: Int)->Date{
        return cal.date(byAdding: .year, value: value, to: self)!
    }
    public func addMonth(value: Int)->Date{
        return cal.date(byAdding: .month, value: value, to: self)!
    }
    
    public func addWeak(value:Int)->Date {
        return cal.date(byAdding: .weekOfYear, value: value, to: self)!
    }
    
    public func addDay(value: Int)->Date{
        return cal.date(byAdding: .day, value: value, to: self)!
    }
    
    public func addMinute(value: Int)->Date{
        return cal.date(byAdding: .minute, value: value, to: self)!
    }
    
    public func addSecond(value: Int)->Date{
        return cal.date(byAdding: .second, value: value, to: self)!
    }
    
    public var startDateOfGivenMonth: Date {
        // we set the first day as the day that we subtract 1 day from the first day like 2008-08-20:23H-59M-99S
        return Date(year: year, month: month, day: 1, hour: 0, minute: 0, second: 0).addSecond(value: -1)
    }
    
    public var endDateOfGivenMonth: Date {
        return Date(year: year, month: month+1, day: 1, hour: 0, minute: 0, second: 0).addSecond(value: -1)
    }
    
    public enum Weekday: String {
        case monday, tuesday, wednesday, thursday, friday, saturday, sunday
    }
    
    public enum SearchDirection {
        case Next
        case Previous
        
        var calendarSearchDirection: Calendar.SearchDirection {
            switch self {
            case .Next:
                return .forward
            case .Previous:
                return .backward
            }
        }
    }
    
    public func next(_ weekday: Weekday, considerToday: Bool = false) -> Date {
        return get(.Next,
                   weekday,
                   considerToday: considerToday)
    }
    
    public func previous(_ weekday: Weekday, considerToday: Bool = false) -> Date {
        return get(.Previous,
                   weekday,
                   considerToday: considerToday)
    }
    
    
    // MARK: -Filepriavte
    fileprivate var cal: Calendar {
        return Calendar.current
    }
    
    fileprivate func getWeekDaysInEnglish() -> [String] {
//        var calendar = Calendar(identifier: .gregorian)
        var calendar = Calendar.current
        calendar.locale = Locale.current
        return calendar.weekdaySymbols
    }
    
    fileprivate func get(_ direction: SearchDirection,_ weekDay: Weekday, considerToday consider: Bool = false) -> Date {
        
        let dayName = weekDay.rawValue
        
        let weekdaysName = getWeekDaysInEnglish().map { $0.lowercased() }
        
        assert(weekdaysName.contains(dayName), "weekday symbol should be in form \(weekdaysName)")
        
        let searchWeekdayIndex = weekdaysName.index(of: dayName)! + 1
        
        var calendar = Calendar.current
        calendar.locale = Locale.current
        calendar.timeZone = TimeZone.current
        
        // FIXME: I think it should be current local and time zone since everything is based on current local.
        
//        var calendar = Calendar.init(identifier: .gregorian)
//        calendar.locale = Date.getUSLocale()
//        calendar.timeZone =  Date.getUTCTimeZone()
        
        if consider && calendar.component(.weekday, from: self) == searchWeekdayIndex {
            return self
        }
        
        var nextDateComponent = DateComponents()
        nextDateComponent.weekday = searchWeekdayIndex
        
        
        let date = calendar.nextDate(after: self,
                                     matching: nextDateComponent,
                                     matchingPolicy: .nextTime,
                                     direction: direction.calendarSearchDirection)
        
        return date!
    }
    
}


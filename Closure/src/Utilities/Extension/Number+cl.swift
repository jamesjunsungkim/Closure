//
//  Number+cl.swift
//  Closure
//
//  Created by James Kim on 7/15/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

extension Int {
    
    public var toCGFloat: CGFloat {
        return CGFloat(self)
    }
    public var toIndexSet: IndexSet {
        return IndexSet.init(integer: self)
    }
    public var toDouble:Double {
        return Double(self)
    }
    
    public func ConvertToStringAndAddSeparator() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        guard let r = formatter.string(from: NSNumber.init(value: self)) else {assertionFailure();return ""}
        return r
    }
    
    
    public func convertSecondsToHourMinSecond() ->(hour:Int, min:Int,second:Int) {
        guard self > 60 else {return (0,0,self)}
        
        guard self > 3600 else {
            let second = self % 60
            let min = self / 60
            return (0,min,second)
        }
        
        let second = self % 60
        
        let hour = self / (3600)
        let min = ((self) / (60)) - (hour)*60
        
//        print("converting num to seconds: ", hour,min,second, hour*3600+min*60+second,  hour*3600+min*60+second == self)
        return (hour,min,second)
    }
    
//    public func getReadableUnit() -> String {
//        switch self {
//        case 0..<1_024:
//            return "\(bytes) bytes"
//        case 1_024..<(1_024 * 1_024):
//            return "\(String(format: "%.2f", kilobytes)) kb"
//        case 1_024..<(1_024 * 1_024 * 1_024):
//            return "\(String(format: "%.2f", megabytes)) mb"
//        case (1_024 * 1_024 * 1_024)...Int64.max:
//            return "\(String(format: "%.2f", gigabytes)) gb"
//        default:
//            return "\(bytes) bytes"
//        }
//    }
    
    static func random(start: Int, to end: Int) -> Int {
        guard start < end else {assertionFailure(); return 1}
        let a = start
        let b = end
        
        return Int(arc4random_uniform(UInt32(b - a))) + a
    }
}

extension CGFloat {
    public var toInt: Int {
        return Int(self)
    }
}

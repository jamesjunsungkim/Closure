//
//  UIColor.swift
//  Connect
//
//  Created by James Kim on 5/6/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

extension UIColor {
    
    // MARK: Public
    
    public var inverted: UIColor {
        var r: CGFloat = 0.0, g: CGFloat = 0.0, b: CGFloat = 0.0, a: CGFloat = 0.0
        UIColor.red.getRed(&r, green: &g, blue: &b, alpha: &a)
        return UIColor(red: (1 - r), green: (1 - g), blue: (1 - b), alpha: a)
    }
    
    var navigationBarAdjusted: UIColor {
        let offset: CGFloat = 20/255
        
        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        var a: CGFloat = 0.0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        return UIColor(red: r - offset, green: g - offset, blue: b - offset, alpha: a - offset)
    }
    
    public var components:[CGFloat] {
        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        var a: CGFloat = 0.0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        return [r,g,b,a]
    }
    
    // MARK: Static
    
    static func colorFromCode(_ code: Int) -> UIColor {
        let red = CGFloat((code & 0xFF0000) >> 16) / 255
        let green = CGFloat((code & 0xFF00) >> 8) / 255
        let blue = CGFloat(code & 0xFF) / 255
        return UIColor(red: red, green: green, blue: blue, alpha: 1)
    }
    
    static func create(R: Int, G:Int, B:Int, alpha: CGFloat = 1) -> UIColor {
        return UIColor(red: CGFloat(R/255), green: CGFloat(G/255), blue: CGFloat(B/255), alpha: alpha)
    }
    
    static var eateryBlue: UIColor {
        return colorFromCode(0x437EC5)
    }
    
    static var mainBlue: UIColor {
        return eateryBlue
    }
    
    static var launchScreenBackgroundColor:UIColor {
        return colorFromCode(0x3498DB)
    }
    
    static var backgroundColor: UIColor {
//        return UIColor.create(R: 253, G: 253, B: 248)
        return UIColor(red: 99.2, green: 99.2, blue: 97.3, alpha: 1)
    }
    
    static var shadowBlue: UIColor {
        return UIColor.create(R: 26, G: 178, B: 255)
    }
    
    static var mainGray: UIColor {
        return UIColor.create(R: 85, G: 85, B: 85)
    }
    
    static var transparentEateryBlue: UIColor {
        return UIColor.eateryBlue.withAlphaComponent(0.8)
    }
    
    static var lightBackgroundGray: UIColor {
        return UIColor(white: 0.96, alpha: 1.0)
    }
    
    static var lightSeparatorGray: UIColor {
        return UIColor(white: 0.9, alpha: 1.0)
    }
    
    static var offBlack: UIColor {
        return colorFromCode(0x333333)
    }
    
    static var openGreen: UIColor {
        return colorFromCode(0x7ECC7E)
    }
    
    static var openTextGreen: UIColor {
        return UIColor(red:0.34, green:0.74, blue:0.38, alpha:1)
    }
    
    static var openYellow: UIColor {
        return UIColor(red:0.86, green:0.85, blue:0, alpha:1)
    }
    
    static var closedRed: UIColor {
        return UIColor(red:0.85, green:0.28, blue:0.25, alpha:1)
    }
    
    static var favoriteYellow: UIColor {
        return colorFromCode(0xF8E71C)
    }
    
    static var titleDarkGray: UIColor {
        return colorFromCode(0x7e7e7e)
    }
    
    static var passCodeBackground: UIColor {
        return UIColor.create(R: 102, G: 102, B: 102)
    }
}

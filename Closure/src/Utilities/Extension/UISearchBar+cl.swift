//
//  UISearchBar.swift
//  Closure
//
//  Created by montapinunt Pimonta on 7/24/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

extension UISearchBar {
    
    
    // MARK: Static
    public static func create(backgroundColor: UIColor = .white, placeholder: String = "") -> UISearchBar {
        let s = UISearchBar()
        s.backgroundColor = backgroundColor
        s.autocapitalizationType = .none
        s.placeholder = placeholder
        s.searchBarStyle = .minimal
        return s
    }
}

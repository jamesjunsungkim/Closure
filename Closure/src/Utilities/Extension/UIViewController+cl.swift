//
//  UIViewController.swift
//  Connect
//
//  Created by James Kim on 5/6/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout
import PopupDialog
import DeckTransition
import MZFormSheetPresentationController

extension UIViewController {
    
    // MARK: - Alert & Error
    func presentDefaultAlertWithoutCancel(withTitle title: String, message: String?, okAction: (block)? = nil) {
        let pop = PopupDialog(title: title, message: message)
        pop.transitionStyle = .zoomIn
        pop.buttonAlignment = .horizontal
        let okBtn = PopupDialogButton(title: "Ok".localized, action: {okAction?()})
        
        pop.addButton(okBtn)
        present(pop, animated: true, completion: nil)
    }
    
    func presentDefaultAlert(withTitle title: String?, message: String?, okAction: (block)? = nil, cancelAction: (block)? = nil) {
        let pop = PopupDialog(title: title, message: message)
        pop.transitionStyle = .zoomIn
        pop.buttonAlignment = .horizontal
        let cancelBtn = CancelButton(title: "Cancel".localized, action: {cancelAction?()})
        let okBtn = PopupDialogButton(title: "Ok".localized, action: {okAction?()})
        
        pop.addButtons([cancelBtn, okBtn])
        present(pop, animated: true, completion: nil)
    }
    
    func presentDefaultError(message: String = "Something went wrong.. \nPlease try it again".localized ,okAction: (block)? = nil) {
        let pop = PopupDialog(title: "Error".localized, message: message)
        pop.transitionStyle = .zoomIn
        pop.buttonAlignment = .horizontal
        let okBtn = PopupDialogButton(title: "Ok".localized) {okAction?()}
        pop.addButton(okBtn)
        present(pop, animated: true, completion: nil)
    }
    
    func presentDefaultAlertViewControllerWithoutCancel(title:String?, message:String?, okButtonTitle:String = "Ok".localized, okAction: (()->Void)? = nil) {
        let al = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: okButtonTitle, style: .default, handler: {(_) in okAction?()})
        al.addAction(okAction)
        present(al, animated: true, completion: nil)
    }

    // MARK: - Action Sheet
    
    func presentActionSheetWithCancel(title: String?, message: String?, firstTitle: String?, firstAction: (block)?, cancelAction: (block)? = nil, configuration: ((UIAlertController)->())? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        let firstAction = UIAlertAction(title: firstTitle, style: .default) { (_) in
            firstAction?()
        }
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel) { (_) in
            cancelAction?()
        }
        cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
        
        alertController.addAction(firstAction)
        configuration?(alertController)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
   
    // MARK: - Segue
    
    func presentFormSheetVC<T:FormSheetViewController>(targetVC: T, userInfo: [String:Any]?, completion:((T)->())? = nil,shouldDismissOnBackgroundViewTap: Bool = true ) {
        let type = UIDevice.current.userInterfaceIdiom
        switch type {
        case .pad:
            targetVC.modalPresentationStyle = .formSheet
            targetVC.preferredContentSize = targetVC.preferredSize
            
            self.present(targetVC, animated: true, completion: nil)
            targetVC.setup(fromVC: self, userInfo: userInfo)
            completion?(targetVC)
            
        case .phone:
            let formVC = MZFormSheetPresentationViewController(contentViewController: targetVC)
            formVC.presentationController?.contentViewSize = targetVC.preferredSize
            formVC.presentationController?.shouldDismissOnBackgroundViewTap = shouldDismissOnBackgroundViewTap
            formVC.contentViewControllerTransitionStyle = .slideFromBottom
            formVC.presentationController?.shouldCenterVertically = true
            formVC.presentationController?.shouldCenterHorizontally = true
            targetVC.setup(fromVC: self, userInfo: userInfo)
            present(formVC, animated: true, completion: nil)
            completion?(targetVC)
        default:assertionFailure()
        }
    }
    
    func presentPartialPopupFromBottom<T:FormSheetViewController>(targetVC: T, userInfo: [String:Any]?,shouldDismissOnBackgroundTap: Bool = true, completion:((T)->())? = nil) {
        MZTransition.registerClass(FromBottomUpTransition.self, for: .custom)
        
        let formVC = MZFormSheetPresentationViewController(contentViewController: targetVC)
        formVC.presentationController?.contentViewSize = targetVC.preferredSize
        formVC.presentationController?.shouldDismissOnBackgroundViewTap = shouldDismissOnBackgroundTap
        formVC.contentViewControllerTransitionStyle = .custom
        targetVC.setup(fromVC: self, userInfo: userInfo)
        present(formVC, animated: true, completion: nil)
        completion?(targetVC)
    }
    
    func presentPopOverVC<T:PopOverViewController>(targetVC: T, sourceView: UIView, userInfo:[String:Any]?, completion:((T)->())? = nil) {
        targetVC.modalPresentationStyle = .popover
        targetVC.preferredContentSize = targetVC.preferredSize
        targetVC.popoverPresentationController?.permittedArrowDirections = targetVC.permittedDirection
        targetVC.popoverPresentationController?.sourceRect = sourceView.bounds
        targetVC.popoverPresentationController?.sourceView = sourceView
        
        present(targetVC, animated: true, completion: nil)
        targetVC.setup(fromVC: self, userInfo: userInfo)
        completion?(targetVC)
    }
    
    func presentDefaultVC<T:DefaultViewController>(targetVC: T, userInfo:[String:Any]?, shouldPushOnNavigationController:Bool = true, completion:((T)->())? = nil) {
        
        shouldPushOnNavigationController ? navigationController?.pushViewController(targetVC, animated: true) : self.present(targetVC, animated: true, completion: nil)
        
        targetVC.setup(fromVC: self, userInfo: userInfo)
        completion?(targetVC)
    }
    
    func presentPopUpNavigationController<T:DefaultViewController>(targetVC: T,targetSize: CGSize, userInfo:[String:Any]?, completion:((T)->())? = nil) {
        
        let nav = UINavigationController(rootViewController: targetVC)
        nav.navigationBar.setupToMainBlueTheme(withLargeTitle: false)
        
        let target = MZFormSheetPresentationViewController(contentViewController: nav)
        target.presentationController?.contentViewSize = targetSize
        target.allowDismissByPanningPresentedView = true
        target.presentationController?.shouldCenterVertically = true
        target.presentationController?.shouldCenterHorizontally = true
        target.contentViewControllerTransitionStyle = .slideFromBottom
        
        present(target, animated: true, completion: {completion?(targetVC)})
        targetVC.setup(fromVC: self, userInfo: userInfo)
    }
    
    func presentNavigationController<T:DefaultViewController>(targetVC:T, userInfo:[String:Any]?, completion:((T)->())? = nil) {
        let nav = UINavigationController(rootViewController: targetVC)
        nav.navigationBar.setupToMainBlueTheme(withLargeTitle: false)
        present(nav, animated: true, completion: {completion?(targetVC)})
        targetVC.setup(fromVC: self, userInfo: userInfo)
    }
    
    func presentDeckTransition<T:DefaultViewController>(targetVC: T, userInfo:[String:Any]?, completion:((T)->Void)? = nil) {
        let transitionDelegate = DeckTransitioningDelegate()
        targetVC.transitioningDelegate = transitionDelegate
        targetVC.modalPresentationStyle = .custom
        targetVC.setup(fromVC: self, userInfo: userInfo)
        present(targetVC, animated: true, completion: nil)
        completion?(targetVC)
    }
    
    func presentPopup(targetVC: DefaultViewController, cancelButtonTitle: String = "Cancel".localized, cancelAction: (()->Void)? = nil, okButtonTitle:String, okAction: (()->Void)? = nil, completion:(()->Void)? = nil, panGestureDismissal: Bool = true, dismissOnTap:Bool = true, userInfo:[String:Any]? = nil) {

        let popup = PopupDialog(viewController: targetVC, buttonAlignment: .horizontal, transitionStyle: .zoomIn, tapGestureDismissal: true, panGestureDismissal: false, hideStatusBar: true, completion: nil)
        
        let cancelBtn = CancelButton(title: cancelButtonTitle, action: {cancelAction?()})
        let okBtn = DefaultButton(title: okButtonTitle, height: 45, dismissOnTap: dismissOnTap, action: {okAction?()})
        
        popup.addButtons([cancelBtn, okBtn])
        present(popup, animated: true, completion: completion)
        targetVC.setup(fromVC: self, userInfo: userInfo)
    }
    
    func presentPopupWithOnlyOK(targetVC: UIViewController, okButtonTitle:String, okAction: (()->Void)? = nil, completion:(()->Void)? = nil, panGestureDismissal: Bool = true, tapGestureDismissal:Bool = true) {
        
        let popup = PopupDialog(viewController: targetVC, buttonAlignment: .horizontal, transitionStyle: .zoomIn, preferredWidth: 340, tapGestureDismissal: tapGestureDismissal, panGestureDismissal: panGestureDismissal, hideStatusBar: true, completion: nil)
        
        let okBtn = DefaultButton(title: okButtonTitle, action: {okAction?()})
        popup.addButton(okBtn)
        present(popup, animated: true, completion: completion)
    }
    
    func presentPopupWithoutAnyButton(targetVC: DefaultViewController, completion:(()->Void)? = nil, panGestureDismissal: Bool = true, tapGestureDismissal:Bool = true, prefferedWidth: CGFloat = 340, userInfo:[String:Any]? = nil) {
        
        let popup = PopupDialog(viewController: targetVC, buttonAlignment: .horizontal, transitionStyle: .zoomIn, preferredWidth: prefferedWidth, tapGestureDismissal: tapGestureDismissal, panGestureDismissal: panGestureDismissal, hideStatusBar: true, completion: nil)
        
        targetVC.setup(fromVC: self, userInfo: userInfo)
        present(popup, animated: true, completion: completion)
    }
    
    public func openAppSettings() {
        guard let url = URL(string: UIApplicationOpenSettingsURLString), UIApplication.shared.canOpenURL(url) else {assertionFailure();return}
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
//    public func presentMasterAlbumViewController() {
//        let destination = AlbumMasterViewController()
//        destination.fromVC = self
//        let nav = UINavigationController(rootViewController: destination)
//        nav.navigationBar.setupToMainBlueTheme(withLargeTitle: false)
//        present(nav, animated: true, completion: nil)
//    }
    
    // MARK: - Animation
    
    func placeHolderBeginningAnimation(label: UILabel, bottomView: UIView, leadingMargin: CGFloat, bottomMargin: CGFloat) {
        label.snp.remakeConstraints { (make) in
            make.left.equalToSuperview().offset(leadingMargin)
            make.bottom.equalTo(bottomView.snp.top).offset(-bottomMargin)
        }
        label.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        label.textColor = UIColor.mainBlue
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func placeHolderEndingAnimation(textField: UITextField, label: UILabel,bottomView: UIView, leadingMargin: CGFloat, bottomMargin: CGFloat) {
        if textField.text!.isEmpty {
            label.snp.remakeConstraints { (make) in
                make.left.equalToSuperview().offset(leadingMargin)
                make.bottom.equalTo(bottomView.snp.top).offset(-bottomMargin)
            }
            label.textColor = .lightGray
            label.transform = .identity
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    
    // MARK: - Helpers
    
    func getHeightOfNavigationBarAndStatusBar() -> CGFloat {
        return UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
    
    
    // MARK: - Static funcs
    static func newController(fromViewController: UIViewController)->UIViewController {
        return newController(
            withView: fromViewController.view,
            frame: fromViewController.view.frame)
    }
    
    
    static func newController(withView view: UIView, frame: CGRect) -> UIViewController {
        view.frame = frame
        let controller = UIViewController()
        controller.view = view
        return controller
    }
}













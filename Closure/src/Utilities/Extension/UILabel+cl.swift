//
//  UILabel.swift
//  Connect
//
//  Created by James Kim on 5/9/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

extension UILabel {
    
    // MARK: - Public
    public func changeFont(to font: UIFont) {
        self.font = font
    }
    
    public func change(toString st:String) {
        if let attriText = self.attributedText {
            let attributes = attriText.attributes(at: 0, effectiveRange: nil)
            let newString = NSAttributedString(string: st, attributes: attributes)
            self.attributedText = newString
        } else {
            self.text = st
        }
    }
    
    // MARK: - Static
    static public func create(text: String, textAlignment:NSTextAlignment, textColor: UIColor = .black ,fontSize: CGFloat, boldFont flag: Bool = false, numberOfLine: Int = 0) -> UILabel {
        let lb = UILabel()
        lb.textAlignment = textAlignment
        lb.text = text
        lb.textColor = textColor
        lb.font = UIFont.mainFont(size: fontSize, shouldBold: flag)
        lb.numberOfLines = numberOfLine
        return lb
    }
    
    
    
    
}

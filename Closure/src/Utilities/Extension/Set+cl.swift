//
//  Set+cl.swift
//  Closure
//
//  Created by James Kim on 8/22/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation


extension Set {
    public var toArray: [Element] {
        return Array(self)
    }
}

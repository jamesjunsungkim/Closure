//
//  Iterator+cl.swift
//  Closure
//
//  Created by James Kim on 8/7/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation

extension AnyIterator {
    public var count: Int {
        return self.reduce(0, {(result, _) in return result+1})
    }
    
    public var isEmpty: Bool {
        for _ in self {
            return false
        }
        return true
    }
    
    public var array: [Element] {
        var result = [Element]()
        for e in self {
            result.append(e)
        }
        return result
    }
}


//
//  UIButton.swift
//  Connect
//
//  Created by James Kim on 5/9/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

extension UIButton {
    
    // MARK: - Public / Internal
    
    public func setTitleWhileKeepingAttributes(title:String) {
        if let attributedTitle = currentAttributedTitle {
            let mutableAttributedTitle = NSMutableAttributedString(attributedString: attributedTitle)
            mutableAttributedTitle.replaceCharacters(in: NSMakeRange(0, mutableAttributedTitle.length), with: title)
            self.setAttributedTitle(mutableAttributedTitle, for: .normal)
        }
    }
    
    public func adjustFontSize(minimumScaleFactor:CGFloat = 0.5, numberOfLines: Int = 1) {
        self.titleLabel?.minimumScaleFactor = minimumScaleFactor
        self.titleLabel?.numberOfLines = numberOfLines
        self.titleLabel?.adjustsFontSizeToFitWidth = true
    }
    
    public func set(normalBC: UIColor, hightlightedBC: UIColor, selectedBC: UIColor, focusedBG: UIColor) {
        setBackgroundImage(UIImage.image(withColor: normalBC), for: .normal)
        setBackgroundImage(UIImage.image(withColor: hightlightedBC), for: .highlighted)
        setBackgroundImage(UIImage.image(withColor: selectedBC), for: .selected)
        setBackgroundImage(UIImage.image(withColor: focusedBG), for: .focused)
    }
    
    public func changeTo(title:String) {
        if let attriString = self.currentAttributedTitle {
            let attributes = attriString.attributes(at: 0, effectiveRange: nil)
            let newString = NSAttributedString(string: title, attributes: attributes)
            self.setAttributedTitle(newString, for: .normal)
        } else {
            self.setTitle(title, for: .normal)
        }
    }
    
    // MARK: - static
    public static func create(title: String, titleColor: UIColor = .black, fontSize: CGFloat = 10, backgroundColor: UIColor = .clear) -> UIButton {
        let bt = UIButton(type: .system)
        bt.backgroundColor = backgroundColor
        let attributedString = NSAttributedString(string: title, attributes: [NSAttributedStringKey.foregroundColor:titleColor, NSAttributedStringKey.font: UIFont.mainFont(size: fontSize)])
        bt.setAttributedTitle(attributedString, for: .normal)
        return bt
    }
    
    public static func create(withImageName imageName: String) -> UIButton {
        let bt = UIButton(type: .system)
        let img = UIImage(named: imageName)?.withRenderingMode(.alwaysOriginal)
        bt.setImage(img, for: .normal)
        bt.backgroundColor = .clear
        return bt
    }
    
    public static func create(withNameKey key: UIImage.Name) -> UIButton {
        return create(withImageName: key.rawValue)
    }
    
    public static func create(withImage image: UIImage) -> UIButton {
        let bt = UIButton(type: .system)
        bt.setImage(image.withRenderingMode(.alwaysOriginal), for: .normal)
        bt.backgroundColor = .clear
        return bt
    }
}



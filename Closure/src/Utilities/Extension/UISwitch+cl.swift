//
//  UISwitch+cl.swift
//  Closure
//
//  Created by James Kim on 7/29/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

extension UISwitch {
    
    // MARK: Public
    
    // MARK: Static
    public static func create(tintColor:UIColor,onTintColor: UIColor, isOn:Bool = false) -> UISwitch {
        let s = UISwitch()
        s.tintColor = tintColor
        s.onTintColor = onTintColor
        s.isOn = isOn
        return s
    }
}

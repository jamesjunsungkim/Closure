//
//  String.swift
//  Connect
//
//  Created by James Kim on 5/6/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit


extension String {
    
    // MARK: - Public / Internal
    
    public func contains(string:String) -> Bool {
        return self.range(of: string) != nil
    }
    
    public func checkIfContainNumber() -> Bool {
        let decimalCharacters = CharacterSet.decimalDigits
        let decimalRange = self.rangeOfCharacter(from: decimalCharacters)
        return decimalRange != nil
    }
    
    public func removeCharacters(in string: String) -> String {
        var chars = Substring(self)
        let convertSet = CharacterSet(charactersIn: string)
        for idx in chars.indices.reversed() {
            if convertSet.contains(String(chars[idx]).unicodeScalars.first!) {
                chars.remove(at: idx)
            }
        }
        return String(chars)
    }
    
    public func removeCharacters(in set: CharacterSet) -> String {
        var chars = Substring(self)
        for idx in chars.indices.reversed() {
            if set.contains(String(chars[idx]).unicodeScalars.first!) {
                chars.remove(at: idx)
            }
        }
        return String(chars)
    }
    
    public func removeEverythingExceptForLetterAndNumber() -> String {
        var alphaSet = CharacterSet.alphanumerics
        let numberSet = CharacterSet.decimalDigits
        alphaSet.formUnion(numberSet)
        
        var chars = Substring()
        
        for char in self {
            if alphaSet.contains(char.unicodeScalars.first!) {
                chars.append(char)
            }
        }
        return String(chars)
    }
    
    public func getWidth() -> CGFloat {
        let txt = UITextField(frame: .zero)
        txt.text = self
        txt.sizeToFit()
        return txt.frame.size.width
    }
    
    public func convertToURL()-> URL {
        guard let url = URL(string: self) else {fatalError("wrong type")}
        return url
    }
    
    public func validateForEmail() -> Bool {
        let arg = "[A-Z0-9a-z._%+-]+@[A-Z0-9a-z.-]+\\.[A-Za-z]{2,64}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", arg)
        return predicate.evaluate(with: self)
    }
    
    public func validateForNumber() -> Bool {
        let arg = "^[0-9]+(\\.[0-9]+)?$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", arg)
        return predicate.evaluate(with: self)
    }
    
    public func checkIfEmptyOr(defaultValue:String) -> String {
        guard !self.isEmpty else {return defaultValue}
        return self
    }
    
    public var firstLetterCapitalized: String {
        let first = String(prefix(1)).capitalized
        let other = String(dropFirst())
        return first + other
    }
    
    public func checkIfNotEmptyOrPresent(defaultValue:String) -> String {
        guard !self.isEmpty else {return defaultValue}
        return self
    }
    
    public var checkIfNotEmptyOrNull: Any {
        guard !self.isEmpty else {return NSNull()}
        return self
    }
    
    public func checkIfNotEmptyOrDefaultValue(_ value: String) -> String {
        guard !self.isEmpty else {return value}
        return self
    }
    
    // MARK: Date
    public func toDate(type:DateConversionType) -> Date {
        guard !self.isEmpty else {assertionFailure();return Date()}
        
        let formatter = DateFormatter()
        formatter.locale = type.toLocal
        formatter.timeZone = type.toTimeZone
        
        let dateFormatPool = ["yyyy-MM-dd'T'HH:mm:ss", "yyyy-MM-dd", "HH:mm a", "HH:mm"]
        
        var result:Date!
        var currentIndex = 0
        
        var poolCoversAllTypeCondition: Bool {
            return currentIndex < dateFormatPool.count
        }
        
        while result == nil {
            guard poolCoversAllTypeCondition else {assertionFailure();return Date()}
            formatter.dateFormat = dateFormatPool[currentIndex]
            guard let r = formatter.date(from: self) else {
                currentIndex += 1
                continue
            }
            result = r
        }
        
        return result
    }
    
//    public func toDate(withFormat: String) -> String {
//        return toDate.format(with: withFormat, timeZone: Date.getUTCTimeZone(), locale: Date.getUSLocale())
//    }
    
    // MARK: - Static
    
    
}

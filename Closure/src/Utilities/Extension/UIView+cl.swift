//
//  UIView.swift
//  Connect
//
//  Created by James Kim on 5/8/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import PKHUD

extension UIView {
    private struct AnimationKey {
        static let Rotation = "rotation"
        static let Bounce = "bounce"
        static let Shake = "shake"
    }
    
    // MARK: - Public/Internal
    
    public static func getMostTopViewController() -> UIViewController? {
        var topVC = UIApplication.shared.keyWindow?.rootViewController
        while topVC?.presentedViewController != nil {
            topVC = topVC?.presentedViewController!
        }
        return topVC
    }
    
    public func getParentViewController() -> UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if parentResponder is UIViewController {
                return parentResponder as? UIViewController
            }
        }
        return nil
    }
    
    public func setCornerRadious(value: CGFloat) {
        self.layer.cornerRadius = value
        self.layer.masksToBounds = true
    }
    
    public func setBorder(color: UIColor, width: CGFloat) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
    
    public func setShadow(contentViewCornerRadius: CGFloat ,shadowRadius: CGFloat, shadowOpacity: Float, shadowOffset:CGSize, shadowRect:CGRect) {
        // this is desinged for collectionview or tableview cell.
        if let collectionViewCell = self as? UICollectionViewCell {
            collectionViewCell.contentView.setCornerRadious(value: contentViewCornerRadius)
        } else if let tableViewCell = self as? UITableViewCell {
            tableViewCell.contentView.setCornerRadious(value: contentViewCornerRadius)
        } else {
            assertionFailure()
        }

        layer.shadowPath = UIBezierPath(rect: shadowRect).cgPath
        layer.shadowRadius = shadowRadius
        layer.shadowOpacity = shadowOpacity
        layer.shadowOffset = shadowOffset
        layer.masksToBounds = false
    }
    
    public func snapshot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        drawHierarchy(in: bounds, afterScreenUpdates: true)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
    
    public var width: CGFloat {
        return self.bounds.width
    }
    
    public var height: CGFloat {
        return self.bounds.height
    }
    
    func shakeHorizontally(duration: TimeInterval = 0.5, values: [CGFloat] = [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, -2.0, 2, -1, 1, 0.0]) {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        
        // Swift 4.1 and below
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        
        animation.duration = duration // You can set fix duration
        animation.values = values  // You can set fix values here also
        layer.add(animation, forKey: AnimationKey.Shake)
    }
    
    public func startWiggle() {
        let rotation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        rotation.values = [-0.005, 0.005]
        rotation.repeatCount = .infinity
        layer.add(rotation, forKey: AnimationKey.Rotation)
        
        let bounce = CAKeyframeAnimation(keyPath: "transform.translation.y")
        bounce.values = [0.5, 0]
        bounce.repeatCount = .infinity
        layer.add(bounce, forKey: AnimationKey.Bounce)
    }
    
    public func stopWiggle() {
        layer.removeAnimation(forKey: AnimationKey.Rotation)
        layer.removeAnimation(forKey: AnimationKey.Bounce)
    }
   
    public func setEdgesToSuperView() {
        self.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    public func updateWithCurrentLanguage() {
        /**
         I think it will fall into three categories, either label, button or textfield
         point would be that it could be attributed text or just plain text.
         
         review: I think this is a bad approach because one way from a language to other is fine but
         when it's back, the origianl text is gone.. in order to work it out, I need to figure out how to
         save the original text somehow but I think it's not cool.
         */
        if let label = self as? UILabel {
            if let attriText = label.attributedText {
                let attributes = attriText.attributes(at: 0, effectiveRange: nil)
                let newString = NSAttributedString(string: attriText.string.localized, attributes: attributes)
                label.attributedText = newString
            } else {
                label.text = label.text!.localized
            }
        } else if let button = self as? UIButton {
            if let attri = button.currentAttributedTitle {
                let attributes = attri.attributes(at: 0, effectiveRange: nil)
                let newString = NSAttributedString(string: attri.string.localized, attributes: attributes)
                button.setAttributedTitle(newString, for: .normal)
            } else {
                button.setTitle( button.currentTitle.unwrapOrBlank().localized, for: .normal)
            }
        } else if let textField = self as? UITextField {
            if let placeholder = textField.placeholder {
                textField.placeholder = placeholder.localized
            }
        } else {
            assertionFailure()
            
        }
    }
    
    // MARK: - static
    public static func create(withColor color: UIColor = .lightGray, alpha: CGFloat = 1) -> UIView {
        let v = UIView()
        v.backgroundColor = color
        v.alpha = alpha
        return v
    }
    
    public static func debugArea(target:[UIView]) {
        let debugColor: [UIColor] = [.red, .blue, .green, .magenta, .cyan, .orange, .brown, .mainBlue]
        guard target.count <= debugColor.count else {assertionFailure();return}
        
        for index in 0..<target.count {
            target[index].backgroundColor = debugColor[index]
        }
    }
    
    public static func showLoading(title:String? = nil, subtitle: String? = nil) {
        HUD.show(.labeledProgress(title: title, subtitle: subtitle))
    }

    public static func showSuccess() {
        HUD.show(.success)
    }

    public static func hideHUD(completion: ((Bool)->Void)? = nil) {
        HUD.hide(completion)
    }

    // MARK: - Fileprivate
}

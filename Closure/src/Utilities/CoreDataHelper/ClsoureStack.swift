//
//  ClsoureStack.swift
//  Closure
//
//  Created by James Kim on 7/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import CoreData

public func createConnectContainer(completion: @escaping (NSPersistentContainer)->()) {
    let container = NSPersistentContainer(name: "Closure")
    container.loadPersistentStores { (_, error) in
        guard error == nil else {fatalError()}
        completion(container)
    }
}

public func createClosureContainer(migrating:Bool = false, progress: Progress? = nil, completion:@escaping(NSPersistentContainer)->()) {
    closureContainer.loadPersistentStores { (_, error) in
        if error == nil {
            closureContainer.viewContext.mergePolicy = ClosureMergyPolicy.init(mode: .local)
            DispatchQueue.main.async {completion(closureContainer)}
        } else {
            guard !migrating else {fatalError("was unable to migrate store")}
            DispatchQueue.global(qos: .userInitiated).async {
                migrateStore(from: storeURL, to: storeURL, targetVersion: ClosureModelVersion.current, deleteSource: true, progress: progress)
                createClosureContainer(migrating: true, progress: progress, completion: completion)
            }
        }
    }
}

private let closureContainer:NSPersistentContainer = {
    let container = NSPersistentContainer(name: "Closure", managedObjectModel: ClosureModelVersion.current.managedObjectModel())
    let storeDescription = NSPersistentStoreDescription(url: storeURL)
    storeDescription.shouldMigrateStoreAutomatically = false
    container.persistentStoreDescriptions = [storeDescription]
    return container
}()

private let ubiquityToken: String = {
    guard let token = FileManager.default.ubiquityIdentityToken else {return "unknown"}
    let string = NSKeyedArchiver.archivedData(withRootObject: token).base64EncodedString(options: [])
    return string.removeCharacters(in: CharacterSet.letters.inverted)
}()

private let storeURL = URL.documents.appendingPathComponent("\(ubiquityToken).closure")

extension NSManagedObjectContext {
    static func testContext(_ addStore: (NSPersistentStoreCoordinator) -> ()) -> NSManagedObjectContext {
        let model = NSManagedObjectModel.mergedModel(from: [Bundle.main])!
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: model)
        addStore(coordinator)
        let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        context.persistentStoreCoordinator = coordinator
        return context
    }
}

extension NSManagedObjectContext {
    
    static func memoryContext() -> NSManagedObjectContext {
        return closureContext({$0.addInMemoryStore()})
    }
    
    fileprivate static func closureContext(_ addStore: (NSPersistentStoreCoordinator)->Void) -> NSManagedObjectContext {
        let managedObjectModel = NSManagedObjectModel.mergedModel(from: [Bundle.main])!
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        addStore(coordinator)
        let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        context.persistentStoreCoordinator = coordinator
        return context
    }
}

extension NSPersistentStoreCoordinator {
    fileprivate func addInMemoryStore() {
        try! addPersistentStore(ofType: NSInMemoryStoreType, configurationName: nil, at: nil, options: nil)
    }
}

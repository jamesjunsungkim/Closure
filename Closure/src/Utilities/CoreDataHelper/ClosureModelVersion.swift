//
//  ClosureModelVersion.swift
//  Closure
//
//  Created by James Kim on 9/12/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation

enum ClosureModelVersion: String {
    case Version1 = "Closure"
}

extension ClosureModelVersion:ModelVersion {
    static var all: [ClosureModelVersion] { return [.Version1]}
    static var current: ClosureModelVersion { return .Version1}
    
    var name: String { return rawValue}
    var modelBundle: Bundle {return Bundle(for: Review.self)}
    var modelDirectoryName: String {return "Closure.momd"}
}

//
//  AlbumDetailViewController.swift
//  Connect
//
//  Created by James Kim on 6/3/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import Photos

//TODO: think about how to support multiple selections. In order to do so, we need a model that holds a boolean to tell if it's selected or not.
final class AlbumDetailViewController: DefaultViewController {
    
    // UI
    fileprivate var collectionView: UICollectionView!
    fileprivate var doneButton: UIBarButtonItem!
    
    init(photoSelectionAction:@escaping ([UIImage])->Void, isMultipleSelectionEnabled:Bool) {
        self.photoSelectionAction = photoSelectionAction
        self.isMultipleSelectionEnabled = isMultipleSelectionEnabled
        super.init(nibName: nil, bundle: nil)
    }
    
    func setup(fromVC: UIViewController, userInfo: [String : Any]?) {
        
        let collectionKey = AlbumMasterViewController.Keys.collection
        let fetchKey = AlbumMasterViewController.Keys.fetch
        
        if let collection = userInfo?[collectionKey] as? PHAssetCollection {
            assetCollection = collection
        }
        fetchResult = userInfo![fetchKey]! as? PHFetchResult<PHAsset>
        
        // ViewDidLoad
        enterViewControllerMemoryLog(self)
        setupUI()
        setupVC()
        setupCollectionView()
        resetCachedAssets()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateCachedAssets()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateCachedAssets()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideStatusBar = false
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var prefersStatusBarHidden: Bool {
        return hideStatusBar
    }
    
    // MARK: - Actions
    @objc fileprivate func doneBtnClicked() {
        /**
         we need to fetch the image with target quality.
         for now, let's focus on fetching only one photo.
         
         
         */
        
        logInfo(type: .action)
        
        guard let targetCells = selectedCells.first else {assertionFailure();return}
        let asset = fetchResult.object(at: targetCells.indexPath.item)
        let targetSize = CGSize(width: 1000, height: 1000)
        
        let options = PHImageRequestOptions()
        options.isSynchronous = true
        
        imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: .aspectFill, options: options) {[unowned self] (i, _) in
            
            /**
             Problem: I don't know why it says it tries to access this instane that has already been deallocated.
             Solution: It was because how the requestImage method is set up. (If it's not clear enough, please take a look at the official explanation on the method.)
             
             it returns the requested process right away with very low resolution and then it's called again when the desirable outpcome is available. that's why it's excecuted more than one time.
             
             But If the PHImageRequestOption is set isSynchrnous, it doesn't call the handler until it's finished with the required image quality.
             */
            guard let image = i else {assertionFailure();return}
            self.photoSelectionAction([image])
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    
    fileprivate func didSelectCollectionViewItem(atIndexPath indexPath: IndexPath) {
        logInfo(type: .action)
        
        let cell_ = cell(forIndexPath: indexPath)
        cell_.didGetSelected()
        
        if !isMultipleSelectionEnabled {
            if selectedCells.count > 0 {
                selectedCells.forEach({[unowned self] in
                    $0.didGetDeselected()
                    self.collectionView.deselectItem(at: $0.indexPath, animated: false)
                })
                selectedCells.removeAll()
            }
        }
        _ = selectedCells.insert(cell_)
        enableOrDisableDoneButton()
    }
    
    fileprivate func didDeSelectCollectionViewItem(atIndexPath indexPath: IndexPath) {
        logInfo(type: .action)
        let cell_ = cell(forIndexPath: indexPath)
        cell_.didGetDeselected()
        _ = selectedCells.remove(cell_)
        enableOrDisableDoneButton()
    }
    
    // MARK: - Fileprivate
    fileprivate let photoSelectionAction:([UIImage])->Void
    
    fileprivate var fetchResult: PHFetchResult<PHAsset>!
    fileprivate var assetCollection: PHAssetCollection!
    fileprivate var selectedCells = Set<AlbumDetailCell>()
    
    fileprivate var thumbnailSize: CGSize!
    fileprivate var previousPreheatRect = CGRect.zero
    fileprivate let imageManager = PHCachingImageManager()
    
    fileprivate var isMultipleSelectionEnabled = true
    fileprivate var hideStatusBar = true
    
    fileprivate func setupVC() {
        view.backgroundColor = .white
        PHPhotoLibrary.shared().register(self)
        
        doneButton = UIBarButtonItem(title: "Done".localized, style: .plain, target: self, action: #selector(doneBtnClicked))
        navigationItem.rightBarButtonItem = doneButton
        enableOrDisableDoneButton()
    }
    
    fileprivate func setupCollectionView() {
        thumbnailSize = CGSize(width: 200, height: 200)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(AlbumDetailCell.self, forCellWithReuseIdentifier: AlbumDetailCell.reuseIdentifier)
        collectionView.register(AlbumDetailFooter.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: AlbumDetailFooter.className)
        collectionView.allowsMultipleSelection = true
        collectionView.alwaysBounceVertical = true
        
        collectionView.reloadData()
        
        performOnMain {[unowned self] in
            //Get to the bottom
            let indexPathForLast = IndexPath(item: self.fetchResult.count-1, section: 0)
            self.collectionView.scrollToItem(at: indexPathForLast, at: .bottom, animated: false)
            if self.collectionView.contentSize.height >= self.view.bounds.height {
                self.collectionView.contentOffset.y += 50
            }
        }
    }
    
    fileprivate func resetCachedAssets() {
        imageManager.stopCachingImagesForAllAssets()
        previousPreheatRect = .zero
    }
    
    fileprivate func updateCachedAssets() {
        guard isViewLoaded && view.window != nil else { return }
        
        let visibleRect = CGRect(origin: collectionView!.contentOffset, size: collectionView!.bounds.size)
        let preheatRect = visibleRect.insetBy(dx: 0, dy: -0.5 * visibleRect.height)
        
        let delta = abs(preheatRect.midY - previousPreheatRect.midY)
        guard delta > view.bounds.height / 3 else { return }
        
        let (addedRects, removedRects) = differencesBetweenRects(previousPreheatRect, preheatRect)
        let addedAssets = addedRects
            .flatMap { rect in collectionView!.indexPathsForElements(in: rect) }
            .map { indexPath in fetchResult.object(at: indexPath.item) }
        let removedAssets = removedRects
            .flatMap { rect in collectionView!.indexPathsForElements(in: rect) }
            .map { indexPath in fetchResult.object(at: indexPath.item) }
        
        imageManager.startCachingImages(for: addedAssets,
                                        targetSize: thumbnailSize, contentMode: .aspectFill, options: nil)
        imageManager.stopCachingImages(for: removedAssets,
                                       targetSize: thumbnailSize, contentMode: .aspectFill, options: nil)
        
        previousPreheatRect = preheatRect
    }
    
    fileprivate func differencesBetweenRects(_ old: CGRect, _ new: CGRect) -> (added:[CGRect], removed:[CGRect]) {
        if old.intersects(new) {
            var added = [CGRect]()
            if new.maxY > old.maxY {
                added += [CGRect(x: new.origin.x, y: old.maxY,
                                 width: new.width, height: new.maxY - old.maxY)]
            }
            if old.minY > new.minY {
                added += [CGRect(x: new.origin.x, y: new.minY,
                                 width: new.width, height: old.minY - new.minY)]
            }
            var removed = [CGRect]()
            if new.maxY < old.maxY {
                removed += [CGRect(x: new.origin.x, y: new.maxY,
                                   width: new.width, height: old.maxY - new.maxY)]
            }
            if old.minY < new.minY {
                removed += [CGRect(x: new.origin.x, y: old.minY,
                                   width: new.width, height: new.minY - old.minY)]
            }
            return (added, removed)
        } else {
            return ([new], [old])
        }
    }
    
    fileprivate func enableOrDisableDoneButton() {
        doneButton.isEnabled = selectedCells.count != 0
        doneButton.tintColor = .white
    }
    
    fileprivate func cell(forIndexPath indexPath:IndexPath) -> AlbumDetailCell {
        return (collectionView.cellForItem(at: indexPath) as! AlbumDetailCell)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
}
extension AlbumDetailViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return fetchResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let asset = fetchResult.object(at: indexPath.item)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AlbumDetailCell.reuseIdentifier, for: indexPath) as! AlbumDetailCell
        cell.representedAssetIdentifier = asset.localIdentifier
        
        imageManager.requestImage(for: asset, targetSize: thumbnailSize, contentMode: .aspectFill, options: nil, resultHandler: { image, _ in
            if cell.representedAssetIdentifier == asset.localIdentifier {
                cell.indexPath = indexPath
                cell.albumImageView.image = image
            }
        })
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectCollectionViewItem(atIndexPath: indexPath)
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        didDeSelectCollectionViewItem(atIndexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: AlbumDetailFooter.className, for: indexPath) as! AlbumDetailFooter
            footerView.configure(withCount: fetchResult.count)
            return footerView
        default: assertionFailure("Only footer!")
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: view.bounds.width, height: 50)
    }
}

extension AlbumDetailViewController: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        
        guard let changes = changeInstance.changeDetails(for: fetchResult) else {return}
        
        DispatchQueue.main.sync {
            fetchResult = changes.fetchResultAfterChanges
            if changes.hasIncrementalChanges {
                guard let collectionView = self.collectionView else {return}
                collectionView.performBatchUpdates({
                    if let removed = changes.removedIndexes, removed.count > 0 {
                        collectionView.deleteItems(at: removed.map({ IndexPath(item: $0, section: 0) }))
                    }
                    if let inserted = changes.insertedIndexes, inserted.count > 0 {
                        collectionView.insertItems(at: inserted.map({ IndexPath(item: $0, section: 0) }))
                    }
                    if let changed = changes.changedIndexes, changed.count > 0 {
                        collectionView.reloadItems(at: changed.map({ IndexPath(item: $0, section: 0) }))
                    }
                    changes.enumerateMoves { fromIndex, toIndex in
                        collectionView.moveItem(at: IndexPath(item: fromIndex, section: 0),
                                                to: IndexPath(item: toIndex, section: 0))
                    }
                }, completion: nil)
            } else {
                collectionView.reloadData()
            }
            resetCachedAssets()
        }
    }
}

extension AlbumDetailViewController {
    
    fileprivate func setupUI() {
        let width = (view.frame.width-3) / 4
        collectionView = UICollectionView.create(backgroundColor: .white, configuration: { (layout) in
            layout.itemSize = CGSize(width: width, height: width)
            layout.minimumLineSpacing = 1
            layout.minimumInteritemSpacing = 1
        })
        collectionView.isMultipleTouchEnabled = false
        collectionView.allowsMultipleSelection = isMultipleSelectionEnabled
//        collectionView.contentInset = UIEdgeInsetsMake(1, 0, 0, 0)
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}

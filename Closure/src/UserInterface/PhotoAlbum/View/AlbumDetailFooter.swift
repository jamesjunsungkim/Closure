//
//  AlbumDetailFooter.swift
//  Closure
//
//  Created by montapinunt Pimonta on 7/28/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

final class AlbumDetailFooter: UICollectionReusableView, NameDescribable {
    
    // UI
    fileprivate var countLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        
    }
    
    // MARK: Public
    public func configure(withCount c: Int) {
        let result = c.ConvertToStringAndAddSeparator()
        
        countLabel.text = "\(result) " + "Photos".localized
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension AlbumDetailFooter {
    fileprivate func setupUI() {
        countLabel = UILabel.create(text: "2011 Photos", textAlignment: .center, textColor: .lightGray, fontSize: 14, boldFont: true, numberOfLine: 1)
        
        addSubview(countLabel)
        
        countLabel.snp.makeConstraints { (make) in
            make.centerY.centerX.equalToSuperview()
        }
        
    }
}



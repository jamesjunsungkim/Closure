//
//  AppStatus.swift
//  Closure
//
//  Created by James Kim on 7/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import RxSwift
import Foundation
import CoreData
import Reachability

final class AppStatus:Unwrappable {
    
    init(context:NSManagedObjectContext, application:UIApplication) {
        /**
         0. About user. due to singin/up view controller.
         since language manager resides in app status, can't translate texts in signup process.
         so we initialize appstatus with only context and then we register user when it's ready.
         
         I didn't know it took a while for ios to prepare managedContext. so I can't initialize it instantly. which means I need to update it when it's ready.
         */
        
        self.mainContext = context
//        self.currentUser = user
        
        setupConnectionObserver()
        notificationManager = NotificationManager(application: application, appStatus: self)
    }
    
    // MARK: Public
    fileprivate(set) var mainContext: NSManagedObjectContext
    fileprivate(set) var currentUser: User! {
        didSet {
            /**
             I put methods that require user information.
             0. passcode is saved to dict with key of user uid.
             */
            passCodeManager.initialLoadPassCodeIfExist()
        }
    }
    
    public func registerUser(_ user: User) {
        currentUser = user
    }
    // Network
    public var networkManager = NetworkManager()
    
    // WordCount
    fileprivate(set) lazy var wordCountManager = WordCountManager(context: self.mainContext)
    
    // Reminder
    // I moved the customReminderManager to NotificationManager since it's notification
//    fileprivate(set) var customReminderManager = CustomReminderManager()
    
    // PassCode
    fileprivate(set) lazy var passCodeManager = PassCodeManager(appStatus: self)
    
    // Email
    fileprivate(set) var emailManager = EmailManager()
    
    // Langauge
    fileprivate(set) var languageManager = LanguageManager()
    
    // DiskStatus
    fileprivate(set) var diskStatusManager = DiskStatusManager()
    
    // AppReview
    fileprivate(set) var appReviewManager = AppReviewManager()
    
    // Weather
    fileprivate(set) var weatherManager = WeatherManager()
    
    // Notification
    fileprivate(set) var notificationManager: NotificationManager!
    
    fileprivate(set) var currentConnection = Reachability.Connection.wifi
    fileprivate(set) var currentImageSizeOptimization = ImageSizeOptimization.optimizedSize
    fileprivate(set) var subscriptionVersion = SubscriptionVersion.free
    
    // Observable
    
    public var connectionObservable: Observable<Reachability.Connection> {
        return connectionSubject.asObservable()
    }
    
    public var colorChangeObservable: Observable<Void> {
        return colorChangeSubject.asObservable()
    }
    
    public var allCoreDataRemovedObservable:Observable<Void>{
        return allCoreDataRemovedSubject.asObservable()
    }
    
    public func userDidSetInternetDisconnectionWarning(needsToShow: Bool) {
        notificationManager.internetDisconnectionWarningTracker.value = needsToShow
    }
    
    public func userDidSetaccountStatusTracker(withVersion v: SubscriptionVersion) {
        accountStatusTracker.value = v.description
    }
    
    // Trackers
    
    // Trackers-Settings
    
    fileprivate(set) var accountStatusTracker = Tracker.init(initialValue: SubscriptionVersion.free.description)
    
    fileprivate(set) lazy var syncOnTracker = Tracker.init(
        forUserDefaultKey: .isSyncOn, defaultValue: true,
        additionalDidSet: {[unowned self] (isOn) in
            // check sync on cellular and if it's on, turn that off too.
            if !isOn {self.syncOnCelluarTracker.value = false}
            
            // check if there is any clousres thar aren't saved to backend.
//            let unsentReviews = Review.FetchUnsentReviews(from: self.mainContext)
            //TODO: create an operation array to take care of sending reviews to backend.
    })
    
    fileprivate(set) lazy var syncOnCelluarTracker = Tracker.init(
        forUserDefaultKey: .isSyncOverCelluarOn, defaultValue: true,
        additionalDidSet: {[unowned self] (isOn) in
            // when it's toggled, check if sync on is true. if it's not, turn this off too.
            guard isOn else {return}
            guard self.syncOnTracker.value else {
                waitFor(milliseconds: 100, completion: {[unowned self] in
                    self.syncOnCelluarTracker.value = false
                })
                return
            }
    })
    
    fileprivate(set) lazy var imageSizeOptimizationTracker = Tracker.init(
        forUserDefaultKey: .imageSizeOptimization, defaultValue: "optimizedSize",
        additionalDidSet: {[unowned self] (stringValue) in
            self.currentImageSizeOptimization = ImageSizeOptimization(rawValue: stringValue)!
        }, blockForInitalLoading: {[unowned self] (stringValue) in
            self.currentImageSizeOptimization = ImageSizeOptimization(rawValue: stringValue)!
    })
    
    // MARK: Static
    static var showAllLog = true {
        didSet {
            showViewControllerLog = showAllLog
            showObservationLog = showAllLog
            showActionLog = showAllLog
            showViewInstanceLog = showAllLog
            showObjectInstancesLog = showAllLog
            showNetworkJSONLog = showAllLog
            showCoredataJSONLog = showAllLog
        }
    }
    
    static var showViewControllerLog = true
    static var showObservationLog = true
    static var showActionLog = true
    static var showViewInstanceLog = true
    static var showObjectInstancesLog = true
    static var showNetworkJSONLog = true
    static var showCoredataJSONLog = true
    
    // when it's in the beginning stage or exceptions, it shouldn't allow the blurry vc to popup
    static var shouldAllowLockScreen = false
    
    // Send notification
    public func sendCalendarColorChangeNotification() {
        colorChangeSubject.onNext(())
    }
    
    public func sendAllCoreDataRemovedNotification() {
        allCoreDataRemovedSubject.onNext(())
        mainContext.reset()
        mainContext.refreshAllObjects()
    }
    
    // MARK: Fileprivate
    
    fileprivate let reachability = Reachability()!
    
    fileprivate let connectionSubject = PublishSubject<Reachability.Connection>()
    fileprivate let colorChangeSubject = PublishSubject<Void>()
    fileprivate let allCoreDataRemovedSubject = PublishSubject<Void>()
    
    fileprivate func setupConnectionObserver() {
        reachability.whenReachable = {[unowned self] (r)in
            guard self.currentConnection != r.connection else {return}
            self.connectionSubject.onNext(r.connection)
            self.currentConnection = r.connection
            
        }
        
        reachability.whenUnreachable = {[unowned self] (r)in
            guard self.currentConnection != r.connection else {return}
            self.connectionSubject.onNext(r.connection)
            self.currentConnection = r.connection
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            assertionFailure("failed to initialize the notification observer for connections.")
        }
    }
    
    
    
    
    
    
    
    
    
    
    
}

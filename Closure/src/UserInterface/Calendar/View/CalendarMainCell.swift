//
//  CalendarCell.swift
//  Closure
//
//  Created by James Kim on 7/29/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import RxSwift
import CodiumLayout

final class CalendarMainCell: CoreDataReusableTableViewCell {
    static var count = 0
    // UI
    fileprivate var dateLabel: UILabel!
    fileprivate var rateImageView: UIImageView!
    fileprivate var rateDescriptionLabel: UILabel!
    
    fileprivate var favoriteIndicatorImageView:UIImageView!
    fileprivate var noteIndicatorImageView: UIImageView!
    fileprivate var imageIndicatorImageView: UIImageView!
    
    override func prepareForReuse() {
        bag = nil
        isMarked = false
        subviews.forEach({$0.removeFromSuperview()})
    }
    
    // MARK: Public
    func setup(withObject object: Review, parentViewController: UIViewController, currentIndexPath: IndexPath, userInfo: [String : Any]?) {
        currentReview = object
        let appStatus = AppStatus.unwrapInstanceFrom(userInfo: userInfo)
        
        setupUI()
        configure(withReview: object)
        setupObserver(from: appStatus)
        markOrUnmarkBorder(shouldMark: false)
    }
    
    public func configure(withReview r:Review) {
        dateLabel.text = r.date.format(with: currentLanguage.toDayDateFormat)
        rateImageView.image = r.rate.image
        rateDescriptionLabel.text = r.rate.description
        
        // for now, let's hide them all.
//        favoriteIndicatorImageView.isHidden = !r.isFavorite
//        noteIndicatorImageView.isHidden = r.thoughts == nil
//        imageIndicatorImageView.isHidden = r.image == nil
        favoriteIndicatorImageView.isHidden = true
        noteIndicatorImageView.isHidden = true
        imageIndicatorImageView.isHidden = true
    }
    
    public func markOrUnmarkBorder(shouldMark:Bool) {
        // this is designed to make a border when user selects a day in calendar so users know what day they're looking at
        isMarked = shouldMark
        setBorder(color: shouldMark ? currentReview.rate.currentColor : .clear, width: 2.5)
    }
    
    // MARK: Actions
    
    fileprivate lazy var observeLanguageChange:(Language)->Void = {[weak self] (language) in
        /**
         it crahses one time... it must not crash because this observation has to be flushed when this cell is prepared for reuse.
         but let's just put some conditions so that it might be okay??
         */
        guard let target = self, target.currentReview != nil else {return}
        target.rateDescriptionLabel.change(toString: target.currentReview.rate.description)
        target.dateLabel.text = target.currentReview.date.format(with: language.toDayDateFormat)
    }
    
    fileprivate lazy var observeRateColorChange:()->Void = {[unowned self] in
        guard self.isMarked else {return}
        self.markOrUnmarkBorder(shouldMark: true)
    }
    
    // MARK: Fileprivate
    fileprivate weak var currentReview: Review!
    
    fileprivate var bag: DisposeBag!
    fileprivate var isMarked = false
    
    fileprivate func setupObserver(from appStatus:AppStatus) {
        bag = DisposeBag()
        appStatus.languageManager.changeLanguageObservable.subscribe(onNext: observeLanguageChange).disposed(by: bag)
        appStatus.colorChangeObservable.subscribe(onNext: observeRateColorChange).disposed(by: bag)
    }
}


extension CalendarMainCell {
    fileprivate func setupUI() {
        dateLabel = UILabel.create(text: "10-10", textAlignment: .left, textColor: .black, fontSize: 20, boldFont: true, numberOfLine: 1)
        
        favoriteIndicatorImageView = UIImageView.create(withImageName: "favorite_indicator")
        
        noteIndicatorImageView = UIImageView.create(withImageName: "note_indicator")
        
        imageIndicatorImageView = UIImageView.create(withImageName: "image_indicator")
        
        rateImageView = UIImageView.create(withImage: UIImage(), contentMode: .scaleAspectFill)
        
        rateDescriptionLabel = UILabel.create(text: "Description", textAlignment: .center, fontSize: 15)
        
        let rightArrowImageView = UIImageView.create(withImageName: "right_arrow")
        
        let group: [UIView] = [dateLabel, favoriteIndicatorImageView, noteIndicatorImageView, imageIndicatorImageView, rateImageView, rateDescriptionLabel,rightArrowImageView]
        group.forEach(addSubview(_:))
        
        constraint(dateLabel, favoriteIndicatorImageView, noteIndicatorImageView, imageIndicatorImageView, rateImageView, rateDescriptionLabel,rightArrowImageView) { (_dateLabel, _favoriteIndicatorImageView, _noteIndicatorImageView, _imageIndicatorImageView, _rateImageView, _rateDescriptionLabel,_rightArrowImageView) in
            
            _dateLabel.centerY.equalToSuperview()
            _dateLabel.leading.equalToSuperview().offset(15)
            
            _favoriteIndicatorImageView.centerY.equalTo(dateLabel)
            _favoriteIndicatorImageView.left.equalTo(dateLabel.snp.right).offset(5)
            _favoriteIndicatorImageView.sizeEqualTo(width: 20, height: 20)
            
            _imageIndicatorImageView.centerY.equalTo(dateLabel)
            _imageIndicatorImageView.left.equalTo(favoriteIndicatorImageView.snp.right).offset(5)
            _imageIndicatorImageView.sizeEqualTo(width: 23, height: 23)
            
            _noteIndicatorImageView.centerY.equalTo(dateLabel)
            _noteIndicatorImageView.left.equalTo(imageIndicatorImageView.snp.right).offset(5)
            _noteIndicatorImageView.sizeEqualTo(width: 20, height: 20)
            
            _rateImageView.top.equalToSuperview().offset(5)
            _rateImageView.centerY.equalTo(dateLabel).offset(-10)
            _rateImageView.right.equalTo(rightArrowImageView.snp.left).offset(-15)
            _rateImageView.sizeEqualTo(width: 60, height: 60)
            
            _rateDescriptionLabel.centerX.equalTo(rateImageView)
            _rateDescriptionLabel.top.equalTo(rateImageView.snp.bottom).offset(2.5)
            _rateDescriptionLabel.bottom.equalToSuperview().offset(-5)
            
            _rightArrowImageView.centerY.equalToSuperview()
            _rightArrowImageView.right.equalToSuperview().offset(-10)
            _rightArrowImageView.sizeEqualTo(width: 30, height: 30)
            
        }
        
        
    }
}

//
//  CalendarMainViewController.swift
//  Closure
//
//  Created by James Kim on 7/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CoreData
import FSCalendar
import CodiumLayout

final class CalendarMainViewController: DefaultViewController {
    
    //UI
    fileprivate var calendar: FSCalendar!
    fileprivate var tableView:UITableView!
    
    fileprivate var emptyDescriptionView: UIView!
    fileprivate var emptyDescriptionLabel: UILabel!
    init(status:AppStatus) {
        self.appStatus = status
        super.init(nibName: nil, bundle: nil)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        setupVC()
        setupObserver()
        setupTableView()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    // MARK: - Actions
    @objc fileprivate func colorButtonClicked() {
        logInfo(type: .action)
        
        //Present the color picker view controller
        let target =  ColorPickerViewController()
        presentPopup(targetVC: target, cancelButtonTitle: "Cancel".localized, cancelAction: nil, okButtonTitle: "Save".localized, okAction: {[unowned self] in
            target.saveCurrentColorsToUserDefaults()
            self.calendar.reloadData()
            // send the notification to notify that colors are changed so the charts should be reloaded.
            self.appStatus.sendCalendarColorChangeNotification()
        }, completion: nil, panGestureDismissal: false)
    }

    fileprivate func userDidSwipeCalendar(_ calendar: FSCalendar) {
        /**
         when user swipes a calendar into a different month, we reset the cell for last index path for selected day and then remove the value as well.
         */
        logInfo(type: .action)
        
        tableViewDataSource.reconfigureFetchRequest { (request) in
            request.predicate = Review.getPredicateForSelectedMonth(calendar.currentPage)
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
            request.returnsObjectsAsFaults = false
        }
        
        checkIfLastIndexPathForSelectedDayInCalendarNotNillAndRemoveBorder()
        
        guard tableViewDataSource.fetchedObjects?.count != 0 else {return}
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
        
    }
    
    fileprivate func userDidSelectDay(selectedDate: Date) {
        /**
         ------------------------- LOGIC RUN DOWN ------------------------------
         0. when they select a day in calendar, calendar spits out the date.
         1. we find the target review with given date from the fetched object array from data source.
         2. we look for a indexpath from tableview data source(ns fetched controller) with the review found in the previous step.
         3. we scroll tableview to the indexpath
         
         4. set border color to current review's color to make it look different. if there is a cell previously selected by user, we need to remove border of it.
         5. if a cell is not visible, it needs to give tableview sometime to scroll to a target indexpath. but if it's visible, we can mark it right away.
         */
        
        guard let array = tableViewDataSource.fetchedObjects else {return}
        guard let targetReview = array.first(where: {$0.date.isSameDay(date: selectedDate)}) else {return}
        let targetIndexPath = tableViewDataSource.indexPath(for: targetReview)
        tableView.scrollToRow(at: targetIndexPath, at: .none, animated: true)
        
        checkIfLastIndexPathForSelectedDayInCalendarNotNillAndRemoveBorder()
        lastIndexPathForSelectedDayInCalendar = targetIndexPath

        //5
        if let targetCell = self.tableViewDataSource.cellForRow(at: targetIndexPath) {
             targetCell.markOrUnmarkBorder(shouldMark: true)
        } else {
            waitFor(milliseconds: 250, completion: {[unowned self] in
                /**
                 maybe the target cell is not invisible. if so, we do nothing.
                 */
                guard let targetCell = self.tableViewDataSource.cellForRow(at: targetIndexPath) else {return}
                targetCell.markOrUnmarkBorder(shouldMark: true)
            })
        }
        
        
    }
    
    fileprivate lazy var observeReviewChanges:([Review])->Void = {[unowned self] (reviews) in
        
        /**
         ------------------------- LOGIC RUN DOWN ------------------------------
         When user swipes on collection view
         0. user swipe on calendar collection view
         1. userDidSwipeCalendar is triggered
         2. tableview data source fetch data based on current month from calendar and then send them out
         3. this method observes the said review array and sort data array in order to present them in calendar
         4. reload calendar
         
         When user adds/removes a new review
         0. tableview data source listens to context did change notification.
         1. when user adds/removes a review, it triggers didchange method on ns fetched controller.
         2. at the end of each type(insert, delete), we send out new review array.
         3. this method observes the said review array and sort data array in order to present them in calendar
         4. reload calendar
         */
        
        logInfo("Reviews from tableview data source changed", type: .observation)
        self.reviewDateDict = reviews.convertToDictionary(configureKey: {$0.date.toYearToDateString(type:.current)}) as! [String : Review]
        self.calendar.reloadData()
        
        self.emptyDescriptionView.isHidden = reviews.count != 0
    }
    
    @objc fileprivate func userDidTapTodayButton() {
        /**
         when user taps on today button, we go to the month of today.
         */
        calendar.select(Date(), scrollToDate: true)
        calendar.deselect(Date())
        
        userDidSelectDay(selectedDate: Date())
    }
    
    fileprivate lazy var observeLanguageChange:(Language)->Void = {[unowned self] (_) in
        // When language changes to a different one, the calendar title and week description labels have to change.
        guard self.isViewLoaded else {return}
        self.navigationItem.title = "Calendar".localized
        self.todayButton.title = "Today".localized
        self.emptyDescriptionLabel.text = "It looks empty for the selected month.".localized
        self.changeCalendarLabelsToCurrentLanguage()
    }
    
    fileprivate func userDidSelectRow(at indexPath:IndexPath) {
        let object = tableViewDataSource.objectAtIndexPath(indexPath)
        let targetVC = FeedDetailViewController(status: appStatus, review: object)
        presentDefaultVC(targetVC: targetVC, userInfo: nil)
    }
    
    fileprivate lazy var observeAllCoreDataRemoved:()->Void = {[weak self] in
        guard let target = self else {return}
        target.userDidSwipeCalendar(target.calendar)
    }
    
    // MARK: - Fileprivate
    fileprivate weak var appStatus:AppStatus!
    
    fileprivate var tableViewDataSource: CoreDataTableViewDataSource<CalendarMainCell>!
    
    fileprivate let bag = FilterDisposeBag.defaultBag
    
    fileprivate var todayButton: UIBarButtonItem!
    
    fileprivate var lastIndexPathForSelectedDayInCalendar:IndexPath!
    fileprivate var reviewDateDict = [String:Review]()
    fileprivate let calendarID = "calendarID"
    
    
    fileprivate func setupVC() {
        view.backgroundColor = .white
        navigationItem.title = "Calendar".localized
        let markerImage = UIImage(named: "color_marker")!
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: markerImage.resizeImage(width: 25, height: 25), style: .plain, target: self, action: #selector(colorButtonClicked))
        
        todayButton = UIBarButtonItem(title: "Today".localized, style: .plain, target: self, action: #selector(userDidTapTodayButton))
        navigationItem.rightBarButtonItem = todayButton
    }
    
    fileprivate func setupObserver() {
        appStatus.languageManager.changeLanguageObservable.subscribe(onNext: observeLanguageChange).disposed(by: bag)
        
        appStatus.allCoreDataRemovedObservable.subscribe(onNext: observeAllCoreDataRemoved).disposed(by: bag)
    }

    
    fileprivate func setupTableView() {
        tableView.delegate = self
        
        let request = Review.newFetchRequest
        request.predicate = Review.getPredicateForSelectedMonth(Date())
        request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        request.returnsObjectsAsFaults = false

        let frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: appStatus.mainContext, sectionNameKeyPath: nil, cacheName: nil)
 
        tableViewDataSource = CoreDataTableViewDataSource<CalendarMainCell>.init(
            tableView: tableView,
            fetchedResultsController: frc,
            parentViewController: self,
            userInfo: appStatus.toUserInfo,
            observeFetchResult: observeReviewChanges)
    }

    fileprivate func checkIfLastIndexPathForSelectedDayInCalendarNotNillAndRemoveBorder() {
        if lastIndexPathForSelectedDayInCalendar != nil {
            guard let previousCell = tableViewDataSource.cellForRow(at: lastIndexPathForSelectedDayInCalendar) else {
                return
            }
            previousCell.markOrUnmarkBorder(shouldMark: false)
            lastIndexPathForSelectedDayInCalendar = nil
        }
    }
    
    fileprivate func changeCalendarLabelsToCurrentLanguage() {
        //Header
        self.calendar.appearance.headerDateFormat = currentLanguage.toMonthDateFormat
        
        //weekdays
        let pool = ["Sun", "Mon", "Tue", "Wed", "Thu","Fri","Sat"]
        for (index, label) in calendar.calendarWeekdayView.weekdayLabels.enumerated() {
            label.text = pool[index].localized
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension CalendarMainViewController: FSCalendarDataSource, FSCalendarDelegateAppearance {
    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: calendarID, for: date, at: position)
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        guard let review = reviewDateDict[date.toYearToDateString(type: .current)] else {return nil}
        return review.rate.currentColor
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
        return nil
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        userDidSelectDay(selectedDate: date)
        calendar.deselect(date)
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        userDidSwipeCalendar(calendar)
    }
}

extension CalendarMainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        userDidSelectRow(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension CalendarMainViewController {
    fileprivate func setupUI() {
        //set up callendar
        calendar = FSCalendar()
        calendar.locale = Locale.current
        calendar.delegate = self
        calendar.dataSource = self
        calendar.register(FSCalendarCell.self, forCellReuseIdentifier: calendarID)
        calendar.today = Date()
        calendar.allowsSelection = true
        
        calendar.appearance.headerTitleFont = UIFont.create(forKey: .helveticaNeueInBold, size: 15)
        calendar.appearance.headerTitleColor = .mainBlue
        calendar.appearance.weekdayTextColor = .mainBlue
        calendar.appearance.todayColor = nil
        calendar.appearance.titleTodayColor = .black
        changeCalendarLabelsToCurrentLanguage()
        
        tableView = UITableView.create()
        
        emptyDescriptionView = UIView.create(withColor: .white, alpha: 1)
        
        emptyDescriptionLabel = UILabel.create(text: "It looks empty for the selected month.".localized, textAlignment: .center, fontSize: 15)
        let emptyBoxImageView = UIImageView.create(withImageKey: .emptyBox)
        
        let groupForEmptyDescriptionView:[UIView] = [emptyDescriptionLabel, emptyBoxImageView]
        groupForEmptyDescriptionView.forEach(emptyDescriptionView.addSubview(_:))
        
        constraint(emptyBoxImageView, emptyDescriptionLabel) { (_emptyBoxImageView,_emptyDescriptionLabel) in
            _emptyBoxImageView.centerY.equalToSuperview().offset(-80)
            _emptyBoxImageView.centerX.equalToSuperview()
            _emptyBoxImageView.sizeEqualTo(width: 120, height: 120)
            
            _emptyDescriptionLabel.centerX.equalToSuperview()
            _emptyDescriptionLabel.centerY.equalToSuperview().offset(10)
        }
        
        let group:[UIView] = [calendar, tableView,emptyDescriptionView]
        group.forEach(view.addSubview(_:))
        
        constraint(calendar, tableView,emptyDescriptionView) {[unowned self] (_calendar, _tableView, _emptyIndicatorView) in
            _calendar.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
            _calendar.leading.trailing.equalToSuperview()
            _calendar.width.equalTo(self.view.bounds.width)
            _calendar.height.equalTo(self.view.bounds.width-90)
            
            _tableView.top.equalTo(calendar.snp.bottom)
            _tableView.leading.trailing.bottom.equalToSuperview()
            
            _emptyIndicatorView.left.top.right.bottom.equalTo(tableView)
        }

    }
}


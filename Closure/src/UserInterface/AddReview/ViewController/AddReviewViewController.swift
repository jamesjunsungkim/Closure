//
//  AddReviewController.swift
//  Closure
//
//  Created by James Kim on 7/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout
import RxSwift
import TPKeyboardAvoiding
import PKHUD
import CoreData
import StepSlider

final class AddReviewViewController: DefaultViewController {
    
    //UI
    fileprivate var scrollView:UIScrollView!
    fileprivate var headTitle: UILabel!
    fileprivate var weatherView:UIView!
    fileprivate var weatherImageView:UIImageView!
    fileprivate var addButton: UIButton!
    
    fileprivate var yesterdayView: UIView!
    fileprivate var yesterdayGoal: UILabel!
    fileprivate var badView: ReviewView!
    fileprivate var goodView: ReviewView!
    fileprivate var goalView: ReviewView!
    
    fileprivate var rateImageView: UIImageView!
    fileprivate var rateSlider: StepSlider!
    fileprivate var rateDescription: UILabel!
    
    fileprivate var favoriteToggle: UISwitch!
    fileprivate var finishButton: UIButton!
    
    init(appStatus: AppStatus, targetDate:Date, review:Review?) {
        /**
         Now it supports editting review.
         */
        self.appStatus = appStatus
        self.targetDate = targetDate
        self.currentReview = review
        if review != nil {
            currentReview!.shouldOverwrite = true
        }
        super.init(nibName: nil, bundle: nil)
        
        //ViewDidLoad
        enterViewControllerMemoryLog(self)
        setupUI()
        setupVC()
        addTargets()
        updateUI()
        setupObservers()
        checkIfYesterdayReivewAndUpdateConstraints()
        checkIfLocationAuthorizedAndGetWeather()
        
//        currentNonCDWeather = NonCDWeather(temperature: 32, condition: 520, city: "Chatuchak", weatherIconName: "shower3", weatherDescription: "light intensity shower rain", countryCodeInEnglish: "TH", countryName: "THailand")
//        observeWeatherResult(.success(currentNonCDWeather))
    }
    
    deinit {
        // if users don't create a review, we just roll back so that weather is removed since it's not associated with anything.
        appStatus.mainContext.rollback()
        leaveViewControllerMomeryLog(self)
    }
    
    // MARK: - Actions
    
    @objc fileprivate func addBtnClicked() {
        logInfo(type: .action)
        let target = AddThoughtAndPhotoViewController(withThought: thoughtString, image: chosenImage)
        let conditionForEditting = thoughtString != nil || chosenImage != nil
        
        presentPopup(targetVC: target,
                     cancelButtonTitle: "Cancel".localized,
                     cancelAction: nil,
                     okButtonTitle: conditionForEditting ? "Edit".localized : "Save".localized,
                     okAction: {[unowned self]in
                        self.thoughtString = target.thoughtString
                        self.chosenImage = target.selectedImage
            },
                     completion: nil)
        
    }
    
    @objc fileprivate func finishBtnClicked() {
        logInfo(type: .action)
        let good = goodView.textFieldContent
        let bad = badView.textFieldContent
        let goal = goalView.textFieldContent
        
        guard !good.isEmpty || !bad.isEmpty || !goal.isEmpty else {
            /**
             // FIXME: I think it doesn't need to obsserve typing because when user clicks on this button, we could get those text from it.. but let's just go with it for now
             isValid is bool that indicates if textfield is empty
             */
            
            presentDefaultError(message: "It can not be all empty.".localized, okAction: nil)
            return
        }
        
        createOrEditAndThenSaveToServer(
            appStatus: appStatus, wordCountManager: appStatus.wordCountManager, good: good, bad: bad, goal: goal, ratingValue: currentRate.rawValue, date: targetDate, imageData: chosenImage?.jpegData, thoughts: thoughtString, isFavorite: favoriteToggle.isOn, nonCDweather: currentNonCDWeather, path: Path.toReview(withUID: appStatus.currentUser.uid, date: targetDate.toYearToDateString(type:.current)).stringValue)
    }
    
    @objc fileprivate func rateSliderValueChanged(sender: StepSlider) {
        rateImageView.image = currentRate.image
        rateDescription.text = currentRate.description
    }
    
    @objc fileprivate func userDidTapWeatherView() {
        let manager = appStatus.weatherManager
        guard let status = manager.currentStatus else {assertionFailure();return}
        let needsToGetWeather = (currentNonCDWeather == nil)
        
        /**
         we can put this condition at the top because if it turns out to be false, it means it was actually successful to receive data from server which we don't need to care about.
         
         if we fail to fetch weather from server, we set isWeatherAvailable false. and if user tabs on the weather view AFTER failing to get data, we just show the pop up.
         
         There are two layers of protections from excecuting this method more than once.
         0. isCurrentlyFetchingWeather is an indicator that when it's in the middle of fetching, it's true. when it's done, it's set false.
         
         1. isWeatherAvailable is indicator where it's set false when the return value is invalid so there is no point in trying to fetch it again.
         */
        
        guard needsToGetWeather else {
            //it already fetches weather successfully. in this case, we segue to the weather detail view controller.
            let targetVC = WeatherDetailViewController(nonCDWeather: currentNonCDWeather)
            presentPopupWithoutAnyButton(targetVC: targetVC)
            return
        }
        
        guard !manager.isCurrentlyFetchingWeather else {
            presentDefaultError(message: "It's currently fetching weather information.".localized + "\n" + "Please be patient with us a little bit".localized, okAction: nil)
            return
        }
        
        guard isWeatherAvailable else {
            // it means the app coudln't get the weather from server.
            presentDefaultError(message: "Weather currently unavailable. Please try it again later".localized, okAction: nil)
            return
        }
        
        
        switch status {
        case .authorizedWhenInUse:
            manager.startUpdatingLocation()
            
        case .notDetermined:
            manager.askForLocationPermission()
            
        case .denied:
            performOnMain {[unowned self] in
                self.presentDefaultAlert(withTitle: "Permission denied".localized, message: "In order to get weather, We need you to allow it to access your location.".localized, okAction: {
                    self.openAppSettings()
                }, cancelAction: nil)
            }
        case .phoneLocationServiceDisabled:
            performOnMain {[unowned self] in
                self.presentDefaultAlert(withTitle: "Permission denied".localized, message: "It seems like location service is not enabled. Please enable it to get weather.".localized, okAction: {
                    self.openAppSettings()
                }, cancelAction: nil)
            }
        default:break
        }
    }
    
    fileprivate lazy var observeWeatherResult:(CompletionResult<NonCDWeather?, Error>)->Void = {[weak self] (fetchResult) in
        logInfo(type: .observation)
        /**
         let's conver it to weather only WHEN user clicks on the save button.
         */
        guard let target = self else {return}
        switch fetchResult {
        case .success(let r):
            performOnMain {
                guard let result = r else {
                    //weather is unavailable.
                    target.isWeatherAvailable = false
                    target.weatherImageView.image = UIImage.create(forKey: .questionMark)
                    return
                }
                target.weatherImageView.image = UIImage(named:result.weatherIconName)
                target.currentNonCDWeather = result
            }
        case .failure(let e):
            break
        }
        
    }
    
    fileprivate lazy var observeUserTapOnReturnButton:(ReviewView)->Void = {[unowned self] (targetView) in
        
        switch targetView {
        case self.badView:
            waitFor(milliseconds: 150, completion: {
                self.goodView.textField.becomeFirstResponder()
            })
        case self.goodView:
            waitFor(milliseconds: 150, completion: {
                self.goalView.textField.becomeFirstResponder()
            })
        case self.goalView:
            self.view.endEditing(true)
        default:assertionFailure()
        }
    }
    
    // MARK: - Fileprivate
    fileprivate weak var appStatus: AppStatus!
    fileprivate weak var currentReview: Review?
    
    fileprivate var targetDate: Date!
    fileprivate var thoughtString: String?
    fileprivate var chosenImage: UIImage?
    
    fileprivate let scrollViewHeightWithoutYesterdayView:CGFloat = 730
    fileprivate let yesterdayViewHeight:CGFloat = 60
    
    fileprivate var isWeatherAvailable = true
    
    fileprivate var currentNonCDWeather:NonCDWeather!
    
    fileprivate lazy var needToFetchWeather = (targetDate != nil && targetDate.isSameDay(date: Date()))
    
    fileprivate let bag = DisposeBag()

    fileprivate var currentRate: Rate {
        let totalNumberOfRates:UInt = 4
        guard let target = Rate.init(rawValue: Int16(totalNumberOfRates-rateSlider.index)) else {assertionFailure();return Rate.init(rawValue: 0)!}
        return target
    }
    
    fileprivate func setupVC() {
        view.backgroundColor = UI.viewBackgroundColor
    }
    
    fileprivate func addTargets() {
        finishButton.addTarget(self, action: #selector(finishBtnClicked), for: .touchUpInside)
        addButton.addTarget(self, action: #selector(addBtnClicked), for: .touchUpInside)
        rateSlider.addTarget(self, action: #selector(rateSliderValueChanged(sender:)), for: .valueChanged)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(userDidTapWeatherView))
        weatherView.addGestureRecognizer(tap)
    }

    fileprivate func updateUI() {
        // review views.
        let badDescription = ReviewDescription(title: "Rooms for improvements".localized, textPlaceHolder: "What could've been better?".localized, userDefault: .forImprovements, content: currentReview?.bad)
        let goodDescription = ReviewDescription(title: "Achievements".localized, textPlaceHolder: "What good did you do?".localized, userDefault: .forAchievemnets,content:currentReview?.good)
        let goalDescription = ReviewDescription(title: "Main focus for tomorrow".localized, textPlaceHolder: "What do you need to keep in mind?".localized, userDefault: .forGoals,content:currentReview?.goal)

        badView.configure(withDescription: badDescription)
        goodView.configure(withDescription: goodDescription)
        goalView.configure(withDescription: goalDescription)
        
        // when it's editing mode
        guard let review = currentReview else {return}
        rateImageView.image = review.rate.image
        rateDescription.text = review.rate.description
        rateSlider.index = UInt(4 - review.rate.rawValue)
        
        favoriteToggle.isOn = review.isFavorite
        
        finishButton.changeTo(title: "Edit".localized)
        
        thoughtString = review.thoughts
        chosenImage = review.image
    }
    
    fileprivate func setupObservers() {
        // observe weather result
        appStatus.weatherManager.weatherFetchObservable.subscribe(onNext: observeWeatherResult).disposed(by: bag)
        
        // observe user tap return button on textfield
        [goodView, goalView, badView].forEach({[unowned self] in
            $0!.textFieldReturnObservable.subscribe(onNext: observeUserTapOnReturnButton).disposed(by: self.bag)})
    }
    
    fileprivate func postReviewToServer(withReview r: Review, path:String, completion: (()->Void)? = nil) {
        guard appStatus.syncOnTracker.value else {completion?();return}
        
        if !appStatus.syncOnCelluarTracker.value {
            guard appStatus.currentConnection == .wifi else {completion?();return}
        }
        
        performOnBackground {[weak self] in
            self?.appStatus.networkManager.postJSON(value: r.toDictionary, toPath: path, completion: { (result) in
                switch result {
                case .success(_):
                    r.didFinishUpload()
                    completion?()
                case .failure(_): completion?()
                }
            })
        }
    }
    
    fileprivate func checkIfYesterdayReivewAndUpdateConstraints() {
        let moc = appStatus.mainContext
        
        guard let r = Review.findOrFetchByDate(from: moc, dateString: targetDate.addDay(value: -1).toYearToDateString(type:.current)), !r.goal.isEmpty else {
            
            addButton.snp.remakeConstraints {(make) in
                make.trailing.equalTo(yesterdayView)
                make.bottom.equalTo(yesterdayView.snp.top).offset(10)
                make.sizeEqualTo(width: 25, height: 25)
            }
            return
        }
        yesterdayGoal.text = r.goal
        
        yesterdayView.snp.updateConstraints {[unowned self] (make) in
            make.height.equalTo(self.yesterdayViewHeight)
        }
        
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: scrollViewHeightWithoutYesterdayView + yesterdayViewHeight)
        
    }
    
    
    // MARK: Testing
    public func createOrEditAndThenSaveToServer(appStatus:AppStatus, wordCountManager: WordCountManager, good:String, bad:String,goal:String, ratingValue:Int16, date: Date, imageData:Data?, thoughts:String?, isFavorite: Bool, nonCDweather:NonCDWeather?, path:String, completion: ((Review)->Void)? = nil) {
        /**
         Everytime a review is created, we check current count of opening the app and ask to review.
         */
        let innerCompletionBlock:(CompletionResult<Review,Error?>)->Void = {[weak self] (result) in
            guard let target = self else {return}
            switch result {
            case .success(let r):
                completion?(r)
                target.dismiss(animated: true, completion: {
                    self?.appStatus.appReviewManager.checkAndAskReview()
                })
                
            case .failure(let e):
                if let error = e {
                    target.presentDefaultError(message: error.localizedDescription, okAction: nil)
                } else {
                    target.presentDefaultError(message: "Connection is unstable".localized, okAction: nil)
                }
            }
        }
        
        guard currentReview == nil else {
            // it's editing mode so we don't need to go through list to find it.
            let weather = nonCDweather?.toWeather(into: appStatus.mainContext)
            guard checkIfNeedToSaveCurrentReview(good: good, bad: bad, goal: goal, ratingValue: ratingValue, date: date, imageData: imageData, thoughts: thoughts, isFavorite: isFavorite) else {
                presentDefaultError(message: "It looks like you haven't changed anything yet.".localized, okAction: nil)
                return
            }
            
            self.editReviewAndSaveToServer(toPath: path, review: currentReview!, appStatus: appStatus, good: good, bad: bad, goal: goal, ratingValue: ratingValue, date: date, imageData: imageData, thoughts: thoughts, isFavorite: isFavorite, weather: weather, editCompletion: innerCompletionBlock)
            return
        }
        
        let compressedImageData = appStatus.diskStatusManager.isFreeSpaceMoreThan500MB ? imageData : imageData?.resizeImageData(toMB: 1)
        
        let r = Review.findByDateOrCreate(into: appStatus.mainContext, wordCountManager:wordCountManager, good: good, bad: bad, goals: goal, ratingValue: ratingValue, date: date, imageData: compressedImageData, thoughts: thoughts, isFavorite: isFavorite, nonCDweather: nonCDweather)
        
        if r.shouldOverwrite {
            presentDefaultAlert(withTitle: "Alert".localized, message: "The review exists for the chosen date.".localized + "\n" + "Do you want to replace it?".localized, okAction: {[unowned self]in
                
                let weather = nonCDweather?.toWeather(into: appStatus.mainContext)
                self.editReviewAndSaveToServer(toPath: path, review: r, appStatus: appStatus, good: good, bad: bad, goal: goal, ratingValue: ratingValue, date: date, imageData: imageData, thoughts: thoughts, isFavorite: isFavorite, weather: weather, editCompletion: innerCompletionBlock)
                }, cancelAction: nil)
        } else {
            r.uploadReviewToServer(appStatus: appStatus, path: path, completion: innerCompletionBlock)
        }
    }
    
    public func editReviewAndSaveToServer(toPath p: String, review:Review, appStatus:AppStatus, good:String, bad:String, goal:String, ratingValue:Int16, date:Date, imageData:Data?, thoughts:String?, isFavorite: Bool, weather:Weather?, editCompletion:((CompletionResult<Review,Error?>)->Void)? = nil) {
        guard review.shouldOverwrite else {assertionFailure(); return}
        
        review.edit(appStatus: appStatus, withGood: good, bad: bad, goals: goal, ratingValue: ratingValue, date: date , imageData: imageData, thoughts: thoughts, isFavorite: isFavorite, weather: weather)
        
        review.uploadReviewToServer(appStatus: appStatus, path: p, completion: editCompletion)
    }
    
    fileprivate func checkIfLocationAuthorizedAndGetWeather() {
        /**
         if it's denied or phone location service disabled, just do nothing.
         when user clicks on weather view, do a proper action based on it.
         
         if it's not for today, let's not show it.
         */
        guard needToFetchWeather else {return}
        
        let manager = appStatus.weatherManager
        let status = manager.checkIfWeatherServiceAvailable()
        
        switch status {
        case .authorizedWhenInUse:
            manager.startUpdatingLocation()
        case .notDetermined:
            manager.askForLocationPermission()
        case .denied:break
        case .phoneLocationServiceDisabled:break
        default:break
        }
    }
    
    fileprivate func checkIfNeedToSaveCurrentReview(good:String, bad:String,goal:String, ratingValue:Int16, date: Date, imageData:Data?, thoughts:String?, isFavorite: Bool) -> Bool {
        guard let review = currentReview else {assertionFailure();return false}
        return (review.good != good) || review.bad != bad || review.rateValue != ratingValue || review.imageData != imageData || review.thoughts != thoughts || review.isFavorite != isFavorite
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension AddReviewViewController {
    fileprivate func setupUI() {
        
        scrollView = TPKeyboardAvoidingScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.alwaysBounceVertical = true
        let screenSize = UIScreen.main.bounds
        scrollView.contentSize = CGSize(width: screenSize.width, height: scrollViewHeightWithoutYesterdayView)
        
        view.addSubview(scrollView)
        
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        let targetFontSize:CGFloat = view.bounds.width > 400 ? UI.Component.titleSize: UI.Component.smallTitleSize
        
        headTitle = UILabel.create(text: "How was your day?".localized, textAlignment: .center, textColor: .black, fontSize: targetFontSize + 3.toCGFloat, boldFont: false, numberOfLine: 1)
        
        weatherView = UIView.create(withColor: .white, alpha: 1)
        weatherView.isHidden = !needToFetchWeather
        
        let weatherDescriptionLabel = UILabel.create(text: "Weather".localized, textAlignment: .left, textColor: .black, fontSize: 15, boldFont: false, numberOfLine: 0)
        
        weatherImageView = UIImageView.create(withBackgroundColor: .clear)
        
        ([weatherDescriptionLabel, weatherImageView] as [UIView]).forEach(weatherView.addSubview(_:))
        
        constraint(weatherDescriptionLabel, weatherImageView) { (_weatherDescriptionLabel, _weatherImageView) in
            _weatherDescriptionLabel.left.equalToSuperview().offset(3)
            _weatherDescriptionLabel.centerY.equalToSuperview()
            
            _weatherImageView.sizeEqualTo(width: 23.5, height: 23.5)
            _weatherImageView.centerY.equalToSuperview()
            _weatherImageView.left.equalTo(weatherDescriptionLabel.snp.right).offset(5)
            _weatherImageView.right.equalToSuperview()
        }

        addButton = UIButton.create(withImageName: "plus_note")
        
        yesterdayView = UIView.create(withColor: .clear, alpha: 1)
        yesterdayView.setCornerRadious(value: 5)
        yesterdayView.setBorder(color: .mainBlue, width: 1)
        
        let yesterdayTitle = UILabel.create(text: "Yesterday, you said...".localized, textAlignment: .left, fontSize: targetFontSize)
        yesterdayGoal = UILabel.create(text: "Does it work? I assume it works just fine.", textAlignment: .left, fontSize: UI.Component.contentSize)
        yesterdayGoal.textColor = UIColor.init(white: 0.1, alpha: 0.7)
        
        let groupForYesterday:[UIView] = [yesterdayTitle, yesterdayGoal]
        groupForYesterday.forEach(yesterdayView.addSubview(_:))
        
        constraint(yesterdayTitle, yesterdayGoal) { (_yesterdayTitle, _yesterdayGoal) in
            _yesterdayTitle.leading.equalToSuperview().offset(15)
            _yesterdayTitle.top.equalToSuperview().offset(5)
            
            _yesterdayGoal.leading.equalTo(yesterdayTitle)
            _yesterdayGoal.top.equalTo(yesterdayTitle.snp.bottom).offset(5)
        }
        
        badView = ReviewView()
        goodView = ReviewView()
        goalView = ReviewView()
        
        let ratingTitleLabel = UILabel.create(text: "Rate your day".localized, textAlignment: .left, fontSize: 17)
        
        rateImageView = UIImageView.create(withImageName: "rate_ok")
        rateImageView.contentMode = .scaleAspectFill
        
        rateSlider = StepSlider()
        rateSlider.maxCount = 5
        rateSlider.setIndex(2, animated: false)
        rateSlider.tintColor = .mainBlue
        rateSlider.trackCircleRadius = 0
        rateSlider.sliderCircleRadius = 6
        rateSlider.sliderCircleColor = .mainBlue
        
        rateDescription = UILabel.create(text: "Nah, okay".localized, textAlignment: .right, fontSize: 15)
        
        let favoriteTitleLabel = UILabel.create(text: "Label it as favorite".localized, textAlignment: .left, fontSize: 17)
        favoriteToggle = UISwitch.create(tintColor: .mainBlue, onTintColor: .mainBlue)
        
        finishButton = UIButton.create(title: "Finish".localized, titleColor: UI.Button.titleColor, fontSize: UI.Button.fontSize, backgroundColor: UI.Button.backgroundColor)
        finishButton.setCornerRadious(value: 10)
 
        let group: [UIView] = [headTitle, weatherView, addButton, yesterdayView, badView, goodView, goalView, ratingTitleLabel,rateImageView,rateDescription, rateSlider,favoriteTitleLabel,favoriteToggle, finishButton]
        group.forEach(scrollView.addSubview(_:))
        
        constraint(headTitle,weatherView, addButton, yesterdayView, badView, goodView, goalView, ratingTitleLabel, rateImageView,rateDescription, rateSlider) { (_headTitle, _weatherView ,_addButton, _yesterdayView, _badView, _goodView, _goalView, _ratingTitleLabel, _rateImageView,_rateDescription,_rateSlider) in
            
            _headTitle.top.equalToSuperview().offset(30)
            _headTitle.centerX.equalToSuperview()
            
            _weatherView.top.bottom.centerY.equalTo(addButton)
            _weatherView.left.equalTo(yesterdayView)
            
            _addButton.trailing.equalTo(yesterdayView)
            _addButton.bottom.equalTo(yesterdayView.snp.top).offset(-5)
            _addButton.sizeEqualTo(width: 25, height: 25)
            
            _yesterdayView.top.equalTo(headTitle.snp.bottom).offset(15)
            _yesterdayView.leading.equalToSuperview().offset(15)
            _yesterdayView.width.equalToSuperview().offset(-30)
            _yesterdayView.height.equalTo(0)
            
            _badView.top.equalTo(yesterdayView.snp.bottom).offset(15)
            _badView.leading.equalToSuperview().offset(15)
            _badView.width.equalToSuperview().offset(-30)
            
            _goodView.leading.trailing.equalTo(badView)
            _goodView.top.equalTo(badView.snp.bottom).offset(20)
            
            _goalView.leading.trailing.equalTo(badView)
            _goalView.top.equalTo(goodView.snp.bottom).offset(20)
            
            _ratingTitleLabel.left.equalTo(goalView)
            _ratingTitleLabel.top.equalTo(goalView.snp.bottom).offset(15)
            
            _rateImageView.top.equalTo(ratingTitleLabel)
            _rateImageView.centerX.equalTo(goalView)
            _rateImageView.sizeEqualTo(width: 90, height: 90)
            
            _rateDescription.top.equalTo(rateImageView.snp.bottom).offset(7.5)
            _rateDescription.centerX.equalToSuperview()
            
            _rateSlider.top.equalTo(rateDescription.snp.bottom).offset(7.5)
            _rateSlider.left.right.equalTo(goalView)
            _rateSlider.height.equalTo(30)
        }
        
        constraint(favoriteTitleLabel, favoriteToggle, finishButton) { (_favoriteTitleLabel, _favoriteToggle, _finishButton) in
            
            _favoriteTitleLabel.top.equalTo(rateSlider.snp.bottom).offset(5)
            _favoriteTitleLabel.left.equalTo(goalView)
            
            _favoriteToggle.centerY.equalTo(favoriteTitleLabel)
            _favoriteToggle.right.equalTo(goalView)
            _favoriteToggle.sizeEqualTo(width: 50, height: 30)
            
            _finishButton.left.right.equalTo(badView)
            _finishButton.top.equalTo(favoriteTitleLabel.snp.bottom).offset(15)
            _finishButton.height.equalTo(50)
        }
    }
}

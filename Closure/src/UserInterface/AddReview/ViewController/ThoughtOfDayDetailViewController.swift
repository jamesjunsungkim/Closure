//
//  ThoughtOfDayDetailViewController.swift
//  Closure
//
//  Created by James Kim on 8/30/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout

final class ThoughtOfDayDetailViewController: DefaultViewController {
    
    //UI
    fileprivate var thoughtInputView: UITextView!
    
    init(targetText:String) {
        self.targetText = targetText
        super.init(nibName: nil, bundle: nil)
        enterViewControllerMemoryLog(self)
        setupUI()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }

    // MARK: Fileprivate
    fileprivate let targetText:String
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension ThoughtOfDayDetailViewController {
    fileprivate func setupUI() {
        view.backgroundColor = .white
        
        let titleLabel = UILabel.create(text: "Thought of the day".localized, textAlignment: .left, fontSize: 18)
        
        let titleSeparator = UIView.create()
        
//        let textWithPadding = targetText+"\n"
        thoughtInputView = UITextView.create(text: targetText, mainFontSize: 15)
        thoughtInputView.setBorder(color: .mainBlue, width: 1)
        thoughtInputView.setCornerRadious(value: 5)
        
        let targetSize = thoughtInputView.getFitSize(maximumSize: CGSize(width: 320, height: 500))
        
        let group:[UIView] = [titleLabel, titleSeparator, thoughtInputView]
        group.forEach(view.addSubview(_:))
        
        constraint(titleLabel,titleSeparator, thoughtInputView) { (_titleLabel, _titleSeparator, _thoughtInputView) in
            _titleLabel.top.equalToSuperview().offset(20)
            _titleLabel.left.equalToSuperview().offset(15)
            
            _titleSeparator.top.equalTo(titleLabel.snp.bottom).offset(10)
            _titleSeparator.leftRightEqualToSuperView(withOffset: 0)
            _titleSeparator.height.equalTo(0.5)
            
            _thoughtInputView.top.equalTo(titleSeparator.snp.bottom).offset(15)
            _thoughtInputView.leftRightEqualToSuperView(withOffset: 10)
            _thoughtInputView.bottom.equalToSuperview().offset(-10)
            _thoughtInputView.sizeEqualTo(width: 320, height: targetSize.height)
            
        }
        
    }
}


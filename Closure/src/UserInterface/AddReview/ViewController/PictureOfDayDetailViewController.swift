//
//  PictureOfDayDetailViewController.swift
//  Closure
//
//  Created by James Kim on 8/30/18.
//  Copyright © 2018 James Kim. All rights reserved.
//
import UIKit
import CodiumLayout

final class PictureOfDayDetailViewController: DefaultViewController {
    
    //UI
    fileprivate var photoInputView: UIImageView!
    
    init(imageToPresent:UIImage) {
        super.init(nibName: nil, bundle: nil)
        enterViewControllerMemoryLog(self)
        setupUI(with: imageToPresent)
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    
    // MARK: Fileprivate
//    fileprivate let targetImage: UIImage

    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension PictureOfDayDetailViewController {
    fileprivate func setupUI(with image:UIImage) {
        view.backgroundColor = .white
        
        let titleLabel = UILabel.create(text: "Picture of the day".localized, textAlignment: .left, fontSize: 18)
        let titleSeparator = UIView.create()
        
        photoInputView = UIImageView.create(withImage: image)
        photoInputView.setCornerRadious(value: 5)
        photoInputView.contentMode = .scaleAspectFill
        
        /**
         view controller width is 340. changing it dynamically would be too much work for little gain.
         so let's just fix width at 320 for now and change height of images depending on image data.
         */
//        let maximumWidth:CGFloat = 350
        let maximumHeight:CGFloat = 450
        
        let ratio = image.size.height/image.size.width
//        let width = Swift.min(image.size.width, maximumWidth)
        let width:CGFloat = 320
        
        let calculatedHeight = ratio*width
        let height = Swift.min(calculatedHeight, maximumHeight)
        let targetSize = CGSize(width: width, height: height)
        
        let group:[UIView] = [titleLabel,titleSeparator, photoInputView]
        group.forEach(view.addSubview(_:))
        
        constraint(titleLabel,titleSeparator, photoInputView) { (_titleLabel,_titleSeparator, _photoInputView) in
            _titleLabel.top.equalToSuperview().offset(20)
            _titleLabel.left.equalToSuperview().offset(15)
            
            _titleSeparator.top.equalTo(titleLabel.snp.bottom).offset(10)
            _titleSeparator.leftRightEqualToSuperView(withOffset: 0)
            _titleSeparator.height.equalTo(0.5)
            
            _photoInputView.top.equalTo(titleSeparator.snp.bottom).offset(15)
            _photoInputView.leftRightEqualToSuperView(withOffset: 10)
            _photoInputView.bottom.equalToSuperview().offset(-10)
            _photoInputView.size.equalTo(targetSize)
        }
    }
}

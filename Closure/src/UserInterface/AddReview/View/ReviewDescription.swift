//
//  ReviewDescription.swift
//  Closure
//
//  Created by James Kim on 7/21/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation

struct ReviewDescription {
    let title: String
    let textPlaceHolder: String
    let userDefault: UserDefaults.Key
    let content:String?
}

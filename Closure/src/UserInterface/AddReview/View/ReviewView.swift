//
//  ReviewView.swift
//  Closure
//
//  Created by montapinunt Pimonta on 7/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import CodiumLayout
import DropDown
import PopupDialog
import MSCircularSlider

final class ReviewView: UIView {
    
    //UI
    fileprivate var titleLabel: UILabel!
    fileprivate(set) var textField: UITextField!
    
    fileprivate var textCountLabel: UILabel!
    fileprivate var countCircularSlider: MSCircularSlider!
    fileprivate var typeSegment: UISegmentedControl!
    
    fileprivate var deleteButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        setupView()
        setupTextField()
        setupDropDown()
    }

    // MARK: - Public
    
    public var textFieldContent:String {
        return textField.text!
    }
    
    public var textFieldReturnObservable:Observable<ReviewView> {
        return textFieldReturnSubject.asObservable()
    }
    
    public func configure(withDescription description: ReviewDescription) {
        /**
         0. now it supports editting mode which means it needs a way to load data.
         if content variable is not nil, we present it.
         
         1. adjust the font size depending on the screen size since it's too much to display on a small screen of iphone 6.
         */
        titleLabel.text = description.title
        textField.placeholder = description.textPlaceHolder
        textField.text = description.content.unwrapOrBlank()
        userDefaultKey = description.userDefault
        
        loadListAndUpdateDropDown()
        
        //font
        let targetFontSize:CGFloat = UIScreen.main.bounds.width > 400 ? UI.Component.titleSize: UI.Component.smallTitleSize
        let font = UIFont.mainFont(size: targetFontSize)
        titleLabel.font = font
        textField.font = font
    }
    
    // MARK: - Actions
    
    @objc fileprivate func userDidChangeTypeOfInput(sender:UISegmentedControl) {
        logInfo(type: .action)
        clearTextfield()
        isManual = sender.selectedSegmentIndex == 0
        
        switch isManual {
        case true:
            textField.becomeFirstResponder()
            showOrHideDeleteButton()
        case false:
            textField.resignFirstResponder()
            endEditing(true)
            dropDown.show()
        }
        
        // if user selects an item on dropdown and switch to manual, drop down still selects the itme so it should be manually removed.
        guard lastSelectedItemIndex != nil else {return}
        dropDown.deselectRow(at: lastSelectedItemIndex)
        lastSelectedItemIndex = nil
    }
    
    fileprivate lazy var userDidTypeForContent: (String)->Void = {[unowned self] (s) in
        logInfo(type: .action)
        self.updateCountLabelAndValidateCount(text: s)
    }
    
    fileprivate lazy var userDidChooseDropDown: (Int,String) ->Void = {[unowned self] (index, input) in
        logInfo(type: .action)
        guard input != self.addString else {
            self.clearTextfield()
            self.dropDown.deselectRow(at: index)
            self.dropDown.hide()
            self.presentNewDropDownEntry()
            return}
        
        self.lastSelectedItemIndex = index
        self.textField.text = input
        self.showOrHideDeleteButton()
        self.userDidTypeForContent(input)
    }
    
    @objc fileprivate func deleteBtnClicked() {
        logInfo(type: .action)
        guard !isManual else {assertionFailure();return}
        let currentInput = textField.text!
        performOnMain {
            UIView.getMostTopViewController()?.presentDefaultAlert(withTitle: "Are you sure to remove the current input?".localized, message: "You can not undo it".localized, okAction: {[unowned self] in
                self.addOrDeleteInputFromUserDefaultAndUpdateDropDown(input: currentInput, shouldDelete: true)
                self.clearTextfield()
                }, cancelAction: nil)
        }
    }
    
    // MARK: - Fileprivate
//    fileprivate var contentSubject = PublishSubject<(String,Bool)>()
    fileprivate let textFieldReturnSubject = PublishSubject<ReviewView>()
    
    fileprivate var content = BehaviorRelay(value: "")
    fileprivate let dropDown = DropDown()
    fileprivate let bag = DisposeBag()
    
    fileprivate var lastSelectedItemIndex: Int!
    
    fileprivate let countLimit = 45
    fileprivate let textFieldHieght:CGFloat = 25.0
    fileprivate let addString = "Add New Entry...".localized
    fileprivate var userDefaultKey: UserDefaults.Key!
    fileprivate var isContentValid = true
    fileprivate var isManual = true
    
    fileprivate func setupTextField() {
        textField.rx.text.orEmpty
        .bind(to: content)
        .disposed(by: bag)
        
        content.asObservable()
        .subscribe(onNext: userDidTypeForContent)
        .disposed(by: bag)
        
        textField.delegate = self
    }
    
    fileprivate func setupView() {
        self.backgroundColor = UI.Component.backgroundColor
        self.setCornerRadious(value: 5)
        self.setBorder(color: .mainBlue, width: 1)
    }
    
    fileprivate func updateCountLabelAndValidateCount(text:String) {
        let count = countLimit-text.count
        
        if count <= 5{
            countCircularSlider.filledColor = UIColor.red.withAlphaComponent(0.8)
        }else if count <= 10 {
            countCircularSlider.filledColor = UIColor.create(R: 255, G: 255, B: 77, alpha: 0.65)
        } else {
            countCircularSlider.filledColor = .mainBlue
        }
        
        textCountLabel.text = "\(count)"
        textCountLabel.isHidden = !(count <= 15)
        textCountLabel.textColor = count <= 5 ? .red : .black
        
        countCircularSlider.isHidden = text.isEmpty
        countCircularSlider.currentValue = Double(text.count)
        isContentValid = count <= countLimit ? true : false
    }
    
    fileprivate func setupDropDown() {
        dropDown.backgroundColor = .white
        dropDown.separatorColor = .init(white: 0.3, alpha: 0.3)
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y: textFieldHieght+4)
        dropDown.anchorView = textField
        dropDown.selectionAction = userDidChooseDropDown
    }
    
    fileprivate func loadListAndUpdateDropDown() {
        var list = UserDefaults.retrieveValue(forKey: userDefaultKey, defaultValue: [String]())
        list.append(addString)
        dropDown.dataSource = list
    }
    
    fileprivate func addOrDeleteInputFromUserDefaultAndUpdateDropDown(input: String, shouldDelete:Bool) {
        var list = UserDefaults.retrieveValue(forKey: userDefaultKey, defaultValue: [String]())
        
        if !shouldDelete {
            list.append(input)
        } else {
            list = list.removeElement(condition: {$0 == input})
        }
        UserDefaults.store(object: list, forKey: userDefaultKey)
        list.append(addString)
        dropDown.dataSource = list
    }
    
    fileprivate func presentNewDropDownEntry() {
        let target = CreateTextEntryViewController(title: "Add your favorite line.".localized, detail: "Please input what you will use often.".localized, placeHolder: titleLabel.text!, textLimit: 45)
        
        let addInsertedLineToUserDefault = {[unowned self] in
            let input = target.inputText
            self.textField.text = input
            self.userDidTypeForContent(input)
            self.addOrDeleteInputFromUserDefaultAndUpdateDropDown(input: input, shouldDelete: false)
            self.updateCountLabelAndValidateCount(text: input)
            self.showOrHideDeleteButton()
            self.textFieldReturnSubject.onNext(self)
        }
        
        target.userDidTapEnterObservable.subscribe(onNext: {
            addInsertedLineToUserDefault()
            target.dismiss(animated: true, completion: nil)
        }).disposed(by: bag)
        
        getParentViewController()?.presentPopup(targetVC: target, cancelAction: {
            self.showOrHideDeleteButton()
        }, okButtonTitle: "Create".localized, okAction: addInsertedLineToUserDefault)
    }
    
    fileprivate func clearTextfield() {
        self.textField.text = ""
        self.userDidTypeForContent("")
        self.showOrHideDeleteButton()
    }
    
    fileprivate func showOrHideDeleteButton() {
        deleteButton.isHidden = textField.text!.isEmpty
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension ReviewView: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if isManual {
            return true
        } else {
            endEditing(true)
            dropDown.show()
            return false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {return true}
        // The reason why it obmits the equal equasion when it hits the limit, prevent from typing but allow for the input upto the limit.
        return textField.text!.count < countLimit
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        textFieldReturnSubject.onNext(self)
        return true
    }
    
}


extension ReviewView {
    fileprivate func setupUI() {
        titleLabel = UILabel.create(text: "TITLE", textAlignment: .center, textColor: UI.Component.titleColor, fontSize: UI.Component.titleSize, boldFont: false, numberOfLine: 1)
        
        typeSegment = UISegmentedControl.create(withTitles: ["Manual".localized, "Auto".localized], tintColor: .mainBlue)
        typeSegment.addTarget(self, action: #selector(userDidChangeTypeOfInput(sender:)), for: .valueChanged)
        
        deleteButton = UIButton.create(withImageName: "minus_button")
        deleteButton.addTarget(self, action: #selector(deleteBtnClicked), for: .touchUpInside)
        deleteButton.isHidden = true
        
        textField = UITextField.create(placeHolder: "PLACEHOLDER", textSize: UI.Component.contentSize, textColor: UI.Component.contentColor, keyboardType: .default)
        textField.textAlignment = .left
        textField.borderStyle = .none
        
        let separatorLine = UIView.create()
        
        countCircularSlider = MSCircularSlider()
        countCircularSlider.currentValue = 0
        countCircularSlider.handleType = .smallCircle
        countCircularSlider.isUserInteractionEnabled = false
        countCircularSlider.lineWidth = 3
        countCircularSlider.maximumValue = 45
        countCircularSlider.isHidden = true
        
        textCountLabel = UILabel.create(text: "11/90", textAlignment: .left, textColor: .gray, fontSize: 13, boldFont: false, numberOfLine: 1)
        
        let group : [UIView] = [titleLabel,typeSegment,deleteButton, textField, countCircularSlider, textCountLabel, separatorLine]
        group.forEach(addSubview(_:))
        
        constraint(titleLabel,typeSegment, deleteButton, textField, countCircularSlider, textCountLabel, separatorLine) { (_titleLabel, _typeSegment,_deleteButton, _textField, _countCircularSlider, _textCountLabel, _separatorLine) in
            
            _titleLabel.left.equalToSuperview().offset(15)
            _titleLabel.top.equalToSuperview().offset(10)
            
            _typeSegment.centerY.equalTo(titleLabel)
            _typeSegment.right.equalToSuperview().offset(-15)
            _typeSegment.sizeEqualTo(width: 130, height: 20)
            
            _deleteButton.centerY.equalTo(separatorLine)
            _deleteButton.leading.equalTo(separatorLine).offset(-15/2)
            _deleteButton.sizeEqualTo(width: 15, height: 15)
            
            _textField.leftRightEqualToSuperView(withOffset: 15)
            _textField.top.equalTo(titleLabel.snp.bottom).offset(10)
            _textField.height.equalTo(textFieldHieght)
            
            _countCircularSlider.top.equalTo(textField.snp.bottom).offset(2.5)
            _countCircularSlider.bottom.right.equalToSuperview().offset(-2.5)
            _countCircularSlider.sizeEqualTo(width: 25, height: 25)
            
            _textCountLabel.centerX.centerY.equalTo(countCircularSlider)
            
            _separatorLine.left.right.equalTo(textField)
            _separatorLine.height.equalTo(0.5)
            _separatorLine.top.equalTo(textField.snp.bottom).offset(0.5)
        }
    }
}






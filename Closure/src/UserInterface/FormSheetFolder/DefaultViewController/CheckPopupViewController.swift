//
//  CheckPopupViewController.swift
//  Closure
//
//  Created by montapinunt Pimonta on 8/12/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout

final class CheckPopupViewController: DefaultViewController {
    
    //UI
    fileprivate var descriptionLabel: UILabel!
    fileprivate var checkBoxWithDescription: CheckBoxWithDescription<Bool>!
    
    init(title:String = "Warning".localized, body:String, checkDescription:String = "Please don't show up again.".localized) {
        self.titleText = title
        self.bodyText = body
        self.checkDescription = checkDescription
        super.init(nibName: nil, bundle: nil)
        setupUI()
    }
    
    
    // MARK: Public
    public var isChecked: Bool {
        return checkBoxWithDescription.isChecked
    }
    
    // MARK: Filepriavte
    fileprivate let titleText:String
    fileprivate let bodyText:String
    fileprivate let checkDescription: String
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}


extension CheckPopupViewController{
    fileprivate func setupUI() {
        
        let title = UILabel.create(text: titleText, textAlignment: .center, textColor: .black, fontSize: 20, boldFont: false, numberOfLine: 0)
        
        descriptionLabel = UILabel.create(text: bodyText, textAlignment: .center, fontSize: 15, numberOfLine: 0)
        
        checkBoxWithDescription = CheckBoxWithDescription(object: nil, label: checkDescription, shouldBeChecked: false)
        
        let group: [UIView] = [title, descriptionLabel, checkBoxWithDescription]
        group.forEach(view.addSubview(_:))
        
        constraint(title, descriptionLabel, checkBoxWithDescription) { (_title, _descriptionLabel, _checkBoxWithDescription) in
            
            _title.top.equalTo(15)
            _title.centerX.equalToSuperview()
            
            _descriptionLabel.top.equalTo(title.snp.bottom).offset(10)
            _descriptionLabel.leftRightEqualToSuperView(withOffset: 20)
            
            _checkBoxWithDescription.top.equalTo(descriptionLabel.snp.bottom).offset(7.5)
            _checkBoxWithDescription.centerX.equalToSuperview()
            _checkBoxWithDescription.sizeEqualTo(width: 245, height: 40)
            _checkBoxWithDescription.height.equalTo(40)
            _checkBoxWithDescription.bottom.equalToSuperview().offset(-7.5)
            
        }
    }
}

//
//  AddThoughtAndPhotoViewController.swift
//  Closure
//
//  Created by James Kim on 7/22/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import Photos
import RxSwift
import CodiumLayout

final class AddThoughtAndPhotoViewController: DefaultViewController {
    
    //UI
    fileprivate var thoughtTextView: ThoughtInputTextView!
    fileprivate var imageInputView: PhotoInputView!
    fileprivate var deleteButton: UIButton!
    
    init(withThought thought:String?, image: UIImage?) {
        super.init(nibName: nil, bundle: nil)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        setupVC()
        addTarget()
        configure(withThought: thought, image: image)
    }

    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    // MARK: - Public
    
    public var thoughtString:String? {
        return thoughtTextView.text
    }
    
    public var selectedImage:UIImage? {
        return imageInputView.selectedImage
    }
    
    public func configure(withThought thought:String!, image: UIImage!) {
        guard thought != nil || image != nil else {return}
        thoughtTextView.configure(withThought: thought)
        imageInputView.configure(withImage: image)
    }
    
    // MARK: - Actions
    @objc fileprivate func userDidTapView() {
        view.endEditing(true)
    }
    
    // MARK: - Fileprivate
    fileprivate let bag = DisposeBag()
    
    fileprivate func setupVC() {
        view.backgroundColor = .white
    }
    
    fileprivate func addTarget() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(userDidTapView))
        view.addGestureRecognizer(tap)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension AddThoughtAndPhotoViewController {
    fileprivate func setupUI() {
        let textViewTitle = UILabel.create(text: "Thought of the day".localized, textAlignment: .left, fontSize: 17)
        
        thoughtTextView = ThoughtInputTextView()
        
        let imageViewTitle = UILabel.create(text: "Picture of the day".localized, textAlignment: .left, fontSize: 17)
        
        imageInputView = PhotoInputView()

        let group: [UIView] = [textViewTitle,thoughtTextView, imageViewTitle, imageInputView]
        group.forEach(view.addSubview(_:))
        
        
        constraint(textViewTitle, thoughtTextView, imageViewTitle,imageInputView) { (_textViewTitle, _thoughtTextView, _imageViewTitle, _imageInputView) in
            
            _textViewTitle.top.equalToSuperview().offset(20)
            _textViewTitle.leading.equalToSuperview().offset(15)
            
            _thoughtTextView.top.equalTo(textViewTitle.snp.bottom).offset(15)
            _thoughtTextView.leading.equalToSuperview().offset(15)
            _thoughtTextView.trailing.equalToSuperview().offset(-15)
            _thoughtTextView.height.equalTo(85)
            
            _imageViewTitle.top.equalTo(thoughtTextView.snp.bottom).offset(20)
            _imageViewTitle.leading.equalTo(thoughtTextView)
            
            _imageInputView.top.equalTo(imageViewTitle.snp.bottom).offset(15)
            _imageInputView.centerX.equalToSuperview()
            _imageInputView.sizeEqualTo(width: 220, height: 190)
            _imageInputView.bottom.equalToSuperview().offset(-30)
        }
    }
}


//
//  AddNewDropDownList.swift
//  Closure
//
//  Created by James Kim on 7/21/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout
import RxSwift

final class CreateTextEntryViewController: DefaultViewController {
    
    //UI
    fileprivate var titleLabel: UILabel!
    fileprivate var detailLabel: UILabel!
    fileprivate var countLabel: UILabel!
    fileprivate var textField: UITextField!
    
    // It's set public so that when user clicks on create btn, the text is accessible. but it could be another function instead of public textfield.
    
    init(title:String, detail:String, placeHolder:String, textLimit:Int? = 45) {
        
        // TODO: Need to implement the limit label for text input. for now, I will just hide it.
        self.titleText = title
        self.detailText = detail
        self.placeHolder = placeHolder
        self.textLimit = textLimit
        super.init(nibName: nil, bundle: nil)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        setupVC()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        textField.becomeFirstResponder()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    // MARK: - Public
    public var inputText: String {
        return textField.text!
    }
    
    public var userDidTapEnterObservable: Observable<Void> {
        return userDidTapEnterSubject.asObservable()
    }
    
    // MARK: - Fileprivate
    fileprivate let titleText:String
    fileprivate let detailText:String
    fileprivate let placeHolder:String
    fileprivate let textLimit: Int?
    fileprivate let userDidTapEnterSubject = PublishSubject<Void>()
    
    fileprivate func setupVC() {
        view.backgroundColor = .white
        textField.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension CreateTextEntryViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //TODO: Limit the input? maybe 45 characters

        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard !textField.text!.isEmpty else {return false}
        
        userDidTapEnterSubject.onNext(())
        userDidTapEnterSubject.onCompleted()
        
        return true
    }
}
extension CreateTextEntryViewController {
    fileprivate func setupUI() {
        let targetColor = UIColor.black.withAlphaComponent(0.8)
        titleLabel = UILabel.create(text: titleText, textAlignment: .center, textColor: targetColor, fontSize: 17, boldFont: false, numberOfLine: 0)
        
        detailLabel = UILabel.create(text: detailText, textAlignment: .center, textColor: targetColor, fontSize: 14, boldFont: false, numberOfLine: 0)
        
        textField = UITextField.create(placeHolder: placeHolder, textSize: 14, textColor: targetColor, keyboardType: .default, clearMode: .whileEditing)
        textField.borderStyle = .roundedRect
        
        countLabel = UILabel.create(text: "11/45", textAlignment: .left, textColor: .gray, fontSize: 13, boldFont: false, numberOfLine: 1)
        countLabel.isHidden = true
        
        let group:[UIView] = [titleLabel, detailLabel, textField, countLabel]
        group.forEach(view.addSubview(_:))
        
        constraint(titleLabel, detailLabel, textField, countLabel) { (_titleLabel, _detailLabel, _textField, _countLabel) in
            _titleLabel.centerX.equalToSuperview()
            _titleLabel.top.equalToSuperview().offset(20)
            
            _detailLabel.centerX.equalTo(titleLabel)
            _detailLabel.top.equalTo(titleLabel.snp.bottom).offset(8)
            _detailLabel.leftRightEqualToSuperView(withOffset: 30)
            
            _textField.top.equalTo(detailLabel.snp.bottom).offset(8)
            _textField.leftRightEqualToSuperView(withOffset: 20)
            _textField.bottom.equalToSuperview().offset(-15)
            
        }
    }
}


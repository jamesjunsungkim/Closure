//
//  DateSelectionCalendarViewController.swift
//  Closure
//
//  Created by James Kim on 9/11/18.
//  Copyright © 2018 James Kim. All rights reserved.
//
import UIKit
import FSCalendar
import RxSwift

final class DateSelectionCalendarViewController: DefaultViewController {
    
    public struct Keys {
        static let startDate = "startDate"
        static let endDate = "endDate"
    }
    //UI
    fileprivate var calendar: FSCalendar!
    
    func setup(fromVC: UIViewController, userInfo: [String : Any]?) {
        startDate = userInfo?[Keys.startDate] as? Date
        endDate = userInfo?[Keys.endDate] as? Date
        
        enterViewControllerMemoryLog(self)
        
        setupUI()
        checkIfStartOrEndDateExistAndMoveToThatMonth()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    // MARK: - Public
    public var dateSelectionObservable:Observable<Date>{
        return dateSelectionSubject.asObservable()
    }
    
    // MARK: - Actions
    fileprivate func userDidSelectItem(at date: Date) {
        dateSelectionSubject.onNext(date)
        dateSelectionSubject.onCompleted()
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Fileprivate
    fileprivate var startDate:Date!
    fileprivate var endDate:Date!
    
    fileprivate let dateSelectionSubject = PublishSubject<Date>()
    fileprivate let calendarID = "calendarID"
    
    fileprivate func checkIfStartOrEndDateExistAndMoveToThatMonth() {
        
        /**
         ------------------------- LOGIC RUN DOWN ------------------------------
         calendar.select takes optional value which is good for this case because there are some scenarios where startdate is nil.
         
         in 3 out of 4 cases with combination of startdate being nil and enddate being nil,
         we will scroll to the start date no matter what.
         
         but if start date is nil and end date exists, we scroll to the end date.
         
         especially when start date and end date reside in different months, we just ignore the month that end date is in.
         
         we need to deselect the date because it changes the selection color to the default selection color.
         */
        
        calendar.select(startDate, scrollToDate: true)
        if startDate != nil  {
            calendar.deselect(startDate)
        }
        
        guard startDate == nil else {return}
        calendar.select(endDate, scrollToDate: true)
        if endDate != nil {
            calendar.deselect(endDate)
        }
    }

    fileprivate func changeCalendarLabelsToCurrentLanguage() {
        //Header
        self.calendar.appearance.headerDateFormat = currentLanguage.toMonthDateFormat
        
        //weekdays
        let pool = ["Sun", "Mon", "Tue", "Wed", "Thu","Fri","Sat"]
        for (index, label) in calendar.calendarWeekdayView.weekdayLabels.enumerated() {
            label.text = pool[index].localized
        }
    }
    
}
extension DateSelectionCalendarViewController: FSCalendarDelegateAppearance, FSCalendarDataSource {
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: calendarID, for: date, at: position)
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        let conditionForStartDate = (startDate != nil) && startDate.isSameDay(date: date)
        let conditionForEndDate = (endDate != nil) && endDate.isSameDay(date: date)
        // if it's today, I'd like to present the main blue color instead of red.
        let finalCondition = (conditionForStartDate || conditionForEndDate) && (!date.isSameDay(date: Date()))
        
        return finalCondition ? .red : nil
    }
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        return startDate != nil ? startDate : Date().addYear(value: -10)
    }
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        return endDate != nil ? endDate : Date().addYear(value: 10)
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        userDidSelectItem(at:date)
    }
}

extension DateSelectionCalendarViewController {
    fileprivate func setupUI() {
        view.backgroundColor = .white
        
        calendar = FSCalendar()
        calendar.locale = Locale.current
        calendar.delegate = self
        calendar.dataSource = self
        calendar.register(FSCalendarCell.self, forCellReuseIdentifier: calendarID)
        calendar.today = Date()
        
        calendar.appearance.headerTitleFont = UIFont.create(forKey: .helveticaNeueInBold, size: 15)
        calendar.appearance.headerTitleColor = .mainBlue
        calendar.appearance.weekdayTextColor = .mainBlue
        calendar.appearance.todayColor = .mainBlue
        changeCalendarLabelsToCurrentLanguage()
        
        view.addSubview(calendar)
        
        calendar.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.height.equalTo(350)
        }
    }
}


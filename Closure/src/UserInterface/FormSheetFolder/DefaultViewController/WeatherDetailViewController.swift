//
//  WeatherDetailViewController.swift
//  Closure
//
//  Created by James Kim on 9/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout

final class WeatherDetailViewController: DefaultViewController {
    
    //UI
    fileprivate var weatherImageView:UIImageView!
    fileprivate var locationLabel:UILabel!
    fileprivate var weatherDescriptionLabel:UILabel!
    fileprivate var temperatureLabel:UILabel!
    fileprivate var fahrenheitCelsiusSegmentControl: UISegmentedControl!
    
    init(nonCDWeather:NonCDWeather) {
        self.nonCDWeather = nonCDWeather
        super.init(nibName: nil, bundle: nil)
        enterViewControllerMemoryLog(self)
        setupUI()
        addTargets()
        loadTempraturePreferenceTemperatureUnitAndApplyItToUI()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    
    // MARK: - Actions
    @objc fileprivate func userDidTapSegmentControl() {
        changeTempratureUnitBasedOn(selectedIndex: fahrenheitCelsiusSegmentControl.selectedSegmentIndex)
    }
    
    
    // MARK: - Fileprivate
    fileprivate let nonCDWeather:NonCDWeather
    
    fileprivate func addTargets() {
        fahrenheitCelsiusSegmentControl.addTarget(self, action: #selector(userDidTapSegmentControl), for: .valueChanged)
    }
    
    fileprivate func loadTempraturePreferenceTemperatureUnitAndApplyItToUI() {
        let target = UserDefaults.retrieveValue(forKey: .currentTemperatureUnitPreference, defaultValue: 0)
        fahrenheitCelsiusSegmentControl.selectedSegmentIndex = target
        changeTempratureUnitBasedOn(selectedIndex: target)
    }

    fileprivate func changeTempratureUnitBasedOn(selectedIndex: Int) {
        let isFahrenheit = selectedIndex == 0
        let targetUnit = isFahrenheit ? "°F" : "°C"
        
        switch isFahrenheit {
        case true:
            let targetTemp = Int((1.8)*(nonCDWeather.temperature).toDouble + 32)
            temperatureLabel.text = "\(targetTemp) \(targetUnit)"
        case false:
            temperatureLabel.text = "\(nonCDWeather.temperature) \(targetUnit)"
        }
        
        // let's save it to userdefault so that when user opens it again, it's automatically set up.
        UserDefaults.store(object: fahrenheitCelsiusSegmentControl.selectedSegmentIndex, forKey: .currentTemperatureUnitPreference)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension WeatherDetailViewController {
    fileprivate func setupUI() {
        view.backgroundColor = .white
        
        fahrenheitCelsiusSegmentControl = UISegmentedControl.create(withTitles: ["°F", "°C"], tintColor: .mainBlue)
        
        weatherImageView = UIImageView.create(withImageName: nonCDWeather.weatherIconName)
        
        locationLabel = UILabel.create(text: nonCDWeather.city + ", " + nonCDWeather.countryName, textAlignment: .center, fontSize: 15)
        
        weatherDescriptionLabel = UILabel.create(text: nonCDWeather.weatherDescription, textAlignment: .center, fontSize: 14)
        
        temperatureLabel = UILabel.create(text: "22", textAlignment: .center, fontSize: 22)
        
        
        let group:[UIView] = [fahrenheitCelsiusSegmentControl, weatherImageView, locationLabel, weatherDescriptionLabel, temperatureLabel]
        group.forEach(view.addSubview(_:))
        
        constraint(fahrenheitCelsiusSegmentControl, weatherImageView, locationLabel, weatherDescriptionLabel, temperatureLabel) { (_fahrenheitCelsiusSegmentControl, _weatherImageView, _locationLabel, _weatherDescriptionLabel, _temperatureLabel) in
            
            _fahrenheitCelsiusSegmentControl.topRightqualToSuperView(withOffset: 10)
            _fahrenheitCelsiusSegmentControl.sizeEqualTo(width: 80, height: 25)
            
            _weatherImageView.top.equalTo(fahrenheitCelsiusSegmentControl.snp.bottom).offset(10)
            _weatherImageView.centerX.equalToSuperview()
            _weatherImageView.sizeEqualTo(width: 130, height: 130)
            
            _locationLabel.top.equalTo(weatherImageView.snp.bottom).offset(5)
            _locationLabel.centerX.equalToSuperview()
            
            _weatherDescriptionLabel.top.equalTo(locationLabel.snp.bottom).offset(5)
            _weatherDescriptionLabel.centerX.equalToSuperview()
            
            _temperatureLabel.top.equalTo(weatherDescriptionLabel.snp.bottom).offset(10)
            _temperatureLabel.centerX.equalToSuperview()
            _temperatureLabel.bottom.equalToSuperview().offset(-10)
        }
    }
}


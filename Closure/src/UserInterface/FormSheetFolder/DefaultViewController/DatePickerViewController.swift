//
//  DatePickerViewController.swift
//  Closure
//
//  Created by James Kim on 7/15/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout
import RxSwift

final class DatePickerViewController: DefaultViewController {
    
    //UI
    fileprivate var datePicker: UIDatePicker!
    fileprivate var cancelButton: UIButton!
    fileprivate var chooseButton: UIButton!
    
    init(needsToSelectFullDate:Bool = true,startDate:Date? = nil, endDate:Date? = nil, local:Locale = Locale.current, timezone: TimeZone = TimeZone.current) {
        self.needsToSelectFullDate = needsToSelectFullDate
        self.startDate = startDate
        self.endDate = endDate
        self.local = local
        self.timezone = timezone
        super.init(nibName: nil, bundle: nil)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        setupVC()
        addTargets()
    }

    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    // MARK: - Public
    public var dateObservable: Observable<Date> {
        return dateSubject.asObservable()
    }
    
    // MARK: - Actions
    @objc fileprivate func chooseBtnClicked() {
        dateSubject.onNext(datePicker.date)
        dateSubject.onCompleted()
        dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func cancelBtnClicked() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Fileprivate
    fileprivate let startDate:Date?
    fileprivate let endDate:Date?
    fileprivate let local:Locale!
    fileprivate let timezone: TimeZone!
    fileprivate let dateSubject = PublishSubject<Date>()
    fileprivate let needsToSelectFullDate:Bool
    
    fileprivate func setupVC() {
        view.backgroundColor = UI.viewBackgroundColor
    }
    
    fileprivate func addTargets() {
        chooseButton.addTarget(self, action: #selector(chooseBtnClicked), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(cancelBtnClicked), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension DatePickerViewController {
//    var preferredSize: CGSize {
//        return CGSize(width: 280, height: 220)
//    }
    
    fileprivate func setupUI() {
        datePicker = UIDatePicker()
        datePicker.locale = local
        datePicker.timeZone = timezone
        datePicker.datePickerMode = needsToSelectFullDate ? .date : .time
        
        if startDate != nil {
            datePicker.minimumDate = startDate!
        }
        
        if endDate != nil {
            datePicker.maximumDate = endDate!
        }
        
        cancelButton = UIButton.create(title: "Cancel".localized, titleColor: .mainBlue, fontSize: UI.Button.fontSize, backgroundColor: .white)
        cancelButton.setCornerRadious(value: 5)
        
        chooseButton = UIButton.create(title: "Choose".localized, titleColor: .mainBlue, fontSize: UI.Button.fontSize, backgroundColor: .white)
        chooseButton.setCornerRadious(value: 5)
        
        let group: [UIView] = [datePicker, cancelButton, chooseButton]
        group.forEach(view.addSubview(_:))
        
        constraint(cancelButton, chooseButton, datePicker) { (a1, a2, a3) in
            a1.left.top.equalToSuperview().offset(10)
            a1.sizeEqualTo(width: 80, height: 30)
            
            a2.topRightqualToSuperView(withOffset: 10)
            a2.sizeEqualTo(width: 80, height: 30)
            
            a3.top.equalTo(chooseButton.snp.bottom).offset(10)
            a3.left.right.bottom.equalToSuperview()
        }
    }
}


//
//  ColorPickerViewController.swift
//  Closure
//
//  Created by James Kim on 7/30/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import ChromaColorPicker
import CodiumLayout
import RxSwift

final class ColorPickerViewController: DefaultViewController {
    
    struct ColorTag {
        static let awesome = 0
        static let good = 1
        static let ok = 2
        static let unsatisfactory = 3
        static let terrible = 4
    }
    
    //UI
    fileprivate var terribleColorView:CheckView!
    fileprivate var unsatisfactoryColorView:CheckView!
    fileprivate var okColorView:CheckView!
    fileprivate var goodColorView:CheckView!
    fileprivate var awesomeColorView:CheckView!
    fileprivate var colorPickerView: ChromaColorPicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enterViewControllerMemoryLog(self)
        
        setupUI()
        setupVC()
        setupObserve()
        chooseAwesomeAsFirstPicker()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isLoading = false
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    // MARK: Public
    public func saveCurrentColorsToUserDefaults() {
        UserDefaults.storeColor(awesomeColorView.backgroundColor!, forKey: .awesomeColor)
        UserDefaults.storeColor(goodColorView.backgroundColor!, forKey: .goodColor)
        UserDefaults.storeColor(okColorView.backgroundColor!, forKey: .okColor)
        UserDefaults.storeColor(unsatisfactoryColorView.backgroundColor!, forKey: .unsatisfactoryColor)
        UserDefaults.storeColor(terribleColorView.backgroundColor!, forKey: .terribleColor)
    }
    
    // MARK: Actions
    fileprivate lazy var observeSelectView:(Int)->Void = {[unowned self] (tag) in
        logInfo(type: .observation)
        // uncheck the previous one first and then check the selected one.
        self.currentColorView.toggleCheck()
        switch tag {
        case ColorTag.awesome:
            self.currentColorView = self.awesomeColorView
        case ColorTag.good:
            self.currentColorView = self.goodColorView
        case ColorTag.ok:
            self.currentColorView = self.okColorView
        case ColorTag.unsatisfactory:
            self.currentColorView = self.unsatisfactoryColorView
        case ColorTag.terrible:
            self.currentColorView = self.terribleColorView
        default:assertionFailure(); break
        }
        self.currentColorView.toggleCheck()
        self.colorPickerView.adjustToColor(self.currentColorView.backgroundColor!)
    }
    
    @objc fileprivate func userDidDragThumb() {
        logInfo(type: .action)
        guard !isLoading else {return}
        currentColorView.backgroundColor = colorPickerView.currentColor
    }
    
    @objc fileprivate func userDidClickOnResetButton() {
        logInfo(type: .action)
        guard let r = Rate.init(rawValue: Int16(currentColorView.tag)) else {assertionFailure(); return}
        currentColorView.backgroundColor = r.defaultColor
        colorPickerView.adjustToColor(currentColorView.backgroundColor!)
    }
    
    // MARK: Fileprivate
    fileprivate weak var currentColorView: CheckView!
    fileprivate var isLoading = true
    fileprivate let bag = FilterDisposeBag.defaultBag
    
    fileprivate func setupVC() {
        view.backgroundColor = .white
    }
    
    fileprivate func setupObserve() {
        
        let group: [CheckView] = [awesomeColorView, goodColorView, okColorView, unsatisfactoryColorView, terribleColorView]
        group.forEach({$0.tagObservable.subscribe(onNext: observeSelectView, onDisposed: observerDisposedDescription).disposed(by: bag)})

        colorPickerView.addTarget(self, action: #selector(userDidDragThumb), for: .touchDragInside)
        colorPickerView.addTarget(self, action: #selector(userDidDragThumb), for: .valueChanged)
        
        colorPickerView.addButton.addTarget(self, action: #selector(userDidClickOnResetButton), for: .touchUpInside)
    }
    
    fileprivate func chooseAwesomeAsFirstPicker() {
        currentColorView = awesomeColorView
        awesomeColorView.toggleCheck()
        colorPickerView.adjustToColor(awesomeColorView.backgroundColor!)
    }
}

extension ColorPickerViewController {
    fileprivate func setupUI() {
        
        let titleLabel = UILabel.create(text: "Choose your custom colors!".localized, textAlignment: .center, textColor: .black, fontSize: 17, boldFont: true, numberOfLine: 1)
        
        let separatorLine = UIView.create()
        
        let awesomeDescriptionLabel = UILabel.create(text: Rate.awesome.title+":", textAlignment: .left, textColor: .black, fontSize: 15, boldFont: false, numberOfLine: 1)
        
        let backgroundColorForAwesome = UserDefaults.readColor(forKey: .awesomeColor, defaultColor: Rate.awesomeDefaultColor)
        awesomeColorView = CheckView(backgroundColor: backgroundColorForAwesome, tag: ColorTag.awesome)
        awesomeColorView.setCornerRadious(value: 5)
    
        let goodDescriptionLabel = UILabel.create(text: Rate.good.title+":", textAlignment: .left, textColor: .black, fontSize: 15, boldFont: false, numberOfLine: 1)
        let backgroundColorForGood = UserDefaults.readColor(forKey: .goodColor, defaultColor: Rate.goodDefaultColor)
        goodColorView = CheckView(backgroundColor: backgroundColorForGood, tag: ColorTag.good)
        goodColorView.setCornerRadious(value: 5)
        
        let okDescriptionLabel = UILabel.create(text: Rate.ok.title+":", textAlignment: .left, textColor: .black, fontSize: 15, boldFont: false, numberOfLine: 1)
        let backgroundColorForOk = UserDefaults.readColor(forKey: .okColor, defaultColor: Rate.okDefaultColor)
        okColorView = CheckView(backgroundColor: backgroundColorForOk, tag: ColorTag.ok)
        okColorView.setCornerRadious(value: 5)
        
        let unsatisfacotryDescriptionLabel = UILabel.create(text: Rate.unsatisfactory.title+":", textAlignment: .left, textColor: .black, fontSize: 15, boldFont: false, numberOfLine: 1)
        let backgroundColorForUnsatisfactory = UserDefaults.readColor(forKey: .unsatisfactoryColor, defaultColor: Rate.unsatisfactoryDefaultColor)
        unsatisfactoryColorView = CheckView(backgroundColor: backgroundColorForUnsatisfactory, tag: ColorTag.unsatisfactory)
        unsatisfactoryColorView.setCornerRadious(value: 5)
        
        let terribleDescriptionLabel = UILabel.create(text: Rate.terrible.title+":", textAlignment: .left, textColor: .black, fontSize: 15, boldFont: false, numberOfLine: 1)
        let backgroundColorForTerrible = UserDefaults.readColor(forKey: .terribleColor, defaultColor: Rate.terribleDefaultColor)
        terribleColorView = CheckView(backgroundColor: backgroundColorForTerrible, tag: ColorTag.terrible)
        terribleColorView.setCornerRadious(value: 5)
       
        colorPickerView = ChromaColorPicker()
        colorPickerView.addButton.plusIconLayer = nil
        colorPickerView.addButton.setTitle("Reset".localized, for: .normal)

        let group: [UIView] = [titleLabel,separatorLine,awesomeDescriptionLabel,awesomeColorView,goodDescriptionLabel,goodColorView,okDescriptionLabel, okColorView,unsatisfacotryDescriptionLabel,unsatisfactoryColorView,terribleDescriptionLabel, terribleColorView,colorPickerView]
        group.forEach(view.addSubview(_:))
        
        constraint(titleLabel, separatorLine, awesomeDescriptionLabel, awesomeColorView, goodDescriptionLabel, goodColorView, okDescriptionLabel, okColorView, unsatisfacotryDescriptionLabel, unsatisfactoryColorView) { (_titleLabel, _separatorLine,_awesomeDescriptionLabel, _awesomeColorView,_goodDescriptionLabel,_goodColorView, _okDescriptionLabel, _okColorView, _unsatisfacotryDescriptionLabel, _unsatisfactoryColorView) in
            
            _titleLabel.top.equalToSuperview().offset(15)
            _titleLabel.centerX.equalToSuperview()
            
            _separatorLine.top.equalTo(titleLabel.snp.bottom).offset(10)
            _separatorLine.leftRightEqualToSuperView(withOffset: 15)
            _separatorLine.height.equalTo(0.5)
            
            _awesomeDescriptionLabel.centerX.equalToSuperview().offset(-35)
            _awesomeDescriptionLabel.centerY.equalTo(awesomeColorView)
            _awesomeDescriptionLabel.sizeEqualTo(width: 120, height: 28)
            
            _awesomeColorView.top.equalTo(separatorLine.snp.bottom).offset(15)
            _awesomeColorView.sizeEqualTo(width: 30, height: 30)
            _awesomeColorView.left.equalTo(awesomeDescriptionLabel.snp.right).offset(5)
            
            _goodDescriptionLabel.centerX.equalTo(awesomeDescriptionLabel)
            _goodDescriptionLabel.centerY.equalTo(goodColorView)
            _goodDescriptionLabel.size.equalTo(awesomeDescriptionLabel.snp.size)
            
            _goodColorView.top.equalTo(awesomeColorView.snp.bottom).offset(10)
            _goodColorView.left.equalTo(awesomeColorView)
            _goodColorView.size.equalTo(awesomeColorView.snp.size)
            
            _okDescriptionLabel.centerX.equalTo(awesomeDescriptionLabel)
            _okDescriptionLabel.centerY.equalTo(okColorView)
            _okDescriptionLabel.size.equalTo(awesomeDescriptionLabel.snp.size)
            
            _okColorView.top.equalTo(goodColorView.snp.bottom).offset(10)
            _okColorView.left.equalTo(goodColorView)
            _okColorView.size.equalTo(goodColorView.snp.size)
            
            _unsatisfacotryDescriptionLabel.centerX.equalTo(awesomeDescriptionLabel)
            _unsatisfacotryDescriptionLabel.centerY.equalTo(unsatisfactoryColorView)
            _unsatisfacotryDescriptionLabel.size.equalTo(awesomeDescriptionLabel.snp.size)
            
            _unsatisfactoryColorView.top.equalTo(okColorView.snp.bottom).offset(10)
            _unsatisfactoryColorView.left.equalTo(goodColorView)
            _unsatisfactoryColorView.size.equalTo(goodColorView.snp.size)
            
        }
        
        constraint(terribleDescriptionLabel, terribleColorView, colorPickerView) { (_terribleDescriptionLabel, _terribleColorView, _colorPickerView) in
            
            _terribleDescriptionLabel.centerX.equalTo(awesomeDescriptionLabel)
            _terribleDescriptionLabel.centerY.equalTo(terribleColorView)
            _terribleDescriptionLabel.size.equalTo(awesomeDescriptionLabel.snp.size)
            
            _terribleColorView.top.equalTo(unsatisfactoryColorView.snp.bottom).offset(10)
            _terribleColorView.left.equalTo(goodColorView)
            _terribleColorView.size.equalTo(goodColorView.snp.size)
            
            _colorPickerView.top.equalTo(terribleColorView.snp.bottom).offset(10)
            _colorPickerView.centerX.equalToSuperview()
            _colorPickerView.sizeEqualTo(width: 300, height: 300)
            _colorPickerView.bottom.equalToSuperview().offset(-10)
            
        }
    }
}











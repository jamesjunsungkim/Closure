//
//  AddNewDropDownCell.swift
//  Closure
//
//  Created by James Kim on 7/21/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import DropDown
import SwipeCellKit

class AddNewDropDownCell: DropDownCell {
    
    // UI
    fileprivate var titleLabel: UILabel!
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    // MARK: Public
    public func configure(title:String) {
        titleLabel.text = title
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension AddNewDropDownCell {
    fileprivate func setupUI() {
        
        
        
        
        
        
    }
}

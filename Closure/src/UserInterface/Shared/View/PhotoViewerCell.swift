//
//  PhotoViewerCell.swift
//  Closure
//
//  Created by montapinunt Pimonta on 7/26/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import Hero

final class PhotoViewerCell: ReusableCollectionViewCell {
    typealias Object = UIImage
    
    //UI
    fileprivate(set) var scrollView: UIScrollView!
    fileprivate(set) var imageView: UIImageView!
    
    public var topInset:CGFloat = 0 {
        didSet {
            centerIfNeeded()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        scrollView.setZoomScale(1, animated: false)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        scrollView.frame = bounds
        let size: CGSize
        if let image = imageView.image {
            let containerSize = CGSize(width: bounds.width, height: bounds.height - topInset)
            if containerSize.width / containerSize.height < image.size.width / image.size.height {
                size = CGSize(width: containerSize.width, height: containerSize.width * image.size.height / image.size.width )
            } else {
                size = CGSize(width: containerSize.height * image.size.width / image.size.height, height: containerSize.height )
            }
        } else {
            size = CGSize(width: bounds.width, height: bounds.width)
        }
        imageView.frame = CGRect(origin: .zero, size: size)
        scrollView.contentSize = size
        centerIfNeeded()
    }
    
    // MARK: Public
    func setup(withObject object: UIImage, parentViewController: UIViewController, currentIndexPath: IndexPath, userInfo: [String : Any]?) {
        setupUI()
        setupGestureRecognizer()
        configure(withImage: object)
        setupHero(withIndexPath: currentIndexPath)
    }

    // MARK: Actions
    @objc fileprivate func useDidDoubleTap(gr: UITapGestureRecognizer) {
        if scrollView.zoomScale == 1 {
            scrollView.zoom(to: zoomRectForScale(scale: scrollView.maximumZoomScale, center: gr.location(in: gr.view)), animated: true)
        } else {
            scrollView.setZoomScale(1, animated: true)
        }
    }
    
    // MARK: Fileprivate
    fileprivate func configure(withImage i : UIImage) {
        imageView.image = i
        
        //TODO: I think I won't use top inset. better to set it here? instead of setting it outside of this view controller??
        
        if #available(iOS 11.0, *) {
            guard let r = UIApplication.shared.keyWindow?.window?.safeAreaInsets else {return}
            topInset = r.top
        }
    }
    
    fileprivate func setupGestureRecognizer() {
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(useDidDoubleTap))
        doubleTap.numberOfTapsRequired = 2
        addGestureRecognizer(doubleTap)
    }
    
    fileprivate func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = imageView.frame.size.height / scale
        zoomRect.size.width  = imageView.frame.size.width  / scale
        let newCenter = imageView.convert(center, from: scrollView)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
    fileprivate func centerIfNeeded() {
        var inset = UIEdgeInsets(top: topInset, left: 0, bottom: 0, right: 0)

        if scrollView.contentSize.height < scrollView.bounds.height - topInset {
            let insetV = (scrollView.bounds.height - topInset - scrollView.contentSize.height)/2
            inset.top += insetV
            inset.bottom = insetV
        }
        if scrollView.contentSize.width < scrollView.bounds.width {
            let insetV = (scrollView.bounds.width - scrollView.contentSize.width)/2
            inset.left = insetV
            inset.right = insetV
        }
        scrollView.contentInset = inset
    }
    
    fileprivate func setupHero(withIndexPath ip: IndexPath) {
        let size = UIScreen.main.bounds
        //TODO: how to set id when there is more than one image.
        imageView.hero.id = "image"
        imageView.hero.modifiers = [.position(CGPoint(x:size.width/2, y:size.height+size.width/2)), .scale(0.6), .fade]
    }
}

extension PhotoViewerCell:UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        centerIfNeeded()
    }
}


extension PhotoViewerCell {
    fileprivate func setupUI() {
        backgroundColor = PhotoViewerViewController.BackgroundColor.collectionView
        
        scrollView = UIScrollView(frame: bounds)
        scrollView.delegate = self
        scrollView.maximumZoomScale = 3
        scrollView.contentMode = .center
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        
        addSubview(scrollView)
        
        imageView = UIImageView.create(withBackgroundColor: .clear)
        imageView.isOpaque = true
        scrollView.addSubview(imageView)
    }
}

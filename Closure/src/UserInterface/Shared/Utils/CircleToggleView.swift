//
//  NumberButton.swift
//  Closure
//
//  Created by montapinunt Pimonta on 8/18/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

final class CircleToggleView: UIView {
    
    init(borderColor:UIColor, uncheckedBackgroundColor:UIColor, checkedBackgroundColor:UIColor ) {
        self.borderColor = borderColor
        self.uncheckedBackgroundColor = uncheckedBackgroundColor
        self.checkedBackgroundColor = checkedBackgroundColor
        super.init(frame: .zero)
        
        setupUI()
    }
    
    //MARK: Public
    public var isChecked = false {
        didSet {
            toggle(_isChecked: isChecked)
        }
    }
    
    public func toggle(_isChecked: Bool) {
        backgroundColor = _isChecked ? checkedBackgroundColor : uncheckedBackgroundColor
    }
    
    // MARK: Fileprivate
    let borderColor:UIColor
    let uncheckedBackgroundColor:UIColor
    let checkedBackgroundColor:UIColor
    
    
    fileprivate func setupUI() {
        self.setBorder(color: borderColor, width: 1)
        self.backgroundColor = uncheckedBackgroundColor
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}







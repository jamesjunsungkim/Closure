//
//  FilterBar.swift
//  Closure
//
//  Created by James Kim on 7/20/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation
import CodiumLayout
import RxSwift

struct Filter:Hashable {
    let name: String
}

final class FilterBar: UIView {
    //UI
    public var scrollView: UIScrollView!
    
    init(buttonTitles: [String] ) {
        self.filters = buttonTitles.map({Filter.init(name: $0)})
        super.init(frame: .zero)
        setupUI()
    }
    
    // MARK: Public
    public var filterObservable: Observable<Filter> {
        return filterSubject.asObservable()
    }

    // MARK: Actions
    @objc fileprivate func filterBtnPressed(sender:UIButton) {
        if let b = selectedButtons.first {
            b.isSelected = false
            selectedButtons.remove(b)
        }
        
        _ = selectedButtons.insert(buttons[sender.tag])
        sender.isSelected = true
        
        guard let n = buttons[sender.tag].currentTitle else{assertionFailure();return}
        let filter = Filter.init(name: n)
        filterSubject.onNext(filter)
    }
    
    // MARK: Fileprivate
    fileprivate let filters: [Filter]
    fileprivate var buttons = [UIButton]()
    fileprivate var selectedButtons  = Set<UIButton>()
    fileprivate let filterSubject = PublishSubject<Filter>()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension FilterBar {
    fileprivate func setupUI(){
        backgroundColor = .clear
        
        scrollView = UIScrollView()
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.contentInset.left = UI.ScrollView.padding
        scrollView.contentInset.right = UI.ScrollView.padding
        scrollView.alwaysBounceHorizontal = true
        
        addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        // Buttons
        var totalWidth: CGFloat = 0.0
        
        for (index,filter) in filters.enumerated() {
            let bt = UIButton()
            bt.setTitle(filter.name, for: .normal)
            
            bt.setTitleColor(UIColor.mainBlue.withAlphaComponent(0.8), for: .normal)
            bt.setTitleColor(UIColor.mainBlue.withAlphaComponent(0.8), for: .highlighted)
            bt.setTitleColor(.white, for: .selected)
            
            bt.setBackgroundImage(UIImage.image(withColor: UIColor(white: 0.95, alpha: 1.0)), for: .normal)
            bt.setBackgroundImage(UIImage.image(withColor: UIColor(white: 0.95, alpha: 1.0)), for: .normal)
            bt.setBackgroundImage(UIImage.image(withColor: UIColor(white: 0.85, alpha: 1.0)), for: .highlighted)
            bt.setBackgroundImage(UIImage.image(withColor: UIColor.eateryBlue), for: .selected)
            bt.setBackgroundImage(UIImage.image(withColor: UIColor.transparentEateryBlue), for: .focused)
            
            bt.setCornerRadious(value: 8)
            bt.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .medium)
            bt.sizeToFit()
            bt.frame.size.width += 16.0
//            bt.frame.size.height = frame.height
            bt.center.y = frame.height/2
            bt.tag = index
            
            bt.addTarget(self, action: #selector(filterBtnPressed(sender:)), for: .touchUpInside)
            
            scrollView.addSubview(bt)
            buttons.append(bt)
            bt.snp.makeConstraints { (make) in
                make.centerY.equalToSuperview()
                make.width.equalTo(bt.frame.width)
                make.height.equalToSuperview().offset(-UI.ScrollView.padding)
                if index > 0 {
                    make.left.equalTo(buttons[index-1].snp.right).offset(UI.ScrollView.padding / 2)
                } else {
                    make.left.equalToSuperview()
                }
            }
            totalWidth += bt.frame.width + (index == filters.count - 1 ? 0.0 : UI.ScrollView.padding/2)
        }
        scrollView.contentSize = CGSize(width: totalWidth, height: frame.height)
        scrollView.setContentOffset(CGPoint(x: -UI.ScrollView.padding, y: 0), animated: false)
    }
}

//
//  ThoughtInputTextView.swift
//  Closure
//
//  Created by James Kim on 7/24/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import RxSwift
import CodiumLayout

final class ThoughtInputTextView: UIView {
    
    /**
     full screen mode is postponed til further notice..
     */
    
    //UI
    fileprivate var textView: UITextView!
    fileprivate var placeholderLabel: UILabel!
    fileprivate var fullScreenButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        setupView()
        addTarget()
    }
    
    // MARK: Public
    public var isEditingMode = false {
        didSet {
            changeEditingMode(needsToEdit: isEditingMode)
        }
    }
    
    public func configure(withReview r: Review) {
        textView.text = r.thoughts
        showOrHidePlaceHolder()
    }
    
    public func configure(withThought thought: String?) {
        textView.text = thought
        showOrHidePlaceHolder()
    }
    
    public func disableFullscreenWhenOnlyNested() {
        fullScreenButton.isHidden = true
    }
    
    public var text: String? {
        return textView.text
    }
    
    // MARK: Actions
    
    @objc fileprivate func fullscreenBtnClicked() {
        //TBD
    }
    
    // MARK: Fileprivate
    fileprivate let bag = DisposeBag()
    
    fileprivate func setupView() {
        textView.delegate = self
        
    }
    
    fileprivate func addTarget() {
        fullScreenButton.addTarget(self, action: #selector(fullscreenBtnClicked), for: .touchUpInside)
    }
    
    fileprivate func showOrHidePlaceHolder() {
        placeholderLabel.isHidden = textView.text.count != 0
    }
    
    fileprivate func changeEditingMode(needsToEdit: Bool) {
        textView.isEditable = needsToEdit
//        fullScreenButton.isHidden = !needsToEdit
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension ThoughtInputTextView: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        guard textView.text.count < 2 else {return}
        showOrHidePlaceHolder()
    }
}

extension ThoughtInputTextView {
    fileprivate func setupUI() {
        
        textView = UITextView.create(text: "", mainFontSize: 14, customFont: nil, textColor: .black, backgroundColor: .white, textAlignment: .natural, isEditable: true, isScrollEnabled: true, attributedText: nil)
        textView.autocorrectionType = .no
        textView.showsVerticalScrollIndicator = false
        textView.showsHorizontalScrollIndicator = false
        textView.setCornerRadious(value: 8)
        textView.setBorder(color: .mainBlue, width: 1)
        
        placeholderLabel = UILabel.create(text: "Enter your thoughts...".localized, textAlignment: .left, textColor: .lightGray, fontSize: 14)
        
        fullScreenButton = UIButton.create(withImageName: "fullscreen")
        fullScreenButton.isHidden = true
        
        let group: [UIView] = [textView, placeholderLabel, fullScreenButton]
        group.forEach(addSubview(_:))
        
        
        constraint(textView, placeholderLabel, fullScreenButton) { (_textview, _placeholderLabel, _fullscreenButton) in
            _textview.edges.equalToSuperview()
            
            _placeholderLabel.top.equalToSuperview().offset(7.5)
            _placeholderLabel.leading.equalToSuperview().offset(4.5)
            
            _fullscreenButton.right.bottom.equalToSuperview().offset(-5)
            _fullscreenButton.sizeEqualTo(width: 15, height: 15)
        }
    }
}

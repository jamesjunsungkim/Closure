//
//  FacebookLoginButton.swift
//  Closure
//
//  Created by James Kim on 9/13/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import PKHUD
import Firebase
import FBSDKLoginKit

final class FacebookLoginButton: UIButton {
    
    init(appStatus:AppStatus) {
        self.appStatus = appStatus
        super.init(frame: .zero)
        initialSetup()
    }
    
    // MARK: Actions
    @objc fileprivate func userDidTapButton() {
        logInfo(type: .action)
        /**
         ------------------------- LOGIC RUN DOWN ------------------------------
         since the facebook only gives us email and name and our goal is to save other data like sign up date, we need a work around it.
         
         0. when users sign up through facebook, we retrieve data we want such as email and name.
         1. we add whatever data we want to dictionary
         2. we need to check if email-uid value exists in data. if so, we need to fetch user data and proceed
         3. if it doesn't exist, we save two jsons, one for facebook.
         user -> FacebookUser -> [email address: UID]
         user -> UID -> UserInfo
         
         4. when both of them are finished, we proceed to next page. if run into any error during the process, we shows an error.
         
         */
        
        func saveNewUserToServerAndSegueToMainWindow(newUser: User, emailUIDPath: String, userPath:String) {
            // it's empty. we need to save it.
            let group = DispatchGroup()
            var anyOfOperationFailed = false
            
            group.enter()
            self.appStatus.networkManager.post(value: newUser.uid, toPath: emailUIDPath, completion: { (uidResult) in
                group.leave()
                switch uidResult {
                case .failure(_): anyOfOperationFailed = true
                default: break
                }
            })
            
            group.enter()
            self.appStatus.networkManager.postJSON(value: newUser.toDictionary, toPath: userPath, completion: { (userResult) in
                group.leave()
                switch userResult {
                case .failure(_): anyOfOperationFailed = true
                default: break
                }
            })
            
            group.notify(queue: .main, execute: {
                HUD.hide()
                guard !anyOfOperationFailed else {
                    self.getParentViewController()?.presentDefaultError(message: "Error occured during the process. please try it again.".localized + "\n" + "If this keeps happening, please sign up with email.".localized, okAction: nil)
                    return
                }
                self.saveUserToUserDefaultAndCheckIfThereAreExistingDataAndswitchToMainWindow(with: newUser)
            })
        }
        
        //1
        AppStatus.shouldAllowLockScreen = false
        
        FBSDKLoginManager().logIn(withReadPermissions: ["email"], from: getParentViewController()) {[unowned self] (result, error) in
            guard error == nil else {
                self.getParentViewController()?.presentDefaultError(message: error!.localizedDescription, okAction: nil)
                return
            }
            
            // we're not going to do anything if it's cancelled.
            guard let _result = result, !_result.isCancelled else {return}
            
            let accessToken = FBSDKAccessToken.current()
            guard let accessTokenString = accessToken?.tokenString else {
                return
            }
            
            //2
            HUD.show(.labeledProgress(title: nil, subtitle: nil))
            FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email"]).start { (_, result, err) in
                guard err == nil else {
                    HUD.hide()
                    self.getParentViewController()?.presentDefaultError(message: err!.localizedDescription, okAction: nil)
                    return
                }
                
                // I'm wondering if result can ever return nil after it succeeds?
                
                guard let dict = result as? [String:Any], let email = dict["email"] as? String, let name = dict["name"] as?String else {
                    HUD.hide()
                    self.getParentViewController()?.presentDefaultError(message: "Can not retrieve data from facebook.".localized + "\n" + "Please sign up with email".localized, okAction: nil)
                    return
                }
                
                //3
                let credentials = FacebookAuthProvider.credential(withAccessToken: accessTokenString)
                
                Auth.auth().signInAndRetrieveData(with: credentials) { (user, error) in
                    guard error == nil else {
                        print(error!)
                        HUD.hide()
                        self.getParentViewController()?.presentDefaultError(message: "Can not retrieve data from facebook.".localized + "\n" + "Please sign up with email".localized, okAction: nil)
                        return
                    }
                    /**
                     we need to save two jsons to server.
                     but what if users already sign up with facebook and click on sign up without thinking ahead.
                     so we need to check if the email-uid json already exists, if so we don't need to save it.
                     
                     PS. when fetching data from server, path must not have '.' '#' '$' '[' or ']'' otherwise, it will crash.
                     
                     */
                    
                    let newUser = User(uid: UUID().uuidString, name: name, emailAddress: email)
                    let emailUIDPath = Path.toFacebookUser(withEmail: email.removeCharacters(in: "/@.#$[]")).stringValue
                    let userPath = Path.toUser(withUID: newUser.uid).stringValue
                    
                    self.appStatus.networkManager.fetch(fromPath: emailUIDPath, completion: { (result) in
                        // since what we want to check if there exists the user or not. if user haven't singed up via facebook then, it will fall in to failure as InvalidData.
                        switch result {
                        case .success(let uidAnyValue):
                            /**
                             user did sign up via facebook before. we need to fetch data.
                             what if there is uid but no user data?
                             it might be possible use force quit the app when it was sending data to server and didn't finish it.
                             */
                            guard let existingUID = uidAnyValue as? String else {
                                HUD.hide()
                                assertionFailure()
                                return
                            }
                            
                            self.appStatus.networkManager.fetchJSON(fromPath: Path.toUser(withUID: existingUID).stringValue, completion: { (jsonFetchResult) in
                                switch jsonFetchResult {
                                case .success(let json):
                                    HUD.hide()
                                    let existingUser = User(from: json)
                                    self.saveUserToUserDefaultAndCheckIfThereAreExistingDataAndswitchToMainWindow(with: existingUser)
                                    
                                case .failure(let e):
                                    /**
                                     if it's invalid data, it means it force-quits while saving data. in that case, we just overwrites the user data.
                                     */
                                    if e == .invalidData {
                                        saveNewUserToServerAndSegueToMainWindow(newUser: newUser, emailUIDPath: emailUIDPath, userPath: userPath)
                                    } else {
                                        // it means network error
                                        HUD.hide()
                                        self.getParentViewController()?.presentDefaultError(message: "Can not retrieve data from server.".localized + "\n" + "Please try it again".localized, okAction: nil)
                                    }
                                }
                            })
                            
                        case .failure(let e):
                            guard e == .invalidData else {
                                // actual error
                                HUD.hide()
                                self.getParentViewController()?.presentDefaultError(message: e.localizedDescription, okAction: nil)
                                return
                            }
                            // user haven't signed up with us via facebook. save user and uid data to server.
                            
                            saveNewUserToServerAndSegueToMainWindow(newUser: newUser, emailUIDPath: emailUIDPath, userPath: userPath)
                        }
                    })
                }
            }
        }
    }
    
    public func fetchReviewDataFromServer(uid:String, actionAfterFetch:@escaping ()->Void) {
        appStatus.networkManager.fetchJSONList(fromPath: Path.toReviews(withUID: uid).stringValue) {[unowned self] (result) in
            HUD.hide()
            switch result {
            case .success(let dicts):
                let count = dicts.count
                
                guard count != 0 else {
                    actionAfterFetch()
                    return
                }
                
                UIView.getMostTopViewController()?.presentDefaultAlert(withTitle: "Alert".localized, message: "\(count) " + "reviews are found in server.".localized + "\n" + "Would you like to save them to device?".localized, okAction: {
                    
                    /**
                     ------------------------- LOGIC RUN DOWN ------------------------------
                     if user agrees with it, we need to download images and then save them to disk.
                     
                     0. categories the array into two arrays.
                     1. if the count of needToDownloadImage is not 0, we download images -> add it to the dictionary -> add that dictionary to readyToInsert Array
                     2. convert them into Review and add them to disk.
                     
                     */
                    
                    let categorizedArray = dicts.categorizeIntoTwoArrays(condition: {$0[Review.Keys.imageURL] != nil})
                    let needsToDownloadImages = categorizedArray.satisfied
                    var readyToInsertNewEntries = categorizedArray.unsatisfied
                    
                    needsToDownloadImages.asyncForEach(completion: {[unowned self] in
                        // insert all entries to coredata
                        HUD.hide()
                        readyToInsertNewEntries.forEach({Review.convertFromJsonAndInsert(withJSON: $0, into: self.appStatus.mainContext, wordCountManager: self.appStatus.wordCountManager)})
                        actionAfterFetch()
                        
                        }, block: { (dict, innerCompletion) in
                            HUD.show(.labeledProgress(title: "Downloading images...".localized, subtitle: nil))
                            guard let urlStringValue = dict[Review.Keys.imageURL] as? String, let url = URL.init(string: urlStringValue) else {assertionFailure();return}
                            // when download is finished, we add the image to the dictionary and then add that dictionary to readytoInsertnewentry
                            
                            URLSession.shared.dataTask(with: url, completionHandler: { (data, _, _) in
                                let newValue = dict.addValueIfNotEmpty(forKey: Review.Keys.imageData, value: data)
                                readyToInsertNewEntries.append(newValue)
                                innerCompletion()
                            }).resume()
                    })
                }, cancelAction: actionAfterFetch)
                
            case .failure(_):
                UIView.getMostTopViewController()?.presentDefaultError(message: "Failed to fetch data from server. it Will proceed to main page".localized, okAction: actionAfterFetch)
            }
        }
    }
    
    
    // MARK: Fileprivate
    fileprivate weak var appStatus:AppStatus!
    
    fileprivate func initialSetup() {
        addTarget(self, action: #selector(userDidTapButton), for: .touchUpInside)
    }
    
    fileprivate func saveUserToUserDefaultAndCheckIfThereAreExistingDataAndswitchToMainWindow(with user:User) {
        fetchReviewDataFromServer(uid: user.uid, actionAfterFetch: {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.saveUserToUserDefaultAndSwitchToMainTabVCAndShowBlurryVC(WithUser: user)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}











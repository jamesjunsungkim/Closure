//
//  CardView.swift
//  Closure
//
//  Created by montapinunt Pimonta on 7/23/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout

final class CardView: UIView {
    //UI
    fileprivate var badTextField: UITextField!
    fileprivate var goodTextField: UITextField!
    fileprivate var goalTextField: UITextField!
    
    // for language change
    fileprivate var titleForBadView:UILabel!
    fileprivate var titleForGoodView:UILabel!
    fileprivate var titleForGoalView:UILabel!
    
    init(languageManager: LanguageManager) {
        self.languageManager = languageManager
        super.init(frame: .zero)
        setupUI()
        enableOrDisableTextfieldAndAdjustAlignment(needsToEnable: isEditingMode)
        setupObserver()
    }
    
    // MARK: Public
    public var isEditingMode = false {
        didSet {
            enableOrDisableTextfieldAndAdjustAlignment(needsToEnable: isEditingMode)
        }
    }
    
    public func observeLangaugeChange(from manager:LanguageManager) {
        manager.changeLanguageObservable.subscribe(onNext: observeLanguageChange).disposed(by: bag)
    }
    
    public func configure(withReview r: Review) {
        badTextField.text = r.bad.checkIfNotEmptyOrPresent(defaultValue: "(Empty)".localized)
        goodTextField.text = r.good.checkIfNotEmptyOrPresent(defaultValue: "(Empty)".localized)
        goalTextField.text = r.goal.checkIfNotEmptyOrPresent(defaultValue: "(Empty)".localized)
    }
    
    // MARK: Actions
    fileprivate lazy var observeLanguageChange:(Language) -> Void = {[weak self] (_) in
        /**
         it's deallocated when switching between languages.
         */
        guard let self = self else {return}
        self.titleForBadView.text = "Rooms For Improvement".localized
        self.titleForGoodView.text = "Achievements".localized
        self.titleForGoalView.text = "Goal for the following day".localized
    }
    
    // MARK: - Fileprivate
    fileprivate weak var languageManager: LanguageManager!
    fileprivate let bag = FilterDisposeBag.defaultBag
    
    fileprivate func enableOrDisableTextfieldAndAdjustAlignment(needsToEnable:Bool) {
        let group = [badTextField, goodTextField, goalTextField]
        group.forEach({
            $0!.isEnabled = needsToEnable
            $0!.textAlignment = needsToEnable ? .left : .center
        })
    }
    
    fileprivate func setupObserver() {
        languageManager.changeLanguageObservable.subscribe(onNext: observeLanguageChange).disposed(by: bag)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension CardView {
    fileprivate func setupUI() {
        
        titleForBadView = UILabel.create(text: "Rooms for improvements".localized, textAlignment: .left, textColor: UI.Component.titleColor, fontSize: UI.Component.titleSize, boldFont: false, numberOfLine: 1)
        badTextField = UITextField.create(placeHolder: "", textSize: UI.Component.contentSize, textColor: UI.Component.contentColor, keyboardType: .default)
        badTextField.borderStyle = .none
        let badSeparatorLine = UIView.create()
        
        titleForGoodView = UILabel.create(text: "Achievements".localized, textAlignment: .left, textColor: UI.Component.titleColor, fontSize: UI.Component.titleSize, boldFont: false, numberOfLine: 1)
        goodTextField = UITextField.create(placeHolder: "", textSize: UI.Component.contentSize, textColor: UI.Component.contentColor, keyboardType: .default)
        goodTextField.borderStyle = .none
        let goodSeparatorLine = UIView.create()
        
        titleForGoalView = UILabel.create(text: "Goal for the following day".localized, textAlignment: .left, textColor: UI.Component.titleColor, fontSize: UI.Component.titleSize, boldFont: false, numberOfLine: 1)
        goalTextField = UITextField.create(placeHolder: "", textSize: UI.Component.contentSize, textColor: UI.Component.contentColor, keyboardType: .default)
        goalTextField.borderStyle = .none
        let goalSeparatorLine = UIView.create()
        
        
        let group: [UIView] = [titleForBadView, badTextField, badSeparatorLine, titleForGoodView, goodTextField, goodSeparatorLine, titleForGoalView, goalTextField, goalSeparatorLine ]
        group.forEach(addSubview(_:))

        
        constraint(titleForBadView, badTextField, badSeparatorLine, titleForGoodView, goodTextField, goodSeparatorLine, titleForGoalView, goalTextField, goalSeparatorLine) { (_badTitle, _badTextField, _badSeparator, _goodTitle, _goodTextField, _goodSeparator, _goalTitle, _goalTextField, _goalSeparator) in
            
            _badTitle.left.right.equalToSuperview()
            _badTitle.top.equalToSuperview().offset(5)
            
            _badTextField.left.right.equalTo(titleForBadView)
            _badTextField.top.equalTo(titleForBadView.snp.bottom).offset(8)
            _badTextField.height.equalTo(25)
            
            _badSeparator.left.right.equalTo(titleForBadView)
            _badSeparator.height.equalTo(0.5)
            _badSeparator.top.equalTo(badTextField.snp.bottom)
            
            _goodTitle.left.right.equalTo(titleForBadView)
            _goodTitle.top.equalTo(badSeparatorLine.snp.bottom).offset(8)
            
            _goodTextField.left.right.equalTo(titleForBadView)
            _goodTextField.top.equalTo(titleForGoodView.snp.bottom).offset(8)
            _goodTextField.height.equalTo(25)
            
            _goodSeparator.left.right.equalTo(titleForBadView)
            _goodSeparator.height.equalTo(0.5)
            _goodSeparator.top.equalTo(goodTextField.snp.bottom)
            
            _goalTitle.left.right.equalTo(titleForBadView)
            _goalTitle.top.equalTo(goodSeparatorLine.snp.bottom).offset(8)
            
            _goalTextField.left.right.equalTo(titleForBadView)
            _goalTextField.top.equalTo(titleForGoalView.snp.bottom).offset(8)
            _goalTextField.height.equalTo(25)
            
            _goalSeparator.left.right.equalTo(titleForBadView)
            _goalSeparator.height.equalTo(0.5)
            _goalSeparator.top.equalTo(goalTextField.snp.bottom)
        }
        
    }
}

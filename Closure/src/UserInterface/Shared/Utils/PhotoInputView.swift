//
//  PictureInputImageView.swift
//  Closure
//
//  Created by James Kim on 7/24/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import Photos
import CodiumLayout
import RxSwift

final class PhotoInputView: UIView, NameDescribable {
    // UI
    fileprivate var imageView: UIImageView!
    fileprivate var deleteButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        addTarget()
    }
    
    // MARK: Public
    public var isEditingMode = true {
        didSet {
            changeEditingMode(needsToChange: isEditingMode)
        }
    }
    
    public func configure(withImage image: UIImage?) {
        imageView.image = image == nil ? UIImage.create(forKey: .noImage) : image!
        showOrHideDeleteButton()
    }
    
    public var selectedImage: UIImage? {
        guard let targetImage = imageView.image, targetImage != UIImage.create(forKey: .noImage), targetImage != UIImage.create(forKey: .plusBlueCircle) else {return nil}
        return targetImage
    }
    
    // MARK: Actions
    @objc fileprivate func showAlbums() {
        // when checking status and asking for permission, we don't want our blurry vc to pop up.
        AppStatus.shouldAllowLockScreen = false
        PHPhotoLibrary.authorized.skip(1)
            .subscribe(onNext: observePhotoLibraryPermission, onDisposed: observerDisposedDescription).disposed(by: bag)
    }
    
    fileprivate lazy var observePhotoLibraryPermission : (Bool) -> Void = {[unowned self] (authorized) in
        AppStatus.shouldAllowLockScreen = true
        
        switch authorized {
        case true:
            waitFor(milliseconds: 150, completion: {
                guard let topVC = UIView.getMostTopViewController() else {assertionFailure();return}
                let targetVC = AlbumMasterViewController(photoSelectionAction: self.userDidChoosePhotos)
                topVC.presentPopUpNavigationController(targetVC: targetVC,
                                                       targetSize: UIScreen.main.bounds.size,
                                                       userInfo: nil)
            })
            
        case false:
            waitFor(milliseconds: 200, completion:{
                guard let topVC = UIView.getMostTopViewController() else {assertionFailure();return}
                topVC.presentDefaultAlert(withTitle: "Permission denied".localized, message: "We need you to allow it to access your photos".localized + "\n" + "in order to use photos".localized, okAction: {
                    topVC.openAppSettings()
                }, cancelAction: nil)
            })
        }
    }
    fileprivate lazy var userDidChoosePhotos: ([UIImage])->Void = {[unowned self] (photos) in
        guard let i = photos.first else {assertionFailure();return}
        self.imageView.image = i
        self.showOrHideDeleteButton()
    }
    
    @objc fileprivate func deleteBtnClicked() {
        imageView.image = UIImage.create(forKey: isEditingMode ? .plusBlueCircle : .noImage)
        showOrHideDeleteButton()
    }
    
    @objc fileprivate func showImageViewer() {
        guard selectedImage != nil else {return}
        hero.isEnabled = true
        imageView.hero.id = "image"
        
        let target = PhotoViewerViewController(photoArray: [imageView.image!])
        performOnMain {
            UIView.getMostTopViewController()?.presentDefaultVC(targetVC: target, userInfo: nil, shouldPushOnNavigationController: false, completion: nil)
        }
    }
    
    // MARK: Fileprivate
    fileprivate let bag = DisposeBag()
    fileprivate lazy var showAlbumTap = UITapGestureRecognizer(target: self, action: #selector(showAlbums))
    fileprivate lazy var showPhotoViewerTap = UITapGestureRecognizer(target: self, action: #selector(showImageViewer))
    
    fileprivate func addTarget() {
        deleteButton.addTarget(self, action: #selector(deleteBtnClicked), for: .touchUpInside)
        imageView.addGestureRecognizer(showAlbumTap)
    }
    fileprivate func showOrHideDeleteButton() {
        deleteButton.isHidden = selectedImage == nil
    }
    
    fileprivate func changeEditingMode(needsToChange: Bool) {
        // if it's editing mode, when tapping, it's photo scrollview instead of selecting a photo.
        if needsToChange {
            imageView.removeGestureRecognizer(showPhotoViewerTap)
            imageView.addGestureRecognizer(showAlbumTap)
            showOrHideDeleteButton()
        } else {
            imageView.removeGestureRecognizer(showAlbumTap)
            imageView.addGestureRecognizer(showPhotoViewerTap)
            deleteButton.isHidden = true
        }
        
        guard selectedImage == nil else {return}
        imageView.image = UIImage.create(forKey: needsToChange ? .plusBlueCircle : .noImage)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension PhotoInputView {
    fileprivate func setupUI() {
        imageView = UIImageView.create(withImageKey: .plusBlueCircle)
        imageView.setCornerRadious(value: 5)
        imageView.contentMode = .scaleAspectFill
        
        deleteButton = UIButton.create(withNameKey: .minusButton)
        deleteButton.isHidden = true
        
        let group: [UIView] = [imageView, deleteButton]
        group.forEach(addSubview(_:))
        
        constraint(imageView, deleteButton) { (_imageView, _deleteButton) in

            _imageView.topRightqualToSuperView(withOffset: 10)
            _imageView.leftBottomEqualToSuperView(withOffset: 10)
            
            _deleteButton.centerY.equalTo(imageView.snp.top)
            _deleteButton.left.equalTo(imageView).offset(-10)
            _deleteButton.sizeEqualTo(width: 20, height: 20)
        }
    }
}

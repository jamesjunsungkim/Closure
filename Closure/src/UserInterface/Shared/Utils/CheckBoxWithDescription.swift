//
//  CheckBoxWithDescription.swift
//  Closure
//
//  Created by montapinunt Pimonta on 8/12/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import RxSwift
import CodiumLayout

final class CheckBoxWithDescription<T>: UIView {
    
    //UI
    fileprivate var checkBoxImageView: UIImageView!
    fileprivate var descriptionLabel: UILabel!
    
    init(object:T? = nil, label:String, shouldBeChecked:Bool = false, fontSize:CGFloat = 15) {
        self._object = object
        super.init(frame: .zero)
        setupUI(fontSize:fontSize)
        addTarget()
        configure(withLabel: label, shouldBeChecked: shouldBeChecked)
    }

    // MARK: Public
    public var isChecked: Bool {
        return _isChecked
    }
    
    public var object:T? {
        return _object
    }
    
    public func checkOrUncheckView(needsToCheck:Bool) {
        guard _isChecked != needsToCheck else {return}
        userDidTapView()
    }
    
    public var checkObservable:Observable<Bool> {
        return checkSubject.asObservable()
    }
    
    public var needToAddObjectObservable:Observable<(T,Bool)> {
        return needToAddObjectSubject.asObservable()
    }
    
    public func checkOrUncheckWithoutSendingNotification(needsToCheck:Bool) {
        guard _isChecked != needsToCheck else {return}
        _isChecked = needsToCheck
        changeCheckBoxImage(shouldBeChecked: _isChecked)
    }

    // MARK: Actions
    @objc fileprivate func userDidTapView() {
        _isChecked = !_isChecked
        changeCheckBoxImage(shouldBeChecked: _isChecked)
        checkSubject.onNext(_isChecked)
        
        // we only send the object if it exists..
        guard let _object = _object else {return}
        needToAddObjectSubject.onNext((_object, _isChecked))
    }
    
    // MARK: Fileprivate
    fileprivate var _isChecked = false
    fileprivate let _object:T?
    
    fileprivate let checkSubject = PublishSubject<Bool>()
    fileprivate let needToAddObjectSubject = PublishSubject<(T,Bool)>()
    
    fileprivate func addTarget() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(userDidTapView))
        self.addGestureRecognizer(tap)
    }
    
    fileprivate func changeCheckBoxImage(shouldBeChecked:Bool) {
        checkBoxImageView.image = shouldBeChecked ? UIImage.create(forKey: .checkedBox) : UIImage.create(forKey: .uncheckedBox)
    }
    
    fileprivate func configure(withLabel l : String, shouldBeChecked:Bool = false) {
        _isChecked = shouldBeChecked
        changeCheckBoxImage(shouldBeChecked: _isChecked)
        descriptionLabel.text = l
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension CheckBoxWithDescription {
    fileprivate func setupUI(fontSize:CGFloat) {
        
        checkBoxImageView = UIImageView.create(withImageKey: .uncheckedBox)
        
        descriptionLabel = UILabel.create(text: "Hello", textAlignment: .left, fontSize: fontSize)
        
        let group:[UIView] = [checkBoxImageView, descriptionLabel]
        group.forEach(self.addSubview(_:))
        
        constraint( checkBoxImageView, descriptionLabel) { (_checkBoxImageView, _descriptionLabel) in
            _checkBoxImageView.left.equalToSuperview().offset(5)
            _checkBoxImageView.centerY.equalToSuperview()
            _checkBoxImageView.sizeEqualTo(width: 25, height: 25)
            
            _descriptionLabel.left.equalTo(checkBoxImageView.snp.right).offset(5)
            _descriptionLabel.centerY.equalToSuperview()
        }
    }
}

//
//  StringDisplayView.swift
//  Closure
//
//  Created by James Kim on 9/1/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout

final class StringDisplayView: UIView {
    /**
     there are two versions, one for when we create it with strings to display from the get-go.
     the other is for when we need to update it later.
     */
    
    //UI
    fileprivate var titleLabel: UILabel!
    fileprivate var displayLabelAndText = [(label:UILabel,text:String)]()
    
    init() {
        super.init(frame: .zero)
        setHeightConstraintToZero()
    }
    
    init(title:String, strings:[String], targetWidth: CGFloat, fontSize:CGFloat, displayBackgroundColor:UIColor, borderColor:UIColor,viewBackgroundColor:UIColor) {
        super.init(frame: .zero)
        
        setupUI(title:title, englishStringToPresent: strings, targetWidth:targetWidth, fontSize:fontSize,displayBackgroundColor:displayBackgroundColor, borderColor: borderColor,viewBackgroundColor:viewBackgroundColor )
    }
    
    // MARK: Public
    public var totalHeight:CGFloat {
        return _totalHeightOfDisplayViews
    }
    
    public func createDisplayString(title:String,values:[String], targetWidth: CGFloat, fontSize:CGFloat, displayBackgroundColor:UIColor,borderColor:UIColor,viewBackgroundColor:UIColor = .white) {
        reset()
        guard values.count != 0 else {return}
        setupUI(title: title, englishStringToPresent: values, targetWidth: targetWidth, fontSize: fontSize, displayBackgroundColor: displayBackgroundColor,borderColor: borderColor, viewBackgroundColor:viewBackgroundColor)
    }
    
    public func translateTextAssignedToLabels() {
        /**
         since translated words take up different space, we need to make it look nice.
         when string display view is not initalized and then observe language change, just do nothing.
         
         if present string includes date string, we have to break it down and translate only non-date parts.
         
         if it contains number, it's date.
         that's how we categories it into two arrays.
         
         */
        
        guard !presentStringsInEnglish.isEmpty else {return}
        
        // reset contraints.
        reset()
        createUILabels(title: title.localized, stringValues: translatedStrings, targetWidth: targetWidth, fontSize: fontSize, displayBackgroundColor: displayBackgroundColor, borderColor: borderColor, viewBackgroundColor: viewBackgroudnColor)
        
    }
    
    // MARK: Fileprivate
    fileprivate var title:String!
    fileprivate var targetWidth:CGFloat!
    fileprivate var fontSize:CGFloat!
    fileprivate var displayBackgroundColor: UIColor!
    fileprivate var borderColor:UIColor!
    fileprivate var viewBackgroudnColor:UIColor!
    fileprivate var _totalHeightOfDisplayViews:CGFloat!
    fileprivate var presentStringsInEnglish = [String]()
    
    fileprivate var translatedStrings:[String] {
        let pool = presentStringsInEnglish.categorizeIntoTwoArrays(condition: {$0.checkIfContainNumber()})
        let nonNumbers = pool.unsatisfied.map({$0.localized})
        let dateStrings = pool.satisfied.map { (input) -> String in
            // we split it with ":" and translate the first part and put it back and return it.
            
            let splits = input.split(separator: ":").map({String($0)})
            guard let stringPart = splits.first, let datePart = splits.last else {assertionFailure();return input}
            
            let result = stringPart.localized + ":" + datePart
            
            return result
        }
        
        return nonNumbers + dateStrings
    }
    
    fileprivate let titleLeftPadding: CGFloat = 5
    fileprivate let titleRightPadding:CGFloat = 5
    fileprivate let paddingBetweenLabels:CGFloat = 10
    
    fileprivate var initialHeightConstraint: Constraint!

    
    fileprivate func setupUI(title:String, englishStringToPresent:[String], targetWidth: CGFloat, fontSize:CGFloat, displayBackgroundColor:UIColor, borderColor:UIColor, viewBackgroundColor:UIColor) {
        // we save it for when translating words later.
        self.title = title
        self.targetWidth = targetWidth
        self.fontSize = fontSize
        self.presentStringsInEnglish = englishStringToPresent
        self.displayBackgroundColor = displayBackgroundColor
        self.borderColor = borderColor
        self.viewBackgroudnColor = viewBackgroundColor
        
        createUILabels(title: title.localized, stringValues: translatedStrings, targetWidth: targetWidth, fontSize: fontSize, displayBackgroundColor: displayBackgroundColor, borderColor: borderColor, viewBackgroundColor: viewBackgroudnColor)
    }
    
    fileprivate func reset() {
        self.subviews.forEach({$0.removeFromSuperview()})
        displayLabelAndText.removeAll()
        setHeightConstraintToZero()
    }
    
    fileprivate func setHeightConstraintToZero() {
        // initial height before we set it
        
        if initialHeightConstraint != nil {
            initialHeightConstraint.deactivate()
            initialHeightConstraint = nil
        }
        
        self.snp.makeConstraints {[unowned self] (make) in
            self.initialHeightConstraint = make.height.equalTo(0).constraint
            self._totalHeightOfDisplayViews = 0
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension StringDisplayView {
    fileprivate func createUILabels(title:String, stringValues:[String], targetWidth: CGFloat, fontSize:CGFloat, displayBackgroundColor:UIColor, borderColor:UIColor, viewBackgroundColor:UIColor) {
        
        guard stringValues.count != 0 else {
            setHeightConstraintToZero()
            return
        }
        
        self.backgroundColor = viewBackgroundColor
        
        titleLabel = UILabel.create(text: "\(title): ", textAlignment: .left, fontSize: fontSize+2)
        titleLabel.sizeToFit()
        
        self.addSubview(titleLabel)
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview().offset(titleLeftPadding)
        }
        
        var currentPositionX:CGFloat = 5.0
        var currentCenterY:CGFloat = 0
        
        var leftWidth:CGFloat = targetWidth - (titleLabel.frame.size.width + titleLeftPadding + titleRightPadding)
        
        
        //if there is height consrtaint set during the initializer, we need to remove it.
        if initialHeightConstraint != nil {
            initialHeightConstraint.deactivate()
            initialHeightConstraint = nil
        }
        
        
        for (index, text) in stringValues.enumerated() {
            let displayLabel = UILabel.create(text: text, textAlignment: .center, fontSize: fontSize)
            
            self.addSubview(displayLabel)
            displayLabelAndText.append((displayLabel,text))
            displayLabel.backgroundColor = displayBackgroundColor
            displayLabel.setCornerRadious(value: 5)
            displayLabel.setBorder(color: borderColor, width: 1)
            
            displayLabel.sizeToFit()
            displayLabel.frame.size.width += 16
            
            let displayWidth = displayLabel.frame.width
            let displayHeight = displayLabel.frame.height
            
            displayLabel.snp.makeConstraints({[unowned self] (make) in
                if leftWidth < displayWidth {
                    // needs to go to new line
                    currentPositionX = self.titleRightPadding
                    currentCenterY += displayHeight + self.paddingBetweenLabels
                    leftWidth = targetWidth - (titleLabel.bounds.width + self.titleLeftPadding + self.titleRightPadding)
                }
                
                make.left.equalTo(titleLabel.snp.right).offset(currentPositionX)
                make.centerY.equalTo(titleLabel).offset(currentCenterY)
                make.width.equalTo(displayLabel.frame.width)
                
                currentPositionX += displayWidth + paddingBetweenLabels
                leftWidth -= (displayWidth+paddingBetweenLabels)
                
                if index == stringValues.count-1 {
                    self._totalHeightOfDisplayViews = currentCenterY + displayHeight + 5
                    make.bottom.equalToSuperview().offset(-5)
                }
            })
        }
    }
}





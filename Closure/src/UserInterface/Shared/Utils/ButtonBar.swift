//
//  FilterBar.swift
//  Closure
//
//  Created by James Kim on 7/20/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout
import RxSwift

final class ButtonBar: UIView {
    
    //UI
    fileprivate var scrollView: UIScrollView!
    
    init(buttonTitles: [String] ) {
        self.buttonNames = buttonTitles
        super.init(frame: .zero)
        setupUI()
    }
    
    // MARK: Public
    public var isScrollEnabled = false {
        didSet {
            scrollView.isScrollEnabled = isScrollEnabled
            scrollView.alwaysBounceHorizontal = isScrollEnabled
        }
    }
    
    public var buttonIndexObservable: Observable<Int> {
        return buttonIndexSubject.asObservable()
    }
    
    public func changeButtonNames(_ names:[String]) {
        guard names.count == buttons.count else {assertionFailure();return}
        for (index, button) in buttons.enumerated() {
            button.changeTo(title: names[index])
        }
    }
    
    // MARK: Actions
    @objc fileprivate func filterBtnPressed(sender:UIButton) {
        buttonIndexSubject.onNext(sender.tag)
    }
    
    // MARK: Fileprivate
    fileprivate let buttonNames: [String]
    fileprivate var buttons = [UIButton]()
    fileprivate let buttonIndexSubject = PublishSubject<Int>()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension ButtonBar {
    fileprivate func setupUI(){
        backgroundColor = .clear
        
        scrollView = UIScrollView()
        scrollView.contentInset.left = UI.ScrollView.padding
        scrollView.contentInset.right = UI.ScrollView.padding
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.alwaysBounceHorizontal = false
        scrollView.isScrollEnabled = false
        
        addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        // Buttons
        var totalWidth: CGFloat = 0.0
        
        for (index,buttonName) in buttonNames.enumerated() {
            let bt = UIButton()
            bt.setTitle(buttonName, for: .normal)
            
            
            bt.setTitleColor(.mainBlue, for: .normal)
            bt.setTitleColor(.white, for: .highlighted)
            
            bt.setBackgroundImage(UIImage.image(withColor: .white), for: .normal)
            bt.setBackgroundImage(UIImage.image(withColor: UIColor.eateryBlue).withAlpha(0.8), for: .highlighted)
            bt.setBackgroundImage(UIImage.image(withColor: UIColor.eateryBlue), for: .focused)
            bt.setBackgroundImage(UIImage.image(withColor: UIColor.eateryBlue), for: .selected)
            
            bt.setCornerRadious(value: 5)
            bt.setBorder(color: .mainBlue, width: 1)
            
            bt.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .medium)
//            bt.sizeToFit()
            bt.frame.size.width = 50
            bt.frame.size.width += 16.0
            bt.center.y = frame.height/2
            bt.tag = index
            
            bt.addTarget(self, action: #selector(filterBtnPressed(sender:)), for: .touchUpInside)
            
            scrollView.addSubview(bt)
            buttons.append(bt)
            bt.snp.makeConstraints { (make) in
                make.centerY.equalToSuperview()
                make.width.equalTo(bt.frame.width)
                make.height.equalToSuperview().offset(-UI.ScrollView.padding)
                if index > 0 {
                    make.left.equalTo(buttons[index-1].snp.right).offset(UI.ScrollView.padding / 2)
                } else {
                    make.left.equalToSuperview()
                }
            }
            totalWidth += bt.frame.width + (index == buttonNames.count - 1 ? 0.0 : UI.ScrollView.padding/2)
        }
        scrollView.contentSize = CGSize(width: totalWidth, height: frame.height)
        scrollView.setContentOffset(CGPoint(x: -UI.ScrollView.padding, y: 0), animated: false)
    }
}

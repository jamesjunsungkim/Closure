//
//  CheckView.swift
//  Closure
//
//  Created by James Kim on 7/30/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import RxSwift

final class CheckView:UIView {
    
    //UI
    fileprivate var checkImageView:UIImageView!
    
    init(backgroundColor: UIColor = .white, tag: Int) {
        super.init(frame: .zero)
        self.backgroundColor = backgroundColor
        self.tag = tag
        
        setupUI()
        addTarget()
    }
    
    override var backgroundColor: UIColor? {
        get {
            return super.backgroundColor
        }
        set {
            super.backgroundColor = newValue
            // for when it's loading
            guard checkImageView != nil else {return}
            checkImageView.tintColor = newValue?.inverted
        }
    }
    
    // MARK: Public
    public var tagObservable: Observable<Int> {
        return tagSubject.asObservable()
    }
    
    public var isChecked: Bool {
        return !checkImageView.isHidden
    }
    
    public func toggleCheck() {
        checkImageView.isHidden = !checkImageView.isHidden
    }
    
    // MARK: Actions
    @objc fileprivate func userDidTapView() {
        tagSubject.onNext(tag)
    }
    
    // MARK: Fileprivate
    fileprivate let tagSubject = PublishSubject<Int>()
    
    fileprivate func addTarget() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(userDidTapView))
        self.addGestureRecognizer(tap)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension CheckView {
    fileprivate func setupUI() {
        checkImageView = UIImageView.create(withImage: UIImage(named: "check")!.withRenderingMode(.alwaysTemplate))
        checkImageView.tintColor = backgroundColor!.inverted
        checkImageView.isHidden = true
        
        addSubview(checkImageView)
        checkImageView.snp.makeConstraints { (make) in
            make.topRightqualToSuperView(withOffset: 10)
            make.leftBottomEqualToSuperView(withOffset: 10)
        }
    }
}

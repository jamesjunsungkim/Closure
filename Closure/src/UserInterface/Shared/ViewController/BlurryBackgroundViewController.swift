//
//  BlurryBackgroundViewController.swift
//  Closure
//
//  Created by James Kim on 8/19/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout

final class BlurryBackgroundViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enterViewControllerMemoryLog(self)
        setupUI()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
}

extension BlurryBackgroundViewController {
    fileprivate func setupUI() {
        view.backgroundColor = UIColor.passCodeBackground
        
        let lockImageView = UIImageView.create(withImageName: "lock")
        let label = UILabel.create(text: "Closure", textAlignment: .center, textColor: .gray, fontSize: 35, boldFont:true)
        
        let group:[UIView] = [lockImageView, label]
        group.forEach(view.addSubview(_:))
        
        constraint(lockImageView, label) { (_lockImageView, _label) in
            _lockImageView.centerY.equalToSuperview().offset(-40)
            _lockImageView.centerX.equalToSuperview()
            _lockImageView.sizeEqualTo(width: 70, height: 70)
            
            _label.top.equalTo(lockImageView.snp.bottom).offset(20)
            _label.centerX.equalToSuperview()
        }
    }
}




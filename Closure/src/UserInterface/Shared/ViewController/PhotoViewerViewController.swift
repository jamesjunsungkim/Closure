//
//  PhotoViewerViewController.swift
//  Closure
//
//  Created by montapinunt Pimonta on 7/26/18.
//  Copyright © 2018 James Kim. All rights reserved.
//
import UIKit
import Hero
import CoreData

final class PhotoViewerViewController: DefaultViewController {
    
    //UI
    fileprivate var collectionView: UICollectionView!
    
    init(photoArray: [UIImage]) {
        self.photoArray = photoArray
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enterViewControllerMemoryLog(self)
        setupUI()
        setupCollectionView()
        setupGestureRecognier()
    }
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        hideStatusBar = true
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var prefersStatusBarHidden: Bool {
        return hideStatusBar
    }
    
    // MARK: Public
    struct BackgroundColor {
        static let collectionView = UIColor(white: 0.1, alpha: 0.8)
    }
    
    
    // MARK: Action
    @objc fileprivate func userDidPan() {
        let translation = panGR.translation(in: nil)
        let progress = translation.y / 2 / collectionView!.bounds.height
        switch panGR.state {
        case .began:
            hero.dismissViewController()
        case .changed:
            Hero.shared.update(progress)
            if let cell = collectionView.visibleCells[0]  as? PhotoViewerCell {
                let currentPos = CGPoint(x: translation.x + view.center.x, y: translation.y + view.center.y)
                Hero.shared.apply(modifiers: [.position(currentPos)], to: cell.imageView)
            }
        default:
            if progress + panGR.velocity(in: nil).y / collectionView!.bounds.height > 0.3 {
                Hero.shared.finish()
            } else {
                Hero.shared.cancel()
            }
        }
    }
    
    
    // MARK: Fileprivate
    fileprivate let photoArray :[UIImage]
    fileprivate var panGR = UIPanGestureRecognizer()
    fileprivate var dataSource: DefaultCollectionViewDataSource<PhotoViewerCell>!
    fileprivate var hideStatusBar = false
    
    fileprivate func setupCollectionView() {
        dataSource = DefaultCollectionViewDataSource<PhotoViewerCell>.init(collectionView: collectionView, parentViewController: self, initialData: [0:photoArray])
        collectionView.isPagingEnabled = true
        collectionView.alwaysBounceHorizontal = true
    }
    
    fileprivate func setupGestureRecognier() {
        panGR.addTarget(self, action: #selector(userDidPan))
        panGR.delegate = self
        collectionView.addGestureRecognizer(panGR)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension PhotoViewerViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let cell = collectionView.visibleCells[0] as? PhotoViewerCell,
            cell.scrollView.zoomScale == 1 {
            let v = panGR.velocity(in: nil)
            return v.y > abs(v.x)
        }
        return false
    }
}

extension PhotoViewerViewController {
    fileprivate func setupUI() {
        self.hero.isEnabled = true
        
        collectionView = UICollectionView.create(backgroundColor: .white, configuration: {[unowned self] (layout) in
            layout.scrollDirection = .horizontal
            layout.itemSize = self.view.bounds.size
        })
        
        collectionView.backgroundColor = BackgroundColor.collectionView
        
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}

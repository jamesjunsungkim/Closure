//
//  ThoughtInputDetailViewController.swift
//  Closure
//
//  Created by montapinunt Pimonta on 7/26/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

final class ThoughtInputDetailViewController: DefaultViewController {
    //UI
    fileprivate var thoughtInputView: ThoughtInputTextView!
    
    init(text:String) {
        super.init(nibName: nil, bundle: nil)
        thoughtInputView.configure(withThought: text)
        thoughtInputView.isEditingMode = true
        thoughtInputView.disableFullscreenWhenOnlyNested()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension ThoughtInputDetailViewController {
    fileprivate func setupUI() {
        thoughtInputView = ThoughtInputTextView()
        view.addSubview(thoughtInputView)
        thoughtInputView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}

//
//  LaunchScreenTillSetupReady.swift
//  Closure
//
//  Created by James Kim on 9/15/18.
//  Copyright © 2018 James Kim. All rights reserved.
//
import UIKit

final class LaunchScreenTillSetupReady: DefaultViewController {
    
    //UI
    fileprivate var logoImageView:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enterViewControllerMemoryLog(self)
        setupUI()
    }
    deinit {
        leaveViewControllerMomeryLog(self)
    }

}

extension LaunchScreenTillSetupReady {
    fileprivate func setupUI() {
        view.backgroundColor = UIColor.mainBlue
        
        logoImageView = UIImageView.create(withImageName: "book_logo")
        view.addSubview(logoImageView)
        logoImageView.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
            make.sizeEqualTo(width: 100, height: 100)
        }
    }
}


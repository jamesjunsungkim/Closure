//
//  MainTabbarController.swift
//  Closure
//
//  Created by James Kim on 7/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import RxSwift
import DeckTransition
import Reachability
import SwiftEntryKit

final class MainTabBarController: UITabBarController, UITabBarControllerDelegate {
    enum Tap:Int {
        case feed
        case stats
        case addReview
        case calendar
        case setting
    }
    
    //UI
    fileprivate var feedMainViewController: FeedMainViewController!
    
    init(appStatus: AppStatus) {
        self.appStatus = appStatus
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabbar()
        setupViewControllers()
        setupObserver()
        
//        waitFor(milliseconds: 100) {[unowned self]in
//            self.selectedIndex = 4
////            self.presentDeckTransition(targetVC: AddReviewViewController(appStatus: self.appStatus), userInfo: [Review.Keys.date:Date()])
//        }
    }
    
    // MARK: - Public/Intenral
    public var tabObservable:Observable<Tap> {
        return tabSubject.asObservable()
    }
    
    // MARK: Actions
    
    fileprivate lazy var observeDatePicker: (Date)->Void = {[unowned self] (date) in
        waitFor(milliseconds: 300, completion: {
            let targetVC = AddReviewViewController(appStatus: self.appStatus, targetDate: date, review: nil)
            self.presentDeckTransition(targetVC: targetVC, userInfo: nil)
        })
    }

    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        guard let _currentTap = Tap.init(rawValue: tabBarController.selectedIndex) else {assertionFailure();return}
        currentTap = _currentTap
        tabSubject.onNext(_currentTap)
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        guard let array = viewControllers else {assertionFailure(); return false}
        
        if array.index(of: viewController) == Tap.addReview.rawValue {
            let alertActionForDate = UIAlertAction(title: "Choose date".localized, style: .default) {[unowned self] (_) in
                let targetVC = DateSelectionCalendarViewController()
                let userInfo:[String:Any] = [DateSelectionCalendarViewController.Keys.endDate:Date()]
                targetVC.dateSelectionObservable.subscribe(onNext: self.observeDatePicker).disposed(by: self.bag)
                self.presentPopupWithoutAnyButton(targetVC: targetVC, userInfo: userInfo)
            }
            
            presentActionSheetWithCancel(title: nil, message: nil, firstTitle: "For today".localized, firstAction: {[unowned self] in
                let targetVC = AddReviewViewController(appStatus: self.appStatus, targetDate: Date(), review: nil)
                self.presentDeckTransition(targetVC: targetVC, userInfo: nil)
                }, cancelAction: nil,
                   configuration: {
                    $0.addAction(alertActionForDate)
                    
                    /**
                     if it's simulator, we will show debug button
                     */
                    #if targetEnvironment(simulator)
                    let debugActionButton = UIAlertAction(title: "Debug", style: .default, handler: { (_) in
                        self.presentDeckTransition(targetVC: DebugSettingViewController(), userInfo:  nil)
                    })
                    $0.addAction(debugActionButton)
                    #endif
            })
            return false
        }
        
        return true
    }
    
    fileprivate lazy var observeNetworkConnection:(Reachability.Connection)->Void = {[unowned self](connection) in
        // if user toggle network change toggle off, we don't do anything.
        guard self.appStatus.notificationManager.internetDisconnectionWarningTracker.value else {return}
        
        let networkNotifiactionView = UIView.create(withColor: connection.backgroundColor, alpha: 1)
        let descriptionLabel = UILabel.create(text: connection.notificationDescriptionLabel, textAlignment: .center, textColor: .white, fontSize: 15)
        descriptionLabel.font = UIFont.systemFont(ofSize: 15)
        
        networkNotifiactionView.addSubview(descriptionLabel)
        
        descriptionLabel.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
        }
        networkNotifiactionView.snp.makeConstraints { (make) in
            make.height.equalTo(45)
        }
        
        var attributes = EKAttributes.topNote
        attributes.entryBackground = EKAttributes.BackgroundStyle.color(color: connection.backgroundColor)
        attributes.windowLevel = .statusBar
        
        SwiftEntryKit.display(entry: networkNotifiactionView, using: attributes)
    }
    
    // MARK: - Fileprivate
    fileprivate weak var appStatus: AppStatus!
    
    fileprivate var currentTap: Tap = .feed
    fileprivate let tabSubject = PublishSubject<Tap>()
    fileprivate let bag = DisposeBag()

    fileprivate func setupTabbar() {
        tabBar.backgroundColor = .white
        tabBar.barTintColor = .white
        tabBar.shadowImage = UIImage()
        self.delegate = self
    }
    
    fileprivate func setupObserver() {
        /**
         ------------------------- LOGIC RUN DOWN ------------------------------
         I want to show the description to tell that internet disconnect or changes it to celluar.
         0. observe rechability change
         1. when it spits out result, we add a simple UIview that inlcudes label saying the result.
         2. put uiview animation and then dismiss animation
         */
        
        appStatus.connectionObservable.subscribe(onNext: observeNetworkConnection).disposed(by: bag)
        
    }

    fileprivate func setupViewControllers() {
        
        feedMainViewController = FeedMainViewController(status: appStatus, parent: self)
        
        let feedNav = templateNavController(unselected: #imageLiteral(resourceName: "feed_unchecked"), selected: #imageLiteral(resourceName: "feed_checked"), rootViewController: feedMainViewController )
        let statNav = templateNavController(unselected: #imageLiteral(resourceName: "stats_unchecked"), selected: #imageLiteral(resourceName: "stats_checked"), rootViewController: StatMainViewController(status: appStatus))
        let plusNav = templateNavController(unselected: #imageLiteral(resourceName: "plus_unchecked"), selected: #imageLiteral(resourceName: "plus_unchecked"), rootViewController: UIViewController())
        let calendarNav = templateNavController(unselected: #imageLiteral(resourceName: "calendar_unchecked"), selected: #imageLiteral(resourceName: "calendar_checked"), rootViewController: CalendarMainViewController(status: appStatus))
        let settingNav = templateNavController(unselected: #imageLiteral(resourceName: "setting_unchecked"), selected: #imageLiteral(resourceName: "setting_checked"), rootViewController: SettingMainViewController(status: appStatus))

        viewControllers = [feedNav, statNav, plusNav ,calendarNav, settingNav]
    }
    
    fileprivate func templateNavController(unselected: UIImage, selected: UIImage, rootViewController: UIViewController, withLargetitle flag: Bool = false)-> UINavigationController {
        let navController = UINavigationController(rootViewController: rootViewController)
        navController.navigationBar.setupToMainBlueTheme(withLargeTitle: flag)
        
        let tbItem = navController.tabBarItem
        tbItem?.image = unselected.withRenderingMode(.alwaysOriginal)
        tbItem?.selectedImage = selected.withRenderingMode(.alwaysOriginal)
        tbItem?.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        return navController
    }
}

extension Reachability.Connection {
    public var backgroundColor: UIColor {
        switch self {
        case .cellular: return .orange
        case .wifi: return .mainBlue
        case .none: return .red
        }
    }
    
    public var notificationDescriptionLabel:String {
        switch self {
        case .cellular: return "Network switched to Ceullar".localized
        case .wifi: return "Network switched to Wifi".localized
        case .none: return "Network disconnected".localized
        }
    }
}

//
//  MainFeedCell.swift
//  Closure
//
//  Created by James Kim on 7/15/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import Hero
import CodiumLayout
import SwipeCellKit
import RxSwift

final class FeedMainCell: CoreDataReusableCollectionViewCell{
    typealias Object = Review
    
    public enum HeroAnimation: String {
        case cardView
        case rateImageView
        case image
        case note
        case date
        case feedCell
    }
    
    // UI
    fileprivate var dateLabel: UILabel!
    fileprivate var weatherImageView:UIImageView!
    fileprivate var rateImageView: UIImageView!
    fileprivate var cardView: CardView!
    fileprivate var imageIndicator: UIImageView!
    fileprivate var noteIndicator: UIImageView!
    fileprivate var favoriteIndicator:UIImageView!
    fileprivate var deleteButton: UIButton!
    
    fileprivate var animationBackgroundView:UIView!
    override func layoutSubviews() {
        super.layoutSubviews()
        
        /**
         the cardview is set 12 px away from each side.
         it is set in a way to make shadow being shed from left to right side
         */
        
        let targetRect = CGRect(x: bounds.minX+6.5, y: bounds.minY+13, width: bounds.width-10, height: bounds.height-10)
        setShadow(contentViewCornerRadius: 8, shadowRadius: 5, shadowOpacity: 0.1, shadowOffset: CGSize.init(width: 2, height: 2), shadowRect: targetRect)
    }
    
    override func prepareForReuse() {
        // call the closure passed from the view controller
        prepareForReuseClosure?(self)
        
        // reset all the non-UI variables
        bag = nil
        currentReview = nil
        prepareForReuseClosure = nil
        isFavorite = false
        isImageDisplaying = false
        isThoughtDisplaying = false
        
        // remove all subviews.
        subviews.forEach({$0.removeFromSuperview()})
    }
    
    // MARK: Public
    public var tapObservable: Observable<Review> {
        return tapSubject.asObservable()
    }
    
    public func setup(withObject object: Review, parentViewController: UIViewController, currentIndexPath: IndexPath, userInfo: [String : Any]?) {
        
        let editingTracker = userInfo![FeedMainViewController.Key.editingTracker] as! Tracker<Bool>
        let editingObservable = userInfo![FeedMainViewController.Key.editingObserve] as! Observable<Bool>
        
        prepareForReuseClosure = userInfo![keyForPrepareForRuese] as? ((FeedMainCell)->Void)
        languageManager = LanguageManager.unwrapInstanceFrom(userInfo: userInfo)
        
        currentReview = object
        isFavorite = object.isFavorite
        self.parentViewController = parentViewController
        
        /**
         pass language manager to card view.
         */
        setupUI()
        addTarget()
        setupHero()
        setupObserver(for: editingObservable)
        configure(withReview: object)
        checkEditingModeAndAdjustUI(isEditing: editingTracker.value)
    }

    public func update(withObject object: Review, atIndexPath indexPath: IndexPath) {
        /**
         my goal was to configure it only when it needs to do. but since review is a reference type which means
         when it's changed, all the variables that have reference to it change too.
         */
//        guard checkIfNeedToUpdate(review1: currentReview, review2: object) else {return}
        configure(withReview: object)
    }
    
    // MARK: Actions
    fileprivate lazy var observeEditingMode:(Bool)->Void = {[weak self] (isEditingMode) in
        guard let self = self else {return}
        isEditingMode ? self.startWiggle() : self.stopWiggle()
        self.deleteButton.isHidden = !isEditingMode
        self.favoriteIndicator.isHidden = isEditingMode || !self.isFavorite
    }
    
    @objc fileprivate func userDidTapDeleteBtn() {
        tapSubject.onNext(currentReview)
    }
    
    @objc fileprivate func userDidLongTapImageIndicatorView(sender:UILongPressGestureRecognizer) {
        guard !isImageDisplaying else {return}
        guard let targetImage = currentReview.image else {assertionFailure();return}
        let popupVC = PictureOfDayDetailViewController(imageToPresent: targetImage)
        parentViewController.presentPopupWithOnlyOK(
            targetVC: popupVC, okButtonTitle: "Ok".localized,
            okAction: {[unowned self] in
                popupVC.dismiss(animated: true, completion: nil)
                self.isImageDisplaying = false
        }, panGestureDismissal: true, tapGestureDismissal: true)
        isImageDisplaying = true
    }
    
    @objc fileprivate func userDidLongTapThoughtIndicatorView(sender:UILongPressGestureRecognizer) {
        guard !isThoughtDisplaying else {return}
        guard let targetText = currentReview.thoughts, !targetText.isEmpty else {assertionFailure();return}
        let popupVC = ThoughtOfDayDetailViewController(targetText: targetText)
        parentViewController.presentPopupWithOnlyOK(
            targetVC: popupVC, okButtonTitle: "Ok".localized,
            okAction: {[unowned self] in
                popupVC.dismiss(animated: true, completion: nil)
                self.isThoughtDisplaying = false
            }, panGestureDismissal: true, tapGestureDismissal: true)
        isThoughtDisplaying = true
    }
    
    fileprivate lazy var observeLanguageChange:(Language)->Void = {[weak self] (lan) in
        guard let self = self, let r = self.currentReview else {return}
        self.dateLabel.text = r.date.format(with: lan.toLongMonthToDayFormat)
    }
    
    // MARK: Fileprivate
    fileprivate weak var currentReview: Review!
    // in order to present image and thought on parent view controller.
    fileprivate weak var parentViewController:UIViewController!
    fileprivate weak var languageManager:LanguageManager!
    
    fileprivate let tapSubject = PublishSubject<Review>()
    
    fileprivate var prepareForReuseClosure: ((FeedMainCell)->Void)?
    fileprivate var isFavorite = false
    fileprivate var bag: DisposeBag!
    
    fileprivate var isImageDisplaying = false
    fileprivate var isThoughtDisplaying = false
    
    fileprivate func addTarget() {
        deleteButton.addTarget(self, action: #selector(userDidTapDeleteBtn), for: .touchUpInside)
        
        // add long tap to photo input view and thought view
        let imageLongTap = UILongPressGestureRecognizer(target: self, action: #selector(userDidLongTapImageIndicatorView))
        imageIndicator.addGestureRecognizer(imageLongTap)
        
        let thoughtLongTap = UILongPressGestureRecognizer(target: self, action: #selector(userDidLongTapThoughtIndicatorView(sender:)))
        noteIndicator.addGestureRecognizer(thoughtLongTap)
    }
    
    fileprivate func configure(withReview review: Review) {
        favoriteIndicator.isHidden = !review.isFavorite
        dateLabel.text = review.date.format(with: currentLanguage.toLongMonthToDayFormat)
        rateImageView.image = review.rate.image
        cardView.configure(withReview: review)
        showOrHideIndicators(forReview: review)
        
        guard review.weather != nil else {
            weatherImageView.isHidden = true
            return
        }

        weatherImageView.image = UIImage.create(for: review.weather!.weatherIconName)
    }
    
    fileprivate func setupHero() {
        /**
         hero id has to be unique like identifier. so let's add date to id
         
         if cell is used for animation, subviews are streched out during the segue which is not ideal.
         so I created the same background View without subviews so when I use it for animation, only outline is streched out.
         */
//        self.hero.isEnabled = true
        animationBackgroundView.hero.id = HeroAnimation.feedCell.rawValue + currentReview.dateString
        rateImageView.hero.id = HeroAnimation.rateImageView.rawValue + currentReview.dateString
        
        let group:[UIView] = [dateLabel, cardView]
        group.forEach({$0.hero.modifiers = [.useGlobalCoordinateSpace, .fade] })
        
//        let groupForOptional:[UIView] = [favoriteIndicator, weatherImageView, noteIndicator, imageIndicator]
//        groupForOptional.forEach({
//            $0.hero.modifiers = [.useGlobalCoordinateSpace, .useNoSnapshot]
//        })
    }
    
    fileprivate func showOrHideIndicators(forReview r: Review) {
        imageIndicator.isHidden = r.image == nil
        noteIndicator.isHidden = r.thoughts.checkIfEmpty
    }
    
    fileprivate func setupObserver(for observable:Observable<Bool>) {
        bag = DisposeBag()
        observable.subscribe(onNext: observeEditingMode).disposed(by: bag)
        
        // we need to observee language change in order to change date format.. annyoing..
        languageManager.changeLanguageObservable.subscribe(onNext: observeLanguageChange).disposed(by: bag)
    }
    
    fileprivate func checkEditingModeAndAdjustUI(isEditing:Bool) {
        observeEditingMode(isEditing)
    }
}

extension FeedMainCell {
    fileprivate func setupUI() {
        
        favoriteIndicator = UIImageView.create(withImageName: "favorite_indicator")
        deleteButton = UIButton.create(withNameKey: .cancel)
        weatherImageView = UIImageView.create(withImageKey: .questionMark)
        
        let backgroundView = UIView.create(withColor: .white, alpha: 1)
        backgroundView.setCornerRadious(value: 8)
        backgroundView.setBorder(color: .mainBlue, width: 1)
        
        animationBackgroundView = UIView.create(withColor: .white, alpha: 1)
        animationBackgroundView.setCornerRadious(value: 8)
        animationBackgroundView.setBorder(color: .mainBlue, width: 1)
        
        let groupForView: [UIView] = [animationBackgroundView,backgroundView, deleteButton, favoriteIndicator]
        groupForView.forEach(addSubview(_:))
        
        dateLabel = UILabel.create(text: "June-03", textAlignment: .left, textColor: UI.Component.titleColor, fontSize: UI.Component.headerSize, boldFont: true, numberOfLine: 1)
        dateLabel.backgroundColor = UI.Component.backgroundColor
        
        imageIndicator = UIImageView.create(withImageName: "image_indicator")
        noteIndicator = UIImageView.create(withImageName: "note_indicator")
        
        rateImageView = UIImageView.create(withImageKey: .noImage)
        rateImageView.contentMode = .scaleAspectFill
    
        cardView = CardView(languageManager: languageManager)
        
        let groupForBackgroundView : [UIView] = [dateLabel,weatherImageView, imageIndicator,noteIndicator,rateImageView, cardView]
        groupForBackgroundView.forEach(backgroundView.addSubview(_:))
        
        constraint(deleteButton,backgroundView, dateLabel, weatherImageView, favoriteIndicator, imageIndicator,noteIndicator,rateImageView, cardView) { (_deleteButton,_backgroundView, _dateLabel, _weatherImageView ,_favoriteIndicator, _imageIndicator, _noteIndicator, _rateImageView, _cardView) in
            
            _deleteButton.centerY.equalTo(backgroundView.snp.top)
            _deleteButton.left.equalTo(backgroundView).offset(-12.50)
            _deleteButton.sizeEqualTo(width: 25, height: 25)
            
            _favoriteIndicator.centerY.equalTo(backgroundView.snp.top).offset(2.5)
            _favoriteIndicator.left.equalTo(backgroundView).offset(-10)
            _favoriteIndicator.sizeEqualTo(width: 25, height: 25)
            
            _backgroundView.topRightqualToSuperView(withOffset: 12.50)
            _backgroundView.leftBottomEqualToSuperView(withOffset: 12.50)
            
            _dateLabel.top.equalToSuperview().offset(10)
            _dateLabel.left.equalToSuperview().offset(15)
            
            _weatherImageView.left.equalTo(dateLabel.snp.right).offset(3)
            _weatherImageView.centerY.equalTo(dateLabel)
            _weatherImageView.sizeEqualTo(width: 25, height: 25)
            
            _imageIndicator.centerY.equalTo(dateLabel)
            _imageIndicator.sizeEqualTo(width: 25, height: 25)
            _imageIndicator.right.equalTo(noteIndicator.snp.left).offset(-2.5)
            
            _noteIndicator.centerY.equalTo(dateLabel)
            _noteIndicator.sizeEqualTo(width: 25, height: 23)
            _noteIndicator.right.equalTo(rateImageView.snp.left).offset(-4)
            
            _rateImageView.top.equalTo(dateLabel)
            _rateImageView.right.equalToSuperview().offset(-15)
            _rateImageView.sizeEqualTo(width: 70, height: 70)

            _cardView.top.equalTo(dateLabel.snp.bottom).offset(7)
            _cardView.leftRightEqualToSuperView(withOffset: 15)
            _cardView.height.equalTo(120)
        }
        
        constraint(animationBackgroundView) { (_animationBackgroundView) in
            _animationBackgroundView.topRightqualToSuperView(withOffset: 12.50)
            _animationBackgroundView.leftBottomEqualToSuperView(withOffset: 12.50)
        }
    }
}

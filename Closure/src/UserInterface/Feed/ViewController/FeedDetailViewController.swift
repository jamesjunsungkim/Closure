//
//  FeedDetailViewController.swift
//  Closure
//
//  Created by James Kim on 7/23/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import Hero
import CodiumLayout
import TPKeyboardAvoiding

final class FeedDetailViewController: DefaultViewController {
    
    //UI
    fileprivate var scrollView:UIScrollView!
    fileprivate var weatherImageView:UIImageView!
    fileprivate var rateImageView:UIImageView!
    fileprivate var rateDescriptionLabel:UILabel!
    fileprivate var cardView: CardView!
    fileprivate var favoriteToggle: UISwitch!
    fileprivate var thoughtTextView: UITextView!
    fileprivate var photoInputView: PhotoInputView!
    
    // For observing language change
    fileprivate var editButton:UIBarButtonItem!
    fileprivate var rateTitleLabel: UILabel!
    fileprivate var favoriteTitleLabel:UILabel!
    fileprivate var textViewTitle:UILabel!
    fileprivate var photoInputTitle:UILabel!
    
    //for Hero animation
    fileprivate var rateSeparatorView:UIView!
    fileprivate var favoriteSeparatorView:UIView!
    fileprivate var photoSeparatorView:UIView!
    
    init(status:AppStatus, review:Review) {
        self.appStatus = status
        self.review = review
        super.init(nibName: nil, bundle: nil)
        enterViewControllerMemoryLog(self)
        
        /**
          since we ensure to get a hold of review, we just display info when setting UI.
         will use configure method only when getting callbacks from NSManagedObjectContext notification.
         */
        
        setupUIAndInitialConfigure()
        setupVC()
        addTargetsAndSetupObservers()
        setupHero()
//        configure()
    }
    
    func setup(fromVC: UIViewController, userInfo: [String : Any]?) {
        /**
         backbutton doesn't belong to THIS view controller. it does to PREVIOUS VC
         so we need to retrieve the title from PREVIOUS VC
         */
        guard let title = fromVC.navigationItem.title else {assertionFailure();return}
        backButtonTitle = title
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }

    // MARK: - Actions
    
    @objc fileprivate func editBtnClicked() {
        // when user taps editting button, segue to the add rewiew view controller and update UI
        let targetVC = AddReviewViewController(appStatus: appStatus, targetDate: review.date, review: review)
        
        presentDeckTransition(targetVC: targetVC, userInfo: nil)
    }
    
    @objc fileprivate func userDidTapOnBackButton() {
        navigationController?.popViewController(animated: true)
    }
    
    fileprivate lazy var observeLanguageChange:(Language)->Void = {[unowned self] (language) in
        guard self.isViewLoaded else {return}
        self.rateTitleLabel.text = "Rate for your day".localized
        self.rateDescriptionLabel.text = self.review.rate.description
        self.favoriteTitleLabel.text = "Favorite".localized
        self.textViewTitle.text = "Thoughts of the day".localized
        self.photoInputTitle.text = "Picture of the day".localized
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: self.backButtonTitle.localized, style: .plain, target: self, action: #selector(self.userDidTapOnBackButton))
        self.editButton.title = "Edit".localized
        
    }
    
    @objc fileprivate func userDidTapOnWeatherImageView() {
        guard let weather = review.weather else {assertionFailure();return}
        let targetVC = WeatherDetailViewController(nonCDWeather: weather.toNonCDWeather)
        presentPopupWithoutAnyButton(targetVC: targetVC)
    }
    
    
    fileprivate lazy var observeAllCoreDataRemoved:()->Void = {[unowned self] in
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Fileprivate
    fileprivate weak var appStatus:AppStatus!
    fileprivate weak var review: Review!
    
    fileprivate var backButtonTitle:String!
    
    fileprivate let contentHeightWithoutThought:CGFloat = 750
    fileprivate let screenWidth = UIScreen.main.bounds.width
    fileprivate lazy var maximumSize = CGSize(width: view.frame.width-30, height: 1000)
    fileprivate var thoughtTextViewHeightConstraint: Constraint!
    
    fileprivate var reviewObserver: ManagedObjectObserver?
    fileprivate let bag = FilterDisposeBag.defaultBag
    
    fileprivate func setupVC() {
        
        view.backgroundColor = .white
        navigationItem.title = review.date.format(with: currentLanguage.toLongMonthToDayFormat)
    }
    
    fileprivate func addTargetsAndSetupObservers() {
        let weatherTap = UITapGestureRecognizer(target: self, action: #selector(userDidTapOnWeatherImageView))
        weatherImageView.addGestureRecognizer(weatherTap)
        
        editButton = UIBarButtonItem(title: "Edit".localized, style: .plain, target: self, action: #selector(editBtnClicked))
        navigationItem.rightBarButtonItem = editButton
        
        // core data notification
        reviewObserver = ManagedObjectObserver(object: review, changeHandler: {[unowned self] (type) in
            /**
             1. if review is deleted, we dismiss this view controller.
             2. if revies is updated, we update UI accordingly.
             */
            switch type {
            case .delete :
                _ = self.navigationController?.popViewController(animated: true)
            case .update:
                self.configure()
            }
        })
        
        // language change
        appStatus.languageManager.changeLanguageObservable.subscribe(onNext: observeLanguageChange).disposed(by: bag)
        
        // observe coredata reset
        appStatus.allCoreDataRemovedObservable.subscribe(onNext: observeAllCoreDataRemoved).disposed(by: bag)
    }


    
    fileprivate func configure() {
        /**
          since we dynamically adjust the size of the textview, we need to adjust it whenever cofigure method is excecuted.
         
         */
        cardView.configure(withReview: review)
        
        rateImageView.image = review.rate.image
        rateDescriptionLabel.text = review.rate.description
        
        photoInputView.configure(withImage: review.image)
        photoInputView.isEditingMode = false
        favoriteToggle.isOn = review.isFavorite
        
        thoughtTextView.text = review.thoughts.unwrapOr(defaultValue: "Empty".localized)
        let targetSize = thoughtTextView.getFitSize(maximumSize: maximumSize)
        let thoughtHeight = targetSize.height
        thoughtTextViewHeightConstraint.update(offset: thoughtHeight)
        scrollView.contentSize = CGSize(width: screenWidth, height: contentHeightWithoutThought+thoughtHeight)
    }
    
    fileprivate func setupHero() {
//        let fadeModifiers: [HeroModifier] = [.fade, .whenPresenting(.delay(0.35)), .useGlobalCoordinateSpace]
        
        self.hero.isEnabled = true
        self.view.hero.id = FeedMainCell.HeroAnimation.feedCell.rawValue + review.dateString
        rateImageView.hero.id = FeedMainCell.HeroAnimation.rateImageView.rawValue + review.dateString
        

        let group:[UIView] = [cardView, rateTitleLabel, rateDescriptionLabel, rateSeparatorView, favoriteTitleLabel, favoriteToggle,photoSeparatorView, photoInputTitle, photoInputView]
//        HeroModifier
        for (index, view) in group.enumerated() {
            let delayValue = 0.1 + 0.05*(Double(index))
            
            view.hero.modifiers = [.fade, .whenPresenting(.delay(delayValue)), .useGlobalCoordinateSpace]
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension FeedDetailViewController {
    fileprivate func setupUIAndInitialConfigure() {
        /**
         since we recieve the target review from the initializer, we just configure it here as well when creating UI.
         
         regarding setting contentsize for scrollview, since screen size differs depending on iphone size. it has to be actual number.
         
         we set the contentsize AFTER getting size of textview.
         */
        
        scrollView = TPKeyboardAvoidingScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.alwaysBounceVertical = true
        
        view.addSubview(scrollView)
        
        let titleSize:CGFloat = 18
        
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        cardView = CardView(languageManager: appStatus.languageManager)
        cardView.configure(withReview: review)
        
        rateTitleLabel = UILabel.create(text: "Rate for your day".localized, textAlignment: .left, fontSize: titleSize)
        
        weatherImageView = UIImageView.create(withImageName: review.weather?.weatherIconName ?? "shower3")
        weatherImageView.isHidden = review.weather == nil
        
        
        rateImageView = UIImageView.create(withImage: review.rate.image)
        
        rateDescriptionLabel = UILabel.create(text: review.rate.description, textAlignment: .center, fontSize: 15)
        
        rateSeparatorView = UIView.create()
        
        favoriteTitleLabel = UILabel.create(text: "Favorite".localized, textAlignment: .left, fontSize: titleSize)
        
        favoriteToggle = UISwitch.create(tintColor: .mainBlue, onTintColor: .mainBlue, isOn: review.isFavorite)
        favoriteToggle.isUserInteractionEnabled = false
        
        favoriteSeparatorView = UIView.create()
        
        photoInputTitle = UILabel.create(text: "Picture of the day".localized, textAlignment: .left, fontSize: titleSize)
        
        photoInputView = PhotoInputView()
        photoInputView.configure(withImage: review.image)
        photoInputView.isEditingMode = false
        
        photoSeparatorView = UIView.create()
        
        textViewTitle = UILabel.create(text: "Thought of the day".localized, textAlignment: .left, fontSize: titleSize)
        
        /**
         the thing is thought text view can be extremely long or can be nothing. figure out the size.
         so let'ts get a maximum size of it and get the size that fits the whole content.
         */
        
        thoughtTextView = UITextView.create(text: review.thoughts.checkIfNotEmptyOrDefaultValue("Empty".localized), mainFontSize: 15)
        thoughtTextView.setBorder(color: .mainBlue, width: 1)
        thoughtTextView.setCornerRadious(value: 5)
        
        let thoughtTextViewTargetSize = thoughtTextView.getFitSize(maximumSize: maximumSize)
        let thoughtTextViewHeight = thoughtTextViewTargetSize.height
        
        scrollView.contentSize = CGSize(width: screenWidth, height: contentHeightWithoutThought+thoughtTextViewHeight)
        
        let group:[UIView] = [cardView, rateTitleLabel,weatherImageView, rateImageView, rateDescriptionLabel,rateSeparatorView, favoriteTitleLabel, favoriteToggle,favoriteSeparatorView,photoInputTitle, photoInputView,photoSeparatorView, textViewTitle, thoughtTextView ]
        group.forEach(scrollView.addSubview(_:))
        
        let gapBetweenComponents:CGFloat = 15
        let heightForSeparator:CGFloat = 0.5
        
        constraint(cardView, rateTitleLabel,weatherImageView, rateImageView, rateDescriptionLabel, rateSeparatorView,  favoriteTitleLabel) { (_cardView, _ratingTitleLabel, _weatherImageView, _rateImageView, _rateDescriptionLabel, _rateSeparatorView, _favoriteTitleLabel) in
            
            _cardView.top.equalToSuperview().offset(20)
            _cardView.leading.equalToSuperview().offset(15)
            _cardView.width.equalToSuperview().offset(-30)
            _cardView.height.equalTo(200)
            
            _ratingTitleLabel.top.equalTo(cardView.snp.bottom).offset(gapBetweenComponents/2)
            _ratingTitleLabel.left.equalTo(cardView)
            
            _weatherImageView.top.equalTo(rateTitleLabel)
            _weatherImageView.right.equalTo(cardView)
            _weatherImageView.sizeEqualTo(width: 45, height: 45)
            
            _rateImageView.top.equalTo(rateTitleLabel.snp.bottom).offset(10)
            _rateImageView.centerX.equalToSuperview()
            _rateImageView.sizeEqualTo(width: 100, height: 100)
            
            _rateDescriptionLabel.top.equalTo(rateImageView.snp.bottom).offset(5)
            _rateDescriptionLabel.centerX.equalToSuperview()
            
            _rateSeparatorView.top.equalTo(rateDescriptionLabel.snp.bottom)
                .offset(gapBetweenComponents/2)
            _rateSeparatorView.left.right.equalTo(cardView)
            _rateSeparatorView.height.equalTo(heightForSeparator)
            
            _favoriteTitleLabel.top.equalTo(rateSeparatorView.snp.bottom)
                .offset(gapBetweenComponents/2)
            _favoriteTitleLabel.left.equalTo(cardView)
            
        }
        
        constraint(favoriteToggle,favoriteSeparatorView,photoInputTitle, photoInputView,photoSeparatorView, textViewTitle, thoughtTextView) {[unowned self] ( _favoriteToggle, _favoriteSeparatorView, _photoInputTitle, _photoInputView, _photoSeparatorView, _textViewTitle, _thoughtTextView) in
            
            _favoriteToggle.centerY.equalTo(favoriteTitleLabel)
            _favoriteToggle.right.equalTo(cardView)
//            _favoriteToggle.sizeEqualTo(width: 50, height: 25)
            
            _favoriteSeparatorView.top.equalTo(favoriteToggle.snp.bottom)
                .offset(gapBetweenComponents/2)
            _favoriteSeparatorView.left.right.equalTo(cardView)
            _favoriteSeparatorView.height.equalTo(heightForSeparator)
            
            _photoInputTitle.top.equalTo(favoriteSeparatorView.snp.bottom)
                .offset(gapBetweenComponents/2)
            _photoInputTitle.leading.equalTo(cardView)
            
            _photoInputView.top.equalTo(photoInputTitle.snp.bottom).offset(15)
            _photoInputView.centerX.equalToSuperview()
            _photoInputView.sizeEqualTo(width: 220, height: 190)
            
            _photoSeparatorView.top.equalTo(photoInputView.snp.bottom)
                .offset(gapBetweenComponents/2)
            _photoSeparatorView.left.right.equalTo(cardView)
            _photoSeparatorView.height.equalTo(heightForSeparator)
            
            _textViewTitle.top.equalTo(photoSeparatorView.snp.bottom)
                .offset(gapBetweenComponents/2)
            _textViewTitle.leading.equalTo(cardView)
            
            _thoughtTextView.top.equalTo(textViewTitle.snp.bottom).offset(10)
            _thoughtTextView.leading.equalTo(cardView).offset(15)
            _thoughtTextView.trailing.equalTo(cardView).offset(-15)
            self.thoughtTextViewHeightConstraint = _thoughtTextView.height.equalTo(thoughtTextViewTargetSize.height).constraint
            
            

        }
    }
}


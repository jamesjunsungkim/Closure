//
//  FeedMainViewController.swift
//  Closure
//
//  Created by James Kim on 7/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CoreData
import CodiumLayout
import RxSwift

final class FeedMainViewController: UIViewController {
    
    public struct Key {
        static let editingObserve = "editingObserve"
        static let editingTracker = "editingTracker"
        static let languageManager = "languageManager"
    }
    
    //UI
    fileprivate var searchBar: UISearchBar!
    fileprivate var searchSettingButton: UIButton!
    fileprivate var searchConditionDisplayView: StringDisplayView!
    fileprivate var searchResetButton: UIButton!
    fileprivate var collectionView: UICollectionView!
    
    fileprivate var resultEmptyDescriptionView:UIView!
    
    // for observing language changes
    fileprivate var emptyDescriptionLabel:UILabel!
    fileprivate var testBarButton: UIBarButtonItem!
    fileprivate var finishTestBarButton:UIBarButtonItem!
    
    
    init(status:AppStatus, parent: MainTabBarController) {
        self.appStatus = status
        self.parentTabbarController = parent
        super.init(nibName: nil, bundle: nil)
        
        setupUI()
        setupVC()
        addTargetsAndSetupObservers()
        setupCollectionView()
        initalSetupForTestingMode()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        /**
         when creating a navigation controller for this tap and set this vc as root vc,
         we initialize thsi vc, create nav vc with it. that's why we don't have nav vc during intialize process.
         we need to put any methods here that need nav.
         */
        setupHeroAnimation()
    }
    
    // MARK: - Actions
    fileprivate func userDidSelectItem(atIndexPath indexPath: IndexPath) {
        logInfo(type: .action)
        view.endEditing(true)
        // if it's editing mode
        let isEditingMode = editingTracker.value
        switch isEditingMode {
        case true:
        toggleEditingMode()
        case false:
            let review = dataSource.objectAtIndexPath(indexPath)
            let targetVC = FeedDetailViewController(status: appStatus, review:review)
            presentDefaultVC(targetVC:targetVC, userInfo: nil)
        }
    }
    
    fileprivate func userDidClickSearchButton(_ searchbar: UISearchBar) {
        logInfo(type: .action)
//        let configureRequest = convertTextToFetchRequest(input: searchbar.text!)
//        dataSource.reconfigureFetchRequest(configureRequest)
        view.endEditing(true)
    }
    
    fileprivate func searchBarTextDidChange(_ searchBar: UISearchBar, textDidChange searchText: String) {
        logInfo(type: .action)
        reconfigureFetchRequest(textBasedConditions: currentTextBasedConditions, textInput: searchText, nonTextBasedPredicate: currentNonTextBasedPredicate, sortDescriptor: currentSortDescriptor)
    }
    
    fileprivate func userDidScroll(_ scrollView: UIScrollView) {
        var offset = scrollView.contentOffset.y
        
        if #available(iOS 11.0, *) {
            offset += scrollView.adjustedContentInset.top
        } else {
            offset += scrollView.contentInset.top
        }
        
        let maxHeaderOffset = searchBar.bounds.height + searchConditionDisplayView.bounds.height + 10
        let headerOffset = min(maxHeaderOffset, offset)
        
        let transform = CGAffineTransform(translationX: 0.0, y: -headerOffset)
        searchBar.transform = transform
        searchSettingButton.transform = transform
        searchConditionDisplayView.transform = transform
        searchResetButton.transform = transform
        view.endEditing(true)
    }
    
    fileprivate lazy var observeUserTapRemoveButton:(Review)->Void = {[unowned self](r) in
        logInfo(type: .action)
        self.presentDefaultAlert(withTitle: "Alert".localized,
                                 message: "Are you sure to remove the selected review?".localized,
                                 okAction: {
                                    r.removeReviewAndPharagraphFromWordDictionary(wordManager:self.appStatus.wordCountManager)
        })
    }
    
    fileprivate lazy var observeUserDidTapOtherViewController:(MainTabBarController.Tap)->Void = {[unowned self] (currentTap) in
        logInfo(type: .action)
        guard currentTap != .feed else {return}
        if self.editingTracker.value {
            self.toggleEditingMode()
        }
        
    }
    
    fileprivate lazy var observeLanguageChange:(Language)->Void = {[weak self] (_) in
        guard let self = self, self.isViewLoaded else {return}
        logInfo(type: .action)
        /**
         I will pass language manager to each cell so that I can set up observation in there instead of reloading the whole collection view.
         */
        // FIXME: in case users select conditions and change language, the height constraint doesn't get updated in this view controller. as a result, the display view goes over the collection view. I just put the logic to reload it but it's not okay.
        self.navigationItem.title = "Feed".localized
        self.searchBar.placeholder = "Search reviews.".localized
        self.emptyDescriptionLabel.text = "It looks empty.".localized + "\n" + "How about creating a new one?".localized
//        self.searchConditionDisplayView.translateTextAssignedToLabels()
        
        if self.testBarButton != nil {
            self.testBarButton.title = "Test".localized
        }
        
        if self.finishTestBarButton != nil {
            self.finishTestBarButton.title = "Finish Testing".localized
        }
        
        // REMOVEME
        self.userDidChooseSearchConditionsSoUpdateUIAndPredicate(self.currentSearchConditions)
    }
    
    @objc fileprivate func userDidTapSearchSettingButton() {
        logInfo(type: .action)
        searchBar.resignFirstResponder()
        let targetVC = FeedSearchSettingViewController()
        
        presentPopup(targetVC: targetVC, cancelButtonTitle: "Cancel".localized, okButtonTitle: "Ok".localized, okAction: {[unowned self] in
//            guard targetVC.finalSearchConditions.count != 0 else {
//                targetVC.presentError(message: "Please make sure choose at least one condition".localized)
//                return
//            }
            self.currentSearchConditions = targetVC.finalSearchConditions
            targetVC.dismiss(animated: true, completion: nil)
            }, panGestureDismissal: true, dismissOnTap: false, userInfo: currentSearchConditions.toUserInfo)
    }
    
    fileprivate func _searchBarShouldBeginEditing(searchBar: UISearchBar)->Bool {
        
//        guard currentSearchConditions.count != 0 else {
//            // load the setting view controller.
//            userDidTapSearchSettingButton()
//            return false
//        }
        return true
    }
    
    
    @objc fileprivate func userDidLongTap(sender:UILongPressGestureRecognizer) {
        logInfo(type: .action)
        switch sender.state {
        case .began:
            toggleEditingMode()
        case .ended:break
        default: break
        }
    }
    
    fileprivate func userDidChooseSearchConditionsSoUpdateUIAndPredicate(_ conditions:[FeedSearchCondition]) {
        logInfo(type: .action)
        /**
         -------------------------- UI ------------------------------
         0. reset the search bar's text
         1. need to update height of a display view located under the search bar
         2. set collection view inset properly so that it looks okay.
         
         it's important to send ENGLSIH WORDS to condition display view for changing languages
         */
        
        let emptyCondition = conditions.count == 0
        
        let sortedDescriptionArray = conditions.sorted(by: {$0.index < $1.index}).map({$0.notLocalizedDescription})
        let conditionDisplayBottomPadding:CGFloat = 10
        
        searchConditionDisplayView.createDisplayString(title: "Conditions", values: sortedDescriptionArray, targetWidth: searchConditionDisplayView.bounds.width, fontSize: 13, displayBackgroundColor: .white, borderColor: .mainBlue)
        
        let newContentOffsetY = collectionViewTopInset + searchConditionDisplayView.totalHeight + conditionDisplayBottomPadding
        
        //TODO: I don't know why I can't just set contentoffset.y to contentinset.top since it's how much it's pulled down from the top. It's not enough to go all the way to the top
        collectionView.contentInset.top = emptyCondition ? collectionViewTopInset : newContentOffsetY
        collectionView.contentOffset.y = emptyCondition ? -(collectionViewTopInset+60) : -(newContentOffsetY+40)
        view.layoutIfNeeded()
        
        /**
         This function is to respond to selection of settings. so it's perfect place to show or hide reset button
         */
        ([searchResetButton, searchConditionDisplayView] as [UIView]).forEach({$0.isHidden = emptyCondition})
        
        /**
         ------------------------- DATA ------------------------------
         there are two types of conditions. andPredicatetype and orPredicatetype
         */
        
        guard !emptyCondition else {
            //Reset all properties related to search conditions
            currentTextBasedConditions = []
            currentNonTextBasedPredicate = nil
            
            // reset the sort descriptor
            currentSortDescriptor = Review.defaultSortDescriptors
            
            //since it's empty. fetch with default settings
            reconfigureFetchRequestWithDefultSetting()
            return
        }
        
        
        let sortedArrayByPriorityAndCategorizedByType = conditions.sorted(by: {$0.index > $1.index})
                                                                  .categorizeIntoTwoArrays(condition: {$0.conditionType == .predicate})
        
        //Sort descriptor
        let finalSortDescriptors = sortedArrayByPriorityAndCategorizedByType.unsatisfied
            .map({$0.toSortDescriptor})
            .checkIfNotEmptyOrPresent(defaultArray: Review.defaultSortDescriptors)
        currentSortDescriptor = finalSortDescriptors
        
        //Predicate
        //Text-Based
        let predicateArray = sortedArrayByPriorityAndCategorizedByType
            .satisfied
            .categorizeIntoTwoArrays {$0.textBasedCondition}
        
        let textBasedConditions = predicateArray.satisfied
        currentTextBasedConditions = textBasedConditions
        
        //NonText-Based
        let otherConditions =
            predicateArray.unsatisfied.categorizeIntoTwoArrays(condition: {$0.predicateType == .and})
        
        let andTypeFromOthers =
            otherConditions.satisfied.map({$0.toPredicate})
            .checkIfNotEmptyOrPresent(defaultArray: [NSPredicate(value: true)])
        
        let orTypeFromOthers =
            otherConditions.unsatisfied.map({$0.toPredicate})
            .checkIfNotEmptyOrPresent(defaultArray: [NSPredicate(value: true)])
        
        let andPredicates = NSCompoundPredicate(andPredicateWithSubpredicates: andTypeFromOthers)
        let orPredicates = NSCompoundPredicate(orPredicateWithSubpredicates: orTypeFromOthers)
        
        let finalNonTextBasedPredicates = NSCompoundPredicate(andPredicateWithSubpredicates: [andPredicates, orPredicates])
        currentNonTextBasedPredicate = finalNonTextBasedPredicates
        
        // if searchbar has any text, add the text-based predicate to final predicate
        let finalTextBasedPredicate = !searchBar.text!.isEmpty ? convertTextBasedConditionToPredicate(from: textBasedConditions, textInput: searchBar.text!) : NSPredicate.init(value: true)
        
        let finalPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [finalNonTextBasedPredicates, finalTextBasedPredicate])
        
        dataSource.reconfigureFetchRequest({[unowned self] in
            $0.predicate = finalPredicate
            $0.sortDescriptors = finalSortDescriptors
            $0.returnsObjectsAsFaults = false
            $0.fetchBatchSize = self.fetchBatchSize
        })
    }
    
    @objc fileprivate func userDidTapResetButton() {
        logInfo(type: .action)
        /**
         since we need to trigger didSetMethod, we assign empty array instead of removing elements.
         all properties related to search conditions are set to default values through didSetMethod from currentSearchConditions
         */
        currentSearchConditions = []
        
    }
    
    fileprivate lazy var observeFetchResultAndUpdateUI:([Review])->Void = {[unowned self] (list) in
        logInfo(type: .observation)
        
        /**
         0. show or hide the result empty description view.
         
         1. If the result is empty, toggle the editing mode off.
         */
        
        self.resultEmptyDescriptionView.isHidden = !list.isEmpty
        
        guard list.isEmpty else {return}
        
        if self.editingTracker.value {
            self.toggleEditingMode()
        }
        
    }
    
    fileprivate lazy var observeContextDidSaveNotification:(ContextDidSaveNotification) -> Void = {[weak self] (note) in
        // FIXME: Don't know why it says it was deallocated and crash.. it should never get deallocated.
        guard !note.insertedObjects.isEmpty || !note.deletedObjects.isEmpty, let target = self else {return}
        target.checkIfDataEmptyAndCreateTestButton()
    }
    
    
    @objc fileprivate func userDidTapTestButton() {
        
        /**
         we need to provide a way to finish the sample-test mode. probably the best idea to make the bar button item.
         
         I don't know why but after inserting a number of reviews, it changes the editing mode...::?;;
         */
        changeAndSaveTestingModeBoolean(value: true)
        
        presentDefaultAlert(withTitle: "Alert".localized, message: "It will add sample data and you can exlpore the app with it.".localized + "\n" + "Would you like to proceed?".localized, okAction: {[unowned self] in
            addRandomDummyReview(into: self.appStatus.mainContext,
                                 wordCountManager: self.appStatus.wordCountManager,
                                 completion: {
                                    self.testBarButton = nil
                                    self.finishTestBarButton = UIBarButtonItem(title: "Finish Testing".localized, style: .plain, target: self, action: #selector(self.userDidTapFinishTestingButton))
                                    self.navigationItem.rightBarButtonItem = self.finishTestBarButton
                                    
            })
        })
    }
    
    @objc fileprivate func userDidTapFinishTestingButton() {
        
        /**
         When they click on this button with intention of finishing the testing mode, we need to revmove all the datas from core data.
         maybe they select some search conditions so I can't just use the dataSource.fetchedObjects.
         0. reset the search condition
         1. remove the objects from dataSource.fetchedObjects.
         
         
         // FIXME: The reason why I intent to delete every object one by one instead of Review.removeall is that if I do that, that doesn't send contextDidSaveNotification so UI of other pages doesn't get updated. so I just delete everything one by one
         */
        
        presentDefaultAlert(withTitle: "Alert".localized, message: "Are you sure to end the test mode?".localized,
                            okAction: {[unowned self] in
                                /**
                                 0. Delete all objects.
                                 
                                 1. Send sendAllCoreDataRemovedNotification so that other view controllers can take a proper action
                                 reload the collection view / table view.
                                 
                                 2. when it's finished, if there is a selected search mode, we remove it.
                                 */
                                
                                Review.deleteAll(fromMOC: self.appStatus.mainContext)
                                self.observeAllCoreDataRemoved()
                                self.appStatus.sendAllCoreDataRemovedNotification()
                                
                                self.changeAndSaveTestingModeBoolean(value: false)
                                
                                self.navigationItem.rightBarButtonItem = nil
                                self.finishTestBarButton = nil
                                
                                if !self.currentSearchConditions.isEmpty {
                                    self.currentSearchConditions = []
                                }
        })
    }
    
    fileprivate lazy var observeAllCoreDataRemoved:()->Void = {[unowned self] in
        
        /**
         ------------------------- LOGIC RUN DOWN ------------------------------
         CACHE DATA is the cause of the issue
         0. clean the cache from fetched result controller.
         1. reset the context
         2. reload the collection view.
         */
        
        self.dataSource.cleanFetchResultControllerCache()
        
        self.dataSource.reconfigureFetchRequest({ (request) in
            request.predicate = Review.defaultPredicate
            request.returnsObjectsAsFaults = false
        })
    }
    
    @objc fileprivate func userDidTapToEndEditing() {
        view.endEditing(true)
    }
    
    // MARK: - Fileprivate
    fileprivate weak var appStatus: AppStatus!
    fileprivate weak var parentTabbarController: MainTabBarController!
    
    fileprivate var needToSetHero = true
    fileprivate lazy var isTestingMode = UserDefaults.retrieveValue(forKey: .isTestingMode, defaultValue: false)
    
    fileprivate var dataSource: CoreDataCollectionViewDataSource<FeedMainCell>!
    fileprivate var contextDidSaveNotificationToken: NSObjectProtocol!
    fileprivate let filterBag = FilterDisposeBag()
    fileprivate let bag = DisposeBag()
    
    fileprivate var collectionViewTopInset: CGFloat = 45 + 20
    
    fileprivate let fetchBatchSize = 10
    fileprivate let editingSubject = PublishSubject<Bool>()
    
    fileprivate var editingTracker = Tracker<Bool>.init(initialValue: false)

    fileprivate var editingObserve: Observable<Bool> {
        return editingSubject.asObservable()
    }
    
    fileprivate var currentSearchConditions = [FeedSearchCondition]() {
        didSet {
            guard currentSearchConditions != oldValue else {return}
            userDidChooseSearchConditionsSoUpdateUIAndPredicate(currentSearchConditions)
        }
    }
    
    fileprivate var currentTextBasedConditions = [FeedSearchCondition]()
    fileprivate var currentNonTextBasedPredicate: NSPredicate!
    fileprivate var currentSortDescriptor = Review.defaultSortDescriptors
    
    fileprivate func convertTextBasedConditionToPredicate(from conditions:[FeedSearchCondition], textInput:String) -> NSPredicate {
        /**
         if there is no text-based condition, just find anything that includes input.
         
         if the input text is empty, return just true predicate
         */
        let pool:[(condition:FeedSearchCondition, predicate:NSPredicate)] = [
            (.textThought, NSPredicate(format: "%K contains[c] %@", #keyPath(Review.thoughts), textInput)),
            (.textBad, NSPredicate(format: "%K contains[c] %@",#keyPath(Review.bad), textInput)),
            (.textGood, NSPredicate(format: "%K contains[c] %@",#keyPath(Review.good), textInput)),
            (.textGoal, NSPredicate(format: "%K contains[c] %@",#keyPath(Review.goal), textInput))
        ]
        
        guard !textInput.isEmpty else {
            return NSPredicate(value:true)
        }
        
        var resultPredicates = [NSPredicate]()
        
        pool.forEach({
            if conditions.contains($0.condition) {
                resultPredicates.append($0.predicate)
            }
        })
        
        guard resultPredicates.count != 0 else {
            return NSCompoundPredicate(orPredicateWithSubpredicates: pool.map({$0.predicate}))
        }
        
        return NSCompoundPredicate(andPredicateWithSubpredicates: resultPredicates)
    }
    
    fileprivate func toggleEditingMode() {
        editingTracker.value = !editingTracker.value
        editingSubject.onNext(editingTracker.value)
    }
    
    fileprivate func setupVC() {
        view.backgroundColor = UI.viewBackgroundColor
        navigationItem.title = "Feed".localized
    }
    
    fileprivate func setupHeroAnimation() {
        /**
         navigation controller is nil when this vc is initialized. so it has to be called AFTER that.
         */
        guard needToSetHero else {return}
        
        self.hero.isEnabled = true
        navigationController?.hero.isEnabled = true
        navigationController?.hero.navigationAnimationType = .none
        
        needToSetHero = false
        
    }
    
    fileprivate func initalSetupForTestingMode() {
        /**
         if user turns on the testing mode and turn the app off. it has to stay the same mode until the user manually shut if off.
         */
        switch isTestingMode {
        case true:
            finishTestBarButton = UIBarButtonItem(title: "Finish Testing".localized, style: .plain, target: self, action: #selector(self.userDidTapFinishTestingButton))
            navigationItem.rightBarButtonItem = finishTestBarButton
        case false:
            checkIfDataEmptyAndCreateTestButton()
        }
    }
    
    fileprivate func checkIfDataEmptyAndCreateTestButton() {
        /**
         ------------------------- LOGIC RUN DOWN ------------------------------
         this method will check if core data is empty. If so, suggess if users would like to try sample data.
         
         it has to listen for events.
         whenever it's zero, we create a bar button item for sample test.
         when it's clicked on, we add dummy reviews and change the bar button item to finishing one.
         */
        
        guard !isTestingMode else {return}
        let request = Review.newFetchRequest
        request.predicate = Review.defaultPredicate
        request.returnsObjectsAsFaults = true
        let count = try! appStatus.mainContext.count(for: request)
        
        testBarButton = count == 0 ? UIBarButtonItem(title: "Test".localized, style: .plain, target: self, action: #selector(userDidTapTestButton)) : nil
        navigationItem.rightBarButtonItem = testBarButton
    }
    
    fileprivate func addTargetsAndSetupObservers() {
        let longTap = UILongPressGestureRecognizer(target: self, action: #selector(userDidLongTap(sender:)))
        collectionView.addGestureRecognizer(longTap)
        
        let endEditingTap = UITapGestureRecognizer(target: self, action: #selector(userDidTapToEndEditing))
        view.addGestureRecognizer(endEditingTap)
        resultEmptyDescriptionView.addGestureRecognizer(endEditingTap)
        
        searchSettingButton.addTarget(self, action: #selector(userDidTapSearchSettingButton), for: .touchUpInside)
        
        searchResetButton.addTarget(self, action: #selector(userDidTapResetButton), for: .touchUpInside)
        
        parentTabbarController.tabObservable.subscribe(onNext: observeUserDidTapOtherViewController).disposed(by: bag)
        
        appStatus.languageManager.changeLanguageObservable.subscribe(onNext: observeLanguageChange).disposed(by: bag)
        
        contextDidSaveNotificationToken = appStatus.mainContext.addContextDidSaveNotificationObserver(observeContextDidSaveNotification)
        
        appStatus.allCoreDataRemovedObservable.subscribe(onNext: observeAllCoreDataRemoved).disposed(by: bag)
    }
    
    fileprivate func setupCollectionView() {
        let request = Review.sortedFetchRequest
        request.returnsObjectsAsFaults = false
        request.fetchBatchSize = fetchBatchSize
        
        let frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: appStatus.mainContext, sectionNameKeyPath: nil, cacheName: nil)
        
        let userInfo = appStatus.languageManager.toUserInfo
            .addValueIfNotEmpty(forKey: Key.editingObserve, value: editingObserve)
            .addValueIfNotEmpty(forKey: Key.editingTracker, value: editingTracker)
        
        dataSource = CoreDataCollectionViewDataSource<FeedMainCell>.init(
            collectionView: collectionView,
            fetchedResultsController: frc,
            parentViewController: self,
            userInfo: userInfo,
            observeFetchResult: observeFetchResultAndUpdateUI,
            additionalSetupCell: {[unowned self] (cell) in
                // setup observer
                let objectIdentifier = ObjectIdentifier.init(cell)
                guard !self.filterBag.checkIfValueExist(forKey: objectIdentifier) else {assertionFailure();return}
                self.filterBag.insert(withIdentifier: objectIdentifier,
                                      newObserver: cell.tapObservable.subscribe(onNext: self.observeUserTapRemoveButton))
                
            }, prepareForReuse: {[unowned self] (cell) in
                // remove observer when it's out of sight
                let objectIdentifier = ObjectIdentifier.init(cell)
                self.filterBag.remove(withIdentifier: objectIdentifier)
        })
    }
    
    fileprivate func reconfigureFetchRequestWithDefultSetting() {
        dataSource.reconfigureFetchRequest({[unowned self] in
            $0.predicate = Review.defaultPredicate
            $0.sortDescriptors = Review.defaultSortDescriptors
            $0.returnsObjectsAsFaults = false
            $0.fetchBatchSize = self.fetchBatchSize
        })
    }
    
    fileprivate func reconfigureFetchRequest(textBasedConditions:[FeedSearchCondition], textInput:String, nonTextBasedPredicate:NSPredicate!, sortDescriptor:[NSSortDescriptor]) {
        let textPredicate = convertTextBasedConditionToPredicate(from: textBasedConditions, textInput: textInput)
        
        let finalPredicate = NSCompoundPredicate(
            andPredicateWithSubpredicates: [textPredicate].addElementIfNotEmpty(nonTextBasedPredicate))
        
        dataSource.reconfigureFetchRequest({ [unowned self] in
            $0.predicate = finalPredicate
            $0.sortDescriptors = sortDescriptor
            $0.fetchBatchSize = self.fetchBatchSize
            $0.returnsObjectsAsFaults = false
        })
    }
    
    fileprivate func changeAndSaveTestingModeBoolean(value:Bool) {
        isTestingMode = value
        UserDefaults.store(object: isTestingMode, forKey: .isTestingMode)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension FeedMainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        userDidSelectItem(atIndexPath: indexPath)
        collectionView.deselectItem(at: indexPath, animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.bounds.width-20, height: 275)
    }
    
    // Disable highlight animation when it's editing mode
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        guard !editingTracker.value else {return}
        guard let cell = collectionView.cellForItem(at: indexPath) else {assertionFailure();return}
        UIView.animate(withDuration: 0.35, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.0, options: [], animations: {
            cell.transform = CGAffineTransform(scaleX: 0.96, y: 0.96)
        }, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) else { return }
        UIView.animate(withDuration: 0.35, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.0, options: [], animations: {
            cell.transform = .identity
        }, completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // when scrolling collectionview, show or hide searchbar accordingly.
        userDidScroll(scrollView)
    }
}

extension FeedMainViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        userDidClickSearchButton(searchBar)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchBarTextDidChange(searchBar, textDidChange: searchText)
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return _searchBarShouldBeginEditing(searchBar: searchBar)
    }
 
}

extension FeedMainViewController {
    fileprivate func setupUI() {
        
        searchBar = UISearchBar.create(backgroundColor: UI.viewBackgroundColor, placeholder: "Search reviews.".localized)
        searchBar.backgroundImage = nil
        searchBar.delegate = self
        
        searchSettingButton = UIButton.create(withImageName: "advance_search_setting")
        
        searchConditionDisplayView = StringDisplayView()
        
        searchResetButton = UIButton.create(withNameKey: .resetButton)
        searchResetButton.isHidden = true
        
        collectionView = UICollectionView.create(backgroundColor: UI.viewBackgroundColor, configuration: { (layout) in
            layout.minimumLineSpacing = 10
        })
        
        collectionView.delegate = self
        collectionView.alwaysBounceVertical = true
        collectionView.delaysContentTouches = false
        collectionView.contentInset.top = collectionViewTopInset
        collectionView.contentInset.bottom = collectionViewTopInset/2
        
        resultEmptyDescriptionView = UIView.create(withColor: .white, alpha: 1)
        resultEmptyDescriptionView.isHidden = true
        
        emptyDescriptionLabel = UILabel.create(text: "It looks empty.".localized + "\n" + "How about creating a new one?".localized, textAlignment: .center, fontSize: 15)
        
        let emptyBoxImageView = UIImageView.create(withImageKey: .emptyBox)
        
        let groupForEmptyDescriptionView:[UIView] = [emptyDescriptionLabel, emptyBoxImageView]
        groupForEmptyDescriptionView.forEach(resultEmptyDescriptionView.addSubview(_:))
        
        constraint(emptyBoxImageView,emptyDescriptionLabel) { (_emptyBoxImageView,_emptyDescriptionLabel) in
            _emptyBoxImageView.centerY.equalToSuperview().offset(-80)
            _emptyBoxImageView.centerX.equalToSuperview()
            _emptyBoxImageView.sizeEqualTo(width: 120, height: 120)
            
            _emptyDescriptionLabel.centerX.equalToSuperview()
            _emptyDescriptionLabel.centerY.equalToSuperview().offset(10)
        }
        
        let group:[UIView] = [collectionView, resultEmptyDescriptionView, searchSettingButton, searchResetButton, searchBar, searchConditionDisplayView]
        group.forEach(view.addSubview(_:))
        
        constraint(searchBar,searchSettingButton, searchConditionDisplayView,searchResetButton, collectionView,resultEmptyDescriptionView) { (_searchBar, _searchSettingButton, _searchConditionDisplayView, _searchResetButton, _collectionview, _resultEmptyDescriptionView) in
            
            _searchBar.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(8)
            _searchBar.left.equalToSuperview().offset(10)
            _searchBar.right.equalTo(searchSettingButton.snp.left).offset(-5)
            
            _searchSettingButton.centerY.equalTo(searchBar)
            _searchSettingButton.sizeEqualTo(width: 25, height: 25)
            _searchSettingButton.right.equalToSuperview().offset(-15)
            
            _searchConditionDisplayView.top.equalTo(searchBar.snp.bottom).offset(5)
            _searchConditionDisplayView.left.right.equalTo(searchBar)
            
            _searchResetButton.top.equalTo(searchConditionDisplayView).offset(-1)
            _searchResetButton.left.equalTo(searchSettingButton)
            _searchResetButton.sizeEqualTo(width: 25, height: 25)
            
            _collectionview.edges.equalToSuperview()
            
            _resultEmptyDescriptionView.top
                .equalTo(searchConditionDisplayView.snp.bottom).offset(3)
            _resultEmptyDescriptionView.left.right.bottom.equalToSuperview()
        }
    }
}













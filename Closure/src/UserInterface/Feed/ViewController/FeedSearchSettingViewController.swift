//
//  FeedSearchSettingViewController.swift
//  Closure
//
//  Created by montapinunt Pimonta on 8/30/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import RxSwift
import CodiumLayout
import DropDown

fileprivate struct Tag {
    // assign the same tag to same type of ui componenets.
    
    //TextField
    static let sortRateTextField = 0
    static let sortDateTextField = 1
    static let startDateTextField = 2
    static let endDateTextField = 3
    
    //Button
    static let startdateButton = 2
    static let enddateButton = 3
}


final class FeedSearchSettingViewController: DefaultViewController {
    
    //UI
    fileprivate var badCheckBoxView: CheckBoxWithDescription<FeedSearchCondition>!
    fileprivate var goodCheckBoxView: CheckBoxWithDescription<FeedSearchCondition>!
    fileprivate var goalCheckBoxView: CheckBoxWithDescription<FeedSearchCondition>!
    fileprivate var thoughtCheckBoxView: CheckBoxWithDescription<FeedSearchCondition>!
    
    fileprivate var favoriteCheckBoxView: CheckBoxWithDescription<FeedSearchCondition>!
    fileprivate var pictureCheckBoxView: CheckBoxWithDescription<FeedSearchCondition>!
    
    fileprivate var rateAllCheckBoxView: CheckBoxWithDescription<FeedSearchCondition>!
    fileprivate var rateAwesomeCheckBoxView: CheckBoxWithDescription<FeedSearchCondition>!
    fileprivate var rateGoodCheckBoxView: CheckBoxWithDescription<FeedSearchCondition>!
    fileprivate var rateOkayCheckBoxView: CheckBoxWithDescription<FeedSearchCondition>!
    fileprivate var rateUnsatisfactoryCheckBoxView: CheckBoxWithDescription<FeedSearchCondition>!
    fileprivate var rateTerribleCheckBoxView: CheckBoxWithDescription<FeedSearchCondition>!
    
    fileprivate var rateSortDescriptorCheckBoxView: CheckBoxWithDescription<FeedSearchCondition>!
    fileprivate var dateSortDescriptorCheckBoxView: CheckBoxWithDescription<FeedSearchCondition>!
    
    fileprivate var rateSortDescriptorTextField: UITextField!
    fileprivate var dateSortDescriptorTextField: UITextField!
    
    fileprivate var startDateTextField:UITextField!
    fileprivate var startDateDeleteButton:UIButton!
    
    fileprivate var endDateTextField:UITextField!
    fileprivate var endDateDeleteButton:UIButton!
    
    func setup(fromVC: UIViewController, userInfo: [String : Any]?) {
        let currentConditions = FeedSearchCondition.unwrapListFrom(userInfo: userInfo)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        addTarget()
        setupObserver()
        setupDropDown()
        configure(with: currentConditions)
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    // MARK: Public
    
    public var finalSearchConditions: [FeedSearchCondition] {
        return currentSelectedOptions
    }
    
    public func presentError(message:String) {
        presentDefaultError(message: message, okAction: nil)
    }
    
    // MARK: Actions
    fileprivate lazy var observeCheckBoxClicked:((FeedSearchCondition, Bool))->Void = {[unowned self] (arg0) in
        let (object, needsToAdd) = arg0

        needsToAdd ? self.currentSelectedOptions.append(object) : self.currentSelectedOptions.removeItem(condition: {$0 == object})
    }
    
    fileprivate lazy var observeAllCheckBoxClicked:(Bool)->Void = {[unowned self] (checked) in
        let group = [self.rateAwesomeCheckBoxView, self.rateGoodCheckBoxView, self.rateOkayCheckBoxView, self.rateUnsatisfactoryCheckBoxView, self.rateTerribleCheckBoxView]
        
        group.forEach({$0!.checkOrUncheckView(needsToCheck:checked)})
    }
    
    @objc fileprivate func userDidClickDeleteButton(sender:UIButton) {
        /**
         -------------------------- UI ------------------------------
         */
        currentTextFieldTag = sender.tag
        targetDateTextfield.text = ""
        targetDateDeleteButton.isHidden = true
        
        
        /**
         ------------------------- DATA ------------------------------
         we need to remove the current start/end date in order to update the date selection calendar view controller.
         */
        
        currentSelectedOptions.removeItem(condition: {[unowned self] in
            if startDatecondition {
                self.currentStartDate = nil
                if case .startDate(_) = $0 {
                    return true
                }
            } else {
                self.currentEndDate = nil
                if case .endDate(_) = $0 {
                    return true
                }
            }
            return false
        })
    }
    
    fileprivate lazy var observeDateSelection:(Date)->Void = {[unowned self] (date) in

        
        /**
         -------------------------- UI ------------------------------
         */
        let displayDateString = date.toYearToDateString(type:.current)
        self.targetDateTextfield.text = displayDateString
        self.targetDateDeleteButton.isHidden = false
        
        /**
         ------------------------- DATA ------------------------------
          we need to make sure user don't put wrong dates.
         now the calendar prevents wrong inputs so we don't need this logic anymore.
         
         0. when they select  date for either start or end date, we iterate over the conditions in order to remove the existing start/end date. other wise it will create a duplicate.
         1. we add a new selected date to the condition array.
         */
        
//        switch self.startDatecondition {
//        case true:
//            if self.currentEndDate != nil {
//                guard date <= self.currentEndDate else {
//                    waitFor(milliseconds: 100, completion: {
//                        self.presentError(message: "Please make sure start date is earlier than end date".localized)
//                    })
//                    return
//                }
//            }
//        case false:
//            if self.currentStartDate != nil {
//                guard date >= self.currentStartDate else {
//                    waitFor(milliseconds: 100, completion: {
//                        self.presentError(message: "Please make sure end date is later than start date".localized)
//                    })
//                    return
//                }
//            }
//        }
 
        self.currentSelectedOptions.removeItem(condition: {[unowned self] in
            switch self.startDatecondition {
            case true:
                if case .startDate(_) = $0 {
                    return true
                }
            case false:
                if case .endDate(_) = $0 {
                    return true
                }
            }
            return false
        })
        
        switch self.startDatecondition {
        case true:
            self.currentStartDate = date
            self.currentSelectedOptions.append(.startDate(displayDateString))
        case false:
            self.currentEndDate = date
            self.currentSelectedOptions.append(.endDate(displayDateString))
        }
        
    }
    
    fileprivate func userDidTapTextField(_ textfield:UITextField) -> Bool {
        currentTextFieldTag = textfield.tag
        
        // for dropdown
        dropDown.anchorView = textfield
        currentTextField = textfield
        
        switch textfield.tag {
        case Tag.sortRateTextField:
            showDropDown(isRateSelected: true)
        case Tag.sortDateTextField:
            showDropDown(isRateSelected: false)
        case Tag.startDateTextField, Tag.endDateTextField:
            let targetVC = DateSelectionCalendarViewController()
            targetVC.dateSelectionObservable.subscribe(onNext: observeDateSelection).disposed(by: bag)
            let userInfo = [String:Any]()
                .addValueIfNotEmpty(forKey: DateSelectionCalendarViewController.Keys.startDate, value: currentStartDate)
                .addValueIfNotEmpty(forKey: DateSelectionCalendarViewController.Keys.endDate, value: currentEndDate)
            
            presentPopupWithoutAnyButton(targetVC: targetVC, completion: nil, panGestureDismissal: true, tapGestureDismissal: true, prefferedWidth: 350, userInfo: userInfo)
        default: assertionFailure()
        }
        return false
    }

    
    fileprivate lazy var observeRateDropDownCheckBox:(Bool) -> Void = {[unowned self] (isChecked) in
        self.currentTextField = self.rateSortDescriptorTextField
        self.rateSortDescriptorTextField.text = ""
        
        switch isChecked {
        case true:
            self.currentTextFieldTag = Tag.sortRateTextField
            self.showDropDown(isRateSelected: true)
        case false:
            // if sort search condition has been added to data array, we need to remove it.
            for (index,item) in self.currentSelectedOptions.enumerated() {
                if case .sortByAscendingRate(_) = item {
                    self.currentSelectedOptions.remove(at: index)
                    break
                }
            }
        }
    }

    fileprivate lazy var observeDateDropDownCheckBox:(Bool) -> Void = {[unowned self] (isChecked) in
        self.currentTextField = self.dateSortDescriptorTextField
        self.dateSortDescriptorTextField.text = ""
        switch isChecked {
        case true:
            self.currentTextFieldTag = Tag.sortDateTextField
            self.showDropDown(isRateSelected: false)
        case false:
            // if sort search condition has been added to data array, we need to remove it.
            for (index, item) in self.currentSelectedOptions.enumerated() {
                if case .sortByAscendingDate(_) = item {
                    self.currentSelectedOptions.remove(at: index)
                    break
                }
            }
        }
    }

    fileprivate lazy var userDidChooseDropDown: (Int,String) ->Void = {[unowned self] (index, input) in
        /**
         -------------------------- UI ------------------------------
         */
        self.currentTextField.text = input
        
        // if the checkbox is not selected, select the check box.
        let targetCheckBox = self.currentTextFieldTag == 0 ? self.rateSortDescriptorCheckBoxView: self.dateSortDescriptorCheckBoxView
        let theOtherCheckBox = self.currentTextFieldTag != 0 ? self.rateSortDescriptorCheckBoxView: self.dateSortDescriptorCheckBoxView
        
        if !targetCheckBox!.isChecked {
            targetCheckBox!.checkOrUncheckWithoutSendingNotification(needsToCheck: true)}
        
        // FIXME: When unchecking this check box, it enumerate the list in order to exclude the condition related to this check box but that action happen one more time below there. The reason why I used this instead of withoutSendingNotificationOne is to reset UI part of textField. I don't think it would be that big deal since it's small array like 10 items at maximum.
        if theOtherCheckBox!.isChecked {
            theOtherCheckBox!.checkOrUncheckView(needsToCheck: false)
        }
        
        /**
         ------------------------- DATA ------------------------------
         if it includes any type of sort option, delete it before adding it to the array.
         because only one sort descriptor can be applied to it.
         */
        
        var sortRateSelected = self.currentTextFieldTag == Tag.sortRateTextField
        let isAscending = index == 0
        
        let condition = sortRateSelected ? FeedSearchCondition.sortByAscendingRate(isAscending) :  FeedSearchCondition.sortByAscendingDate(isAscending)
        
        var filteredArray = [FeedSearchCondition]()
        // FIXME: What is the best way to filter this array?
        self.currentSelectedOptions.forEach({
            if case .sortByAscendingRate(_) = $0  {
                // ignore
            } else if case .sortByAscendingDate(_) = $0 {
                // ignore
            } else {
                filteredArray.append($0)
            }
        })
        
        filteredArray.append(condition)
        self.currentSelectedOptions = filteredArray
    }
    
    fileprivate lazy var observeDropdownDismiss:()->Void = {[unowned self] in
        /**
         -------------------------- UI ------------------------------
         This is only triggered when user tapping outside the drop down to dismiss instead of tapping on one of items. so basically, we just uncheck the current check box.
         we track down the said check box based on current textfield tag since whenever tapping on one of them, we assign the index to tag variable.
         
         If an option is already selected, we don't do anything.
         Logic to detect if anything is already selected is to check if textfield has text.
         */
        
        switch self.currentTextFieldTag {
        case Tag.sortRateTextField:
            guard self.rateSortDescriptorTextField.text!.isEmpty else {return}
            self.rateSortDescriptorCheckBoxView.checkOrUncheckWithoutSendingNotification(needsToCheck: false)
        case Tag.sortDateTextField:
            guard self.dateSortDescriptorTextField.text!.isEmpty else {return}
            self.dateSortDescriptorCheckBoxView.checkOrUncheckWithoutSendingNotification(needsToCheck: false)
        default:assertionFailure()
        }
    }
    
    // MARK: Fileprivate
    fileprivate weak var currentTextField: UITextField!
    
    fileprivate let dropDown = DropDown()
    fileprivate var currentTextFieldTag: Int!
    fileprivate let textFieldHeight:CGFloat = 25
    
    fileprivate let bag = DisposeBag()
    fileprivate var currentSelectedOptions = [FeedSearchCondition]()
    
    // for preventing user from inputting wrong dates. for ex, enddate can't come before startdate
    fileprivate var currentStartDate:Date!
    fileprivate var currentEndDate:Date!
    
    fileprivate let rateSortDescriptorOptionList = ["Lowest come first".localized, "Highest come first".localized]
    
    fileprivate let dateSortDescriptorOptionList = ["Earliest come first".localized, "Latest come first".localized]
    
    fileprivate var startDatecondition:Bool {
        return currentTextFieldTag == Tag.startDateTextField
    }
    
    fileprivate var targetDateTextfield:UITextField {
        return startDatecondition ? startDateTextField: endDateTextField
    }
    
    fileprivate var targetDateDeleteButton:UIButton {
        return startDatecondition ? startDateDeleteButton: endDateDeleteButton
    }
    
    fileprivate func addTarget() {
        // the order is important.. index is crucial for user actions later.
        [rateSortDescriptorTextField, dateSortDescriptorTextField, startDateTextField, endDateTextField]
            .enumerated()
            .forEach({
            $0.element!.delegate = self
            $0.element!.tag = $0.offset
        })
        
        [startDateDeleteButton, endDateDeleteButton].forEach({$0!.addTarget(self, action: #selector(userDidClickDeleteButton(sender:)), for: .touchUpInside)})
    }
    
    fileprivate func setupObserver() {
        // for rate all
        rateAllCheckBoxView.checkObservable.subscribe(onNext: observeAllCheckBoxClicked).disposed(by: bag)
        
        // for the others
        let group = [badCheckBoxView,goodCheckBoxView,goalCheckBoxView, thoughtCheckBoxView, favoriteCheckBoxView, pictureCheckBoxView, rateAwesomeCheckBoxView, rateGoodCheckBoxView, rateOkayCheckBoxView, rateUnsatisfactoryCheckBoxView, rateTerribleCheckBoxView]
        
        group.forEach({$0!.needToAddObjectObservable.subscribe(
            onNext:observeCheckBoxClicked).disposed(by: bag)})
        
        // for dropdown checkbox
        rateSortDescriptorCheckBoxView.checkObservable.subscribe(onNext: observeRateDropDownCheckBox).disposed(by: bag)
        
        dateSortDescriptorCheckBoxView.checkObservable.subscribe(onNext: observeDateDropDownCheckBox).disposed(by: bag)
    }
    
    fileprivate func setupDropDown() {
        dropDown.backgroundColor = .white
        dropDown.separatorColor = .init(white: 0.3, alpha: 0.3)
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y: textFieldHeight+4)
        dropDown.selectionAction = userDidChooseDropDown
        dropDown.cancelAction = observeDropdownDismiss
    }

    
    fileprivate func configure(with list:[FeedSearchCondition]) {
        guard list.count != 0 else {return}
        /**
         -------------------------- UI ------------------------------
         */
        let group:[CheckBoxWithDescription<FeedSearchCondition>] = [badCheckBoxView, goodCheckBoxView, goalCheckBoxView, thoughtCheckBoxView, favoriteCheckBoxView, pictureCheckBoxView, rateAwesomeCheckBoxView, rateGoodCheckBoxView, rateOkayCheckBoxView, rateUnsatisfactoryCheckBoxView, rateTerribleCheckBoxView]

        group.forEach({
            guard let object = $0.object, list.contains(object) else {return}
            $0.checkOrUncheckView(needsToCheck: true)
        })
        
        let groupForRates:[CheckBoxWithDescription<FeedSearchCondition>] = [rateAwesomeCheckBoxView, rateGoodCheckBoxView, rateOkayCheckBoxView, rateUnsatisfactoryCheckBoxView, rateTerribleCheckBoxView]
        
        // if the list contains all kinds of rates, we should check all rate checkbox
        if list.contains(list: groupForRates.compactMap({$0.object})) {
            rateAllCheckBoxView.checkOrUncheckView(needsToCheck: false)
            rateAllCheckBoxView.checkOrUncheckView(needsToCheck: true)
        }
        
        //TODO: it's not efficient way to find value from enum.. what if it's more than 100 elements?
        list.forEach({
            if case .startDate(let dateStringValue) = $0 {
                startDateTextField.text = dateStringValue
                startDateDeleteButton.isHidden = false
            } else if case .endDate(let dateStringValue) = $0 {
                endDateTextField.text = dateStringValue
                endDateDeleteButton.isHidden = false
            } else if case .sortByAscendingRate(let bool) = $0 {
                rateSortDescriptorCheckBoxView.checkOrUncheckWithoutSendingNotification(needsToCheck: true)
                rateSortDescriptorTextField.text = rateSortDescriptorOptionList[bool ? 0 : 1]
            } else if case .sortByAscendingDate(let bool) = $0 {
                dateSortDescriptorCheckBoxView.checkOrUncheckWithoutSendingNotification(needsToCheck: true)
                dateSortDescriptorTextField.text = dateSortDescriptorOptionList[bool ? 0 : 1]
            }
        })
        
        /**
         ------------------------- DATA ------------------------------
         */
        currentSelectedOptions = list
    }
    
    fileprivate func showDropDown(isRateSelected:Bool) {
        dropDown.anchorView = isRateSelected ? rateSortDescriptorTextField : dateSortDescriptorTextField
        dropDown.dataSource = isRateSelected ? rateSortDescriptorOptionList: dateSortDescriptorOptionList
        dropDown.show()
    }
    
}
extension FeedSearchSettingViewController:UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return userDidTapTextField(textField)
    }
}

extension FeedSearchSettingViewController {
    fileprivate func setupUI() {
        let viewcontrollerTitleLabel = UILabel.create(text: "Search Setting".localized, textAlignment: .center, textColor: .black, fontSize: 20, boldFont: false, numberOfLine: 0)
        
        let textCategoryLabel = UILabel.create(text: "Text".localized, textAlignment: .left, fontSize: 15)
        let textSeparator = UIView.create()
        let checkBoxFontSize:CGFloat = 13
        badCheckBoxView = CheckBoxWithDescription<FeedSearchCondition>.init(object: .textBad,  label: "Rooms For Improvement".localized, fontSize: checkBoxFontSize)
        goodCheckBoxView = CheckBoxWithDescription<FeedSearchCondition>.init(object: .textGood, label: "Achievements".localized, fontSize: checkBoxFontSize)
        goalCheckBoxView = CheckBoxWithDescription<FeedSearchCondition>.init(object: .textGoal, label: "Goal for the following day".localized, fontSize: checkBoxFontSize)
        thoughtCheckBoxView = CheckBoxWithDescription<FeedSearchCondition>.init(object: .textThought, label: "Thoughts".localized, fontSize: checkBoxFontSize)
        
        let conditionCategoryLabel = UILabel.create(text: "Condition".localized, textAlignment: .left, fontSize: 15)
        let conditionSeparator = UIView.create()
        favoriteCheckBoxView = CheckBoxWithDescription<FeedSearchCondition>.init(object: .isFavorite, label: "Favorite".localized, fontSize: checkBoxFontSize)
        pictureCheckBoxView = CheckBoxWithDescription<FeedSearchCondition>.init(object: .imageIncluded, label: "Picture of day included".localized, fontSize: checkBoxFontSize)
        
        let rateCategoryLabel = UILabel.create(text: "Rate".localized, textAlignment: .left, fontSize: 15)
        let rateSeparator = UIView.create()
        rateAllCheckBoxView = CheckBoxWithDescription<FeedSearchCondition>.init(label: "All".localized, fontSize: 13)
        rateAwesomeCheckBoxView = CheckBoxWithDescription<FeedSearchCondition>.init(object: .rateAwesome, label: Rate.awesome.title, fontSize: checkBoxFontSize)
        rateGoodCheckBoxView = CheckBoxWithDescription<FeedSearchCondition>.init(object: .rateGood, label: Rate.good.title, fontSize: checkBoxFontSize)
        rateOkayCheckBoxView = CheckBoxWithDescription<FeedSearchCondition>.init(object: .rateOkay, label: Rate.ok.title, fontSize: checkBoxFontSize)
        rateUnsatisfactoryCheckBoxView = CheckBoxWithDescription<FeedSearchCondition>.init(object: .rateUnsatisfactory, label: Rate.unsatisfactory.title, fontSize: checkBoxFontSize)
        rateTerribleCheckBoxView = CheckBoxWithDescription<FeedSearchCondition>.init(object: .rateTerrible, label: Rate.terrible.title, fontSize: checkBoxFontSize)
        
        let sortCategoryLabel = UILabel.create(text: "Sort".localized, textAlignment: .left, fontSize: 15)
        let sortSeparator = UIView.create()
        
        rateSortDescriptorCheckBoxView = CheckBoxWithDescription<FeedSearchCondition>.init(label: "Sorted By Rate".localized, fontSize: checkBoxFontSize)
        rateSortDescriptorTextField = UITextField.create(placeHolder: "Choose your option".localized, textSize: checkBoxFontSize, textColor: .black, textAlignment: .center)
        rateSortDescriptorTextField.textAlignment = .center
        rateSortDescriptorTextField.borderStyle = .roundedRect
        
        dateSortDescriptorCheckBoxView = CheckBoxWithDescription<FeedSearchCondition>.init(label: "Sorted By Date".localized, fontSize: checkBoxFontSize)
        dateSortDescriptorTextField = UITextField.create(placeHolder: "Choose your option".localized, textSize: checkBoxFontSize, textColor: .black, textAlignment: .center)
        dateSortDescriptorTextField.borderStyle = .roundedRect
        
        let dateCategoryLabel = UILabel.create(text: "Date".localized, textAlignment: .left, fontSize: 15)
        let dateSeparator = UIView.create()
        
        let startDateTitleLabel = UILabel.create(text: "Start Date".localized+": ", textAlignment: .left, fontSize: checkBoxFontSize)
        startDateTextField = UITextField.create(placeHolder: "Enter".localized, textSize: checkBoxFontSize, textColor: .black, keyboardType: .default, clearMode: .never)
        startDateDeleteButton = UIButton.create(withNameKey: .deleteButton)
        startDateDeleteButton.tag = Tag.startdateButton
        startDateDeleteButton.isHidden = true
        
        let endDateTitleLabel = UILabel.create(text: "End Date".localized + ": ", textAlignment: .left, fontSize: checkBoxFontSize)
        endDateTextField = UITextField.create(placeHolder: "Enter".localized, textSize: checkBoxFontSize, textColor: .black, keyboardType: .default, clearMode: .never)
        endDateDeleteButton = UIButton.create(withNameKey: .deleteButton)
        endDateDeleteButton.tag = Tag.enddateButton
        endDateDeleteButton.isHidden = true
        
        
        let group:[UIView] = [viewcontrollerTitleLabel, textCategoryLabel, textSeparator, badCheckBoxView, goodCheckBoxView, goalCheckBoxView, thoughtCheckBoxView, conditionCategoryLabel, conditionSeparator, favoriteCheckBoxView, pictureCheckBoxView, rateCategoryLabel, rateSeparator, rateAllCheckBoxView, rateAwesomeCheckBoxView, rateGoodCheckBoxView, rateOkayCheckBoxView, rateUnsatisfactoryCheckBoxView, rateTerribleCheckBoxView, sortCategoryLabel, sortSeparator, rateSortDescriptorCheckBoxView, rateSortDescriptorTextField, dateSortDescriptorCheckBoxView, dateSortDescriptorTextField, dateCategoryLabel, dateSeparator, startDateTitleLabel, startDateTextField, startDateDeleteButton, endDateTitleLabel, endDateTextField, endDateDeleteButton]
        group.forEach(view.addSubview(_:))
        
        let cateogryLabelSize = CGSize(width: 70, height: 20)
        let dateTitleLabelSize = CGSize(width: 70, height: 20)
        let separatorLineWidth: CGFloat = 2
        let checkBoxLeftPadding: CGFloat = 10
        let checkBoxFullSize = CGSize.init(width: 195, height: 25)
        let checkBoxhalfSize = CGSize.init(width: 87.5, height: 25)
        let gapBetweenSection:CGFloat = 15
        
        constraint(viewcontrollerTitleLabel) { (_viewcontrollerTitleLabel) in
            _viewcontrollerTitleLabel.top.equalToSuperview().offset(15)
            _viewcontrollerTitleLabel.centerX.equalToSuperview()
        }
        
        // Text to condition section
        constraint(textCategoryLabel, textSeparator, badCheckBoxView, goodCheckBoxView, goalCheckBoxView, thoughtCheckBoxView, conditionCategoryLabel, conditionSeparator, favoriteCheckBoxView, pictureCheckBoxView) { (_textCategoryLabel, _textSeparator, _badCheckBoxView, _goodCheckBoxView, _goalCheckBoxView, _thoughtCheckBoxView, _conditionCategoryLabel, _conditionSeparator, _favoriteCheckBoxView, _imageCheckBoxView) in
            
            _textCategoryLabel.top.equalTo(viewcontrollerTitleLabel.snp.bottom).offset(15)
            _textCategoryLabel.left.equalToSuperview().offset(18)
            _textCategoryLabel.size.equalTo(cateogryLabelSize)
            
            _textSeparator.top.bottom.equalTo(textCategoryLabel)
            _textSeparator.left.equalTo(textCategoryLabel.snp.right).offset(10)
            _textSeparator.width.equalTo(separatorLineWidth)
            
            _badCheckBoxView.left.equalTo(textSeparator.snp.right).offset(checkBoxLeftPadding)
            _badCheckBoxView.centerY.equalTo(textCategoryLabel)
            _badCheckBoxView.size.equalTo(checkBoxFullSize)
            
            _goodCheckBoxView.top.equalTo(badCheckBoxView.snp.bottom).offset(10)
            _goodCheckBoxView.left.equalTo(badCheckBoxView)
            _goodCheckBoxView.size.equalTo(checkBoxFullSize)
            
            _goalCheckBoxView.top.equalTo(goodCheckBoxView.snp.bottom).offset(10)
            _goalCheckBoxView.left.equalTo(badCheckBoxView)
            _goalCheckBoxView.size.equalTo(checkBoxFullSize)
            
            _thoughtCheckBoxView.top.equalTo(goalCheckBoxView.snp.bottom).offset(10)
            _thoughtCheckBoxView.left.equalTo(badCheckBoxView)
            _thoughtCheckBoxView.size.equalTo(checkBoxFullSize)
            
            _conditionCategoryLabel.top.equalTo(thoughtCheckBoxView.snp.bottom).offset(gapBetweenSection)
            _conditionCategoryLabel.left.equalTo(textCategoryLabel)
            _conditionCategoryLabel.size.equalTo(cateogryLabelSize)
            
            _conditionSeparator.top.bottom.equalTo(conditionCategoryLabel)
            _conditionSeparator.left.right.equalTo(textSeparator)
            
            
            _favoriteCheckBoxView.centerY.equalTo(conditionCategoryLabel)
            _favoriteCheckBoxView.left.equalTo(badCheckBoxView)
            _favoriteCheckBoxView.size.equalTo(checkBoxFullSize)
            
            _imageCheckBoxView.top.equalTo(favoriteCheckBoxView.snp.bottom).offset(10)
            _imageCheckBoxView.left.equalTo(favoriteCheckBoxView)
            _imageCheckBoxView.size.equalTo(checkBoxFullSize)
        }
        
        //Rate section
        constraint( rateCategoryLabel, rateSeparator, rateAllCheckBoxView, rateAwesomeCheckBoxView, rateGoodCheckBoxView, rateOkayCheckBoxView, rateUnsatisfactoryCheckBoxView, rateTerribleCheckBoxView, dateCategoryLabel) { (_rateCategoryLabel, _rateSeparator, _rateAllCheckBoxView, _rateAwesomeCheckBoxView, _rateGoodCheckBoxView, _rateOkayCheckBoxView, _rateUnsatisfactoryCheckBoxView, _rateTerribleCheckBoxView, _dateCategoryLabel) in

            _rateCategoryLabel.top.equalTo(pictureCheckBoxView.snp.bottom).offset(gapBetweenSection)
            _rateCategoryLabel.left.equalTo(textCategoryLabel)
            _rateCategoryLabel.size.equalTo(cateogryLabelSize)
            
            _rateSeparator.top.bottom.equalTo(rateCategoryLabel)
            _rateSeparator.left.right.equalTo(textSeparator)
            
            _rateAllCheckBoxView.centerY.equalTo(rateCategoryLabel)
            _rateAllCheckBoxView.left.equalTo(badCheckBoxView)
            _rateAllCheckBoxView.size.equalTo(checkBoxFullSize)
            
            _rateAwesomeCheckBoxView.top.equalTo(rateAllCheckBoxView.snp.bottom).offset(10)
            _rateAwesomeCheckBoxView.left.equalTo(rateAllCheckBoxView)
            _rateAwesomeCheckBoxView.size.equalTo(checkBoxhalfSize)
            
            _rateGoodCheckBoxView.centerY.equalTo(rateAwesomeCheckBoxView)
            _rateGoodCheckBoxView.left.equalTo(rateAwesomeCheckBoxView.snp.right).offset(10)
            _rateGoodCheckBoxView.size.equalTo(checkBoxhalfSize)
            
            _rateOkayCheckBoxView.top.equalTo(rateGoodCheckBoxView.snp.bottom).offset(10)
            _rateOkayCheckBoxView.left.equalTo(rateAllCheckBoxView)
            _rateOkayCheckBoxView.size.equalTo(checkBoxhalfSize)
            
            _rateUnsatisfactoryCheckBoxView.centerY.equalTo(rateOkayCheckBoxView)
            _rateUnsatisfactoryCheckBoxView.left.equalTo(rateOkayCheckBoxView.snp.right).offset(10)
            _rateUnsatisfactoryCheckBoxView.size.equalTo(checkBoxhalfSize)
            
            _rateTerribleCheckBoxView.top.equalTo(rateOkayCheckBoxView.snp.bottom).offset(10)
            _rateTerribleCheckBoxView.left.equalTo(rateAllCheckBoxView)
            _rateTerribleCheckBoxView.size.equalTo(checkBoxhalfSize)
        }
        
        // Sort section
        constraint(sortCategoryLabel, sortSeparator, rateSortDescriptorCheckBoxView, rateSortDescriptorTextField, dateSortDescriptorCheckBoxView, dateSortDescriptorTextField) { (_orderCategoryLabel, _orderSeparator, _rateSortDescriptorCheckBoxView, _rateSortDescriptorTextField, _dateSortDescriptorCheckBoxView, _dateSortDescriptorTextField) in
            
            _orderCategoryLabel.top.equalTo(rateTerribleCheckBoxView.snp.bottom).offset(gapBetweenSection)
            _orderCategoryLabel.left.equalTo(textCategoryLabel)
            _orderCategoryLabel.size.equalTo(cateogryLabelSize)
            
            _orderSeparator.top.bottom.equalTo(sortCategoryLabel)
            _orderSeparator.left.right.equalTo(textSeparator)
            
            _rateSortDescriptorCheckBoxView.centerY.equalTo(sortCategoryLabel)
            _rateSortDescriptorCheckBoxView.left.equalTo(rateTerribleCheckBoxView)
            _rateSortDescriptorCheckBoxView.sizeEqualTo(width: 130, height: textFieldHeight)
            
            _rateSortDescriptorTextField.top.equalTo(rateSortDescriptorCheckBoxView.snp.bottom).offset(2.5)
            _rateSortDescriptorTextField.left.equalTo(rateSortDescriptorCheckBoxView).offset(35)
            _rateSortDescriptorTextField.sizeEqualTo(width: 160, height: textFieldHeight)
            
            _dateSortDescriptorCheckBoxView.top.equalTo(rateSortDescriptorTextField.snp.bottom).offset(10)
            _dateSortDescriptorCheckBoxView.left.equalTo(rateSortDescriptorCheckBoxView)
            _dateSortDescriptorCheckBoxView.sizeEqualTo(width: 130, height: textFieldHeight)
            
            _dateSortDescriptorTextField.top.equalTo(dateSortDescriptorCheckBoxView.snp.bottom).offset(2.5)
            _dateSortDescriptorTextField.left.equalTo(rateSortDescriptorCheckBoxView).offset(35)
            _dateSortDescriptorTextField.sizeEqualTo(width: 160, height: textFieldHeight)
        }
        
        // Date section
        constraint(dateCategoryLabel, dateSeparator, startDateTitleLabel, startDateTextField, startDateDeleteButton, endDateTitleLabel, endDateTextField, endDateDeleteButton) { (_dateCategoryLabel, _dateSeparator, _startDateTitleLabel, _startDateTextField, _startDateDeleteButton, _endDateTitleLabel, _endDateTextField, _endDateDeleteButton) in
            
            _dateCategoryLabel.top.equalTo(dateSortDescriptorTextField.snp.bottom).offset(gapBetweenSection)
            _dateCategoryLabel.left.equalTo(textCategoryLabel)
            _dateCategoryLabel.size.equalTo(cateogryLabelSize)
            
            _dateSeparator.top.bottom.equalTo(dateCategoryLabel)
            _dateSeparator.left.right.equalTo(textSeparator)
            
            _startDateTitleLabel.centerY.equalTo(dateCategoryLabel)
            _startDateTitleLabel.left.equalTo(rateTerribleCheckBoxView).offset(5)
            _startDateTitleLabel.size.equalTo(dateTitleLabelSize)
            
            _startDateTextField.centerY.equalTo(dateCategoryLabel)
            _startDateTextField.left.equalTo(startDateTitleLabel.snp.right).offset(5)
            _startDateTextField.size.equalTo(dateTitleLabelSize)
            
            _startDateDeleteButton.left.equalTo(startDateTextField.snp.right).offset(5)
            _startDateDeleteButton.centerY.equalTo(dateCategoryLabel).offset(-0.5)
            _startDateDeleteButton.sizeEqualTo(width: 20, height: 20)
            
            _endDateTitleLabel.top.equalTo(startDateTitleLabel.snp.bottom).offset(10)
            _endDateTitleLabel.left.equalTo(startDateTitleLabel)
            _endDateTitleLabel.size.equalTo(dateTitleLabelSize)
            
            _endDateTextField.centerY.equalTo(endDateTitleLabel)
            _endDateTextField.left.equalTo(endDateTitleLabel.snp.right).offset(5)
            _endDateTextField.size.equalTo(dateTitleLabelSize)
            
            _endDateDeleteButton.left.equalTo(endDateTextField.snp.right).offset(5)
            _endDateDeleteButton.centerY.equalTo(endDateTextField).offset(-0.5)
            _endDateDeleteButton.sizeEqualTo(width: 20, height: 20)
            
            _endDateDeleteButton.bottom.equalToSuperview().offset(-15)
        }
    }
}












//
//  PageCell.swift
//  Connect
//
//  Created by James Kim on 5/8/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

class PageCell: ReusableCollectionViewCell {
    
    typealias Object = Page

    
    // UI
    fileprivate var imageView: UIImageView!
    fileprivate var titleLabel: UILabel!
    fileprivate var descriptionLabel: UILabel!
    fileprivate var separatorLineView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(withObject object: Page, parentViewController: UIViewController, currentIndexPath: IndexPath, userInfo: [String : Any]?) {
        imageView.image = UIImage(named: object.imageName)
        titleLabel.text = object.title
        descriptionLabel.text = object.description
    }
    
    public func configure(withPage page: Page) {
        imageView.image = UIImage(named: page.imageName)
        titleLabel.text = page.title
        descriptionLabel.text = page.description
    }
    
    
}

//MARK: - Setup UI
extension PageCell {
    fileprivate func setupUI() {
        
        let backgroundViewForImage = UIView.create(withColor: .mainBlue, alpha: 1)
        
        imageView = {
            let iv = UIImageView()
            iv.contentMode = .scaleAspectFill
            iv.clipsToBounds = true
            return iv
        }()
        
        titleLabel = {
            let lb = UILabel()
            lb.font = UIFont.boldSystemFont(ofSize: 20)
            lb.textAlignment = .center
            lb.text = "TITLE"
            return lb
        }()
        
        
        descriptionLabel = {
            let lb = UILabel()
            lb.textAlignment = .center
            lb.numberOfLines = 0
            lb.text = "DESCRIPTION"
            return lb
        }()
        
        separatorLineView = {
            let v = UIView()
            v.backgroundColor = .lightGray
            return v
        }()
        
        backgroundViewForImage.addSubview(imageView)
        
        imageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(35)
            make.centerX.equalToSuperview()
            make.sizeEqualTo(width: 275, height: 335)
        }
        
        let group: [UIView] = [backgroundViewForImage, titleLabel, descriptionLabel, separatorLineView]
        group.forEach(self.addSubview(_:))
        
        backgroundViewForImage.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.height.equalTo(370)
        }
        
        separatorLineView.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(1)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(self)
            make.top.equalTo(separatorLineView.snp.bottom).offset(15)
            make.height.equalTo(23)
        }
        
        descriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(15)
            make.leftRightEqualToSuperView(withOffset: 15)
//            make.bottom.equalToSuperview()
        }
        
        
    }
}













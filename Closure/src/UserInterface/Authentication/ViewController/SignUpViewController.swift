//
//  SignUpViewController.swift
//  Connect
//
//  Created by James Kim on 5/9/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import CodiumLayout
import CoreData
import PKHUD

final class SignUpViewController: DefaultViewController {
    
    //MARK: - UI
    fileprivate var facebookSignUpButton: FacebookLoginButton!
    fileprivate var thirdPartyLoginView: UIView!
    
    fileprivate var orLabel: UILabel!
    fileprivate var nameSeparatorLine: UIView!
    fileprivate var emailSeparatorLine: UIView!
    fileprivate var passwordSeparatorLine: UIView!
    
    fileprivate var nameTextField: UITextField!
    fileprivate var emailTextField: UITextField!
    fileprivate var passwordTextField: UITextField!
    fileprivate var createAccountButton: UIButton!
    
    fileprivate var namePlaceHolderLabel: UILabel!
    fileprivate var emailPlaceHolderLabel: UILabel!
    fileprivate var passwordPlaceHolderLabel: UILabel!
    
    fileprivate var nameWarningLabel: UILabel!
    fileprivate var emailWarningLabel: UILabel!
    fileprivate var passwordWarningLabel: UILabel!
    
    init(appStatus:AppStatus) {
        self.appStatus = appStatus
        super.init(nibName: nil, bundle: nil)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        setupVC()
        addTargets()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    // MARK: - Actions
    
    @objc fileprivate func createBtnClicked() {
        logInfo(type: .action)
        signup(withName: nameTextField.text!, email: emailTextField.text!, password: passwordTextField.text!)
    }
    
    // MARK: - Filepriavte
    fileprivate weak var appStatus:AppStatus!
    
    fileprivate let animationDuration = 0.8
    
    fileprivate var thirdPartyHeightConstraint: Constraint!
    fileprivate var nameWarningHeightConstraint: Constraint!
    fileprivate var emailWarningHeightConstraint: Constraint!
    fileprivate var passwordWarningHeightConstraint: Constraint!
    
    fileprivate func setupVC() {
        UI.configure(withViewController: self)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(userDidTapview))
        view.addGestureRecognizer(tapGesture)
    }
    
    fileprivate func addTargets() {
        nameTextField.addTarget(self, action: #selector(nameTextFieldEditingDidBegin), for: .editingDidBegin)
        nameTextField.addTarget(self, action: #selector(nameTextFieldEditingDidEnd), for: .editingDidEnd)
        nameTextField.addTarget(self, action: #selector(enableOrDisableCreatButton), for: .editingChanged)
        
        emailTextField.addTarget(self, action: #selector(emailTextFieldEditingDidBegin), for: .editingDidBegin)
        emailTextField.addTarget(self, action: #selector(emailTextFieldEditingDidEnd), for: .editingDidEnd)
        emailTextField.addTarget(self, action: #selector(enableOrDisableCreatButton), for: .editingChanged)
        
        passwordTextField.addTarget(self, action: #selector(passwordTextFieldEditingDidBegin), for: .editingDidBegin)
        passwordTextField.addTarget(self, action: #selector(passwordTextFieldEditingDidEnd), for: .editingDidEnd)
        passwordTextField.addTarget(self, action: #selector(enableOrDisableCreatButton), for: .editingChanged)
        
        createAccountButton.addTarget(self, action: #selector(createBtnClicked), for: .touchUpInside)
    }
    
    fileprivate func showOrHideThirdPartyLoginView(shouldHide flag: Bool) {
        if flag {
            UIView.animate(withDuration: 1) {[unowned self] in
                self.thirdPartyLoginView.alpha = 0
                self.orLabel.alpha = 0
                self.thirdPartyHeightConstraint.update(offset: 50)
                self.view.layoutIfNeeded()
            }
        } else {
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: { [unowned self] in
                self.thirdPartyLoginView.alpha = 1
                self.orLabel.alpha = 1
                self.thirdPartyHeightConstraint.update(offset: 95)
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    @objc fileprivate func userDidTapview() {
        showOrHideThirdPartyLoginView(shouldHide: false)
    }
    
    @objc fileprivate func enableOrDisableCreatButton() {
        if validateName() {
            showOrHideNameWarningLabel(nameIsValid: validateName())
        }
        
        if validateEmail() {
            showOrHideEmailWarningLabel(emailIsValid: validateEmail())
        }
        
        if validatePassword() {
            showOrHidePasswordWarningLabel(passwordIsValid: validatePassword())
        }
        
        _ = checkIfReadyToMoveToNextPage()
    }
    
    @objc fileprivate func nameTextFieldEditingDidBegin() {
        showOrHideThirdPartyLoginView(shouldHide: true)
        placeHolderBeginningAnimation(label: namePlaceHolderLabel, bottomView: nameSeparatorLine, leadingMargin: 58, bottomMargin: 35)
       
    }
    @objc fileprivate func nameTextFieldEditingDidEnd() {
        showOrHideNameWarningLabel(nameIsValid: validateName())
        placeHolderEndingAnimation(textField: nameTextField, label: namePlaceHolderLabel, bottomView: nameSeparatorLine, leadingMargin: 75, bottomMargin: 5)
    }
    
    @objc fileprivate func emailTextFieldEditingDidBegin() {
        showOrHideThirdPartyLoginView(shouldHide: true)
        placeHolderBeginningAnimation(label: emailPlaceHolderLabel, bottomView: emailSeparatorLine, leadingMargin: 56.5, bottomMargin: 35)
    }
    
    @objc fileprivate func emailTextFieldEditingDidEnd() {
        showOrHideEmailWarningLabel(emailIsValid: validateEmail())
        placeHolderEndingAnimation(textField: emailTextField, label: emailPlaceHolderLabel, bottomView: emailSeparatorLine, leadingMargin: 75, bottomMargin: 5)
    }
    
    @objc fileprivate func passwordTextFieldEditingDidBegin() {
        showOrHideThirdPartyLoginView(shouldHide: true)
        placeHolderBeginningAnimation(label: passwordPlaceHolderLabel, bottomView: passwordSeparatorLine, leadingMargin: 58, bottomMargin: 35)
    }
    
    @objc fileprivate func passwordTextFieldEditingDidEnd() {
        showOrHidePasswordWarningLabel(passwordIsValid: validatePassword())
        placeHolderEndingAnimation(textField: passwordTextField, label: passwordPlaceHolderLabel, bottomView: passwordSeparatorLine, leadingMargin: 75, bottomMargin: 5)
    }
    
    fileprivate func validateName()-> Bool {
        return nameTextField.hasText
    }
    
    fileprivate func showOrHideNameWarningLabel(nameIsValid isValid: Bool) {
        UIView.animate(withDuration: animationDuration ) {
            self.nameWarningLabel.alpha = isValid ? 0 : 1
            self.nameWarningHeightConstraint.update(offset: isValid ? 0 : 30)
        }
    }

    fileprivate func validateEmail()->Bool {
        return emailTextField.text!.validateForEmail()
    }
    
    fileprivate func showOrHideEmailWarningLabel(emailIsValid isValid: Bool) {
        UIView.animate(withDuration: animationDuration ) {
            self.emailWarningLabel.alpha = isValid ? 0 : 1
            self.emailWarningHeightConstraint.update(offset: isValid ? 0 : 30)
        }
    }
    
    fileprivate func validatePassword()->Bool {
        return passwordTextField.text!.count > 5
    }
    
    fileprivate func showOrHidePasswordWarningLabel(passwordIsValid isValid: Bool) {
        UIView.animate(withDuration: animationDuration ) {
            self.passwordWarningLabel.alpha = isValid ? 0 : 1
            self.passwordWarningHeightConstraint.update(offset: isValid ? 0 : 30)
        }
    }
    
    fileprivate func checkIfReadyToMoveToNextPage()->Bool {
        let isValid = validateName() && validateEmail() && validatePassword()
        createAccountButton.isEnabled = isValid
        return isValid
    }
    
    // TESTING: It's set as public for testing purpose. shouldn't be called outside the view controller.
    public func signup(withName name: String,email: String, password:String, completion: ((User)->Void)? = nil) {
        UIView.showLoading()
        appStatus.networkManager.signup(withEmail: email, password: password) {[unowned self] (result) in
            switch result {
            case .success(let uid):
                let user = User(uid: uid, name: name, emailAddress: email)
                self.saveUserJSONtoBackend(user: user, completion: completion)
                self.saveUserToUserDefaultAndswitchToMainWindow(with: user)
            case .failure(let apiError):
                if case let .firebaseError(error) = apiError {
                    _logInfo_NEEDTOCHANGE(error.localizedDescription)
                    UIView.hideHUD(completion: { _ in
                        self.presentDefaultError(message: error.localizedDescription, okAction: nil)
                    })
                }
            }
        }
    }
    
    fileprivate func saveUserJSONtoBackend(user:User, completion: ((User)->Void)?) {
        appStatus.networkManager.postJSON(value: user.toDictionary, toPath: Path.toUser(withUID: user.uid).stringValue) {[unowned self] (result) in
            switch result {
            case .success(_):
                completion?(user)
            case .failure(let e):
                self.presentDefaultError(message: e.localizedDescription, okAction: nil)
            }
        }
    }
    
    fileprivate func saveUserToUserDefaultAndswitchToMainWindow(with user:User) {
        UserDefaults.store(object: user.toDictionary, forKey: .currentUser)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.saveUserToUserDefaultAndSwitchToMainTabVC(WithUser: user)
        
        appDelegate.saveUserToUserDefaultAndSwitchToMainTabVCAndShowBlurryVC(WithUser: user)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension SignUpViewController {
    func setup(fromVC: UIViewController, userInfo: [String : Any]?) {/*no-op*/}
}

extension SignUpViewController {
    fileprivate func setupUI() {
        
        facebookSignUpButton = {
            let bt = FacebookLoginButton(appStatus: appStatus)
            bt.backgroundColor = .clear
            let title = NSAttributedString(string: "Sign up with Facebook".localized, attributes: [NSAttributedStringKey.font:UIFont.boldSystemFont(ofSize: 13), NSAttributedStringKey.foregroundColor:UIColor.create(R: 68, G: 89, B: 150)])
            bt.setAttributedTitle(title, for: .normal)
            return bt
        }()
        
        let facebookLogoImageView = UIImageView.create(withImageName: "facebook_icon")
        
        orLabel = UILabel.create(text: "or", textAlignment: .center, textColor: .black, fontSize: 13)
        orLabel.backgroundColor = .white
        
        let orSeparatorLine = UIView.create()
        
        nameSeparatorLine = UIView.create()
        
        emailSeparatorLine = UIView.create()
        
        passwordSeparatorLine = UIView.create()
        
        nameTextField = UITextField.create(placeHolder: "", textSize: 17, textColor: .black, keyboardType: .default)
        
        emailTextField = UITextField.create(placeHolder: "", textSize: 17, textColor: .black, keyboardType: .default)
        
        passwordTextField = UITextField.create(placeHolder: "", textSize: 17, textColor: .black, keyboardType: .default)
        passwordTextField.isSecureTextEntry = true
        
        namePlaceHolderLabel = UILabel.create(text: "Name".localized, textAlignment: .left, textColor: .lightGray, fontSize: 19)
        
        emailPlaceHolderLabel = UILabel.create(text: "Email Address".localized, textAlignment: .left, textColor: .lightGray, fontSize: 19)
        
        passwordPlaceHolderLabel = UILabel.create(text: "Password".localized, textAlignment: .left, textColor: .lightGray, fontSize: 19)
        
        nameWarningLabel = UILabel.create(text: "You can't leave it empty. Please enter your name".localized, textAlignment: .left, textColor: .red, fontSize: 14)
        
        emailWarningLabel = UILabel.create(text: "Invalid Email Address. Please check it again".localized, textAlignment: .left, textColor: .red, fontSize: 14)
        
        passwordWarningLabel = UILabel.create(text: "Password must includ at least 6 characters".localized, textAlignment: .left, textColor: .red, fontSize: 14)
        
        createAccountButton = {
            let bt = UIButton(type: .system)
            bt.backgroundColor = .mainBlue
            bt.setCornerRadious(value: 10)
            let title = NSAttributedString(string: "Create Account".localized, attributes: [NSAttributedStringKey.font:UIFont.boldSystemFont(ofSize: 17), NSAttributedStringKey.foregroundColor:UIColor.white])
            bt.setAttributedTitle(title, for: .normal)
            bt.isEnabled = false
            return bt
        }()
        
        // TODO: Need to create a tappable string to privacy page.
//        var result = NSMutableAttributedString()
//        let url = URL(string: "https://www.freeprivacypolicy.com/privacy/view/0cf90ccaa0a6be151de9cfef13b8b400")
//
//        let first = NSAttributedString(string: "By creating an account, you are agreeing to ")
//
//        let linkTextAttributes: [NSAttributedStringKey: Any] = [
//            NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue,
//            NSAttributedStringKey.foregroundColor: UIColor.mainBlue,
//            NSAttributedStringKey.link: url as Any
//        ]
//
//        let second = NSAttributedString(string: "the Terms of Use and Privacy Policy of Closure.", attributes: linkTextAttributes)
//
//        result.append(first)
//        result.append(second)
        
        let termAgreementLabel = UILabel.create(text: "By creating an account, you are agreeing to the Terms of Use and Privacy Policy of Closure".localized, textAlignment: .center, textColor: .lightGray, fontSize: 13)
        
        
        
        let facebookStackView = UIStackView.create(views: [facebookLogoImageView, facebookSignUpButton], axis: .horizontal, alignment: .center, distribution: .equalSpacing, spacing: 5)
        
        thirdPartyLoginView = {
            let v = UIView()
            let elements = [facebookStackView, orSeparatorLine, orLabel]
            elements.forEach({v.addSubview($0!)})
            return v
        }()
        
        let group : [UIView] = [thirdPartyLoginView,nameTextField, namePlaceHolderLabel, nameSeparatorLine,nameWarningLabel, emailTextField, emailPlaceHolderLabel, emailSeparatorLine, emailWarningLabel, passwordTextField, passwordSeparatorLine, passwordPlaceHolderLabel, passwordWarningLabel, createAccountButton, termAgreementLabel]
        group.forEach(view.addSubview(_:))
        
        thirdPartyLoginView.snp.makeConstraints {[unowned self] (make) in
            make.left.top.right.equalTo(view.safeAreaLayoutGuide)
            self.thirdPartyHeightConstraint = make.height.equalTo(95).constraint
        }
        
        facebookStackView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(30)
            make.centerX.equalToSuperview()
        }
        
        facebookLogoImageView.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 30, height: 30))
        }
        
        orLabel.snp.makeConstraints { (make) in
            make.centerX.centerY.equalTo(orSeparatorLine)
            make.width.equalTo(30)
            make.height.equalTo(20)
        }
        
        orSeparatorLine.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-5)
            make.height.equalTo(0.5)
            make.width.equalTo(90)
        }
        
        nameTextField.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(60)
            make.right.equalToSuperview().offset(-60)
            make.top.equalTo(thirdPartyLoginView.snp.bottom).offset(30)
            make.height.equalTo(30)
        }
        
        namePlaceHolderLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(75)
            make.bottom.equalTo(nameSeparatorLine.snp.top).offset(-5)
        }
        
        nameSeparatorLine.snp.makeConstraints { (make) in
            make.top.equalTo(nameTextField.snp.bottom).offset(3)
            make.left.equalToSuperview().offset(55)
            make.right.equalToSuperview().offset(-55)
            make.height.equalTo(1)
        }
        
        nameWarningLabel.snp.makeConstraints { (make) in
            make.top.equalTo(nameSeparatorLine.snp.bottom)
            make.centerX.equalToSuperview()
            self.nameWarningHeightConstraint = make.height.equalTo(0).constraint
        }
        
        emailTextField.snp.makeConstraints { (make) in
            make.top.equalTo(nameWarningLabel.snp.bottom).offset(40)
            make.left.equalToSuperview().offset(60)
            make.right.equalToSuperview().offset(-60)
            make.height.equalTo(30)
        }
        
        emailPlaceHolderLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(75)
            make.bottom.equalTo(emailSeparatorLine.snp.top).offset(-5)
        }
        
        emailSeparatorLine.snp.makeConstraints { (make) in
            make.top.equalTo(emailTextField.snp.bottom).offset(3)
            make.left.equalTo(55)
            make.right.equalTo(-55)
            make.height.equalTo(1)
        }
        
        emailWarningLabel.snp.makeConstraints { (make) in
            make.top.equalTo(emailSeparatorLine.snp.bottom)
            make.centerX.equalToSuperview()
            self.emailWarningHeightConstraint = make.height.equalTo(0).constraint
        }
        
        passwordTextField.snp.makeConstraints { (make) in
            make.top.equalTo(emailWarningLabel.snp.bottom).offset(40)
            make.left.equalToSuperview().offset(60)
            make.right.equalToSuperview().offset(-60)
            make.height.equalTo(30)
        }
        
        passwordPlaceHolderLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(75)
            make.bottom.equalTo(passwordSeparatorLine.snp.top).offset(-5)
        }
        
        passwordSeparatorLine.snp.makeConstraints { (make) in
            make.top.equalTo(passwordTextField.snp.bottom).offset(3)
            make.left.equalToSuperview().offset(55)
            make.right.equalToSuperview().offset(-55)
            make.height.equalTo(1)
        }
        
        passwordWarningLabel.snp.makeConstraints { (make) in
            make.top.equalTo(passwordSeparatorLine.snp.bottom)
            make.centerX.equalToSuperview()
            self.passwordWarningHeightConstraint = make.height.equalTo(0).constraint
        }
        
        createAccountButton.snp.makeConstraints { (make) in
            make.top.equalTo(passwordWarningLabel.snp.bottom).offset(30)
            make.centerX.equalToSuperview()
            make.width.equalTo(200)
            make.height.equalTo(40)
        }
        
        termAgreementLabel.snp.makeConstraints { (make) in
            make.top.equalTo(createAccountButton.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(60)
            make.right.equalToSuperview().offset(-60)
//            make.height.equalTo(40)
        }

    }
}














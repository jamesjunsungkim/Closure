//
//  WalkThroughViewController.swift
//  Connect
//
//  Created by James Kim on 5/8/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout
import CoreData

final class WalkThroughViewController: DefaultViewController {
    
    // UI
    fileprivate var collectionView: UICollectionView!
    fileprivate var pageControl: UIPageControl!
    fileprivate var createButton: UIButton!
    fileprivate var signInDescriptionLabel: UILabel!
    fileprivate var signInButton: UIButton!

    init(appStatus:AppStatus) {
        self.appStatus = appStatus
        super.init(nibName: nil, bundle: nil)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        addTargets()
    }
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }

        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436, 2688, 1792:
                statusBar.backgroundColor = .mainBlue
            default:
                statusBar.backgroundColor = nil
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = nil
    }
    
    // MARK: - Actions
    
    @objc fileprivate func createButtonClicked() {
        logInfo(type: .action)
        presentDefaultVC(targetVC: SignUpViewController(appStatus: appStatus), userInfo: nil)
    }
    
    @objc fileprivate func signInButtonClicked() {
        logInfo(type: .action)
        presentDefaultVC(targetVC: SignInViewController(appStatus: appStatus), userInfo: nil)
    }
    
    // MAKR: - Fileprivate
    fileprivate weak var appStatus:AppStatus!
    fileprivate lazy var targetCollectionViewSize = CGSize(width: view.bounds.width, height: 507)
    
    fileprivate let pages = Page.fetchPages()

    fileprivate func addTargets() {
        createButton.addTarget(self, action: #selector(createButtonClicked), for: .touchUpInside)
        signInButton.addTarget(self, action: #selector(signInButtonClicked), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension WalkThroughViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    // DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PageCell.reuseIdentifier, for: indexPath) as! PageCell
        cell.configure(withPage: pages[indexPath.item])
        return cell
    }
    
    //CollectionViewDelegate
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageNumber = Int(targetContentOffset.pointee.x / view.bounds.width)
        pageControl.currentPage = pageNumber
    }
}

// MARK: - Setup UI
extension WalkThroughViewController {
    fileprivate func setupUI() {
        view.backgroundColor = .white
        
        collectionView = {[unowned self] in
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.itemSize = self.targetCollectionViewSize
            layout.minimumLineSpacing = 0
            let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
            cv.register(PageCell.self, forCellWithReuseIdentifier: PageCell.reuseIdentifier)
            cv.showsHorizontalScrollIndicator = false
            cv.backgroundColor = .white
            cv.isPagingEnabled = true
            cv.dataSource = self
            cv.delegate = self
            return cv
        }()
        
        pageControl = {
            let pc = UIPageControl(frame: .zero)
            pc.pageIndicatorTintColor = .lightGray
            pc.currentPageIndicatorTintColor = UIColor.mainBlue
            pc.numberOfPages = pages.count
            return pc
        }()
        
        createButton = {
            let bt = UIButton(type: .system)
            
            bt.backgroundColor = .mainBlue
            bt.setTitleColor(.white, for: .normal)
            bt.setCornerRadious(value: 5)
            bt.setTitle("Create Account".localized, for: .normal)
            return bt
        }()
        
        signInDescriptionLabel = {
            let label = UILabel()
            label.text = "Already Have an ID?".localized
            label.font = UIFont(name: "Avenir Next", size: 17)
            return label
        }()
        
        signInButton = {
            let bt = UIButton(type: .system)
            bt.setTitleColor(.mainBlue, for: .normal)
            bt.setTitle("Sign In".localized, for: .normal)
            return bt
        }()
        
        let stackView: UIStackView = {
            let sv = UIStackView()
            sv.axis = .horizontal
            sv.alignment = .center
            sv.distribution = .equalSpacing
            sv.spacing = 0
            sv.addArrangedSubview(signInDescriptionLabel)
            sv.addArrangedSubview(signInButton)
            return sv
        }()
        
        let groupsForSubviews:[UIView] = [collectionView, pageControl,createButton, stackView]
        groupsForSubviews.forEach(view.addSubview)
        
        collectionView.snp.makeConstraints {[unowned self] (make) in
            if #available(iOS 11.0, *) {
                make.top.equalTo(self.view.safeAreaLayoutGuide)
            } else {
                make.top.equalToSuperview()
            }
            make.left.right.equalToSuperview()
            make.height.equalTo(self.targetCollectionViewSize)
        }
        
        pageControl.snp.makeConstraints { (make) in
            make.top.equalTo(collectionView.snp.bottom).offset(10)
            make.centerX.equalTo(view)
        }
        
        createButton.snp.makeConstraints { (make) in
            make.top.equalTo(pageControl.snp.bottom).offset(15)
            make.centerX.equalTo(view)
            make.width.equalTo(200)
            make.height.equalTo(40)
        }
        
        stackView.snp.makeConstraints { (make) in
            make.top.equalTo(createButton.snp.bottom).offset(15)
            make.centerX.equalToSuperview()
            make.width.equalTo(210)
            make.height.equalTo(35)
        }
    }
}















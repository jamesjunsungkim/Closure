//
//  Page.swift
//  Connect
//
//  Created by James Kim on 5/8/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation

struct Page {
    let title: String
    let description: String
    let imageName: String
    
    static func fetchPages()-> [Page] {
        let firstPage = Page(title: "Close your day with Closure".localized, description: "It helps you to close your day with thoughtful reviews on what happens during the day.".localized, imageName: "page1")
        let secondPage = Page(title: "See everything at glance".localized, description: "You can orgianize the calendar so that you can see the whole month in colors.".localized, imageName: "page2")
        let thirdPage = Page(title: "Analyze your days".localized, description: "Stastics help you understand more of what happened in the past.".localized, imageName: "page3" )
        let fourthPage = Page(title: "Secure your secrets".localized, description: "We value your privacy more than anything. Setting passcode up will allow only you to have access to the app.".localized, imageName: "page4")
        return [firstPage, secondPage, thirdPage, fourthPage]
    }
}

//
//  User.swift
//  Closure
//
//  Created by James Kim on 7/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation
import SwiftyJSON

struct User {
    init(uid:String, name:String, emailAddress:String) {
        self.uid = uid
        self.name = name
        self.emailAddress = emailAddress
        self.signupDate = Date()
    }
    
    var uid: String
    var name: String
    var emailAddress:String
    var signupDate: Date
}

extension User {
    struct Keys {
        static let uid = "uid"
        static let name = "name"
        static let emailAddress = "emailAddress"
        static let signupDate = "signupDate"
    }
    
    init(from dict:[String:Any]) {
        let json = JSON(dict)
        
        let uid = json[Keys.uid].stringValue
        let name = json[Keys.name].stringValue
        let email = json[Keys.emailAddress].stringValue
        let dateStringValue = json[Keys.signupDate].stringValue
        
        self.uid = uid
        self.name = name
        self.emailAddress = email
        self.signupDate = dateStringValue.toDate(type: .UTC)
    }
    
    // MARK: Public
    public var toDictionary: [String:Any] {
        return [
            Keys.uid: uid,
            Keys.name:name,
            Keys.emailAddress:emailAddress,
            Keys.signupDate: signupDate.toYearToDateString(type:.UTC)
        ]
    }
    
    public func saveToUserDefault() {
        UserDefaults.store(object: self.toDictionary, forKey: .currentUser)
    }
    
    public func loadCurrentUserFromUserDefault() -> User {
        let json = UserDefaults.retrieveValue(forKey: .currentUser, defaultValue: [String:Any]())
        return User(from:json)
    }
    
    
}

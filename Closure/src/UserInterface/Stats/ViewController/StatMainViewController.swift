//
//  SearchMainViewController.swift
//  Closure
//
//  Created by James Kim on 7/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout
import CoreData
import Charts
import SwipeCellKit

fileprivate struct SortButton {
    static let previous = 0
    static let current = 1
    static let next = 2
}

public enum WordSegment: Int {
    case verb
    case noun
    case place
    case adjective
    case adverb
    
    public var tag: NSLinguisticTag {
        switch self {
        case .verb: return .verb
        case .noun: return .noun
        case .place: return .placeName
        case .adjective: return .adjective
        case .adverb: return .adverb
        }
    }
    
    public var description: String {
        switch self {
        case .verb: return "Verb".localized
        case .noun: return "Noun".localized
        case .place: return "Place".localized
        case .adjective: return "Adjective".localized
        case .adverb: return "Adverb".localized
        }
    }
}


final class StatMainViewController: UIViewController {
    
    //UI
    fileprivate var countDisplayView: StatDisplayView!
    fileprivate var streakDisplayView: StatDisplayView!
    fileprivate var favoriteDisplayView: StatDisplayView!
    
    fileprivate var totalBarChart: BarChartView!
    
    fileprivate var pieButtonBar: ButtonBar!
    fileprivate var pieChartView: PieChartView!
    fileprivate var emptyPieChartView: UIView!
    fileprivate var emptyPieDescriptionLabel: UILabel!
    
    fileprivate var radarButtonBar: ButtonBar!
    fileprivate var radarChartView: RadarChartView!
    fileprivate var radarMonthDescriptionLabel: UILabel!
    fileprivate var radarChartEmptyDescriptionView:UIView!
    
    fileprivate var wordSegmentedControl:UISegmentedControl!
    fileprivate var wordTableView: UITableView!
    fileprivate var emptyTopWordsView: UIView!
    
    //For language change
    
    fileprivate var overviewLabel:UILabel!
    fileprivate var weeklyReviewLabel: UILabel!
    fileprivate var monthlyReviewLabel: UILabel!
    fileprivate var mostWordsLabel: UILabel!
    fileprivate var emptyRadarDescriptionLabel:UILabel!
    fileprivate var emptyWordDescriptionLabel: UILabel!
    fileprivate var wordsCategorizationFeatulreUnavailableView:UIView!
    fileprivate var unavailableEmptyWordDescriptionLabel: UILabel!
    
    init(status:AppStatus) {
        self.appStatus = status
        super.init(nibName: nil, bundle: nil)
        enterViewControllerMemoryLog(self)
        
        setupUI()
        setupObservers()
        setupTableView()
        
        updateTopDisplays(from: appStatus.mainContext)
        initialSetupForCharts()
        updateWordList(from: appStatus.wordCountManager, type: currentWordSegment.tag)
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
        NotificationCenter.default.removeObserver(contextDidSaveNotificationToken)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        [countDisplayView, streakDisplayView, favoriteDisplayView].forEach({$0.startLabelCountAnimation()})
    }
    
    // MARK: - Public
    
    
    // MARK: - Actions
    
    fileprivate lazy var observePieButtonSelection: (Int)->Void = {[unowned self] (index) in
        switch index {
        case SortButton.previous:
            self.currentDateForPieChart = self.currentDateForPieChart.addWeak(value: -1)
        case SortButton.current:
            self.currentDateForPieChart = Date().toDateWithoutHourAndMinutes
        case SortButton.next:
            guard self.currentDateForPieChart < Date() else {return}
            self.currentDateForPieChart = self.currentDateForPieChart.addWeak(value: 1)
        default: assertionFailure()
        }
        self.updatePieChart(forDate: self.currentDateForPieChart)
    }
    
    fileprivate lazy var observeRadarButtonSelection: (Int)->Void = {[unowned self] (index) in
        switch index {
        case SortButton.previous:
            self.currentDateForRadar = self.currentDateForRadar.addMonth(value: -1)
        case SortButton.current:
            self.currentDateForRadar = Date().toDateWithoutHourAndMinutes
        case SortButton.next:
            guard self.currentDateForRadar < Date() else {return}
            self.currentDateForRadar = self.currentDateForRadar.addMonth(value: 1)
        default: assertionFailure()
        }
        self.updateRadarChartAndUpdateMonthDescriptionLabel(withCurrentMonth: self.currentDateForRadar)
    }
    
    fileprivate lazy var observeColorChanges: ()->Void = {[unowned self] in
        self.updatePieChart(forDate: self.currentDateForPieChart)
    }
    
    fileprivate lazy var observeContextDidSaveNotification:(ContextDidSaveNotification)->Void = {[unowned self] (note) in
        /**
         ------------------------- LOGIC RUN DOWN ------------------------------
         we moved the logic to delete words from here to wordCount manager to keep the code neat.
         
         0. we update UI(charts, etc.) depending on notification.
         
         
        Now I understand why the words are not updated right away.
         the current logic run down is
         word manager listens for events from conetxtdidsave notification and then take a proper actions from it. like if it's inserted, add pharagrahs. but it takes time. I moved all the relevant logics to word count manager in order to maintain it better but it makes it hard to make it work.
         */
        
        if !note.insertedObjects.isEmpty || !note.deletedObjects.isEmpty || !note.updatedObjects.isEmpty {
            //TODO: it has to be more sophisticated than this.. it's a waste of memory and performance..
            self.updateTopDisplays(from: self.appStatus.mainContext)
            self.updateRadarChartAndUpdateMonthDescriptionLabel(withCurrentMonth: self.currentDateForRadar)
            self.updatePieChart(forDate: self.currentDateForPieChart)
            self.updateWordList(from: self.appStatus.wordCountManager, type: self.currentWordSegment.tag)
        }
    }
    
    fileprivate func userDidSwipe(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else {return nil}
        
        // it's used for hiding the action when switching to other segmenet
        self.currentIndexPathForWord = indexPath
        
        let deleteAction = SwipeAction(style: .default, title: nil, handler: {[unowned self](_, indexPath) in
            
            let addWordToFilterList = {
                /**
                 ------------------------- LOGIC RUN DOWN ------------------------------
                 Business logic for word limitation
                 add limitation word to the array and retrieve the list again.
                 when retrieveing the list, if it's included in the limitation array, it's excluded
                 */
                self.appStatus.wordCountManager.addLimitationWord(forTag: self.currentWordSegment.tag, word: self.wordTableViewDataSource.object(atIndexPath: indexPath).word)
                self.updateWordList(from: self.appStatus.wordCountManager, type: self.currentWordSegment.tag)
            }
            
            guard self.appStatus.notificationManager.wordDeletionWarningTracker.value else {
                addWordToFilterList()
                return
            }
            
            let checkPopupViewController = CheckPopupViewController(body: "It will add this word to the filter list.".localized + "\n" + "This word will be filtered out.".localized)
            
            self.presentPopup(targetVC: checkPopupViewController, okButtonTitle: "Got it".localized, okAction: {
                if checkPopupViewController.isChecked {
                    self.appStatus.notificationManager.wordDeletionWarningTracker.value = false
                }
                addWordToFilterList()
            }, panGestureDismissal: false)
        })
        
        deleteAction.backgroundColor = UIColor.red
        deleteAction.image = UIImage(named:"swipe_trash")?.resizeImage(width: 25, height: 28)
        
        return [deleteAction]
    }
    
    @objc fileprivate func userDidTapWordSegmentedControl() {
        updateWordList(from: appStatus.wordCountManager, type: currentWordSegment.tag)
        
        // if tableview is swiped, hide it.
        guard currentIndexPathForWord != nil else {return}
        let targetCell = wordTableViewDataSource.cellForRow(at: currentIndexPathForWord)
        targetCell.hideSwipe(animated: true)
    }
    
    fileprivate lazy var observeSignalToReloadTopWords: ()->Void = {[unowned self] in
        self.updateWordList(from: self.appStatus.wordCountManager, type: self.currentWordSegment.tag)
    }
    
    fileprivate lazy var observeLanguageChange:(Language)->Void = {[unowned self] (lan) in
        /**
         it crashes when user haven't clicked on the stat tab and change language because all ui haven't been created by that point. so we need a boolean that indicates if user checkes this page before changing language.
         */
        guard self.isViewLoaded else {return}
        
        self.overviewLabel.text = "Overview".localized
        self.weeklyReviewLabel.change(toString: "Weekly Review".localized)
        self.pieButtonBar.changeButtonNames(["Previous".localized, "Current".localized, "Next".localized])
        
        // for pie empty description.. split it into two parts and localize the first part.
        
        let pieArray = self.emptyPieDescriptionLabel.text!.split(separator: ":").map({String($0)})
        let descPart = "It looks empty.".localized + "\n" + "Current Week".localized + ":"
        let currentWeekForPie = Date.getWeek(forDate: self.currentDateForPieChart)
        var datePart = "\(currentWeekForPie.start.format(with: currentLanguage.toLongMonthToDayFormat)) - \(currentWeekForPie.end.format(with: currentLanguage.toLongMonthToDayFormat))"
        self.emptyPieDescriptionLabel.change(toString: descPart+datePart)
        self.updatePieChart(forDate: self.currentDateForPieChart)
        
        self.monthlyReviewLabel.change(toString: "Monthly Review".localized)
        self.radarButtonBar.changeButtonNames(["Previous".localized, "Current".localized, "Next".localized])
        // for radar description
        self.updateRadarChartAndUpdateMonthDescriptionLabel(withCurrentMonth: self.currentDateForRadar)
        self.emptyRadarDescriptionLabel.text = "It looks empty.".localized + "\n" + "How about creating a new one?".localized
        self.mostWordsLabel.change(toString: "Top 5 most used words".localized)
        self.wordSegmentedControl.changeTitles(to: ["Verb".localized, "Noun".localized, "Place".localized, "Adjective".localized, "Adverb".localized])
        
        self.emptyWordDescriptionLabel.change(toString: "It looks empty for now.".localized + "\n" + "You can create a new closure:)".localized)
        
        self.unavailableEmptyWordDescriptionLabel.text = "This feature is available for only English for now.".localized
        self.wordsCategorizationFeatulreUnavailableView.isHidden = lan == .english
    }
    
    fileprivate lazy var observeAllCoreDataRemoved:()->Void = {[unowned self] in
        /**
         When all core data entires are removed, we need to reset data related to stat dictionaries. Not only revmoe the value from user default but the one saved in memory.
         */
        self.appStatus.wordCountManager.reset()
        
        
        self.updateTopDisplays(from: self.appStatus.mainContext)
        self.updateRadarChartAndUpdateMonthDescriptionLabel(withCurrentMonth: self.currentDateForRadar)
        self.updatePieChart(forDate: self.currentDateForPieChart)
        self.updateWordList(from: self.appStatus.wordCountManager, type: self.currentWordSegment.tag)
    }
    
    // MARK: - Fileprivate
    fileprivate let appStatus: AppStatus

    // Actually don't need this property but for the sake of practicing the good cycle.
    fileprivate var contextDidSaveNotificationToken: NSObjectProtocol!
    
    fileprivate var currentDateForPieChart:Date!
    
    
    fileprivate var currentDateForRadar:Date!
    
    fileprivate let bag = FilterDisposeBag.defaultBag
    
    fileprivate var wordTableViewDataSource: DefaultTableViewDataSource<StatWordTableViewCell>!
    fileprivate var currentIndexPathForWord: IndexPath!
    
    fileprivate let rateGroup: [Rate] = [.terrible,.unsatisfactory, .ok, .good, .awesome]
    
    fileprivate var currentWordSegment: WordSegment {
        guard let targetSegment = WordSegment.init(rawValue: wordSegmentedControl.selectedSegmentIndex) else {assertionFailure();return WordSegment.init(rawValue: 0)!}
        return targetSegment
    }
    
    fileprivate func setupTableView() {
        wordTableViewDataSource = DefaultTableViewDataSource<StatWordTableViewCell>.init(tableView: wordTableView, parentViewController: self)
        
        updateTableViewWithWordRank(wordManager: appStatus.wordCountManager)
    }
    
    fileprivate func updateTableViewWithWordRank(wordManager: WordCountManager) {
        let topSets = wordManager.retrieveTopFiveWords(forTag: currentWordSegment.tag)
        wordTableViewDataSource.update(data: [0:topSets])
    }
    
    fileprivate func updateTopDisplays(from moc: NSManagedObjectContext) {
        let countForAll = Review.getMonthlyCount(from: moc, date: nil, rate: nil)
        
        let countForFavorite = Review.count(in: moc, configure: {(request) in
            request.returnsObjectsAsFaults = true
            request.predicate = NSPredicate(format: "%K == %@", #keyPath(Review.isFavorite), NSNumber(booleanLiteral: true))
        })
        
        countDisplayView.configure(withStat: "\(countForAll)", description: "Closures")
        favoriteDisplayView.configure(withStat: "\(countForFavorite)", description: "Favorite")
        
        calculateStreakCountsAndUpdate(from: moc)
    }
    
    fileprivate func calculateStreakCountsAndUpdate(from moc: NSManagedObjectContext) {
        /**
         ------------------------- LOGIC RUN DOWN ------------------------------
         0. check if there is a review yesterday. if not, return 0
         1. if so, keep going on until it hits no review.
         2. if there is a review for today, add 1 to the count.
         
         PS.
         0. we need to check if it was created on the same day as the registration.
            because users are allowed to create reviews for past.
         1. it needs to listen for context did save notification in order to update number.
         
         // FIXME: when a review is deleted and it updates the streak, the deleted review is not removed from either memory or sqlite.
         */
        
        waitFor(milliseconds: 100, completion: {[weak self] in
            guard let self = self else {return}
            
            var currentDate = Date().addDay(value: -1)
            var result = 0
            
            while Review.checkIfReviewExistsAndCreatedOnSameDayAsRegistration(from: moc, forDate:currentDate) {
                result += 1
                currentDate = currentDate.addDay(value: -1)
            }
            
            if Review.checkIfReviewExistsAndCreatedOnSameDayAsRegistration(from: moc, forDate: Date()) {
                result += 1
            }
            
            self.streakDisplayView.configure(withStat: "\(result)", description: "Streak")
        })
    }
    
    fileprivate func setupObservers() {
        
        pieButtonBar.buttonIndexObservable.subscribe(onNext: observePieButtonSelection).disposed(by: bag)
        radarButtonBar.buttonIndexObservable.subscribe(onNext: observeRadarButtonSelection).disposed(by: bag)
        contextDidSaveNotificationToken = appStatus.mainContext.addContextDidSaveNotificationObserver(observeContextDidSaveNotification)
        appStatus.colorChangeObservable.subscribe(onNext: observeColorChanges).disposed(by: bag)
        
        //Words
        appStatus.wordCountManager.signalToReloadTopWordsObservable
            .subscribe(onNext: observeSignalToReloadTopWords).disposed(by: bag)
        
        // Add Target
        wordSegmentedControl.addTarget(self, action: #selector(userDidTapWordSegmentedControl), for: .valueChanged)
        
        // LanguageChange
        appStatus.languageManager.changeLanguageObservable.subscribe(onNext: observeLanguageChange).disposed(by: bag)
        
        // remove all coredata
        appStatus.allCoreDataRemovedObservable.subscribe(onNext: observeAllCoreDataRemoved).disposed(by: bag)
    }
    
    
    fileprivate func initialSetupForCharts() {
        let initialDate = Date().toDateWithoutHourAndMinutes
        
        currentDateForRadar = initialDate
        currentDateForPieChart = initialDate
        
        updateRadarChartAndUpdateMonthDescriptionLabel(withCurrentMonth: initialDate)
        updatePieChart(forDate: initialDate)
    }

    
    fileprivate func updateRadarChartAndUpdateMonthDescriptionLabel(withCurrentMonth date:Date) {
        /**
         -------------------------- UI ------------------------------
          we set different label count depending on the biggest number from result.
         */
        let monthlyResult = rateGroup.map({(count:Review.getMonthlyCount(from: appStatus.mainContext, date: date, rate: $0), rate:$0)})

        let biggestNumberFromResult = monthlyResult.map({$0.count}).sorted(by: <).last!
        
        let labelCount: Int = biggestNumberFromResult % 6
        radarChartView.yAxis.labelCount = labelCount
        radarChartView.yAxis.axisMaximum = Double(biggestNumberFromResult)
        
        let currentMonthSet = RadarChartDataSet(values: monthlyResult.map({RadarChartDataEntry(value: Double($0.count))}), label: "current month")
        
        currentMonthSet.setColor(.mainBlue)
        currentMonthSet.fillColor = .mainBlue
        currentMonthSet.drawFilledEnabled = true
        currentMonthSet.fillAlpha = 0.7
        currentMonthSet.lineWidth = 2
        currentMonthSet.drawHighlightCircleEnabled = true
        currentMonthSet.setDrawHighlightIndicators(false)

        let data = RadarChartData(dataSets: [currentMonthSet])
        data.setValueFont(.systemFont(ofSize: 8, weight: .light))
        data.setDrawValues(false)
        data.setValueTextColor(.black)

        radarChartView.data = data
        radarMonthDescriptionLabel.text = "Current Month".localized + ": \(date.format(with: currentLanguage.toYearToDayFormat))"
        
        //animate
        radarChartView.animate(xAxisDuration: 1, yAxisDuration: 1, easingOption: .easeOutBack)
        
        //let's hide or show the empty description view depending on the fetch result.
        radarChartEmptyDescriptionView.isHidden = !monthlyResult.map({$0.count}).all({$0 == 0})
    }
    
    fileprivate func updatePieChart(forDate date:Date) {
        /**
          the return value is the exact date like 2018-08-11:00:00:00 but in order to fetch it the way we implement it,
         the end date must be one second away from turning the day after. like 2018-08-11:59:59:59
         */
        let currentWeek = Date.getWeek(forDate: date)
        let validStartDate = currentWeek.start.addSecond(value: -1)
        let validEnddate = currentWeek.end.addDay(value: 1).addSecond(value: -1)
        
        var countAndRate: [(count:Int,rate:Rate)] = rateGroup.map({(Review.getCount(from: appStatus.mainContext, startDate: validStartDate, endDate: validEnddate, rate: $0), $0)})
        
        // Get rid of rates whose count is zero
        countAndRate.removeItem(condition: {$0.count == 0})
        
        showOrHideEmptyPieView(withCurrentWeek: currentWeek, needsToShow: countAndRate.count == 0)
        
        guard countAndRate.count != 0 else {
            return
        }
        
        let entries = countAndRate.map({PieChartDataEntry(value: Double($0.count), label: $0.rate.description)})
        
        let set = PieChartDataSet(values: entries, label: "Closures")
        set.sliceSpace = 3
        set.colors = countAndRate.map{$0.1.currentColor as NSUIColor}
        
        set.valueLinePart1OffsetPercentage = 0.8
        set.valueLinePart1Length = 0.3
        set.valueLinePart2Length = 0.5
        set.yValuePosition = .outsideSlice
        
        let fm = NumberFormatter()
        fm.numberStyle = .none
        
        let data = PieChartData(dataSet: set)
        data.setValueFormatter(DefaultValueFormatter(formatter: fm))
        data.setValueFont(UIFont.mainFont(size: 13, shouldBold: false))
        data.setValueTextColor(.black)
        
        pieChartView.data = data
        pieChartView.centerText = "\(currentWeek.start.format(with: currentLanguage.toShortMonthToDayFormat)) - \(currentWeek.end.format(with: currentLanguage.toShortMonthToDayFormat))"
        pieChartView.spin(duration: TimeInterval.init(0.7), fromAngle: 0, toAngle: 20)
    }
    
    fileprivate func showOrHideEmptyPieView(withCurrentWeek week: (start:Date,end:Date), needsToShow: Bool) {
        if needsToShow {
            emptyPieChartView.isHidden = false
            emptyPieDescriptionLabel.text = "It looks empty.".localized + "\n" + "Current Week".localized + ": \(week.start.format(with: currentLanguage.toLongMonthToDayFormat)) - \(week.end.format(with: currentLanguage.toLongMonthToDayFormat))"
        } else {
            emptyPieChartView.isHidden = true
        }
    }
    
    fileprivate func showOrHideEmptyWordView(needsToShow:Bool) {
        emptyTopWordsView.isHidden = !needsToShow
    }
    
    fileprivate func updateWordList(from countManager: WordCountManager, type: NSLinguisticTag) {
        let wordArray = countManager.retrieveTopFiveWords(forTag: type)
        showOrHideEmptyWordView(needsToShow: wordArray.count == 0)
        
        guard wordArray.count != 0 else {
            return
        }
        
        // change it into [index:[(wordEntry)]] so that it could be paired with section.
        let indexDictionary = wordArray.convertToDictionaryWithIndex(configureValue: {[$0]}) as! [Int: [(String,Int)]]
        wordTableViewDataSource.update(data: indexDictionary)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
extension StatMainViewController: IAxisValueFormatter, ChartViewDelegate {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return rateGroup [Int(value) % rateGroup.count].title
    }
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        // maybe it has something to do with it..?
    }
}

extension StatMainViewController: UITableViewDelegate, SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView.create(withColor: .clear, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard section != 0 else {return 0}
        return 10
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        return userDidSwipe(tableView, editActionsForRowAt: indexPath, for: orientation)
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .selection
        options.transitionStyle = .border
        options.minimumButtonWidth = 50
        return options
    }
}
extension StatMainViewController:UIScrollViewDelegate {
    fileprivate func setupUI() {
        view.backgroundColor = .white
        
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.alwaysBounceVertical = true
        scrollView.contentSize = CGSize(width: view.frame.width, height: 1500)
        view.addSubview(scrollView)
        
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        overviewLabel = UILabel.create(text: "Overview".localized, textAlignment: .center, fontSize: 20)
        
        countDisplayView = StatDisplayView(languageManager: appStatus.languageManager)
        streakDisplayView = StatDisplayView(languageManager: appStatus.languageManager)
        favoriteDisplayView = StatDisplayView(languageManager: appStatus.languageManager)
        
        let groupForStackView: [UIView] = [countDisplayView, streakDisplayView, favoriteDisplayView]
        //        groupForStackView.forEach({$0.setShadow(contentViewCornerRadius: 5, shadowRadius: 5, shadowOpacity: 0.1, shadowOffset: CGSize(width: 2, height: 2))})
        
        let topStackView = UIStackView.create(views: groupForStackView, axis: .horizontal, alignment: .fill, distribution: .fillEqually, spacing: 40)
        
        constraint(countDisplayView, streakDisplayView, favoriteDisplayView) { (_countDisplayView, _streakDisplayView, _favoriteDisplayView) in
            _countDisplayView.sizeEqualTo(width: 70, height: 70)
            _streakDisplayView.sizeEqualTo(width: 70, height: 70)
            _favoriteDisplayView.sizeEqualTo(width: 70, height: 70)
        }
        
        let weeklyReviewSeparator = UIView.create()
        
        weeklyReviewLabel = UILabel.create(text: "Weekly Review".localized, textAlignment: .center, textColor: .black, fontSize: 20, boldFont: false, numberOfLine: 1)
        
        pieButtonBar = ButtonBar(buttonTitles: ["Previous".localized, "Current".localized, "Next".localized])
        
        pieChartView = PieChartView()
        //        pieChartView.delegate = self
        pieChartView.rotationEnabled = true
        pieChartView.rotationAngle = 0
        pieChartView.drawHoleEnabled = true
        pieChartView.drawCenterTextEnabled = true
        pieChartView.centerAttributedText = NSAttributedString(string: "August 2018", attributes: [NSAttributedStringKey.font:UIFont.boldSystemFont(ofSize: 18)])
        pieChartView.setExtraOffsets(left: 5, top: 0, right: 5, bottom: 0)
        pieChartView.legend.enabled = false
        pieChartView.chartDescription = nil
        pieChartView.highlightPerTapEnabled = false
        
        emptyPieChartView = UIView.create(withColor: .white, alpha: 1)
        
        let pieEmptyBoxImageView = UIImageView.create(withImageKey: .emptyBox)
        emptyPieDescriptionLabel = UILabel.create(text: "It looks empty.\nCurrent Week:", textAlignment: .center, fontSize: 15)
        
        let groupForEmptyPieChartView:[UIView] = [emptyPieDescriptionLabel, pieEmptyBoxImageView]
        
        groupForEmptyPieChartView.forEach(emptyPieChartView.addSubview(_:))
        constraint(emptyPieDescriptionLabel, pieEmptyBoxImageView) { (_emptyPieDescriptionLabel, _pieEmptyBoxImageView) in
            _emptyPieDescriptionLabel.centerX.centerY.equalToSuperview()
            
            _pieEmptyBoxImageView.centerX.equalToSuperview()
            _pieEmptyBoxImageView.centerY.equalToSuperview().offset(-100)
            _pieEmptyBoxImageView.sizeEqualTo(width: 100, height: 100)
        }
        
        
        let monthlyReviewSeparator = UIView.create()
        
        monthlyReviewLabel = UILabel.create(text: "Monthly Review".localized, textAlignment: .center, textColor: .black, fontSize: 20, boldFont: false, numberOfLine: 1)
        
        radarButtonBar = ButtonBar(buttonTitles: ["Previous".localized, "Current".localized, "Next".localized])
        
        radarMonthDescriptionLabel = UILabel.create(text: "Current Month: 2018-08", textAlignment: .center, fontSize: 15)
    
        radarChartView = RadarChartView()
        
        radarChartView.chartDescription?.enabled = false
        radarChartView.delegate = self
        radarChartView.webLineWidth = 1
        radarChartView.innerWebLineWidth = 1
        radarChartView.webColor = .lightGray
        radarChartView.innerWebColor = .lightGray
        radarChartView.webAlpha = 1
        
        let xAxis = radarChartView.xAxis
        xAxis.labelFont = .systemFont(ofSize: 11, weight: .light)
        xAxis.xOffset = 0
        xAxis.yOffset = 0
        xAxis.valueFormatter = self
        xAxis.labelTextColor = .black
        
        let yAxis = radarChartView.yAxis
        yAxis.labelTextColor = .black
        yAxis.labelFont = .systemFont(ofSize: 9, weight: .light)
//        yAxis.labelCount = 6
        yAxis.axisMinimum = 0
//        yAxis.axisMaximum = 30
        yAxis.drawLabelsEnabled = true
        
        let l = radarChartView.legend
        l.enabled = false
        
        radarChartEmptyDescriptionView = UIView.create(withColor: .white, alpha: 1)
        
        emptyRadarDescriptionLabel = UILabel.create(text: "It looks empty.".localized + "\n" + "How about creating a new one?".localized, textAlignment: .center, fontSize: 15)
        
        let emptyBoxImageViewForRadarChart = UIImageView.create(withImageKey: .emptyBox)
        
        let groupForEmptyDescriptionView:[UIView] = [emptyBoxImageViewForRadarChart, emptyRadarDescriptionLabel]
        groupForEmptyDescriptionView.forEach(radarChartEmptyDescriptionView.addSubview(_:))
        
        constraint(emptyBoxImageViewForRadarChart, emptyRadarDescriptionLabel) { (_emptyBoxImageViewForRadarChart, _emptyRadarDescriptionLabel) in
            
            _emptyBoxImageViewForRadarChart.centerX.equalToSuperview()
            _emptyBoxImageViewForRadarChart.centerY.equalToSuperview().offset(-70)
            _emptyBoxImageViewForRadarChart.sizeEqualTo(width: 100, height: 100)
            
            _emptyRadarDescriptionLabel.centerX.equalToSuperview()
            _emptyRadarDescriptionLabel.centerY.equalToSuperview().offset(30)
            
        }
        
        
        let mostWordsSeparator = UIView.create()
        
        mostWordsLabel = UILabel.create(text: "Top 5 most used words".localized, textAlignment: .center, textColor: .black, fontSize: 20, boldFont: false, numberOfLine: 1)
        
        wordSegmentedControl = UISegmentedControl.create(withTitles: ["Verb".localized, "Noun".localized, "Place".localized, "Adjective".localized, "Adverb".localized], tintColor: .mainBlue, selectedSegmentIndex: 0)
        
        wordTableView = UITableView.create(style: .plain, backgroundColor: .white)
//        wordTableView.isScrollEnabled = false
        wordTableView.delegate = self
        wordTableView.separatorStyle = .none
        
        emptyTopWordsView = UIView.create(withColor: .white, alpha: 1)
        
        let emptyBoxImageView = UIImageView.create(withImageKey: .emptyBox)
        
        emptyWordDescriptionLabel = UILabel.create(text: "It looks empty for now.".localized + "\n" + "You can create a new closure:)".localized, textAlignment: .center, fontSize: 15)
        
        let groupForEmptyTopWordsView: [UIView] = [emptyBoxImageView, emptyWordDescriptionLabel]
        groupForEmptyTopWordsView.forEach(emptyTopWordsView.addSubview(_:))
        
        constraint(emptyBoxImageView, emptyWordDescriptionLabel) { (_emptyBoxImageView, _emptyWordDescriptionLabel) in
            
            _emptyBoxImageView.centerX.equalToSuperview()
            _emptyBoxImageView.centerY.equalToSuperview().offset(-60)
            _emptyBoxImageView.sizeEqualTo(width: 100, height: 100)
            
            _emptyWordDescriptionLabel.centerX.equalToSuperview()
            _emptyWordDescriptionLabel.centerY.equalToSuperview().offset(30)
        }
        
        wordsCategorizationFeatulreUnavailableView = UIView.create(withColor: .white, alpha: 1)
        wordsCategorizationFeatulreUnavailableView.isHidden = currentLanguage == .english
        
        let unavailbleEmptyBoxImageView = UIImageView.create(withImageKey: .emptyBox)
        
        unavailableEmptyWordDescriptionLabel = UILabel.create(text: "This feature is available for only English for now.".localized, textAlignment: .center, fontSize: 15)
        
        ([unavailbleEmptyBoxImageView, unavailableEmptyWordDescriptionLabel] as [UIView]).forEach(wordsCategorizationFeatulreUnavailableView.addSubview(_:))
        
        constraint(unavailbleEmptyBoxImageView, unavailableEmptyWordDescriptionLabel) { (_unavailbleEmptyBoxImageView, _unavailableEmptyWordDescriptionLabel) in
            _unavailbleEmptyBoxImageView.centerX.equalToSuperview()
            _unavailbleEmptyBoxImageView.centerY.equalToSuperview().offset(-60)
            _unavailbleEmptyBoxImageView.sizeEqualTo(width: 100, height: 100)
            
            _unavailableEmptyWordDescriptionLabel.centerX.equalToSuperview()
            _unavailableEmptyWordDescriptionLabel.centerY.equalToSuperview().offset(30)
        }
        
        
        let group:[UIView] = [overviewLabel,topStackView, weeklyReviewSeparator, weeklyReviewLabel, pieChartView, pieButtonBar, emptyPieChartView , monthlyReviewSeparator ,monthlyReviewLabel, radarChartView,radarChartEmptyDescriptionView, radarButtonBar, radarMonthDescriptionLabel ,mostWordsSeparator, mostWordsLabel,wordSegmentedControl, wordTableView, emptyTopWordsView,wordsCategorizationFeatulreUnavailableView]
        
        group.forEach(scrollView.addSubview(_:))
        
        let gapBetweenSeparatorAndLabel:CGFloat = 15
        
        //first top half
        constraint(overviewLabel, topStackView, weeklyReviewSeparator, weeklyReviewLabel, pieButtonBar, pieChartView, emptyPieChartView ) { (_overviewLabel, _stackView, _weeklyReviewSeparator, _weeklyReviewLabel, _pieButtonBar, _pieChartView, _emptyPieChartView) in
            
            _overviewLabel.top.equalToSuperview().offset(gapBetweenSeparatorAndLabel)
            _overviewLabel.centerX.equalToSuperview()
            
            _stackView.top.equalTo(overviewLabel.snp.bottom).offset(15)
            _stackView.centerX.equalToSuperview()
            
            _weeklyReviewSeparator.top.equalTo(topStackView.snp.bottom).offset(25)
            _weeklyReviewSeparator.centerX.equalToSuperview()
            _weeklyReviewSeparator.width.equalToSuperview()
            _weeklyReviewSeparator.height.equalTo(0.5)
            
            _weeklyReviewLabel.top.equalTo(weeklyReviewSeparator.snp.bottom).offset(gapBetweenSeparatorAndLabel)
            _weeklyReviewLabel.centerX.equalToSuperview()
            
            _pieButtonBar.top.equalTo(weeklyReviewLabel.snp.bottom).offset(5)
            _pieButtonBar.centerX.equalToSuperview()
            _pieButtonBar.width.equalTo(230)
            _pieButtonBar.height.equalTo(50)
            
            // since it give too much padding to the graph, it should be set - offset so that it looks better.
            _pieChartView.top.equalTo(pieButtonBar.snp.bottom).offset(-80)
            _pieChartView.centerX.equalToSuperview()
            _pieChartView.width.equalToSuperview().offset(-40)
            _pieChartView.height.equalTo(500)
            
            _emptyPieChartView.top.equalTo(pieButtonBar.snp.bottom).offset(10)
            _emptyPieChartView.left.right.bottom.equalTo(pieChartView)
            
        }
        
        constraint( monthlyReviewSeparator, monthlyReviewLabel, radarButtonBar, radarMonthDescriptionLabel, radarChartView,radarChartEmptyDescriptionView, mostWordsSeparator, mostWordsLabel, wordSegmentedControl, wordTableView) { ( _monthlyReviewSeparator, _monthlyReviewLabel, _radarButtonBar, _radarMonthDescriptionLabel , _radarChartView , _radarChartEmptyDescriptionView, _comparisonSeparator, _mostWordsLabel, _wordSegmentedControl, _wordTableView) in
            
            // since it give too much padding to the graph, it should be set - offset so that it looks better.
            _monthlyReviewSeparator.top.equalTo(pieChartView.snp.bottom).offset(-70)
            _monthlyReviewSeparator.centerX.equalToSuperview()
            _monthlyReviewSeparator.width.equalToSuperview()
            _monthlyReviewSeparator.height.equalTo(0.5)
            
            _monthlyReviewLabel.top.equalTo(monthlyReviewSeparator.snp.bottom).offset(gapBetweenSeparatorAndLabel)
            _monthlyReviewLabel.centerX.equalToSuperview()
            
            _radarButtonBar.top.equalTo(monthlyReviewLabel.snp.bottom).offset(5)
            _radarButtonBar.centerX.equalToSuperview()
            _radarButtonBar.width.equalTo(230)
            _radarButtonBar.height.equalTo(50)
            
            _radarMonthDescriptionLabel.top.equalTo(radarButtonBar.snp.bottom).offset(5)
            _radarMonthDescriptionLabel.centerX.equalToSuperview()
            
            // since it give too much padding to the graph, it should be set - offset so that it looks better.
            _radarChartView.top.equalTo(radarMonthDescriptionLabel.snp.bottom).offset(-50)
            _radarChartView.centerX.equalToSuperview()
            _radarChartView.width.equalToSuperview()
            _radarChartView.height.equalTo(500)
            
            _radarChartEmptyDescriptionView.edges.equalTo(radarChartView)
            
            _comparisonSeparator.top.equalTo(radarChartView.snp.bottom).offset(-60)
            _comparisonSeparator.left.right.equalTo(monthlyReviewSeparator)
            _comparisonSeparator.height.equalTo(0.5)
            
            _mostWordsLabel.top.equalTo(mostWordsSeparator.snp.bottom).offset(gapBetweenSeparatorAndLabel)
            _mostWordsLabel.centerX.equalToSuperview()
            
            _wordSegmentedControl.top.equalTo(mostWordsLabel.snp.bottom).offset(10)
            _wordSegmentedControl.centerX.equalToSuperview()
            _wordSegmentedControl.sizeEqualTo(width: 320, height: 25)
            
            _wordTableView.top.equalTo(wordSegmentedControl.snp.bottom).offset(gapBetweenSeparatorAndLabel)
            _wordTableView.centerX.equalToSuperview()
            _wordTableView.width.equalTo(210)
            _wordTableView.height.equalTo(280)
            
        }
        
        constraint(emptyTopWordsView,wordsCategorizationFeatulreUnavailableView) { (_emptyTopWordsView, _wordsCategorizationFeatulreUnavailableView) in
            
            _emptyTopWordsView.edges.equalTo(wordTableView)
            
//            _wordsCategorizationFeatulreUnavailableView.edges.equalTo(wordTableView)
            _wordsCategorizationFeatulreUnavailableView.top.bottom.equalTo(wordTableView)
            _wordsCategorizationFeatulreUnavailableView.left.equalTo(wordTableView).offset(-100)
            _wordsCategorizationFeatulreUnavailableView.right.equalTo(wordTableView).offset(100)
        }
        
        
        
        
        
        
        
    }
}


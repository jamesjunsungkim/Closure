//
//  StatDisplayView.swift
//  Closure
//
//  Created by montapinunt Pimonta on 8/4/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout

final class StatDisplayView:UIView {
    
    //UI
    fileprivate var statLabel: UILabel!
    fileprivate var descriptionLabel: UILabel!
    
    init(languageManager:LanguageManager) {
        self.languageManager = languageManager
        super.init(frame: .zero)
        setupUI()
        setupObserver()
    }
    
    // MARK: Public
    public func configure(withStat stat: String, description:String){
        statLabel.text = stat
        descriptionLabel.text = description.localized
        
        // for translation purpose
        descriptionInEnglish = description
        
        /**
         count animation setup. in order to activate it, startlabelcountAnimation has to be excecuted.
         we set different duration value depending on the number. the bigger, the longer
         */
        
        guard let convertedNum = Int(stat) else {assertionFailure();return}
        animationEndValue = convertedNum
        switch convertedNum {
        case 0...10:
            animationDuration = 0.35
        case 11...100:
            animationDuration = 1
        default:
            animationDuration = 1.5
        }
    }
    
    public func startLabelCountAnimation() {
        // this one has to be called after configure method.
        guard animationEndValue != nil else {assertionFailure();return}
        
        displayLink = CADisplayLink(target: self, selector: #selector(updateLabelForAnimation))
        displayLink.add(to: .main, forMode: .defaultRunLoopMode)
        
        animationStartDate = Date()
    }
    
    // MARK: Actions
    fileprivate lazy var observeLanguageChange:(Language)->Void = {[unowned self] (_) in
        guard self.descriptionInEnglish != nil else {assertionFailure();return}
        self.descriptionLabel.text = self.descriptionInEnglish.localized
    }
    
    // MARK: Fileprivate
    fileprivate let bag = FilterDisposeBag.defaultBag
    
    fileprivate var descriptionInEnglish: String!
    fileprivate weak var languageManager: LanguageManager!
    
    //For animation
    fileprivate var animationStartDate: Date!
    fileprivate var animationDuration:Double = 1
    
    fileprivate var animationCurrentValue = 0
    fileprivate var animationEndValue: Int!
    fileprivate var displayLink: CADisplayLink!
    
    fileprivate func setupObserver() {
        languageManager.changeLanguageObservable.subscribe(onNext: observeLanguageChange).disposed(by: bag)
    }
    
    @objc fileprivate func updateLabelForAnimation() {
        let now = Date()
        let elapsedTime = now.timeIntervalSince(animationStartDate)
        
        guard elapsedTime < animationDuration else {
            statLabel.text = "\(animationEndValue!)"
            displayLink.invalidate()
            return
        }
        let percentage = elapsedTime / animationDuration
        let value = Int(percentage * Double(animationEndValue))
        statLabel.text = "\(value)"
    }
    

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension StatDisplayView {
    fileprivate func setupUI() {
        setBorder(color: .mainBlue, width: 0.5)
        setCornerRadious(value: 3)
        
        statLabel = UILabel.create(text: "10", textAlignment: .center, fontSize: 13)
        
        descriptionLabel = UILabel.create(text: "Cards", textAlignment: .center, fontSize: 15)
        
        let group:[UIView] = [statLabel, descriptionLabel]
        group.forEach(addSubview(_:))
        
        constraint(statLabel, descriptionLabel) { (_statLabel, _descriptionLabel) in
            
            _statLabel.centerX.equalToSuperview()
            _statLabel.centerY.equalToSuperview().offset(-10)
            
            _descriptionLabel.centerX.equalToSuperview()
            _descriptionLabel.centerY.equalToSuperview().offset(15)
        }

    }
}

//
//  StatWordTableViewCell.swift
//  Closure
//
//  Created by montapinunt Pimonta on 8/11/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import SwipeCellKit

class StatWordTableViewCell: SwipableDefaultTableViewCell {
    typealias Object = (word:String,count:Int)
    
    //UI
    fileprivate var wordNameLabel: UILabel!
    
    override func prepareForReuse() {
        wordNameLabel.text = ""
//        clipsToBounds = false
    }
    func setup(withObject object: (word: String, count: Int), parentViewController: UIViewController, currentIndexPath: IndexPath, userInfo: [String : Any]?) {
        setupUI()
        configure(withWord: object, withIndex: currentIndexPath.section)
        setupSwipeDelegation(withParentVC: parentViewController)
    }
    
    //MARK: Fileprivate
    fileprivate func configure(withWord set:(word:String,count:Int), withIndex id:Int) {
        let attributedText = NSMutableAttributedString(string: "\(id+1). ", attributes: [NSAttributedStringKey.font: UIFont.mainFont(size: 15, shouldBold: true)])
        let restPart = NSAttributedString(string: "\(set.word.capitalized) - \(set.count) times", attributes: [NSAttributedStringKey.font:UIFont.mainFont(size: 17, shouldBold: false)])
        attributedText.append(restPart)
        
        wordNameLabel.attributedText = attributedText
    }
    
    fileprivate func setupSwipeDelegation(withParentVC vc: UIViewController) {
        delegate = vc as? SwipeTableViewCellDelegate
    }
}

extension StatWordTableViewCell {
    fileprivate func setupUI() {
        let backgroundView = UIView.create(withColor: .white, alpha: 1)
        backgroundView.setCornerRadious(value: 5)
        backgroundView.setBorder(color: .mainBlue, width: 1)
        
        addSubview(backgroundView)
        
        backgroundView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        wordNameLabel = UILabel.create(text: "Crazy", textAlignment: .left, fontSize: 17, boldFont:true)
        
        backgroundView.addSubview(wordNameLabel)
        
        wordNameLabel.snp.makeConstraints { (make) in
            make.topBottomEqualToSuperView(withOffset: 10)
            make.left.equalToSuperview().offset(10)
        }
    }
}








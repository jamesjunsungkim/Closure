//
//  Reminder.swift
//  Closure
//
//  Created by montapinunt Pimonta on 8/13/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation
import SwiftyJSON

public enum Day:String {
    case Daily
    case Monday = "Mon"
    case Tuesday = "Tue"
    case Wednesday = "Wed"
    case Thursday = "Thur"
    case Friday = "Fri"
    case Saturday = "Sat"
    case Sunday = "Sun"
    
    public var localized: String {
        return self.rawValue.localized
    }
    
    public var index:Int {
        switch self {
        case .Monday: return 0
        case .Tuesday: return 1
        case .Wednesday: return 2
        case .Thursday: return 3
        case .Friday: return 4
        case .Saturday: return 5
        case .Sunday: return 6
        default:assertionFailure(); return 1
        }
    }
    
    public var toWeekday:Int {
        switch self {
        case .Sunday: return 1
        case .Monday: return 2
        case .Tuesday: return 3
        case .Wednesday: return 4
        case .Thursday: return 5
        case .Friday: return 6
        case .Saturday: return 7
        default:assertionFailure(); return 1
        }
    }
}

final class Reminder: Unwrappable, Equatable {
    
    public struct Keys {
        static let uuid = "uuid"
        static let message = "message"
        static let time = "time"
        static let days = "days"
    }
    
    init(message:String?, time:Date, days:[Day] = [.Monday, .Tuesday, .Wednesday, .Thursday, .Friday, .Saturday, .Sunday]) {
        self.uuid = UUID().uuidString
        self.messageTracker = Tracker<String>.init(initialValue: message.unwrapOr(defaultValue: ""))
        self.timeTracker = Tracker<Date>.init(initialValue: time)
        self.dayTracker = Tracker<[Day]>.init(initialValue: days)
    }
    
    init(json:JSON) {
        self.uuid = json[Keys.uuid].stringValue
        self.messageTracker = Tracker<String>.init(initialValue: json[Keys.message].stringValue)
        self.timeTracker = Tracker<Date>.init(initialValue: json[Keys.time].stringValue.toDate(type: .current))
        guard let dayArray = json[Keys.days].arrayObject as? [String] else {fatalError()}
        self.dayTracker = Tracker<[Day]>.init(initialValue: dayArray.map({Day.init(rawValue: $0)!}))
//        print(json[Keys.time].stringValue)
//        let a = json[Keys.time].stringValue.toDate(type: .current)
//        print(a)
//        let b = Date(year: Date().year, month: Date().month, day: Date().day, hour: a.hour, minute: a.minute, second: 0)
//        print(b)
    }
    
    public var uuid:String
    public var messageTracker: Tracker<String>
    public var timeTracker: Tracker<Date>
    public var dayTracker: Tracker<[Day]>

    public var toDictionary: [String:Any] {
        return [
            Keys.uuid: uuid,
            Keys.message: messageTracker.value,
            Keys.time:timeTracker.value.toHourToMinString(type: .current),
            Keys.days: dayTracker.value.map({$0.rawValue})
        ]
    }
    
    public var toDayString:String {
        return Reminder.converToDayString(input: dayTracker.value)
    }
    
    public var toDate:[Date] {
        let days = dayTracker.value
        let targetDate = timeTracker.value
        
        let dateArray = days.map({ (day) -> Date in
            var components =  DateComponents()
            components.timeZone = .current
            components.hour = targetDate.hour
            components.minute = targetDate.minute
            components.weekdayOrdinal = 10
            components.weekday = day.toWeekday
            let cal = Calendar.current
            guard let result = cal.date(from: components) else {assertionFailure();return Date()}
            return result
        })
        
        return dateArray
    }

    
    // MARK: Static
    static func converToDayString(input:[Day]) ->String {
        guard input.count != 7 else {
            // it means every day.
            return "Daily".localized
        }
        guard let set = input.decomposed  else {assertionFailure();return ""}
        return set.1.reduce("\(set.0.localized)", {"\($0), \($1.localized)"})
    }
    
    static func == (lhs: Reminder, rhs: Reminder) -> Bool {
        return lhs.uuid == rhs.uuid
    }
    
    
    
    
}














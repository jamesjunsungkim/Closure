//
//  SettingType.swift
//  Closure
//
//  Created by James Kim on 8/13/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation

public enum SettingType {
    case displayDescriptionOnly
    case displayImageOnly
    case displayImageWithDescription
    case toggle
    case textInput
    case check
    case action
}

//
//  SettingData.swift
//  Closure
//
//  Created by James Kim on 8/13/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import Foundation

final class SettingData {
    
    init(body:String, descriptionTracker: Tracker<String>? = nil, placeHolder:String? = nil, isOnTracker: Tracker<Bool>? = nil, type:SettingType, isChecked: Bool = false, userTypeInputTracker:Tracker<String>? = nil, needBlurryEffect:Bool = false,additionalDescriptionChange:((String)->(String))? = nil) {
        self.body = body
        self.descriptionTracker = descriptionTracker
        self.isOnTracker = isOnTracker
        self.placeHolder = placeHolder
        self.type = type
        self.isChecked = isChecked
        self.userTypeInputTracker = userTypeInputTracker
        self.needBlurryEffect = needBlurryEffect
        self.additionalDescriptionChange = additionalDescriptionChange
    }
    
    // MARK: Public
    
    let body: String
    let placeHolder:String?
    let type: SettingType
    let needBlurryEffect: Bool
    let additionalDescriptionChange: ((String)->(String))?
    
    public var isChecked = false
    
    fileprivate(set) weak var userTypeInputTracker:Tracker<String>?
    fileprivate(set) weak var descriptionTracker: Tracker<String>?
    fileprivate(set) weak var isOnTracker: Tracker<Bool>?
    
  
    
}










//
//  NotificationAndReminderMainViewController.swift
//  Closure
//
//  Created by James Kim on 8/13/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import SwipeCellKit

fileprivate struct Section {
    static let first = 0
    static let second = 1
    static let third = 2
    static let fourth = 3
    static let fifth = 4
}

final class NotificationAndReminderMainViewController: DefaultViewController {
    
    //UI
    fileprivate var tableView: UITableView!
    
    init(appStatus:AppStatus) {
        self.appStatus = appStatus
        super.init(nibName: nil, bundle: nil)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        setupTableView()
        setupCustomerReminders()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }

    // MARK: - Public
    
    
    // MARK: - Actions
    fileprivate func userDidSelectRow(atIndexPath indexPath: IndexPath) {
        switch indexPath.section {
        case Section.second:
            let targetVC = ReminderTimeSelectionViewController()
            
            presentDefaultVC(targetVC: targetVC, userInfo: currentDefaultReminderTracker.toUserInfo)
            
        case Section.third:
            //it will be to edit custom reminder
            let targetVC = CreateCustomReminderViewController()
            let userInfo = appStatus.toUserInfo
                                    .addValueIfNotEmpty(forKey: Reminder.className, value: tableViewDataSource.additionalData(atIndexPath: indexPath))
            presentDefaultVC(targetVC: targetVC, userInfo: userInfo)
            

        case Section.fourth:
            let targetVC = CreateCustomReminderViewController()
            presentDefaultVC(targetVC: targetVC, userInfo: appStatus.toUserInfo)
        default:assertionFailure()
        }
    }
    
    fileprivate lazy var userDidChangeCustomReminders: ([Reminder])->Void = {[unowned self] (array) in
        // need to update the datasource for the tableview with new data
        self.tableViewDataSource.update(additionalData: array, atSection: Section.third)
    }
    
    fileprivate func userDidSwipe(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard indexPath.section == Section.third, orientation == .right else {return nil}
        
        let deleteAction = SwipeAction(style: .default, title: "Remove", handler: {[unowned self] (_, indexPath) in
            self.presentDefaultAlert(withTitle: "Alert".localized, message: "Are you sure to remove the selected reminder?".localized, okAction: {
                let reminder = self.tableViewDataSource.additionalData(atIndexPath: indexPath) as! Reminder
                self.appStatus.notificationManager.remove(reminder: reminder)
            })
        })
        deleteAction.backgroundColor = .red
        deleteAction.textColor = .white
        
        return [deleteAction]
    }
    
    // MARK: - Fileprivate
    fileprivate weak var appStatus: AppStatus!
    
    fileprivate var tableViewDataSource: DefaultTableViewDataSource<SettingDefaultTableViewCell>!
    
    fileprivate weak var wordDeletionWarningTracker: Tracker<Bool>!
    fileprivate weak var internetDisconnectionWarningTracker: Tracker<Bool>!
    fileprivate weak var defaultReminderTracker: Tracker<Bool>!
    fileprivate weak var customReminderTracker: Tracker<Bool>!
    fileprivate weak var currentDefaultReminderTracker: Tracker<String>!
    
    fileprivate let bag = FilterDisposeBag.defaultBag
    
    fileprivate func setupTableView() {
        tableView.delegate = self
        
        
        wordDeletionWarningTracker = appStatus.notificationManager.wordDeletionWarningTracker
        internetDisconnectionWarningTracker = appStatus.notificationManager.internetDisconnectionWarningTracker
        defaultReminderTracker = appStatus.notificationManager.defaultReminderAllowed
        customReminderTracker = appStatus.notificationManager.customReminderAllowed
        
        currentDefaultReminderTracker = appStatus.notificationManager.defaultReminderTimeTracker
        
        let data: [Int:[SettingData]] = [
            Section.first:[
            SettingData(body: "Notify me when removing top five words".localized, descriptionTracker: nil, placeHolder: nil, isOnTracker: wordDeletionWarningTracker, type: .toggle),
            SettingData(body: "Notify me when network changes".localized, descriptionTracker: nil, placeHolder: nil, isOnTracker: internetDisconnectionWarningTracker, type: .toggle),
            SettingData(body: "Default Reminder".localized, descriptionTracker: nil, placeHolder: nil, isOnTracker: defaultReminderTracker, type: .toggle),
            SettingData(body: "Custom Reminder".localized, descriptionTracker: nil, placeHolder: nil, isOnTracker: customReminderTracker, type: .toggle)],
            
            Section.second:[SettingData(body: "Default Reminder Time".localized, descriptionTracker: currentDefaultReminderTracker, type: .displayImageWithDescription, additionalDescriptionChange: {(stringValue) in
                let currentReminderTime = ReminderTime(rawValue: stringValue)!
                return currentReminderTime.localized
            })],
            Section.third:[],
            Section.fourth: [SettingData(body: "Add Custom Reminder".localized, type: .action)]
            ]
        
       
        tableViewDataSource = DefaultTableViewDataSource<SettingDefaultTableViewCell>.init(tableView: tableView, parentViewController: self, initialData: data)
    }
    
    fileprivate func setupCustomerReminders() {
        appStatus.notificationManager.reminderObservable.subscribe(onNext: userDidChangeCustomReminders).disposed(by: bag)
        
        let result = appStatus.notificationManager.reminderArray
        tableViewDataSource.addNewCellType(type: CustomReminderCell.self, atSection: Section.third, cellData: result)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}
extension NotificationAndReminderMainViewController: UITableViewDelegate, SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SettingDefaultTableViewCell.hight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        userDidSelectRow(atIndexPath: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section != Section.first
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        return userDidSwipe(tableView, editActionsForRowAt: indexPath, for: orientation)
    }

    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .selection
        options.transitionStyle = .border
        return options
    }
}

extension NotificationAndReminderMainViewController {
    fileprivate func setupUI() {
        tableView = UITableView(frame: .zero, style: .grouped)
        
        view.backgroundColor = tableView.backgroundColor
        
        view.addSubview(tableView)
        tableView.setEdgesToSuperView()
    }
}


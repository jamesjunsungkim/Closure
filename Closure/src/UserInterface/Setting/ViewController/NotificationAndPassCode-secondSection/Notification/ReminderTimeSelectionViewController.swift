//
//  DefaultReminderSettingViewController.swift
//  Closure
//
//  Created by montapinunt Pimonta on 8/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit


final class ReminderTimeSelectionViewController: DefaultViewController {
    //UI
    fileprivate var tableView:UITableView!
    
    func setup(fromVC: UIViewController, userInfo: [String : Any]?) {
        reminderSetting = Tracker<String>.unwrapInstanceFrom(userInfo: userInfo)
        indexForSelectedRow = ReminderTime.init(rawValue: reminderSetting.value)!.index
        
        enterViewControllerMemoryLog(self)
        setupUI()
        setupTableView()
        setupObserver()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    // MARK: Actions
    fileprivate func userDidSelect(atIndexPath indexPath:IndexPath) {
        // send new value
        let target = poolOfReminders[indexPath.row]
        reminderSetting.value = target.rawValue
        
        //update check UI
        hideCheckOnPreviousRowAndShowIt(forIndex: indexPath.row)
        navigationController?.popViewController(animated: true)
    }
    
    fileprivate lazy var observeReminderTimeChange:(String)->Void = {[unowned self](time) in
        guard let r = ReminderTime.init(rawValue: time) else {assertionFailure();return}
        self.hideCheckOnPreviousRowAndShowIt(forIndex: r.index)
    }
    
    // MARK: Fileprivate
    fileprivate weak var reminderSetting: Tracker<String>!
    fileprivate var indexForSelectedRow: Int!
    fileprivate var tableviewDataSource: DefaultTableViewDataSource<SettingDefaultTableViewCell>!
    
    fileprivate let bag = FilterDisposeBag.defaultBag
    
    //it will be deallocated if it's initialized inside a function
    //TODO: Should I look for another way to support value type data in the cell?
    fileprivate let poolOfReminders = ReminderTime.pool
    
    fileprivate lazy var times = self.poolOfReminders.map({Tracker.init(initialValue: $0.displayTime)})
    
    fileprivate func setupTableView() {
        tableView.delegate = self
        
        let descriptions = poolOfReminders.map({$0.localized.capitalized})
        var settingData = (0...4).map({SettingData(body: descriptions[$0], descriptionTracker: times[$0],  type: .check)})
        
        settingData[indexForSelectedRow].isChecked = true
        
        let data:[Int:[SettingData]] = [0:settingData]
        
        tableviewDataSource = DefaultTableViewDataSource<SettingDefaultTableViewCell>.init(tableView: tableView, parentViewController: self, initialData: data)
    }
 
    fileprivate func setupObserver() {
        reminderSetting.valueObservable.subscribe(onNext: observeReminderTimeChange).disposed(by: bag)
    }
    
    fileprivate func hideCheckOnPreviousRowAndShowIt(forIndex index: Int) {
        guard indexForSelectedRow != nil else{assertionFailure();return}
        let cell = tableviewDataSource.cellForRow(at: IndexPath(row: indexForSelectedRow, section: 0))
        cell.didGetDeselected()
        let newCell = tableviewDataSource.cellForRow(at: IndexPath(row: index, section: 0))
        newCell.didGetSelected()
        
        indexForSelectedRow = index
    }

}

extension ReminderTimeSelectionViewController:UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return SettingDescriptionView(description: "You will be notified at some time".localized + "\n" + "between the time slot of your selection.".localized)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        userDidSelect(atIndexPath: indexPath)
    }
}

extension ReminderTimeSelectionViewController{
    fileprivate func setupUI() {
        tableView = UITableView.init(frame: .zero, style: .grouped)
        
        view.addSubview(tableView)
        tableView.setEdgesToSuperView()
    }
}

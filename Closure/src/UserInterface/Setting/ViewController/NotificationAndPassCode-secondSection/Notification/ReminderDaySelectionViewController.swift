//
//  CustomReminderDateSelectionViewController.swift
//  Closure
//
//  Created by montapinunt Pimonta on 8/15/18.
//  Copyright © 2018 James Kim. All rights reserved.
//


import UIKit

final class ReminderDaySelectionViewController: DefaultViewController {
    //UI
    fileprivate var tableView:UITableView!
    
    init(reminder:Reminder) {
        self.reminder = reminder
        super.init(nibName: nil, bundle: nil)
        
        setupUI()
        setupTableViewAndConfigure()
        
    }
    
    // MARK: Actions
    fileprivate func userDidSelectRow(atIndexPath ip: IndexPath) {
        let cell = tableViewDataSource.cellForRow(at: ip)
        pool[ip.row].isChecked ? cell.didGetDeselected() : cell.didGetSelected()
        pool[ip.row].isChecked = !pool[ip.row].isChecked
        
        reminder.dayTracker.value = pool.filter({$0.1}).map({$0.0})
    }
    
    // MARK: Fileprivate
    fileprivate var reminder:Reminder
    
    fileprivate var pool:[(day:Day, isChecked:Bool)] = [(.Monday, false), (.Tuesday,false), (.Wednesday,false), (.Thursday,false), (.Friday,false), (.Saturday,false), (.Sunday,false)]
    
    fileprivate var tableViewDataSource:DefaultTableViewDataSource<SettingDefaultTableViewCell>!
    
    fileprivate func setupTableViewAndConfigure() {
        tableView.delegate = self
        
        // toggle boolean if it is in the current reminder days
        reminder.dayTracker.value.forEach({
            pool[$0.index].1 = true
        })
        
        let initialData = [0:pool.map({
            SettingData(body: $0.day.localized, type: .check, isChecked: $0.isChecked)
        })]

        tableViewDataSource = DefaultTableViewDataSource<SettingDefaultTableViewCell>.init(tableView: tableView, parentViewController: self, initialData: initialData)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}
extension ReminderDaySelectionViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        userDidSelectRow(atIndexPath: indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }
}


extension ReminderDaySelectionViewController {
    fileprivate func setupUI() {
        tableView = UITableView.init(frame: .zero, style: .grouped)
        tableView.allowsMultipleSelection = true
        
        view.addSubview(tableView)
        tableView.setEdgesToSuperView()
    }
}

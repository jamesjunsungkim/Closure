//
//  CreateCustomReminderViewController.swift
//  Closure
//
//  Created by montapinunt Pimonta on 8/15/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
fileprivate struct Row {
    static let text = 0
    static let time = 1
    static let repeatDay = 2
}

final class CreateCustomReminderViewController: DefaultViewController {
    //UI
    fileprivate var tableView: UITableView!
    
    func setup(fromVC: UIViewController, userInfo: [String : Any]?) {
        /**
         ------------------------- LOGIC RUN DOWN ------------------------------
         since I use observer all the time, when they change some details and decide NOT to save them, it's still showing the changes.
         that's why I need a way to return the data when user just returns back.
         
         0. when this vc is loaded, we save all the details to previous datas.
         1. set isSaved false. when view will disppear, if isSaved is false, we return the reminder to the previous data.
         2. if users save it, set isSaved true, it prevents the return using guard statement.
         */
        
        appStatus = AppStatus.unwrapInstanceFrom(userInfo: userInfo)
        reminder = Reminder.unwrapOptionalInstanceFrom(userInfo: userInfo)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        checkIfExistOrSetDefaultReminderAndSetupObservers(for: reminder)
        setupTableView()
        copyAllDataToPreviousVariables()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        resetDataToPreviousSetup()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    //MARK: Action
    
    fileprivate func usetDidSelectRow(atIndexPath indexPath:IndexPath) {
        switch indexPath.row {
        case Row.text: break
        case Row.time:
            //present datePickerViewcontroller
            let targetVC = DatePickerViewController(needsToSelectFullDate: false)
            targetVC.dateObservable.subscribe(onNext: userDidSelectDateFromDatePicker).disposed(by: bag)
            presentPopupWithoutAnyButton(targetVC: targetVC)
            
        case Row.repeatDay:
            let targetVC = ReminderDaySelectionViewController(reminder: reminder)
            presentDefaultVC(targetVC: targetVC, userInfo: nil)
        default:assertionFailure()
        }
    }
    
    @objc fileprivate func userDidTapSaveButton() {
        guard let notificationManager = appStatus.notificationManager else {assertionFailure();return}
        
        needsToCreateNewReminder ? notificationManager.add(reminder: reminder) : notificationManager.edit(reminder: reminder)
        
        isSaved = true
        navigationController?.popViewController(animated: true)
    }
    
    fileprivate lazy var userDidSelectDateFromDatePicker:(Date)->Void = {[unowned self] (date) in
        self.reminder.timeTracker.value = date
    }
    
    fileprivate lazy var observeTimeChange:(Date)->Void = {[unowned self] (date) in
        let dateStringValue = date.toHourToAmPmString(type:.current)
        self.currentTimeTracker.value = dateStringValue
    }
    
    fileprivate lazy var observeDayChange:([Day])->Void = {[unowned self] (array) in
        let repeatStringValue = Reminder.converToDayString(input: array)
        self.currentRepeatTracker.value = repeatStringValue
    }
    
    
    
    //MARK: Fileprivate
    fileprivate weak var appStatus:AppStatus!
    
    fileprivate var reminder: Reminder!
    fileprivate var needsToCreateNewReminder = false
    fileprivate var isSaved = false
    
    fileprivate var previousMessage:String!
    fileprivate var previousTime:Date!
    fileprivate var previousDayArray:[Day]!
    
    fileprivate var tableViewDataSource:DefaultTableViewDataSource<SettingDefaultTableViewCell>!
    
    fileprivate var currentMessageTracker: Tracker<String>!
    fileprivate var currentTimeTracker: Tracker<String>!
    fileprivate var currentRepeatTracker: Tracker<String>!
    
    fileprivate let bag = FilterDisposeBag.defaultBag
    
    fileprivate func setupTableView() {
        tableView.delegate = self
        
        let data : [Int:[SettingData]] =
            [0:[SettingData(body: "Message".localized, descriptionTracker: nil, placeHolder: "Enter your desire alert message".localized, type: .textInput, userTypeInputTracker: currentMessageTracker),
                SettingData(body: "Time".localized,descriptionTracker: currentTimeTracker, type: .displayDescriptionOnly),
                SettingData(body: "Repeat".localized,descriptionTracker: currentRepeatTracker, type: .displayDescriptionOnly)]]

        tableViewDataSource = DefaultTableViewDataSource<SettingDefaultTableViewCell>.init(tableView: tableView, parentViewController: self, initialData: data)
    }
    
    fileprivate func checkIfExistOrSetDefaultReminderAndSetupObservers(for r:Reminder?) {
        // this boolean will be used to determin if it should be created or edited.
        needsToCreateNewReminder = reminder == nil
        
        if let _reminder = r {
            // the reminder is passed from the parent view controller
            currentMessageTracker = _reminder.messageTracker
            currentTimeTracker = Tracker.init(initialValue: _reminder.timeTracker.value.toHourToAmPmString(type:.current))
            currentRepeatTracker = Tracker.init(initialValue: _reminder.toDayString)
        } else {
            // create a new reminder
            reminder = Reminder(message: nil, time: Date.initWith(hour: 10, minute: 0))
            currentMessageTracker = reminder.messageTracker
            currentTimeTracker = Tracker.init(initialValue: reminder.timeTracker.value.toHourToAmPmString(type:.current))
            currentRepeatTracker = Tracker.init(initialValue: "Daily".localized)
        }
        
        // observer
        reminder.timeTracker.valueObservable.subscribe(onNext: observeTimeChange).disposed(by: bag)
        reminder.dayTracker.valueObservable.subscribe(onNext: observeDayChange).disposed(by: bag)
    }
    
    fileprivate func copyAllDataToPreviousVariables() {
        guard !needsToCreateNewReminder else {return}
        previousMessage = reminder.messageTracker.value
        previousTime = reminder.timeTracker.value
        previousDayArray = reminder.dayTracker.value
    }
    
    fileprivate func resetDataToPreviousSetup() {
        guard !needsToCreateNewReminder, !isSaved else {return}
        reminder.messageTracker.value = previousMessage
        reminder.timeTracker.value = previousTime
        reminder.dayTracker.value = previousDayArray
    }
}

extension CreateCustomReminderViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        usetDidSelectRow(atIndexPath: indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension CreateCustomReminderViewController {
    fileprivate func setupUI() {
        tableView = UITableView.init(frame: .zero, style: .grouped)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save".localized, style: .plain, target: self, action: #selector(userDidTapSaveButton))
        view.addSubview(tableView)
        tableView.setEdgesToSuperView()
    }
}

//
//  PassCodeMainViewController.swift
//  Closure
//
//  Created by James Kim on 8/18/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

fileprivate struct Data {
    static let setSecurtyQuestion = IndexPath(row: 1, section: 0)
    static let changePassCode = IndexPath(row: 2, section: 0)
    static let changeRequirementTime = IndexPath(row: 3, section: 0)
    static let removePassCode = IndexPath(row: 0, section: 1)
}

final class PassCodeMainViewController: DefaultViewController {
    //UI
    fileprivate var tableView:UITableView!
    
    init(appStatus:AppStatus) {
        self.passCodeManager = appStatus.passCodeManager
        self.passCodeRequiredTimeTracker = appStatus.passCodeManager.passCodeRequiredTimeTracker
        super.init(nibName: nil, bundle: nil)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        setupObserver()
        setupTableView()
        
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    // MARK: - Public
    
    
    // MARK: - Actions
    fileprivate func userDidSelectRow(atIndexPath ip:IndexPath) {
        switch ip {
        case Data.setSecurtyQuestion:
            let targetVC = PassCodeSecurityQuestionViewController(passCodeManager: passCodeManager)
            presentDefaultVC(targetVC: targetVC, userInfo: nil)
            
        case Data.changePassCode:
            let targetVC = CreateOrCheckPassCodeViewController(passCodeManager: passCodeManager, needsToChangePassCode: true)
            presentDefaultVC(targetVC: targetVC, userInfo: nil, shouldPushOnNavigationController: false)
            
        case Data.changeRequirementTime:
            let targetVC = PassCodeTimeSelectionViewController()
            presentDefaultVC(targetVC: targetVC, userInfo: passCodeRequiredTimeTracker.toUserInfo)
            
        case Data.removePassCode:
            presentDefaultAlert(withTitle: "Alert".localized, message: "Are you sure to remove passcode?".localized, okAction: {[unowned self]in
                self.passCodeManager.removeCurrentPassCodeAndSecurityQuestion()
                self.passCodeManager.isPassCodeOnTracker.value = false
            })
            
        default:break
        }
    }
    
    fileprivate lazy var observePassCodeToggle:(Bool) -> Void = {[unowned self] (value)in
        // if it's true, we show the whole list
        // if it's false, we only show the passCode line.
        let data = self.createTableViewData(isOn: value)
        self.tableViewDataSource.update(data: data)
        
        guard value else {return}
        if self.passCodeManager.currentPassCode == nil {
            let targetVC = CreateOrCheckPassCodeViewController(passCodeManager: self.passCodeManager)
            targetVC.cancelTappedObservable.subscribe(onNext: self.observePrematurePassCodeVCDismiss).disposed(by: self.bag)
            self.presentDefaultVC(targetVC: targetVC, userInfo: nil, shouldPushOnNavigationController: false)
        }
    }
    
    fileprivate lazy var observePrematurePassCodeVCDismiss:()->Void = {[unowned self] in
        self.passCodeManager.isPassCodeOnTracker.value = false
    }
    
    fileprivate lazy var observePasscodeRequiredTimeValueChange: (String)->Void = {[unowned self] (value) in
        let currentTime = PassCodeRequiredTime(rawValue: value)!
        self.passCodeRequiredTimeDescriptionTracker.value = currentTime.description
    }
    
    // MARK: - Fileprivate
    fileprivate weak var passCodeManager: PassCodeManager!
    fileprivate weak var passCodeRequiredTimeTracker: Tracker<String>!
    
    //for displaying the data in cell
    fileprivate var passCodeRequiredTimeDescriptionTracker: Tracker<String>!

    fileprivate var tableViewDataSource:DefaultTableViewDataSource<SettingDefaultTableViewCell>!
    fileprivate let bag = FilterDisposeBag.defaultBag
    
    fileprivate func setupTableView() {
        tableView.delegate = self
        
        let data = createTableViewData(isOn: passCodeManager.isPassCodeOnTracker.value)
        tableViewDataSource = DefaultTableViewDataSource<SettingDefaultTableViewCell>.init(tableView: tableView, parentViewController: self, initialData: data)
    }
    
    fileprivate func createTableViewData(isOn:Bool) -> [Int:[SettingData]] {
        let offData = [0:[SettingData(body: "PassCode".localized, isOnTracker: passCodeManager.isPassCodeOnTracker, type: .toggle)]]
        
        let onData = [0:[
            SettingData(body: "PassCode".localized, isOnTracker: passCodeManager.isPassCodeOnTracker, type: .toggle),
            SettingData(body: "Security Questions".localized, type: .displayImageOnly),
            SettingData(body: "Change PassCode".localized, type: .displayImageOnly),
            SettingData(body: "Require After..".localized, descriptionTracker: passCodeRequiredTimeDescriptionTracker, type: .displayImageWithDescription),
            SettingData(body: "Touch ID/Face ID".localized, isOnTracker: passCodeManager.isTouchIDOnTracker, type: .toggle)],
                      1:[SettingData(body: "Remove Current PassCode".localized, type: .action)]]
        
        return isOn ? onData : offData
    }

    
    fileprivate func setupObserver() {
        let ct = PassCodeRequiredTime(rawValue: passCodeManager.passCodeRequiredTimeTracker.value)!
        passCodeRequiredTimeDescriptionTracker = Tracker.init(initialValue: ct.description)
        
        passCodeManager.isPassCodeOnTracker.valueObservable.subscribe(onNext: observePassCodeToggle).disposed(by: bag)
        //since the passcode required time's raw value doesn't fit the description.. so i needed to create a new one.
        passCodeManager.passCodeRequiredTimeTracker.valueObservable.subscribe(onNext: observePasscodeRequiredTimeValueChange).disposed(by: bag)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension PassCodeMainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        userDidSelectRow(atIndexPath: indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension PassCodeMainViewController {
    fileprivate func setupUI() {
        view.backgroundColor = .white
        
        tableView = UITableView(frame: .zero, style: .grouped)
        view.addSubview(tableView)
        tableView.setEdgesToSuperView()
    }
}


//
//  PassCodeSecurityQuestionViewController.swift
//  Closure
//
//  Created by James Kim on 9/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//
import UIKit
import RxSwift
import DropDown
import CodiumLayout

final class PassCodeSecurityQuestionViewController: DefaultViewController {
    
    //UI
    fileprivate var questionTextField:UITextField!
    fileprivate var answerTextField:UITextField!
    fileprivate var submitButton:UIButton!
    
    init(passCodeManager:PassCodeManager, needsToCheck:Bool = false) {
        self.passCodeManager = passCodeManager
        self.needsToCheck = needsToCheck
        super.init(nibName: nil, bundle: nil)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        addTarget()
        setupDropDown()
        configure()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    // MARK: Public
    public var successObservable:Observable<Void> {
        return successSubject.asObservable()
    }
    
    // MARK: - Actions
    fileprivate lazy var userDidChooseDropDown: (Int,String) ->Void = {[unowned self] (index, input) in
        logInfo(type: .action)
        self.isQuestionCreatingMode = index == self.preparedQuestions.count-1
        
        guard index != self.preparedQuestions.count-1 else {
            // user clicks on creating their one question
            self.dropDown.deselectRow(at: index)
            self.questionTextField.text = ""
            self.questionTextField.becomeFirstResponder()
            return
        }
        
        self.questionTextField.text = input
        self.answerTextField.text = ""
    }
    
    @objc fileprivate func userDidTapOnBackgroundView() {
        guard currentTextField != nil, !needsToCheck else {return}
        currentTextField.resignFirstResponder()
        view.endEditing(true)
        checkIfUserTapSomewhereWithQuestionTextFieldBeingEmpty()
    }
    
    @objc fileprivate func userDidTapSaveButton() {
        logInfo(type: .action)
        let question = questionTextField.text!
        let answer = answerTextField.text!
        guard !question.isEmpty && answer.count > 5 else {
            presentDefaultError(message: "Question must not be empty and answer must be longer than 5 letters.".localized, okAction: nil)
            return
        }
        passCodeManager.saveSequrityQuestionAndAnswer(question: question, answer: answer)
        
        presentDefaultAlertWithoutCancel(withTitle: "Succeed".localized, message: "Now, when you forget passcode, you can unlock it with those.".localized, okAction: {[unowned self] in
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    @objc fileprivate func userDidTapSubmitButton() {
        let answer = answerTextField.text!
        guard !answer.isEmpty && answer.count > 5 else {
            presentDefaultError(message: "Question must not be empty and answer must be longer than 5 letters.".localized, okAction: nil)
            return
        }
        
        guard let currentAnswerFromData = passCodeManager.currentSecurityQuestionAnswer else {assertionFailure();return}
        
        guard answer == currentAnswerFromData else {
            presentDefaultError(message: "Wrong answer".localized, okAction: nil)
            return
        }
        
        successSubject.onNext(())
        successSubject.onCompleted()
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Fileprivate
    fileprivate weak var passCodeManager:PassCodeManager!
    fileprivate weak var currentTextField:UITextField!
    
    fileprivate let successSubject = PublishSubject<Void>()
    
    fileprivate let needsToCheck: Bool
    fileprivate var isQuestionCreatingMode = false
    fileprivate let dropDown = DropDown()
    
    fileprivate let preparedQuestions:[String] = [
        "What is the name of your favorite pet?".localized,
        "In what city were you born?".localized,
        "What high school did you attend?".localized,
        "What is the name of your first school?".localized,
        "What is your favorite movie?".localized,
        "What street did you grow up on?".localized,
        "What was the make of your first car?".localized,
        "Create your own..".localized
    ]
    
    fileprivate func addTarget() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(userDidTapOnBackgroundView))
        view.addGestureRecognizer(tap)
    }
    
    fileprivate func setupDropDown() {
        dropDown.backgroundColor = .white
        dropDown.separatorColor = .init(white: 0.3, alpha: 0.3)
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y: 25+4)
        dropDown.anchorView = questionTextField
        dropDown.selectionAction = userDidChooseDropDown
        dropDown.dataSource = preparedQuestions
    }
    
    fileprivate func checkIfUserTapSomewhereWithQuestionTextFieldBeingEmpty() {
        guard isQuestionCreatingMode, questionTextField.text!.isEmpty else {return}
        isQuestionCreatingMode = false
    }
    
    fileprivate func configure() {
        guard needsToCheck else {return}
        
        // there is a chance users haven't set security questions.
        guard let question = passCodeManager.currentSecurityQuestion else {return}
        
        questionTextField.text = question
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension PassCodeSecurityQuestionViewController:UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        /**
         ------------------------- LOGIC RUN DOWN ------------------------------
         when it's checking mode, questino text field must not show the drop down.
         
         when it's creating mode, it doesn't show anything until user clicks on the last option to create their own question.
         */
        let userDidTapQuestionTextField = textField == questionTextField
        currentTextField = textField
        
        guard userDidTapQuestionTextField else {
            // when user clicks on the answer password with question being empty, we reset it so user can choose question again.
            checkIfUserTapSomewhereWithQuestionTextFieldBeingEmpty()
            return true
        }
        guard !needsToCheck else {return false}
        
        guard isQuestionCreatingMode else {
            dropDown.show()
            return false
        }
        
        return true
    }
}

extension PassCodeSecurityQuestionViewController {
    fileprivate func setupUI() {
        view.backgroundColor = .white
        
        navigationItem.title = "Security Questions".localized
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save".localized, style: .plain, target: self, action: #selector(userDidTapSaveButton))
        
        let passcodeExistingDescriptionLabel = UILabel.create(text: "Seems like you already have security answer and passcode saved to the phone.".localized + "\n" + "If you create a new one, it will overwrite them.".localized, textAlignment: .center, fontSize: 15)
        passcodeExistingDescriptionLabel.isHidden = (passCodeManager.currentSecurityQuestionAnswer == nil || needsToCheck)
        print("SECURITY ANSWER: ", passCodeManager.currentSecurityQuestionAnswer )
        let questionTitleLabel = UILabel.create(text: "Question".localized, textAlignment: .left, fontSize: 18)
        
        questionTextField = UITextField.create(placeHolder: "Click here".localized, textSize: 15, textColor: .black, keyboardType: .default, clearMode: .whileEditing, textAlignment: .left)
        questionTextField.delegate = self
        
        let questionSeparatorLine = UIView.create()
        
        let answerTitleLabel = UILabel.create(text: "Answer".localized, textAlignment: .left, fontSize: 18)
        
        answerTextField = UITextField.create(placeHolder: "Enter here".localized, textSize: 15, textColor: .black, keyboardType: .default, clearMode: .whileEditing, textAlignment: .left)
        answerTextField.delegate = self
        
        let answerSeparatorLine = UIView.create()
        
        submitButton = UIButton.create(title: "Submit".localized, titleColor: .white, fontSize: 18, backgroundColor: .mainBlue)
        submitButton.addTarget(self, action: #selector(userDidTapSubmitButton), for: .touchUpInside)
        submitButton.setCornerRadious(value: 5)
        submitButton.isHidden = !needsToCheck
        
        let group:[UIView] = [passcodeExistingDescriptionLabel,questionTitleLabel, questionTextField, questionSeparatorLine, answerTitleLabel, answerTextField, answerSeparatorLine,submitButton]
        group.forEach(view.addSubview(_:))
        
        let isPopupVC = self.needsToCheck
        constraint(passcodeExistingDescriptionLabel,questionTitleLabel, questionTextField, questionSeparatorLine, answerTitleLabel, answerTextField, answerSeparatorLine,submitButton) { ( _passcodeExistingDescriptionLabel,_questionTitleLabel, _questionTextField, _questionSeparatorLine, _answerTitleLabel, _answerTextField, _answerSeparatorLine, _submitButton) in
            
            _passcodeExistingDescriptionLabel.top.equalToSuperview().offset(70)
            _passcodeExistingDescriptionLabel.leftRightEqualToSuperView(withOffset: 15)
            
            _questionTitleLabel.top.equalToSuperview().offset(isPopupVC ? 60:160)
            _questionTitleLabel.left.equalToSuperview().offset(15)
            
            _questionTextField.top.equalTo(questionTitleLabel.snp.bottom).offset(10)
            _questionTextField.leftRightEqualToSuperView(withOffset: 15)
            _questionTextField.height.equalTo(25)
            
            _questionSeparatorLine.top.equalTo(questionTextField.snp.bottom).offset(0.5)
            _questionSeparatorLine.left.right.equalTo(questionTextField)
            _questionSeparatorLine.height.equalTo(0.5)
            
            _answerTitleLabel.top.equalTo(questionSeparatorLine.snp.bottom).offset(40)
            _answerTitleLabel.left.equalToSuperview().offset(15)
            
            _answerTextField.top.equalTo(answerTitleLabel.snp.bottom).offset(10)
            _answerTextField.leftRightEqualToSuperView(withOffset: 15)
            _answerTextField.height.equalTo(25)
            
            _answerSeparatorLine.top.equalTo(answerTextField.snp.bottom).offset(0.5)
            _answerSeparatorLine.left.right.equalTo(questionTextField)
            _answerSeparatorLine.height.equalTo(0.5)
            
            _submitButton.top.equalTo(answerSeparatorLine.snp.bottom).offset(30)
            _submitButton.centerX.equalToSuperview()
            _submitButton.sizeEqualTo(width: 180, height: 45)
            if isPopupVC  {
                _submitButton.bottom.equalToSuperview().offset(-50)
            }
        }
        
        
        
        
        
        
    }
}


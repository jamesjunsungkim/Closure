//
//  CreatePassCodeViewController.swift
//  Closure
//
//  Created by James Kim on 8/18/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout
import RxSwift

fileprivate indirect enum PassCodeStage {
    case checkExistingPassCode
    case createNewOne
    case needsToConfirmPasscode
    case failedToMatch(PassCodeStage)
    case needsToChange
    
    public var description: String {
        switch self {
        case .createNewOne: return "Create new passcode".localized
        case .needsToConfirmPasscode: return "Re-enter new passcode".localized
        case .failedToMatch: return "Passcode doesn't match. Please enter again.".localized
        default: return ""
        }
    }
}

final class CreateOrCheckPassCodeViewController: DefaultViewController {
    
    //UI
    fileprivate var titleLabel: UILabel!
    fileprivate var passCodeStackView: UIStackView!
    fileprivate var firstToggleView: CircleToggleView!
    fileprivate var secondToggleView: CircleToggleView!
    fileprivate var thirdToggleView: CircleToggleView!
    fileprivate var fourthToggleView: CircleToggleView!
    
    fileprivate var firstNumberButton: UIButton!
    fileprivate var secondNumberButton: UIButton!
    fileprivate var thirdNumberButton: UIButton!
    fileprivate var fourthNumberButton: UIButton!
    fileprivate var fifthNumberButton: UIButton!
    fileprivate var sixthNumberButton: UIButton!
    fileprivate var seventhNumberButton: UIButton!
    fileprivate var eighthNumberButton: UIButton!
    fileprivate var ninthNumberButton: UIButton!
    fileprivate var touchIDButton: UIButton!
    fileprivate var eleventhNumberButton: UIButton!
    fileprivate var deleteNumberButton: UIButton!
    
    fileprivate var forgotButton:UIButton!
    fileprivate var cancelButton:UIButton!
    
    init(passCodeManager:PassCodeManager, needsToChangePassCode:Bool = false, shouldHideCancelButton:Bool = false) {
        self.passCodeManager = passCodeManager
        self.needsToChangePassCode = needsToChangePassCode
        self.needsToHideCancelButton = shouldHideCancelButton
        super.init(nibName: nil, bundle: nil)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        addTarget()
        checkIfPassCodeAndAdjustUI()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    // MARK: - Public
    
    public var cancelTappedObservable:Observable<Void> {
        return cancelTappedSubject.asObservable()
    }
    
    public var successObservable:Observable<Void>{
        return successSubject.asObservable()
    }
    
    // MARK: - Actions
    @objc fileprivate func userDidTapButton(sender:UIButton) {
        guard passCodeTextField.text!.count < 4 else {return}
        let targetNumber = sender.tag
        passCodeTextField.text!.append("\(targetNumber)")
        togglePassCodeCircleView()
        takeActionAccordingly(for: passCodeStage)
    }
    
    @objc fileprivate func userDidDeleteLastNumber() {
        guard passCodeTextField.text!.count >= 1 else {return}
        passCodeTextField.text!.removeLast()
        togglePassCodeCircleView()
    }
    
    @objc fileprivate func userDidTapTouchIDButton() {
        guard passCodeManager.currentPassCode != nil else {
            presentDefaultAlertWithoutCancel(withTitle: "Error".localized, message: "You need to create a passcode first.".localized, okAction: nil)
            return
        }
        
        guard !needsToChangePassCode else {
            presentDefaultAlertWithoutCancel(withTitle: "Error".localized, message: "You need to change a passcode first.".localized, okAction: nil)
            return
        }
        
        guard passCodeManager.isBioAuthenticationAvailable else {
            self.presentDefaultAlertWithoutCancel(withTitle: "Error".localized, message: "Seems it's not available on your device".localized, okAction: nil)
            return
        }
        passCodeManager.startAuthenticationProcess {[unowned self] (result) in
            switch result{
            case .notAvailable: assertionFailure()
            case .failure(let error):
                self.presentDefaultAlertWithoutCancel(withTitle: "Error".localized, message: error.localizedDescription, okAction: nil)
            case .success:
                self.sendSuccessSubjectAndDismiss()
            }
        }
    }
    
    @objc fileprivate func userDidTapCancelButton() {
        cancelTappedSubject.onNext(())
        cancelTappedSubject.onCompleted()
        dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func userDidTapForgotButton() {
        /**
         there is a chance users haven't set seurity question when they click on it.
         */
        
        // only when we check passcode
        guard !needsToChangePassCode, needsToHideCancelButton else {return}
        
        // check if there are security questions
       
        guard passCodeManager.currentSecurityQuestionAnswer != nil, passCodeManager.currentSecurityQuestion != nil else {
            presentDefaultAlertWithoutCancel(withTitle: "Alert".localized, message: "Seems like you haven't set up security questions.".localized)
            return
        }
        
        let targetVC = PassCodeSecurityQuestionViewController(passCodeManager: passCodeManager, needsToCheck: true)
        targetVC.successObservable.subscribe(onNext: {[unowned self] in
            waitFor(milliseconds: 300, completion: {
                self.sendSuccessSubjectAndDismiss()
            })
        }).disposed(by: bag)

        presentPopupWithoutAnyButton(targetVC: targetVC)
    }
    
    fileprivate func togglePassCodeCircleView() {
        //logic: turn everything off and turn on the targets only.
        let group = [firstToggleView, secondToggleView, thirdToggleView, fourthToggleView]
        group.forEach({$0!.isChecked = false})
        let range = (0..<passCodeTextField.text!.count)
        let target = Array(group[range])
        target.forEach({$0!.isChecked = true})
    }
    
    // MARK: - Fileprivate
    fileprivate weak var passCodeManager: PassCodeManager!
    fileprivate let needsToChangePassCode: Bool
    fileprivate let needsToHideCancelButton: Bool
    fileprivate var passCodeStage = PassCodeStage.checkExistingPassCode
    fileprivate var currentPassCode: String!
    
    fileprivate let bag = DisposeBag()
    
    
    fileprivate let cancelTappedSubject = PublishSubject<Void>()
    fileprivate let successSubject = PublishSubject<Void>()
    fileprivate var passCodeTextField = UITextField()
    fileprivate let passCodeCircleSize = CGSize(width: 20, height: 20)
    fileprivate let numberPadSize = CGSize(width: 80, height: 80)
    
    fileprivate func addTarget() {
        let group = [firstNumberButton, secondNumberButton, thirdNumberButton, fourthNumberButton, fifthNumberButton, sixthNumberButton, seventhNumberButton, eighthNumberButton, ninthNumberButton, eleventhNumberButton]
        group.forEach({$0!.addTarget(self, action: #selector(userDidTapButton(sender:)), for: .touchUpInside)})
        
        deleteNumberButton.addTarget(self, action: #selector(userDidDeleteLastNumber), for: .touchUpInside)
        
        touchIDButton.addTarget(self, action: #selector(userDidTapTouchIDButton), for: .touchUpInside)
        
        cancelButton.addTarget(self, action: #selector(userDidTapCancelButton), for: .touchUpInside)
        
        forgotButton.addTarget(self, action: #selector(userDidTapForgotButton), for: .touchUpInside)
    }
    
    fileprivate func checkIfPassCodeAndAdjustUI() {
        guard !needsToChangePassCode else {
            guard let savedPasscode = passCodeManager.retrieveIfExist(for:PassCodeManager.Keys.passCode) else {assertionFailure();return}
            currentPassCode = savedPasscode
            passCodeStage = .needsToChange
            return
        }
        
        if let savedPasscode = passCodeManager.retrieveIfExist(for:PassCodeManager.Keys.passCode) {
            // password exists and check mode
            currentPassCode = savedPasscode
        } else {
            // create a new password mode.
            passCodeStage = .createNewOne
            titleLabel.text = passCodeStage.description
        }
    }
    
    fileprivate func takeActionAccordingly(for stage:PassCodeStage) {
        let passcodeInput = passCodeTextField.text!
        guard passCodeTextField.text!.count == 4 else {return}
        switch stage {
        case .checkExistingPassCode:
            guard passcodeInput == currentPassCode else {
                passCodeStackView.shakeHorizontally()
                passCodeStage = .failedToMatch(.checkExistingPassCode)
                takeActionAccordingly(for: passCodeStage)
                return
            }
            sendSuccessSubjectAndDismiss()
            
        case .failedToMatch(let target):
            titleLabel.text = "Wrong passCode. Enter it again.".localized
            emptyPassCodeInput()
            passCodeStage = target
            
        case .createNewOne:
            currentPassCode = passcodeInput
            titleLabel.text = "Re-enter new passcode".localized
            emptyPassCodeInput()
            passCodeStage = .needsToConfirmPasscode
            
        case .needsToConfirmPasscode:
            guard passcodeInput == currentPassCode else {
                passCodeStackView.shakeHorizontally()
                passCodeStage = .createNewOne
                titleLabel.text = "Create new passcode".localized
                emptyPassCodeInput()
                currentPassCode = nil
                return
            }
            //save it to keychain
            passCodeManager.saveAndRemoveOldPassCodeAndSecurityQuestion(newPassCode: passcodeInput)
            sendSuccessSubjectAndDismiss()
            
        case .needsToChange:
            guard passcodeInput == currentPassCode else {
                passCodeStackView.shakeHorizontally()
                passCodeStage = .failedToMatch(.needsToChange)
                takeActionAccordingly(for: passCodeStage)
                return
            }
            passCodeStage = .createNewOne
            titleLabel.text = "Create new passcode".localized
            emptyPassCodeInput()
            currentPassCode = nil
        }
    }
    
    fileprivate func emptyPassCodeInput() {
        for _ in (0...3) {
            userDidDeleteLastNumber()
        }
    }
    
    fileprivate func sendSuccessSubjectAndDismiss() {
        successSubject.onNext(())
        successSubject.onCompleted()
        dismiss(animated: true, completion: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension CreateOrCheckPassCodeViewController {
    fileprivate func setupUI() {
        view.backgroundColor = UIColor.passCodeBackground
        
        titleLabel = UILabel.create(text: "Enter Passcode".localized, textAlignment: .center, textColor: .white, fontSize: 24, boldFont: true)
        
        firstToggleView = CircleToggleView(borderColor: .gray, uncheckedBackgroundColor: .passCodeBackground, checkedBackgroundColor: .gray)
        secondToggleView = CircleToggleView(borderColor: .gray, uncheckedBackgroundColor: .passCodeBackground, checkedBackgroundColor: .gray)
        thirdToggleView = CircleToggleView(borderColor: .gray, uncheckedBackgroundColor: .passCodeBackground, checkedBackgroundColor: .gray)
        fourthToggleView = CircleToggleView(borderColor: .gray, uncheckedBackgroundColor: .passCodeBackground, checkedBackgroundColor: .gray)
        
        firstNumberButton = UIButton.create(title: "1", titleColor: .white, fontSize: 35, backgroundColor: .passCodeBackground)
        
        secondNumberButton = UIButton.create(title: "2", titleColor: .white, fontSize: 35, backgroundColor: .passCodeBackground)
        
        thirdNumberButton = UIButton.create(title: "3", titleColor: .white, fontSize: 35, backgroundColor: .passCodeBackground)
        
        fourthNumberButton = UIButton.create(title: "4", titleColor: .white, fontSize: 35, backgroundColor: .passCodeBackground)
        
        fifthNumberButton = UIButton.create(title: "5", titleColor: .white, fontSize: 35, backgroundColor: .passCodeBackground)
        
        sixthNumberButton = UIButton.create(title: "6", titleColor: .white, fontSize: 35, backgroundColor: .passCodeBackground)
        
        seventhNumberButton = UIButton.create(title: "7", titleColor: .white, fontSize: 35, backgroundColor: .passCodeBackground)
        
        eighthNumberButton = UIButton.create(title: "8", titleColor: .white, fontSize: 35, backgroundColor: .passCodeBackground)
        
        ninthNumberButton = UIButton.create(title: "9", titleColor: .white, fontSize: 35, backgroundColor: .passCodeBackground)
        
        touchIDButton = UIButton.create(withImageName: "fingerprint")
        
        eleventhNumberButton = UIButton.create(title: "0", titleColor: .white, fontSize: 35, backgroundColor: .passCodeBackground)
        eleventhNumberButton.tag = 0
        
        deleteNumberButton = UIButton.create(withImageName: "back_button")
        
        forgotButton = UIButton.create(title: "I forgot it".localized, titleColor: .white, fontSize: 17, backgroundColor: .passCodeBackground)
        
        cancelButton = UIButton.create(title: "Cancel".localized, titleColor: .white, fontSize: 17, backgroundColor: .passCodeBackground)
        cancelButton.isHidden = needsToHideCancelButton
        
        let groupForCircleImageView = [firstToggleView, secondToggleView, thirdToggleView, fourthToggleView]
        groupForCircleImageView.forEach({$0!.setCornerRadious(value: 10)})
        
        let groupForNumberPad = [firstNumberButton, secondNumberButton, thirdNumberButton, fourthNumberButton, fifthNumberButton, sixthNumberButton, seventhNumberButton, eighthNumberButton, ninthNumberButton, eleventhNumberButton]
        
        var countForNumberPad = 1
        groupForNumberPad.forEach({
            $0!.setCornerRadious(value: 40)
            $0!.setBorder(color: .white, width: 2)
            guard countForNumberPad < 10 else {return}
            $0!.tag = countForNumberPad
            countForNumberPad += 1
        })
        
        
        passCodeStackView = UIStackView.create(views: [firstToggleView, secondToggleView, thirdToggleView, fourthToggleView], axis: .horizontal, alignment: .fill, distribution: .equalSpacing, spacing: 15)
        
        constraint(firstToggleView, secondToggleView, thirdToggleView, fourthToggleView) { (_firstToggleImageView, _secondToggleImageView, _thirdToggleImageView, _fourthToggleImageView) in
            _firstToggleImageView.size.equalTo(passCodeCircleSize)
            _secondToggleImageView.size.equalTo(passCodeCircleSize)
            _thirdToggleImageView.size.equalTo(passCodeCircleSize)
            _fourthToggleImageView.size.equalTo(passCodeCircleSize)
        }
        
        let firstNumberRow = UIStackView.create(views: [firstNumberButton, secondNumberButton, thirdNumberButton], axis: .horizontal, alignment: .fill, distribution: .equalSpacing, spacing: 50)
        
        constraint(firstNumberButton, secondNumberButton, thirdNumberButton) { (_firstNumberButton, _secondNumberButton, _thirdNumberButton) in
            _firstNumberButton.size.equalTo(numberPadSize)
            _firstNumberButton.size.equalTo(numberPadSize)
            _secondNumberButton.size.equalTo(numberPadSize)
            _thirdNumberButton.size.equalTo(numberPadSize)
        }
        
        let secondNumberRow = UIStackView.create(views: [fourthNumberButton, fifthNumberButton, sixthNumberButton], axis: .horizontal, alignment: .fill, distribution: .equalSpacing, spacing: 50)
        
        constraint(fourthNumberButton, fifthNumberButton, sixthNumberButton) { (_fourthNumberButton, _fifthNumberButton, _sixthNumberButton) in
            _fourthNumberButton.size.equalTo(numberPadSize)
            _fifthNumberButton.size.equalTo(numberPadSize)
            _sixthNumberButton.size.equalTo(numberPadSize)
        }
        
        let thirdNumberRow = UIStackView.create(views: [seventhNumberButton, eighthNumberButton, ninthNumberButton], axis: .horizontal, alignment: .fill, distribution: .equalSpacing, spacing: 50)
        
        constraint(seventhNumberButton, eighthNumberButton, ninthNumberButton) { (_seventhNumberButton, _eighthNumberButton, _ninthNumberButton) in
            _seventhNumberButton.size.equalTo(numberPadSize)
            _eighthNumberButton.size.equalTo(numberPadSize)
            _ninthNumberButton.size.equalTo(numberPadSize)
        }
        
        let fourthNumberRow = UIStackView.create(views: [touchIDButton, eleventhNumberButton, deleteNumberButton], axis: .horizontal, alignment: .fill, distribution: .equalSpacing, spacing: 50)
        
        constraint(touchIDButton, eleventhNumberButton, deleteNumberButton) { (_tenthNumberButton, _eleventhNumberButton, _twelvethNumberButton) in
            _tenthNumberButton.size.equalTo(numberPadSize)
            _eleventhNumberButton.size.equalTo(numberPadSize)
            _twelvethNumberButton.size.equalTo(numberPadSize)
        }
        
        let group: [UIView] = [titleLabel, passCodeStackView, firstNumberRow, secondNumberRow, thirdNumberRow, fourthNumberRow, forgotButton, cancelButton]
        group.forEach(view.addSubview(_:))
        
        constraint(titleLabel, passCodeStackView, firstNumberRow, secondNumberRow, thirdNumberRow, fourthNumberRow, forgotButton, cancelButton) { (_titleLabel, _passCodeStackView, _firstNumberRow, _secondNumberRow, _thirdNumberRow, _fourthNumberRow, _forgotButton, _cancelButton) in
            let totalContentHeight = 580.toCGFloat
            let topPadding = (UIScreen.main.bounds.height - totalContentHeight)*(2/3)
            
            _titleLabel.top.equalToSuperview().offset(topPadding)
            _titleLabel.centerX.equalToSuperview()
            
            _passCodeStackView.top.equalTo(titleLabel.snp.bottom).offset(20)
            _passCodeStackView.centerX.equalToSuperview()
            
            _firstNumberRow.top.equalTo(passCodeStackView.snp.bottom).offset(75)
            _firstNumberRow.centerX.equalToSuperview()
            
            _secondNumberRow.top.equalTo(firstNumberRow.snp.bottom).offset(20)
            _secondNumberRow.centerX.equalToSuperview()
            
            _thirdNumberRow.top.equalTo(secondNumberRow.snp.bottom).offset(20)
            _thirdNumberRow.centerX.equalToSuperview()
            
            _fourthNumberRow.top.equalTo(thirdNumberRow.snp.bottom).offset(20)
            _fourthNumberRow.centerX.equalToSuperview()
            
            _forgotButton.top.equalTo(fourthNumberRow.snp.bottom).offset(20)
            _forgotButton.centerX.equalTo(touchIDButton)
            _forgotButton.sizeEqualTo(width: 100, height: 30)
            
            _cancelButton.top.equalTo(fourthNumberRow.snp.bottom).offset(20)
            _cancelButton.centerX.equalTo(deleteNumberButton)
            _cancelButton.sizeEqualTo(width: 70, height: 30)
        }
    }
}


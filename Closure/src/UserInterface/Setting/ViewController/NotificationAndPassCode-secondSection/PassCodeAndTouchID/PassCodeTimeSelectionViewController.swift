//
//  PassCodeTimeSelectionViewController.swift
//  Closure
//
//  Created by James Kim on 8/19/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

final class PassCodeTimeSelectionViewController: DefaultViewController {
    
    //UI
    fileprivate var tableView:UITableView!
    
    func setup(fromVC: UIViewController, userInfo: [String : Any]?) {
        currentRequiredTimeTracker = Tracker<String>.unwrapInstanceFrom(userInfo: userInfo)
        currentRequiredTime = PassCodeRequiredTime(rawValue: currentRequiredTimeTracker.value)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        setupTableView()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    

    // MARK: - Actions
    fileprivate func userDidSelectRow(atIndexPath ip: IndexPath) {
        currentRequiredTime = pool[ip.row]
        currentRequiredTimeTracker.value = currentRequiredTime.rawValue
        
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Fileprivate
    fileprivate weak var currentRequiredTimeTracker: Tracker<String>!
    fileprivate var currentRequiredTime: PassCodeRequiredTime!
    
    fileprivate var tableViewDataSource: DefaultTableViewDataSource<SettingDefaultTableViewCell>!
    fileprivate let pool: [PassCodeRequiredTime] = [.immediately, .after1Min, .after3Min, .after5Min, .after10Min]
    
    fileprivate func setupTableView() {
        tableView.delegate = self
        let poolOfDescription = pool.map({$0.description})
        
        var settingData = (0...4).map({SettingData(body: poolOfDescription[$0], descriptionTracker:Tracker.init(initialValue: ""),  type: .check)})
        settingData[currentRequiredTime.index].isChecked = true
        
        let data: [Int:[SettingData]] = [0:settingData]
        
        tableViewDataSource = DefaultTableViewDataSource<SettingDefaultTableViewCell>.init(tableView: tableView, parentViewController: self, initialData: data)
    }
}
extension PassCodeTimeSelectionViewController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        userDidSelectRow(atIndexPath: indexPath)
    }
}

extension PassCodeTimeSelectionViewController {
    fileprivate func setupUI() {
        tableView = UITableView.init(frame: .zero, style: .grouped)
        view.addSubview(tableView)
        tableView.setEdgesToSuperView()
    }
}


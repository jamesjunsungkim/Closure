//
//  SettingMainViewController.swift
//  Closure
//
//  Created by James Kim on 7/14/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

fileprivate struct Section {
    static let first = 0
    static let second = 1
    static let third = 2
    static let fourth = 3
    static let fifth = 4
}

fileprivate struct Row {
    static let first = 0
    static let second = 1
    static let third = 2
}

final class SettingMainViewController: DefaultViewController {
    
    //UI
    fileprivate var tableView:UITableView!
    
    init(status:AppStatus) {
        self.appStatus = status
        super.init(nibName: nil, bundle: nil)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        setupTableView()
        setupObservers()
        
//        waitFor(milliseconds: 100, completion: {
//            self.userDidSelectRow(atIndexPath: IndexPath(row: Row.second, section: Section.second))
//        })
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    // MARK: - Actions
    
    fileprivate func userDidSelectRow(atIndexPath indexPath: IndexPath) {
        switch indexPath.toTuplie {
        case (Section.first, Row.first):
            //Account
            let targetVC = AccountSettingMainViewController(appStatus: appStatus)
            targetVC.navigationItem.title = "Account".localized
            presentDefaultVC(targetVC: targetVC, userInfo: nil)
            
        case (Section.second, Row.first):
            let targetVC = NotificationAndReminderMainViewController(appStatus: appStatus)
            targetVC.navigationItem.title = "Notification".localized
            presentDefaultVC(targetVC: targetVC, userInfo: nil)
            
        case (Section.second, Row.second):
            let targetVC = PassCodeMainViewController(appStatus: appStatus)
            targetVC.navigationItem.title = "PassCode".localized
            presentDefaultVC(targetVC: targetVC, userInfo: nil)
            
        case (Section.third, Row.first):
            let targetVC = LanguageMainViewController()
            targetVC.navigationItem.title = "Language".localized
            presentDefaultVC(targetVC: targetVC,
                             userInfo: appStatus.languageManager.languageTracker.toUserInfo)
            
        case (Section.third, Row.second):
            let targetVC = WordAnalysisFilterMainViewController(wordCountManager: appStatus.wordCountManager)
            targetVC.navigationItem.title = "Word Filter".localized
            presentDefaultVC(targetVC: targetVC, userInfo: nil)
            
        case (Section.fourth, Row.first):
            
            let targetVC = SettingDataMainViewController(appStatus: appStatus)
            targetVC.navigationItem.title = "Data".localized
            presentDefaultVC(targetVC: targetVC, userInfo: nil)
            
        case (Section.fourth, Row.second):
            let targetVC = SettingSyncMainViewController(appStatus: appStatus)
            targetVC.navigationItem.title = "Sync".localized
            presentDefaultVC(targetVC: targetVC, userInfo: nil)
            
        case (Section.fifth, Row.first):
            let targetVC = CreateEmailViewController(emailManager: appStatus.emailManager, isBugReport: true)
            presentNavigationController(targetVC: targetVC, userInfo: nil)
        case (Section.fifth, Row.second):
            let targetVC = CreateEmailViewController(emailManager: appStatus.emailManager, isBugReport: false)
            presentNavigationController(targetVC: targetVC, userInfo: nil)
        case (Section.fifth, Row.third):
            let targetVC = ReleaseNoteViewController(networkManager: appStatus.networkManager)
            presentDefaultVC(targetVC: targetVC, userInfo: nil)
        default: break
        }
    }
    
    fileprivate lazy var observeLanguageChange:(Language)->Void = {[unowned self] (_) in
        guard self.isViewLoaded else {return}
        self.navigationItem.title = "Settings".localized
        
        // let's reload the tableview so that its language changes to target one.
        let data:[Int:[SettingData]] =
            [Section.first:[
                SettingData(body: "Account".localized, type: .displayImageOnly)],
             Section.second : [
                SettingData(body: "Notification and Reminder".localized, type: .displayImageOnly),
                SettingData(body: "PassCode And Face ID".localized, type: .displayImageOnly)],
             Section.third: [
                SettingData(body: "Language", descriptionTracker: self.appStatus.languageManager.languageTracker, placeHolder: nil, isOnTracker: nil, type: .displayImageWithDescription),
                SettingData(body: "Word analysis filter".localized, type: .displayImageOnly)],
             Section.fourth:[SettingData(body: "Data".localized, type: .displayImageOnly), SettingData(body: "Sync Setting".localized, type: .displayImageOnly)],
             Section.fifth:[SettingData(body: "Bug report".localized, type: .displayImageOnly), SettingData(body: "Suggestion".localized, type: .displayImageOnly),
                            SettingData(body: "Release Note".localized, type: .displayImageOnly)]]
        self.tableViewDataSource.update(data: data)
    }
    
    
    // MARK: - Fileprivate
    fileprivate weak var appStatus:AppStatus!
    fileprivate let bag = FilterDisposeBag.defaultBag
    fileprivate var tableViewDataSource: DefaultTableViewDataSource<SettingDefaultTableViewCell>!
    
    // DataCell Rows
    
    
    fileprivate func setupTableView() {
        tableView.delegate = self
        tableViewDataSource = DefaultTableViewDataSource<SettingDefaultTableViewCell>.init(tableView: tableView, parentViewController: self)
        
        // setup settingData for tableview.
        /**
         let's not show the account status until I create a proper subscription service.
         */
        
//        SettingData(body: "Account Status".localized, descriptionTracker: appStatus.accountStatusTracker, placeHolder: nil, type: .displayImageWithDescription)
        
        let data:[Int:[SettingData]] =
            [Section.first:[
                SettingData(body: "Account".localized, type: .displayImageOnly)],
             Section.second : [
                SettingData(body: "Notification and Reminder".localized, type: .displayImageOnly),
                SettingData(body: "PassCode And Face ID".localized, type: .displayImageOnly)],
             Section.third: [
                SettingData(body: "Language", descriptionTracker: appStatus.languageManager.languageTracker, placeHolder: nil, isOnTracker: nil, type: .displayImageWithDescription),
                SettingData(body: "Word analysis filter".localized, type: .displayImageOnly)],
             Section.fourth:[SettingData(body: "Data".localized, type: .displayImageOnly), SettingData(body: "Sync Setting".localized, type: .displayImageOnly)],
             Section.fifth:[SettingData(body: "Bug report".localized, type: .displayImageOnly), SettingData(body: "Suggestion".localized, type: .displayImageOnly),
                SettingData(body: "Release Note".localized, type: .displayImageOnly)]]
        tableViewDataSource.update(data: data)
    }

    fileprivate func setupObservers() {
        appStatus.languageManager.changeLanguageObservable.subscribe(onNext: observeLanguageChange).disposed(by: bag)
    }

   
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension SettingMainViewController:UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SettingDefaultTableViewCell.hight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard section == Section.fifth else {return 0}
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard section == Section.fifth else {return nil}
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        userDidSelectRow(atIndexPath: indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
}

extension SettingMainViewController {
    fileprivate func setupUI() {
        navigationItem.title = "Settings".localized
        tableView = UITableView(frame: .zero, style: .grouped)
        
        view.addSubview(tableView)
        tableView.setEdgesToSuperView()
    }
}

fileprivate extension IndexPath {
    var toTuplie: (section:Int,row:Int) {
        return (self.section, self.row)
    }
}


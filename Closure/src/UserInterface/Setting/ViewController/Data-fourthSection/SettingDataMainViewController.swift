//
//  SettingDataMainViewController.swift
//  Closure
//
//  Created by James Kim on 8/24/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CoreData

final class SettingDataMainViewController: DefaultViewController {
    
    //UI
    fileprivate var tableView:UITableView!
    
    init(appStatus:AppStatus) {
        self.appStatus = appStatus
        super.init(nibName: nil, bundle: nil)
        
        setupUI()
        setupTableView()
        loadAndUpdateUI()
        setupObserver()
    }

    
    // MARK: - Actions
    fileprivate func userDidSelectRow(atIndexPath ip:IndexPath) {
        /**
         we only care about the clear button.
         
         let's check if there is any data. how? we don't need to fetch the number but just check the totalNumberOfClosureTracker value.
         */

        guard totalNumberOfClosureTracker.value != "0" else {
            presentDefaultAlertWithoutCancel(withTitle: "Alert".localized, message: "There is no data to clear".localized)
            return
        }
        
        presentDefaultAlert(withTitle: "Alert".localized, message: "This action can not undo it.".localized + "\n" + "Are you sure to remove all data?".localized, okAction: {[unowned self] in
            // remove all core data.
            Review.deleteAll(fromMOC: self.appStatus.mainContext)
            self.appStatus.sendAllCoreDataRemovedNotification()
            self.loadAndUpdateUI()
        })
    }
    
    fileprivate lazy var obvserContextDidSaveNotification:(ContextDidSaveNotification)->Void = {[weak self] (note) in
        guard let target = self else {return}
        if !note.insertedObjects.isEmpty || !note.deletedObjects.isEmpty || !note.updatedObjects.isEmpty {
            target.loadAndUpdateUI()
        }
    }
    
    fileprivate lazy var observeCoreDataAllDeletion: ()->Void = {[unowned self] in
        self.loadAndUpdateUI()
    }
    
    // MARK: - Fileprivate
    fileprivate weak var appStatus: AppStatus!
    fileprivate var contextDidSaveNotificationToken: NSObjectProtocol!
    fileprivate let bag = FilterDisposeBag.defaultBag
    
    fileprivate var tableViewDataSource: DefaultTableViewDataSource<SettingDefaultTableViewCell>!
    
    fileprivate var totalNumberOfClosureTracker = Tracker<String>.init(initialValue: "HI")
    fileprivate var unsentClsoureTracker = Tracker<String>.init(initialValue: "HI")
    fileprivate var imageTracker = Tracker<String>.init(initialValue: "HI")
    
    fileprivate func setupTableView() {
        tableView.delegate = self
        
        let data = [0:
            [SettingData(body: "Total of Closures".localized, descriptionTracker: totalNumberOfClosureTracker, type: .displayDescriptionOnly),
             SettingData(body: "Unsent Closures".localized, descriptionTracker: unsentClsoureTracker, type: .displayDescriptionOnly),
             SettingData(body: "Total of Images".localized, descriptionTracker: imageTracker, type: .displayDescriptionOnly)],         1: [SettingData(body: "Clear all data".localized, type: .action)]]
        
        tableViewDataSource = DefaultTableViewDataSource<SettingDefaultTableViewCell>.init(tableView: tableView, parentViewController: self, initialData: data)
    }
    
    fileprivate func loadAndUpdateUI() {
        // count for all
        let moc = appStatus.mainContext
        let countForAll = Review.getMonthlyCount(from: moc, date: nil, rate: nil)
        totalNumberOfClosureTracker.value = "\(countForAll)"
        
        // count for unsent closures
        let predicateForUnsent = NSPredicate(format: "%K == %@", #keyPath(Review.isUploaded), NSNumber(booleanLiteral: false))
        
        let request = Review.sortedFetchRequest
        request.returnsObjectsAsFaults = true
        request.predicate = predicateForUnsent
        
        let countForUnsentClosure = (try? moc.count(for: request)).unwrapOr(defaultValue: 0)
        unsentClsoureTracker.value = "\(countForUnsentClosure)"
        
        //image
        let predicateForimage = NSPredicate(format: "%K != nil", #keyPath(Review.imageData))
        
        let requestForImage = Review.sortedFetchRequest
        requestForImage.returnsObjectsAsFaults = true
        requestForImage.predicate = predicateForimage
        
        let countForImage = (try? moc.count(for: requestForImage)).unwrapOr(defaultValue: 0)
        imageTracker.value = "\(countForImage)"
    }
    
    fileprivate func setupObserver() {
        contextDidSaveNotificationToken = appStatus.mainContext.addContextDidSaveNotificationObserver(obvserContextDidSaveNotification)
        
        appStatus.allCoreDataRemovedObservable.subscribe(onNext: observeCoreDataAllDeletion).disposed(by: bag)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension SettingDataMainViewController:UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section == 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        userDidSelectRow(atIndexPath: indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension SettingDataMainViewController {
    fileprivate func setupUI() {
        view.backgroundColor = .white
        
        tableView = UITableView(frame: .zero, style: .grouped)
        view.addSubview(tableView)
        tableView.setEdgesToSuperView()
    }
}


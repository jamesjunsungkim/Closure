//
//  SyncSettingMainViewController.swift
//  Closure
//
//  Created by James Kim on 8/25/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import PKHUD

fileprivate struct Row {
    static let optimized = 0
    static let original = 1
}

final class SettingSyncMainViewController: DefaultViewController {
    
    //UI
    fileprivate var tableView:UITableView!
    
    init(appStatus:AppStatus){
        self.appStatus = appStatus
        self.sizeOptimizationTracker = appStatus.imageSizeOptimizationTracker
        super.init(nibName: nil, bundle: nil)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        setupTableView()
        setupObserver()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }

    
    // MARK: - Actions
    fileprivate func userDidSelectRow(atIndexPath ip:IndexPath) {
        guard ip.row != currentIndexForOptimization else {return}
        // toggle it off first
        let previousIndexPath = IndexPath(row: currentIndexForOptimization, section: 1)
        tableViewDataSource.cellForRow(at: previousIndexPath).didGetDeselected()
        
        sizeOptimizationTracker.value = pool[ip.row].rawValue
        
        tableViewDataSource.cellForRow(at: ip).didGetSelected()
        currentIndexForOptimization = ip.row
    }
    
    fileprivate lazy var observeSyncOnToggle:(Bool) -> Void = {[unowned self] (isOn) in
        guard isOn else {return}
        /**
         TODO: check how many reviews are unsent to backend and we ask users if they want to save them to server
         */
        let unsentList = Review.FetchUnsentReviews(from: self.appStatus.mainContext)
        guard unsentList.count != 0 else {return}
        
        var currentIndex = 0
        var failureCount = 0
        
        let completion :(CompletionResult<Review, Error?>) -> Void = { (result) in
            currentIndex += 1
            HUD.show(.labeledProgress(title: "uploading:".localized + " \(currentIndex)/\(unsentList.count)", subtitle: nil))
            switch result {
            case .success(_): break
            case .failure(_):
                failureCount += 1
            }
            if currentIndex + failureCount == unsentList.count {
                HUD.hide()
                guard failureCount == 0 else {
                    self.presentDefaultAlertWithoutCancel(withTitle: "Error".localized, message: "\(failureCount)" + "reviews failed to upload. please try it again.".localized)
                    return
                }
            }
        }
        
        self.presentDefaultAlert(withTitle: "Alert".localized, message: "\(unsentList.count) " + "unsent reviews found. Would you like to upload them to server?".localized, okAction: {
            HUD.show(.labeledProgress(title: nil, subtitle: nil))
            unsentList.forEach({$0.uploadReviewToServer(appStatus: self.appStatus, path: Path.toReview(withUID: self.appStatus.currentUser.uid, date: $0.dateString).stringValue, completion: completion)})
        }, cancelAction: nil )
    }
    
    // MARK: - Fileprivate
    fileprivate weak var appStatus: AppStatus!
    fileprivate var tableViewDataSource: DefaultTableViewDataSource<SettingDefaultTableViewCell>!
    
    fileprivate let bag = FilterDisposeBag.defaultBag
    fileprivate let pool: [ImageSizeOptimization] = [.optimizedSize, .originalSize]
    fileprivate weak var sizeOptimizationTracker: Tracker<String>!
    fileprivate var currentIndexForOptimization: Int!
    
    fileprivate func setupTableView(){
        tableView.delegate = self
        
        currentIndexForOptimization = ImageSizeOptimization(rawValue: sizeOptimizationTracker.value)!.index
        
        let convertedData = pool.map({SettingData(body: $0.description, type: .check, isChecked: sizeOptimizationTracker.value == $0.rawValue)})
        
        let data: [Int:[SettingData]] =
            [0:[SettingData(body: "Sync".localized, isOnTracker: appStatus.syncOnTracker, type: .toggle),
                SettingData(body: "Sync Over Celluar".localized, isOnTracker: appStatus.syncOnCelluarTracker, type: .toggle)],
             1:convertedData]
        
        tableViewDataSource = DefaultTableViewDataSource<SettingDefaultTableViewCell>.init(tableView: tableView, parentViewController: self, initialData: data)
    }
 
    
    fileprivate func setupObserver() {
        appStatus.syncOnTracker.valueObservable.subscribe(onNext: observeSyncOnToggle).disposed(by: bag)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
}
extension SettingSyncMainViewController:UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        userDidSelectRow(atIndexPath: indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section == 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let title = section == 0 ? "Data".localized : "Image size".localized
        let descriptionView = SettingDescriptionView(description: title, textAlignment: .left)
        return descriptionView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let freeSpace = appStatus.diskStatusManager.freeDiskSpace
        let totalSpace = appStatus.diskStatusManager.totalDiskSpace
        let description = "Available space on device".localized + ": \(freeSpace) /\(totalSpace)\n" + "Your option will only affect images saved to device.".localized
        let descriptionView = SettingDescriptionView(description: description, textAlignment: .center)
        
        return section == 1 ? descriptionView : nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 1 ? 70 : 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}

extension SettingSyncMainViewController {
    fileprivate func setupUI() {
        view.backgroundColor = .white
        
        tableView = UITableView(frame: .zero, style: .grouped)
        view.addSubview(tableView)
        tableView.setEdgesToSuperView()
    }
}


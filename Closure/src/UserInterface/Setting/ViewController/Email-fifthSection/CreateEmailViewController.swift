//
//  CreateEmailViewController.swift
//  Closure
//
//  Created by James Kim on 8/25/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout
import PKHUD
import IQKeyboardManagerSwift

final class CreateEmailViewController: DefaultViewController {
    
    //UI
    fileprivate var fromEmailTextField:UITextField!
    fileprivate var subjectTextField:UITextField!
    fileprivate var bodyTextView:UITextView!
    fileprivate var sendButton: UIBarButtonItem!
    
    init(emailManager:EmailManager, isBugReport:Bool) {
        self.emailManager = emailManager
        self.isBugReport = isBugReport
        super.init(nibName: nil, bundle: nil)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        setVC()
        setupEmail()
        enableOrDisableTextViewKeyboard(needsToSet: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        enableOrDisableTextViewKeyboard(needsToSet: false)
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    
    // MARK: - Public
    
    
    // MARK: - Actions
    @objc fileprivate func userDidTapSendButton() {
        guard validateEmailField() else {
            presentDefaultError(message: "Email is invalid. Please type it again".localized, okAction: nil)
            return
        }
        
        HUD.show(.labeledProgress(title: nil, subtitle: nil))

        let defaultSubject = isBugReport ? "Bug report".localized : "Suggestion".localized
        
        emailManager.sendEmail(
            isBugReport: isBugReport,
            fromEmail: fromEmailTextField.text!.checkIfEmptyOr(defaultValue: "anonymousUser@gmail.com"),
            subject: subjectTextField.text!.checkIfEmptyOr(defaultValue: defaultSubject),
            body: bodyTextView.text,
            result: {[weak self](result) in
                HUD.hide()
                guard let target = self else {return}
                switch result {
                case .success:
                    target.presentDefaultAlertWithoutCancel(withTitle: "Email successfully sent!".localized, message: "Thank you for your time:)".localized, okAction: {
                        target.dismiss(animated: true, completion: nil)
                    })
                case .failure(let er):
                    switch er {
                    case .overLimit:
                        target.presentDefaultError(message: "We limit emails to once a day.".localized + "\n" + "If you have something to say, please do it tomorrow!".localized, okAction: {
                            target.dismiss(animated: true, completion: nil)
                        })
                    case .alamofireError(_):
                        target.presentDefaultError(message: "Something went wrong..".localized, okAction: nil)
                    }
                }
        })
    }
    
    @objc fileprivate func userDidTapCancelButton() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Fileprivate
    fileprivate let emailManager: EmailManager
    fileprivate let isBugReport:Bool
    
    fileprivate func setVC() {
        navigationItem.title = isBugReport ? "Bug Report".localized : "Suggestion".localized
        
        sendButton = UIBarButtonItem(title: "Send".localized, style: .plain, target: self, action: #selector(userDidTapSendButton))
        navigationItem.rightBarButtonItem = sendButton
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel".localized, style: .plain, target: self, action: #selector(userDidTapCancelButton))
    }
    
    fileprivate func setupEmail() {
        switch isBugReport{
            /**
             app build is going to be the same as app version. so I don't see the point in including the app build in the email.
             */
        case true:
            let deviceModel = UIDevice.modelName
            let systemVersion = UIDevice.current.systemVersion
            let appversion = UIApplication.appVersion
//            let appBuild = UIApplication.appBuild
            
            let deviceDescription =
            """
            Device information
              ________________________________________
             Device model: \(deviceModel)
             System version: iOS \(systemVersion)
             App version: \(appversion)
              ________________________________________
            
            
            """
            bodyTextView.text = deviceDescription
//                "\nWe're sincerely sorry that you experienced inconvenience with our app. \nPlease write your experience in details so that we can take a proper action as soon as possible.".localized + "\n\n"
        case false:
            bodyTextView.text = "Please share your idea to make the app better!".localized + "\n\n"
        }
    }
    
    fileprivate func enableOrDisableTextViewKeyboard(needsToSet:Bool) {
        IQKeyboardManager.shared.enable = needsToSet
        IQKeyboardManager.shared.enableAutoToolbar = needsToSet
        IQKeyboardManager.shared.shouldPlayInputClicks = !needsToSet
        IQKeyboardManager.shared.shouldResignOnTouchOutside = needsToSet
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
    }
    
    fileprivate func validateEmailField() ->Bool {
        guard !fromEmailTextField.text!.isEmpty else {return true}
        return fromEmailTextField.text!.validateForEmail()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension CreateEmailViewController {
    fileprivate func setupUI() {
        view.backgroundColor = .white

        let fromEmailDescriptionLabel = UILabel.create(text: "From".localized+":", textAlignment: .left, fontSize: 15)
        fromEmailTextField = UITextField.create(placeHolder: "Enter your email(Optional)".localized, textSize: 15, textColor: .black, keyboardType: .default, clearMode: .whileEditing)
        fromEmailTextField.borderStyle = .none
        fromEmailTextField.textAlignment = .left
        
        let fromSeparatorView = UIView.create()
        
//        let emailAddress = isBugReport ? "ClosureBug@gmail.com" : "ClosureSuggestion@gmail.com"
        let emailAddress = "applejameskim@gmail.com"
        let toEmailDescriptionLabel = UILabel.create(text: "To".localized + ":", textAlignment: .left, fontSize: 15)
        let toEmailLabel = UILabel.create(text: emailAddress, textAlignment: .left, fontSize: 15)
        
        let toSeparatorView = UIView.create()
        
        let subjectEmailDescriptionLabel = UILabel.create(text: "Subject".localized+":", textAlignment: .left, fontSize: 15)
        
        subjectTextField = UITextField.create(placeHolder: "Subject(Optional)".localized, textSize: 15, textColor: .black, keyboardType: .default, clearMode: .whileEditing)
        subjectTextField.borderStyle = .none
        subjectTextField.textAlignment = .left
        
        let subjectSeparatorView = UIView.create()
        
        bodyTextView = UITextView()
        bodyTextView.font = UIFont.mainFont(size: 15)
        
        let group:[UIView] = [fromEmailDescriptionLabel, fromEmailTextField, fromSeparatorView, toEmailDescriptionLabel,toEmailLabel, toSeparatorView, subjectEmailDescriptionLabel, subjectTextField, subjectSeparatorView, bodyTextView]
        group.forEach(view.addSubview(_:))
        
        let componentDistance = 15
        
        constraint(fromEmailDescriptionLabel, fromEmailTextField, fromSeparatorView, toEmailDescriptionLabel,toEmailLabel, toSeparatorView, subjectEmailDescriptionLabel, subjectTextField, subjectSeparatorView, bodyTextView) { (_fromEmailDescriptionLabel, _fromEmailTextField, _fromSeparatorView, _toEmailDescriptionLabel, _toEmailLabel, _toSeparatorView, _subjectEmailDescriptionLabel, _subjectTextField, _subjectSeparatorView, _bodyTextView) in
            
            _fromEmailDescriptionLabel.top.equalTo(view.safeAreaLayoutGuide).offset(20)
            _fromEmailDescriptionLabel.left.equalToSuperview().offset(15)
            _fromEmailDescriptionLabel.width.equalTo(60)
            
            _fromEmailTextField.top.bottom.equalTo(fromEmailDescriptionLabel)
            _fromEmailTextField.left.equalTo(fromEmailDescriptionLabel.snp.right).offset(5)
            _fromEmailTextField.right.equalToSuperview().offset(-15)
            
            _fromSeparatorView.top.equalTo(fromEmailDescriptionLabel.snp.bottom).offset(3)
            _fromSeparatorView.left.equalTo(fromEmailDescriptionLabel)
            _fromSeparatorView.right.equalTo(fromEmailTextField)
            _fromSeparatorView.height.equalTo(0.5)
            
            _toEmailDescriptionLabel.top.equalTo(fromSeparatorView.snp.bottom)
                .offset(componentDistance)
            _toEmailDescriptionLabel.left.equalTo(fromSeparatorView)
            _toEmailDescriptionLabel.width.equalTo(60)
            
            _toEmailLabel.left.equalTo(toEmailDescriptionLabel.snp.right).offset(5)
            _toEmailLabel.centerY.equalTo(toEmailDescriptionLabel)
            
            _toSeparatorView.top.equalTo(toEmailDescriptionLabel.snp.bottom).offset(3)
            _toSeparatorView.left.right.equalTo(fromSeparatorView)
            _toSeparatorView.height.equalTo(fromSeparatorView)
            
            _subjectEmailDescriptionLabel.top.equalTo(toSeparatorView.snp.bottom)
                .offset(componentDistance)
            _subjectEmailDescriptionLabel.left.equalTo(toEmailDescriptionLabel)
            _subjectEmailDescriptionLabel.width.equalTo(60)
            
            _subjectTextField.top.bottom.equalTo(subjectEmailDescriptionLabel)
            _subjectTextField.left.equalTo(subjectEmailDescriptionLabel.snp.right).offset(5)
            _subjectTextField.right.equalToSuperview().offset(-15)
            
            _subjectSeparatorView.top.equalTo(subjectEmailDescriptionLabel.snp.bottom).offset(3)
            _subjectSeparatorView.left.equalTo(fromEmailDescriptionLabel)
            _subjectSeparatorView.right.equalTo(fromEmailTextField)
            _subjectSeparatorView.height.equalTo(0.5)
            
            _bodyTextView.top.equalTo(subjectSeparatorView.snp.bottom).offset(15)
            _bodyTextView.left.right.equalTo(fromSeparatorView)
            _bodyTextView.bottom.equalToSuperview().offset(-10)
        }
        
        
        
        
        
    }
}


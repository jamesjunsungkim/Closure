//
//  ReleaseNoteViewController.swift
//  Closure
//
//  Created by James Kim on 9/16/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout
import PKHUD

final class ReleaseNoteViewController: DefaultViewController {
    
    //UI
    fileprivate var textView:UITextView!
    
    init(networkManager:NetworkManager) {
        self.networkManager = networkManager
        super.init(nibName: nil, bundle: nil)
        enterViewControllerMemoryLog(self)
        setupUI()
        loadAndUpdateUI()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    // MARK: Fileprivate
    fileprivate weak var networkManager: NetworkManager!
    fileprivate let appVersion = UIApplication.appVersion
    
    fileprivate func loadAndUpdateUI() {
        /**
          we fetch data with current version. BUT firebase doesn't allow . in path so we need to remove all comma in the version only to make it int value.
         */
        let appVersionWithoutComma = UIApplication.appVersion.removeCharacters(in: ".")
        HUD.show(.labeledProgress(title: nil, subtitle: nil))
        
        networkManager.fetch(fromPath: Path.toReleaseNote(withVersion: appVersionWithoutComma).stringValue) {[unowned self] (result) in
            HUD.hide()
            switch result {
            case .success(let r):
                guard let note = r as? String else {assertionFailure();return}
                self.textView.text = note
            case .failure(let e):
                switch e{
                case .invalidData:
                    break
                case .firebaseError(_):
                    self.presentDefaultError()
                }
                self.textView.text = "Release note unavailable".localized
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

extension ReleaseNoteViewController {
    fileprivate func setupUI() {
        view.backgroundColor = .white
        
        
        let titleLabel = UILabel.create(text: "Release Note".localized, textAlignment: .left, fontSize: 20)
        titleLabel.font = UIFont.boldSystemFont(ofSize: 25)
        
        let appversionLabel = UILabel.create(text: "App Version: \(appVersion)", textAlignment: .left, fontSize: 16)
        
        textView = UITextView.create(text: "", mainFontSize: 16)
        
        [titleLabel,appversionLabel, textView].forEach({view.addSubview($0)})
        
        constraint(titleLabel,appversionLabel, textView) { (_titleLabel, _appversionLabel, _textView) in
            
            _titleLabel.top.equalTo(view.safeAreaLayoutGuide).offset(30)
            _titleLabel.left.equalToSuperview().offset(15)
            
            _appversionLabel.top.equalTo(titleLabel.snp.bottom).offset(17)
            _appversionLabel.left.equalToSuperview().offset(17)
            
            _textView.top.equalTo(appversionLabel.snp.bottom).offset(15)
            _textView.leftRightEqualToSuperView(withOffset: 15)
            _textView.height.equalTo(400)
        }
        
    }
}










//
//  WordAnalysisFilterMainViewController.swift
//  Closure
//
//  Created by montapinunt Pimonta on 8/19/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

public enum WordFilterType {
    case filterOut
    case filterIn
    
    public var description:String {
        switch self {
        case .filterIn: return "Filter In"
        case .filterOut: return "Filter Out"
        }
    }
}

final class WordAnalysisFilterMainViewController: DefaultViewController {
    
    // UI
    fileprivate var tableView: UITableView!
    
    init(wordCountManager:WordCountManager) {
        self.wordCountManager = wordCountManager
        super.init(nibName: nil, bundle: nil)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        setupTableView()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
   
    //MARK: Actions
    fileprivate func userDidSelectRow(atIndexPath ip: IndexPath) {
        let targetType = ip.row == 0 ? WordFilterType.filterOut : WordFilterType.filterIn
        let targetVC = SelectedFilterListViewController(wordCountManager: wordCountManager, filterType: targetType )
        targetVC.navigationItem.title = targetType.description.localized
        presentDefaultVC(targetVC: targetVC, userInfo: nil)
    }
    
    //MARK:- Fileprivate
    
    fileprivate var tableViewDataSource:DefaultTableViewDataSource<SettingDefaultTableViewCell>!
    fileprivate weak var wordCountManager: WordCountManager!
    
    fileprivate func setupTableView() {
        tableView.delegate = self
        
        let data: [Int:[SettingData]] = [0:[SettingData(body: "Filter-out list".localized, type: .displayImageOnly), SettingData(body: "Filter-in list".localized, type: .displayImageOnly)]]
        
        tableViewDataSource = DefaultTableViewDataSource<SettingDefaultTableViewCell>.init(tableView: tableView, parentViewController: self, initialData: data)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension WordAnalysisFilterMainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        userDidSelectRow(atIndexPath: indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return SettingDescriptionView(description: "Filter type".localized, textAlignment: .left)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}

extension WordAnalysisFilterMainViewController {
    fileprivate func setupUI() {
        tableView = UITableView(frame: .zero, style: .grouped)
        
        view.addSubview(tableView)
        tableView.setEdgesToSuperView()
        
    }
}

//
//  WordAnalysisFilterMainViewController.swift
//  Closure
//
//  Created by montapinunt Pimonta on 8/19/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

final class SelectedFilterListViewController: DefaultViewController {
    
    // UI
    fileprivate var tableView: UITableView!
    
    init(wordCountManager:WordCountManager, filterType: WordFilterType) {
        self.wordCountManager = wordCountManager
        self.filterType = filterType
        super.init(nibName: nil, bundle: nil)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        setupTableView()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    

    //MARK: Actions
    fileprivate func userDidSelectRow(atIndexPath ip: IndexPath) {
        guard let targetType = WordSegment(rawValue: ip.row) else {assertionFailure();return}
        let targetVC = SelectedFilterWordsViewController(wordCountManager: wordCountManager, filterType: filterType, tag: targetType.tag)
        targetVC.navigationItem.title = targetType.description
        presentDefaultVC(targetVC: targetVC, userInfo: nil)
    }
    
    //MARK:- Fileprivate
    
    fileprivate var tableViewDataSource:DefaultTableViewDataSource<SettingDefaultTableViewCell>!
    fileprivate let pool: [WordSegment] = [.verb, .noun, .place, .adjective, .adverb]
    
    fileprivate var filterType: WordFilterType
    
    fileprivate weak var wordCountManager: WordCountManager!
    
    fileprivate func setupTableView() {
        tableView.delegate = self
        
        let data: [Int:[SettingData]] = [0:pool.map({SettingData(body: $0.description, type: .displayImageOnly)})]
        
        tableViewDataSource = DefaultTableViewDataSource<SettingDefaultTableViewCell>.init(tableView: tableView, parentViewController: self, initialData: data)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

extension SelectedFilterListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        userDidSelectRow(atIndexPath: indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return SettingDescriptionView(description: "Word type".localized, textAlignment: .left)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}

extension SelectedFilterListViewController {
    fileprivate func setupUI() {
        tableView = UITableView(frame: .zero, style: .grouped)
        
        view.addSubview(tableView)
        tableView.setEdgesToSuperView()
        
    }
}

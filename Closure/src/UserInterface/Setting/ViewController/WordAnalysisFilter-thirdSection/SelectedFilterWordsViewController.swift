//
//  WordAnalysisFilterMainViewController.swift
//  Closure
//
//  Created by montapinunt Pimonta on 8/19/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout
import SwipeCellKit

final class SelectedFilterWordsViewController: DefaultViewController {
    
    // UI
    fileprivate var tableView: UITableView!
    fileprivate var emptyDescriptionView: UIView!
    
    init(wordCountManager:WordCountManager, filterType: WordFilterType, tag:NSLinguisticTag) {
        self.wordCountManager = wordCountManager
        self.type = filterType
        self.tag = tag
        super.init(nibName: nil, bundle: nil)
        
        enterViewControllerMemoryLog(self)
        setupUI()
        setupTableView()
        setupObserver()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }

    
    //MARK: Actions
    
    fileprivate func userDidSwipe(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else {return nil}
        
        let deleteAction = SwipeAction(style: .default, title: "Remove".localized, handler: {[unowned self] (_, indexPath) in
            self.presentDefaultAlert(withTitle: "Alert".localized, message: "Are you sure to remove the selected word?".localized, okAction: {[unowned self] in
                
                let word = self.tableViewDataSource.object(atIndexPath: indexPath).body
                
                self.type == .filterIn ? self.wordCountManager.removeWordFromAddFirstDictionary(forTag: self.tag, word: word) : self.wordCountManager.removeLimitationWord(forTag: self.tag, word: word)
            })
        })
        deleteAction.backgroundColor = .red
        deleteAction.textColor = .white
        
        return [deleteAction]
    }
    
    fileprivate lazy var observeTargetWordArrayChanges: ([String])->Void = {[unowned self] (array) in
        self.tableViewDataSource.update(data:
            [0:self.convertStringArrayToSettingData(input: array)])
        self.hideOrShowEmptyDescriptionView(needsToShow: array.count == 0)
        self.currentData = array
    }
    
    @objc fileprivate func userDidTapPlusButton() {
        let detailDescription = type == .filterIn ? "This word will be filtered in the selected category.".localized : "This word will be filtered out from the selected category.".localized
        
        let targetVC = CreateTextEntryViewController(title: "Add your word".localized, detail:detailDescription , placeHolder: "Enter your word".localized , textLimit: 45)
        
        let failurePopup: (WordAdditionFailure, NSLinguisticTag)->Void = {[unowned self](failure, tag) in
            let reason:String
            switch failure {
            case .counterDictionaryContainWord:
                reason = "This word exists in the counter dictionary!".localized
            case .duplicateInDictionary:
                reason = "This word already exists".localized
            }
            waitFor(milliseconds: 200, completion: {
                self.presentDefaultError(message: reason, okAction: nil)
            })
        }
        
        let completionAction = {[unowned self] in
            let word = targetVC.inputText
            
            self.type == .filterIn ? self.wordCountManager.addWordToAddFirstDictionary(forTag: self.tag, word: word, failure: failurePopup) : self.wordCountManager.addLimitationWord(forTag: self.tag, word: word, failure: failurePopup)
            
        }
        
        // observe user tap return.
        _ = targetVC.userDidTapEnterObservable.subscribe(onNext: {
            targetVC.dismiss(animated: true, completion: {completionAction()})
        })
        
        presentPopup(targetVC: targetVC,
                     cancelButtonTitle: "Cancel".localized,
                     cancelAction: nil,
                     okButtonTitle: "Add".localized,
                     okAction: completionAction,
                     panGestureDismissal: false)
        
    }
    
    //MARK:- Fileprivate
    
    fileprivate weak var wordCountManager: WordCountManager!
    fileprivate var tableViewDataSource:DefaultTableViewDataSource<SettingDefaultTableViewCell>!
    fileprivate let tag :NSLinguisticTag
    fileprivate let type:WordFilterType
    
    fileprivate var currentData: [String]!
    
    fileprivate let bag = FilterDisposeBag.defaultBag
    fileprivate let fakeDescriptionTracker = Tracker<String>.init(initialValue: "")
    
    fileprivate func setupTableView() {
        tableView.delegate = self
        
        let data:[String]
        
        if type == .filterIn {
           data = wordCountManager.retrieveAddFirstDictionaryWords(forTag: tag)
        } else {
            data = wordCountManager.retrieveLimitationWords(forTag: tag)
        }
        
        // comparision purpose. whenever we get a new data, we compare and only if it's different, we update it to tableview.
        currentData = data
        
        let settingArray: [Int:[SettingData]] = [0: data.map({SettingData(body: $0, descriptionTracker: fakeDescriptionTracker, type: .displayDescriptionOnly)})]
        
        hideOrShowEmptyDescriptionView(needsToShow: currentData.count == 0)
        
        tableViewDataSource = DefaultTableViewDataSource<SettingDefaultTableViewCell>.init(tableView: tableView, parentViewController: self, initialData: settingArray)
    }
    
    fileprivate func setupObserver() {
        if type == .filterIn {
            wordCountManager.addItFirstWordsObservable
                .filter({ [unowned self] in
                    guard let targetArray = $0[self.tag] else {return false}
                    return targetArray.count != self.currentData.count
                })
                .map({[unowned self] in
                    let result = $0[self.tag]!
                    return result.sorted(by: {$0<$1})
                })
                .subscribe(onNext: observeTargetWordArrayChanges).disposed(by: bag)
        } else {
            wordCountManager.limitationWordsObservable
                .filter({ [unowned self] in
                    guard let targetArray = $0[self.tag] else {return false}
                    return targetArray.count != self.currentData.count
                })
                .map({[unowned self] in
                    let result = $0[self.tag]!
                    return result.sorted(by: {$0<$1})
                })
                .subscribe(onNext: observeTargetWordArrayChanges).disposed(by: bag)
        }
    }

    
    fileprivate func convertStringArrayToSettingData(input:[String])->[SettingData] {
        
        return input.map({SettingData(body: $0.capitalized, descriptionTracker: fakeDescriptionTracker, type: .displayDescriptionOnly)})
    }
    
    fileprivate func hideOrShowEmptyDescriptionView(needsToShow:Bool) {
        emptyDescriptionView.isHidden = !needsToShow
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SelectedFilterWordsViewController: UITableViewDelegate, SwipeTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        return userDidSwipe(tableView, editActionsForRowAt: indexPath, for: orientation)
    }
    
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .selection
        options.transitionStyle = .border
        return options
    }

}

extension SelectedFilterWordsViewController {
    fileprivate func setupUI() {
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "circle_plus")?.resizeImage(width: 23, height: 23), style: .plain, target: self, action: #selector(userDidTapPlusButton))
        
        tableView = UITableView(frame: .zero, style: .grouped)
        tableView.allowsSelection = false
        
        emptyDescriptionView = SettingDescriptionView(description: "Looks like it's empty!".localized + "\n\n" + "why don't you add a new word?".localized, textAlignment: .center)
        
        let group:[UIView] = [tableView, emptyDescriptionView]
        group.forEach(view.addSubview(_:))
        
        constraint(tableView, emptyDescriptionView) { (_tableView, _emptyDescriptionView) in
            _tableView.edges.equalToSuperview()
            
            _emptyDescriptionView.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            _emptyDescriptionView.left.right.bottom.equalToSuperview()
        }
        
    }
}









//
//  LanguageMainViewController.swift
//  Closure
//
//  Created by montapinunt Pimonta on 8/19/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit

fileprivate struct SelectedObject {
    static let english = IndexPath(row: 0, section: 0)
    static let korean = IndexPath(row: 1, section: 0)
    static let thai = IndexPath(row: 2, section: 0)
}

final class LanguageMainViewController: DefaultViewController{
    
    // UI
    fileprivate var tableView: UITableView!
    
    func setup(fromVC: UIViewController, userInfo: [String : Any]?) {
        languageTracker = Tracker<String>.unwrapInstanceFrom(userInfo: userInfo)
        
        setupUI()
        setupTableView()
    }

    
    //MARK: Actions
    fileprivate func userDidSelectRow(atIndexPath ip: IndexPath) {
        switch ip {
        case SelectedObject.english:
            languageTracker.value = pool[0].rawValue
            
        case SelectedObject.korean:
            languageTracker.value = pool[1].rawValue
            
//        case SelectedObject.thai:
//            languageTracker.value = pool[2].rawValue
            
            
        default:assertionFailure()
        }
        
        navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Fileprivate
    fileprivate let pool: [Language] = [.english, .korean, .thai]
    
    fileprivate var tableViewDataSource:DefaultTableViewDataSource<SettingDefaultTableViewCell>!
    
    fileprivate weak var languageTracker: Tracker<String>!
//    fileprivate var japaneseLanguageTracker = Tracker<String>.init(initialValue: "Japanese")
    
    fileprivate func setupTableView() {
        tableView.delegate = self
        
        var settingArray = (0...2).map({
            SettingData(body: pool[$0].rawValue.capitalized,
                        type: .check, isChecked:$0 == Language(rawValue: languageTracker.value)!.index ,needBlurryEffect: false)})
        
        let data: [Int:[SettingData]] = [
            0:Array(settingArray[0...1]),
            1: [SettingData(body: "Thai", type: .check, isChecked: false, needBlurryEffect: true),
                SettingData(body: "Japanese", type: .check, isChecked: false, needBlurryEffect: true)]]
        
        tableViewDataSource = DefaultTableViewDataSource<SettingDefaultTableViewCell>.init(tableView: tableView, parentViewController: self, initialData: data)
    }
    
}
extension LanguageMainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        userDidSelectRow(atIndexPath: indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let currentView = SettingDescriptionView(description: "Currently available".localized, textAlignment: .left)
        
        let upcomingView = SettingDescriptionView(description: "Upcoming".localized, textAlignment: .left)
        
        return section == 0 ? currentView: upcomingView
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section == 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}

extension LanguageMainViewController {
    fileprivate func setupUI() {
        tableView = UITableView(frame: .zero, style: .grouped)
        
        view.addSubview(tableView)
        tableView.setEdgesToSuperView()
        
    }
}

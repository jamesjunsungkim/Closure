//
//  SettingAccountMainViewController.swift
//  Closure
//
//  Created by James Kim on 8/13/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout

final class AccountSettingMainViewController: DefaultViewController {
    
    //UI
    fileprivate var tableView: UITableView!
    fileprivate var logOutButton: UIButton!
    
    init(appStatus:AppStatus) {
        self.appStatus = appStatus
        super.init(nibName: nil, bundle: nil)
        
        enterViewControllerMemoryLog(self)
        
        setupUI()
        setupTableView()
        addTargets()
    }
    
    deinit {
        leaveViewControllerMomeryLog(self)
    }
    
    // MARK: - Actions
    
    @objc fileprivate func userDidTapLogoutButton() {
        presentDefaultAlert(withTitle: "Alert".localized,
                            message: "All data saved to phone will be erased.".localized + "\n" + "Are you sure to log out?".localized,
                            okAction: {[unowned self] in
                                /**
                                 ------------------------- DATA ------------------------------
                                 1. We need to remove all the user defaults data
                                 2. remove all core data saved to phone
                                
                                 UI PART
                                 3. Then we need to segue to the sign/login page.
                                 // TODO: when user logs in and if there are datas in server, we need to allow users to retrieve them.
                                 but that could be really expensive I guess.
                                 */
                                
                                // TODO: CHECK IF IT works. it doesn't work on memory store so I can't check it.
                                // 1. remove all user default data and passcode from keychain
                                let userDefaultList: [UserDefaults.Key] = [.uidForSignedInUser, .currentUser, .forImprovements, .forAchievemnets, .forGoals, .terribleColor, .unsatisfactoryColor, .okColor, .goodColor, .awesomeColor, .countMainDictionary, .countLimitationDictionay, .countAddItFirstDictionary ,.wordDeletionWarningAllowed , .internetDisconnectionWarningAllowed , .defaultReminderAllowed ,.customReminderAllowed, .defaultReminderTime, .customReminder ,.isPassCodeOn ,.passCodeRequiredTime ,.isTouchIDOn, .isSyncOn ,.isSyncOverCelluarOn ,.imageSizeOptimization, .isTestingMode]
                                
                                userDefaultList.forEach({UserDefaults.removeValue(forKey: $0)})
                                
                                
                                // remove passcode
                                self.appStatus.passCodeManager.reset()
                                
                                // 2. remove core data
                                Review.deleteAll(fromMOC: self.appStatus.mainContext)
                                
                                /**
                                 -------------------------- UI ------------------------------
                                  3. Then we need to segue to the sign/login page.
                                 */
                                
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.switchToSignUpNav()
        })
    }
    
    // MARK: - Fileprivate
    fileprivate weak var appStatus:AppStatus!
    
    fileprivate var tableViewDataSource: DefaultTableViewDataSource<SettingDefaultTableViewCell>!
    fileprivate var nameTracker: Tracker<String>!
    fileprivate var emailTracker: Tracker<String>!
    fileprivate var signupDateTracker: Tracker<String>!
    
    fileprivate func setupTableView() {
        tableView.delegate = self
        tableViewDataSource = DefaultTableViewDataSource<SettingDefaultTableViewCell>.init(tableView: tableView, parentViewController: self)
        
        // Technically, we don't need a tracker for this purpose but in order to make other operations easier, let's just go with it.
        let user = User(from: UserDefaults.retrieveValue(forKey: .currentUser, defaultValue: [String:Any]()))
        
        nameTracker = Tracker.init(initialValue: user.name)
        emailTracker = Tracker.init(initialValue: user.emailAddress)
        signupDateTracker = Tracker.init(initialValue: user.signupDate.toYearToDateString(type:.current))
        
        let data: [Int:[SettingData]] = [0:[
            SettingData(body: "Name".localized, descriptionTracker: nameTracker, type: .displayDescriptionOnly),
            SettingData(body: "Email Address".localized, descriptionTracker: emailTracker, type: .displayDescriptionOnly),
            SettingData(body: "SignUp Date".localized, descriptionTracker: signupDateTracker, type: .displayDescriptionOnly)]]
        
        tableViewDataSource.update(data: data)
    }
    
    fileprivate func addTargets() {
        logOutButton.addTarget(self, action: #selector(userDidTapLogoutButton), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
}
extension AccountSettingMainViewController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SettingDefaultTableViewCell.hight
    }
}

extension AccountSettingMainViewController {
    fileprivate func setupUI() {
        
        tableView = UITableView(frame: .zero, style: .grouped)
        tableView.isScrollEnabled = false
        tableView.allowsSelection = false
        
        view.backgroundColor = tableView.backgroundColor
        
        logOutButton = UIButton.create(title: "Log out".localized, titleColor: .white, fontSize: 15, backgroundColor: .mainBlue)
        logOutButton.setCornerRadious(value: 10)
        
        let group: [UIView] = [tableView, logOutButton]
        group.forEach(view.addSubview(_:))
        
        constraint(tableView, logOutButton) { (_tableView, _logOutButton) in
            _tableView.left.top.right.equalToSuperview()
            _tableView.height.equalTo(250)
            
            _logOutButton.top.equalTo(tableView.snp.bottom).offset(30)
            _logOutButton.centerX.equalToSuperview()
            _logOutButton.sizeEqualTo(width: 200, height: 40)
        }
    }
}


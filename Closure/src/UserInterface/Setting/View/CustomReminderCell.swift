//
//  CustomReminderCell.swift
//  Closure
//
//  Created by montapinunt Pimonta on 8/15/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout
import RxSwift
import SwipeCellKit

final class CustomReminderCell: SwipeTableViewCell, AdditionalReusable, NameDescribable  {
    
    //UI
    fileprivate var titleLabel:UILabel!
    fileprivate var timeLabel:UILabel!
    fileprivate var dayLabel:UILabel!
    fileprivate var arrowImageView:UIImageView!
    
    //MARK: Public
    func setup(withObject object: Any, parentViewController: UIViewController, currentIndexPath: IndexPath, userInfo: [String : Any]?) {
        guard let reminder = object as? Reminder else {assertionFailure();return}
        delegate = parentViewController as? SwipeTableViewCellDelegate
        
        setupUI()
        configure(with: reminder)
        // because it has to show only confirmed reminders. no need to listen to change before saving it.
//        setupObservers(with:reminder)
    }
    
    override func prepareForReuse() {
        bag = nil
        titleLabel.text = ""
        timeLabel.text = ""
        dayLabel.text = ""
    }
    
    // MARK: Actions
    fileprivate lazy var observeMessageChange:(String)->Void = {[unowned self] (message)in
        self.titleLabel.text = message.isEmpty ? "Default" : message
    }
    
    fileprivate lazy var observeTimeChange:(Date)->Void = {[unowned self] (time) in
        self.timeLabel.text = time.toHourToAmPmString(type:.current)
    }
    
    fileprivate lazy var observeDayChange:([Day])->Void = {[unowned self] (days)in
        let targetString = Reminder.converToDayString(input: days)
        self.dayLabel.text = targetString
    }
    
    
    //MARK: Fileprivate
    fileprivate var bag: DisposeBag!
    
    fileprivate func configure(with reminder:Reminder) {
        titleLabel.text = reminder.messageTracker.value.isEmpty ? "Default" : reminder.messageTracker.value
        timeLabel.text = reminder.timeTracker.value.toHourToAmPmString(type:.current)
        dayLabel.text = reminder.toDayString
    }
    
    fileprivate func setupObservers(with reminder:Reminder) {
        bag = DisposeBag()
        
        reminder.messageTracker.valueObservable.subscribe(onNext: observeMessageChange).disposed(by: bag)
        
        reminder.dayTracker.valueObservable.subscribe(onNext: observeDayChange).disposed(by: bag)
        
        reminder.timeTracker.valueObservable.subscribe(onNext: observeTimeChange).disposed(by: bag)
    }
    
    
    
}

extension CustomReminderCell {
    fileprivate func setupUI() {
        
        titleLabel = UILabel.create(text: "Title", textAlignment: .left, fontSize: 15)
        
        timeLabel = UILabel.create(text: Date().toHourToAmPmString(type:.current), textAlignment: .left, fontSize: 15)
        
        let dashLabel = UILabel.create(text: "-", textAlignment: .left, fontSize: 15)
        
        dayLabel = UILabel.create(text: "Title", textAlignment: .left, fontSize: 15)
        
        arrowImageView = UIImageView.create(withImageName: "right_arrow")
        
        let group:[UIView] = [titleLabel, timeLabel,dashLabel ,dayLabel, arrowImageView]
        group.forEach(addSubview(_:))
        
        let leftPadding:CGFloat = 20
        let rightPadding: CGFloat = 20
        
        constraint(titleLabel, timeLabel, dashLabel, dayLabel, arrowImageView) { (_titleLabel, _timeLabel, _dashLabel, _dayLabel, _arrowImageView) in
            
            _titleLabel.left.equalToSuperview().offset(leftPadding)
            _titleLabel.centerY.equalToSuperview().offset(-8)
            
            _timeLabel.left.equalToSuperview().offset(leftPadding)
            _timeLabel.centerY.equalToSuperview().offset(8)
            
            _dashLabel.left.equalToSuperview().offset(90)
            _dashLabel.centerY.equalTo(timeLabel)
            
            _dayLabel.left.equalTo(dashLabel.snp.right).offset(3)
            _dayLabel.centerY.equalTo(timeLabel)

            _arrowImageView.sizeEqualTo(width: 20, height: 20)
            _arrowImageView.right.equalToSuperview().offset(-rightPadding/2)
            _arrowImageView.centerY.equalToSuperview()
        }
        
    }
}

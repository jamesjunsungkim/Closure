//
//  SettingDefaultTableViewCell.swift
//  Closure
//
//  Created by James Kim on 8/13/18.
//  Copyright © 2018 James Kim. All rights reserved.
//

import UIKit
import CodiumLayout
import RxSwift
import RxCocoa
import SwipeCellKit

final class SettingDefaultTableViewCell: SwipeTableViewCell, Reusable, NameDescribable {
    typealias Object = SettingData
    static let hight:CGFloat = 45
    
    //UI
    fileprivate var bodyLabel: UILabel!
    fileprivate var descriptionLabel:UILabel!
    fileprivate var textField: UITextField!
    fileprivate var settingImageView:UIImageView!
    fileprivate var toggleSwitch: UISwitch!
    fileprivate var blurredView:UIView!

    override func prepareForReuse() {
        bag = nil
        bodyLabel.text = ""
        descriptionLabel.text = ""
        toggleSwitch.isHidden = true
    }
    
    // MARK: Public
    func setup(withObject object: SettingData, parentViewController: UIViewController, currentIndexPath: IndexPath, userInfo: [String : Any]?) {
        self.object = object
        delegate = parentViewController as? SwipeTableViewCellDelegate
        additionalDescriptionChange = object.additionalDescriptionChange
        
        setupUI(forType: object.type)
        configure(withSetting: object)
        setupObserver(fromST: object)
        addTarget()
    }
    
    func didGetSelected() {
        guard type == .check else {assertionFailure();return}
        settingImageView.isHidden = false
    }
    
    func didGetDeselected() {
        guard type == .check else {assertionFailure();return}
        settingImageView.isHidden = true
    }
    
    public func showOrHideBlurryView(needsToShow:Bool) {
        blurredView.isHidden = !needsToShow
    }
    
    // MARK: Action
    fileprivate lazy var observeDescriptionChange: (String)->Void = {[unowned self](text) in
        self.descriptionLabel.text = self.additionalDescriptionChange != nil ? self.additionalDescriptionChange!(text).firstLetterCapitalized : text.firstLetterCapitalized
    }
    
    fileprivate lazy var observeToggleChange: (Bool) ->Void = {[unowned self](isOn) in
        self.toggleSwitch.isOn = isOn
    }
    
    @objc fileprivate func userDidToggleSwitch() {
        // set new value-> change tracker value->observe change->Set it again from observation.
        guard isOnTracker != nil else {assertionFailure(); return}
        isOnTracker.value = !isOnTracker.value
    }
    
    fileprivate lazy var observeUserType:(String)->Void = {[weak self] (input) in
        guard self?.userTypeInputTracker != nil else {assertionFailure();return}
        self?.userTypeInputTracker.value = input
    }
    
    // MARK: Fileprivate
    fileprivate var bag: DisposeBag!
    
    fileprivate var textFieldInput = BehaviorRelay(value: "")
    fileprivate var additionalDescriptionChange: ((String)->(String))?
    
    fileprivate weak var userTypeInputTracker: Tracker<String>!
    fileprivate weak var isOnTracker: Tracker<Bool>!
    fileprivate weak var descriptionTracker:  Tracker<String>!
    fileprivate weak var object:Object!
    
    fileprivate var type: SettingType {
        return object.type
    }
    
    fileprivate func configure(withSetting st:SettingData) {
        
        bodyLabel.text = st.body.firstLetterCapitalized
        
        let descriptionText = st.descriptionTracker?.value.firstLetterCapitalized
        descriptionLabel.text = additionalDescriptionChange != nil ? additionalDescriptionChange!(descriptionText.unwrapOrBlank()) : descriptionText
        
        textField.placeholder = st.placeHolder
        textField.text = st.userTypeInputTracker?.value.firstLetterCapitalized
        
        toggleSwitch.isOn = st.isOnTracker?.value ?? false
        settingImageView.isHidden = st.type == .check ? !st.isChecked : false
        
        blurredView.isHidden = !st.needBlurryEffect
    }
    
    fileprivate func setupObserver(fromST st: SettingData) {
        bag = DisposeBag()
        
        if let _descriptionTracker = st.descriptionTracker {
            descriptionTracker = _descriptionTracker
            descriptionTracker.valueObservable.subscribe(onNext: observeDescriptionChange).disposed(by: bag)
        }
        
        if let _isOnTracker = st.isOnTracker, st.type == .toggle {
            isOnTracker = _isOnTracker
            isOnTracker.valueObservable.subscribe(onNext: observeToggleChange).disposed(by: bag)
        }
        
        if let _userInputTracker = st.userTypeInputTracker, st.type == .textInput {
            userTypeInputTracker = _userInputTracker
            
            // bind thextfield to setting data
            textField.rx.text.orEmpty
                .bind(to: textFieldInput)
                .disposed(by: bag)
            
            textFieldInput.asObservable()
                .subscribe(onNext: observeUserType)
                .disposed(by: bag)
        }
    }
    
    fileprivate func addTarget() {
        toggleSwitch.addTarget(self, action: #selector(userDidToggleSwitch), for: .valueChanged)
    }
}

extension SettingDefaultTableViewCell{
    fileprivate func setupUI(forType type: SettingType) {
        bodyLabel = UILabel.create(text: "Body label", textAlignment: .left, fontSize: 15)
        
        descriptionLabel = UILabel.create(text: "description label", textAlignment: .right, fontSize: 15)
        
        textField = UITextField.create(placeHolder: "place holder", textSize: 15, textColor: .black, keyboardType: .default, clearMode: .whileEditing)
        textField.textAlignment = .right
        
        settingImageView = UIImageView.create(withImageKey: .check)
        toggleSwitch = UISwitch.create(tintColor: .mainBlue, onTintColor: .mainBlue)
        
        blurredView = UIView.create(withColor: .white, alpha: 0.5)
        blurredView.isHidden = true
        
        let leftPadding:CGFloat = 20
        let rightPadding: CGFloat = 20
//        let imageSize = CGSize(width: 20, height: 20)
        
        switch type {
        case .displayDescriptionOnly:
            let group: [UIView] = [bodyLabel, descriptionLabel]
            group.forEach(self.addSubview(_:))
            
            constraint(bodyLabel, descriptionLabel) { (_bodyLabel, _descriptionLabel) in
                _bodyLabel.left.equalToSuperview().offset(leftPadding)
                _bodyLabel.centerY.equalToSuperview()
                _bodyLabel.width.equalTo(120)
                _bodyLabel.right.equalTo(descriptionLabel.snp.left).offset(-5)
                
                _descriptionLabel.centerY.equalToSuperview()
                _descriptionLabel.right.equalToSuperview().offset(-rightPadding)
            }
            
        case .displayImageOnly:
            // it will be used to display right arraow only.
            settingImageView = UIImageView.create(withImageName: "right_arrow")
            
            let group: [UIView] = [bodyLabel, settingImageView]
            group.forEach(self.addSubview(_:))
            
            constraint(bodyLabel, settingImageView) { (_bodyLabel, _settingImageView) in
                _bodyLabel.left.equalToSuperview().offset(leftPadding)
                _bodyLabel.centerY.equalToSuperview()
                _bodyLabel.right.equalTo(settingImageView.snp.left).offset(-5)
                
                _settingImageView.centerY.equalToSuperview()
                _settingImageView.sizeEqualTo(width: 20, height: 20)
                _settingImageView.right.equalToSuperview().offset(-rightPadding/2)
            }
            
        case .displayImageWithDescription:
            settingImageView = UIImageView.create(withImageName: "right_arrow")
            
            let group: [UIView] = [bodyLabel, descriptionLabel, settingImageView]
            group.forEach(self.addSubview(_:))
            
            constraint(bodyLabel, descriptionLabel, settingImageView) { (_bodyLabel, _descriptionLabel, _settingImageView) in
                _bodyLabel.left.equalToSuperview().offset(leftPadding)
                _bodyLabel.centerY.equalToSuperview()
                _bodyLabel.width.equalTo(220)
                
                _descriptionLabel.left.equalTo(bodyLabel.snp.right).offset(5)
                _descriptionLabel.centerY.equalToSuperview()
                _descriptionLabel.right.equalTo(settingImageView.snp.left).offset(-5)
                
                _settingImageView.centerY.equalToSuperview()
                _settingImageView.sizeEqualTo(width: 20, height: 20)
                _settingImageView.right.equalToSuperview().offset(-rightPadding/2)
            }
        case .check:
            
            let group: [UIView] = [bodyLabel, descriptionLabel, settingImageView]
            group.forEach(self.addSubview(_:))
            
            constraint(bodyLabel,descriptionLabel, settingImageView) { (_bodyLabel, _descriptionLabel, _settingImageView) in
                _bodyLabel.left.equalToSuperview().offset(leftPadding)
                _bodyLabel.centerY.equalToSuperview()
                _bodyLabel.right.equalTo(descriptionLabel.snp.left).offset(-5)
                _descriptionLabel.centerY.equalToSuperview()
                _descriptionLabel.width.equalTo(80)
                _descriptionLabel.right.equalTo(settingImageView.snp.left).offset(-10)
                
                _settingImageView.centerY.equalToSuperview()
                _settingImageView.sizeEqualTo(width: 20, height: 20)
                _settingImageView.right.equalToSuperview().offset(-rightPadding/2)
            }
            
        case .textInput:
            
            let group: [UIView] = [bodyLabel, textField]
            group.forEach(self.addSubview(_:))
            
            constraint(bodyLabel, textField) { (_bodyLabel, _textField) in
                _bodyLabel.left.equalToSuperview().offset(leftPadding)
                _bodyLabel.centerY.equalToSuperview()
                _bodyLabel.width.equalTo(90)
                
                _textField.left.equalTo(bodyLabel.snp.right).offset(5)
                _textField.centerY.equalToSuperview()
                _textField.right.equalToSuperview().offset(-10)
                _textField.height.equalTo(30)
            }
            
        case .toggle:
            let group: [UIView] = [bodyLabel, toggleSwitch]
            group.forEach(self.addSubview(_:))
            
            constraint(bodyLabel, toggleSwitch) { (_bodyLabel, _toggleSwitch) in
                _bodyLabel.left.equalToSuperview().offset(leftPadding)
                _bodyLabel.centerY.equalToSuperview()
                _bodyLabel.right.equalTo(toggleSwitch.snp.left).offset(-5)
                
                _toggleSwitch.centerY.equalToSuperview()
                _toggleSwitch.right.equalToSuperview().offset(-30)
                _toggleSwitch.sizeEqualTo(width: 40, height: 35)
            }
            
        case .action:
            let group: [UIView] = [bodyLabel]
            group.forEach(self.addSubview(_:))
            
            constraint(bodyLabel) { (_bodyLabel) in
                _bodyLabel.centerX.centerY.equalToSuperview()
            }
        }
        // for blurred effects, we add it no matter what type it is.
        addSubview(blurredView)
        blurredView.setEdgesToSuperView()
    }
}



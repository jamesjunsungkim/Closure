////
////  AddReviewTest.swift
////  ClosureTests
////
////  Created by James Kim on 7/27/18.
////  Copyright © 2018 James Kim. All rights reserved.
////
//
//import Foundation
//import XCTest
//import CoreData
//@testable import Closure
//
//class AddReviewTest: XCTestCase {
//
//    fileprivate var testAppStatus = AppStatus(context: NSManagedObjectContext.memoryContext(),
//                                              user: TestDatabase.TestReview.user)
//    fileprivate var testNetwork: TestNetworkSession!
//    fileprivate var addReviewViewController: AddReviewViewController!
//
//    override func setUp() {
//        super.setUp()
//        testNetwork = TestNetworkSession()
//        testNetwork.urlForUploadedFile = TestDatabase.TestReview.firstURL
//
//        testAppStatus.networkManager.changeSession(testNetwork)
//
//        addReviewViewController = AddReviewViewController(appStatus: testAppStatus)
//    }
//
//    override func tearDown() {
//        // Put teardown code here. This method is called after the invocation of each test method in the class.
//        super.tearDown()
//    }
//
//    func testAddingReview() {
//        addReviewViewController.createOrEditAndThenSaveToServer(into: testAppStatus.mainContext, good: TestDatabase.TestReview.rGood, bad: TestDatabase.TestReview.rBad, goal: TestDatabase.TestReview.rGoal, ratingValue: TestDatabase.TestReview.rRating, date: TestDatabase.TestReview.rDate, imageData: TestDatabase.TestReview.imageData, thoughts: TestDatabase.TestReview.rThought, path: TestDatabase.TestReview.path) { (review) in
//
//            let previousDict = review.toDictionary
//            let previousImageData = review.imageData!
//
//            XCTAssert(previousDict.isEqual(toJSON: TestDatabase.TestReview.reviewDict))
//            XCTAssert(previousImageData == TestDatabase.TestReview.imageData)
//        }
//    }
//
//    func testEditReview() {
//        let review = Review.findByDateOrCreate(into: testAppStatus.mainContext, good: "A", bad: "B", goals: "D", ratingValue: 1.5, date: TestDatabase.TestReview.anotherDate, imageData: nil, thoughts: nil)
//
//        addReviewViewController.createOrEditAndThenSaveToServer(into: testAppStatus.mainContext, good: TestDatabase.TestReview.rGood, bad: TestDatabase.TestReview.rBad, goal: TestDatabase.TestReview.rGoal, ratingValue: TestDatabase.TestReview.rRating, date: TestDatabase.TestReview.anotherDate, imageData: TestDatabase.TestReview.imageData, thoughts: TestDatabase.TestReview.rThought, path: TestDatabase.TestReview.path)
//        
//        addReviewViewController.editReviewAndSaveToServer(toPath: TestDatabase.TestReview.path, review: review, good: TestDatabase.TestReview.editGood, bad: TestDatabase.TestReview.editBad, goal: TestDatabase.TestReview.editGoal, ratingValue: TestDatabase.TestReview.editRating, date: TestDatabase.TestReview.editDate, imageData: TestDatabase.TestReview.editImageData, thoughts: TestDatabase.TestReview.editThought, editCompletion: { (editReview) in
//
//            XCTAssert(editReview.toDictionary.isEqual(toJSON: TestDatabase.TestReview.editReviewDict))
//            XCTAssert(editReview.imageData! == TestDatabase.TestReview.editImageData)
//        })
//    }
//
//}
